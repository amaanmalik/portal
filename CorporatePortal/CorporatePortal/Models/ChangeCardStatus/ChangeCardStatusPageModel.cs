﻿using CorporatePortal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.ChangeCardStatus
{
    public class ChangeCardStatusPageModel
    {
        public ChangeCardStatusPageModel()
        {
            
        }

        public IList<SelectListItem> cardStatusList { get; set; }

        public string CurrentStatus { get; set; }

        public string Status { get; set; }

        public string CardProxyId { get; set; }   
        
        public string UserId { get; set; }
    }
}