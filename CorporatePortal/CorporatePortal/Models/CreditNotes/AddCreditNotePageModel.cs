﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.CreditNotes
{
    public class AddCreditNotePageModel
    {
        public int? CreditProfileId { get; set; }

        public string CreditProfileName { get; set; }

        public IList<SelectListItem> CreditProfiles { get; set; }

        public IList<SelectListItem> Types { get; set; }

        public string StatementType { get; set; }

        public string Type { get; set; }

        public bool Paid { get; set; }

        public string Description { get; set; }

        public double TotalSpent { get; set; }
    }
}