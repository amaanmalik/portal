﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CreditNotes
{
    public class UpdateCreditNotePageModel
    {
        public int CreditNoteId { get; set; }

        public int CreditProfileId { get; set; }

        public string CreditProfileName { get; set; }
        
        public string StatementType { get; set; }
     
        public DateTime CreatedDate { get; set; }

        public double Amount { get; set; }
        
        public string Description { get; set; }

    }
}