﻿using CorporatePortal.Api.Requests.CredNotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CreditNotes
{
    public class CreditNotesPageModel
    {
        public List<CreditNoteModel> CreditNotes { get; set; }
    }
}