﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.ProductFee
{
    public class ProductFeeModel
    {
        public int ProductFeeId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime UpdatedDate { get; set; }

        
        public string Name { get; set; }

        public double FinanceFee { get; set; }

        public double CardFee { get; set; }

        public double CardIsueeFee { get; set; }

        public double FXFee { get; set; }
    }
}