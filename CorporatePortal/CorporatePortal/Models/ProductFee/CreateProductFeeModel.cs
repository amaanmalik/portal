﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.ProductFee
{
    public class CreateProductFeeModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid value")]
        public double FinanceFee { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid value")]
        [Display(Name="SubscriptionFee")]
        public double CardFee { get; set; }

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid value")]
        public double CardIsueeFee { get; set; } = 5;

        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid value")]
        public double FXFee { get; set; }
    }
}