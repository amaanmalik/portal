﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.ProductFee
{
    public class ProductFeesListPageModel
    {
        public List<ProductFeeModel> ProductFeeList { get; set; }
    }
}
