﻿using CorporatePortal.Api.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.ProductFee
{
    public class ProductFeeResponseModel : BaseApiResponse
    {
        public ProductFeeModel ProductFee { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}