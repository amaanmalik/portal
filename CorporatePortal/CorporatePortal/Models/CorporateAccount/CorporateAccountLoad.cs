﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CorporateAccount
{
    public class CorporateAccountLoad
    {
        public string CorporateAccountName { get; set; }

        public int CorporateAccountId { get; set; }

        public decimal AmountLoaded { get; set; }

        public DateTime LoadDate { get; set; }

        public bool Status { get; set; }

        public string LoadType { get; set; }
    }
}