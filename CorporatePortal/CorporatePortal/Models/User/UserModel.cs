﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.User
{
    public class UserModel
    {
        public string Id { get; set; }

        public string PaymentGatewayId { get; set; }

        public bool? AcceptedTAndCs { get; set; }

        public bool? ShownAutoCommsOptIn { get; set; }

        public bool? PassedSanctions { get; set; }

        public bool? PassedPeps { get; set; }

        public bool? PassedKyc { get; set; }

        public bool? IsActive { get; set; }

        public string ThirdPartyUserId { get; set; }

        public string Language { get; set; }

        public string SocialSecurityNumber { get; set; }

        public string Nationality { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateOfBirth { get; set; }

        public string Title { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string SecondSurname { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? CreatedDate { get; set; }

        public string EmailAddress { get; set; }

        public bool? EmailConfirmed { get; set; }

        public string Mobile { get; set; }

        public string LandlineTelephone { get; set; }

        public string IdCardNumber { get; set; }

        public string TaxIdCardNumber { get; set; }

        // Address

        public string ISOCountryCode { get; set; }

        public string PostCode { get; set; }

        public string State { get; set; }

        public string City { get; set; }

        public string Address2 { get; set; }

        public string Address1 { get; set; }

        public string HouseNumberOrBuilding { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}