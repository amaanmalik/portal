﻿using CorporatePortal.Api.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.User
{
    public class UserTransactionHistoryVM
    {
        public string UserId { get; set; }
        public string CardProxyId { get; set; }
        public List<ApiTransaction> Transactions { get; set; }
    }
}