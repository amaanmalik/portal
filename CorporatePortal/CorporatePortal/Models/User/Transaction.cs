﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.User
{
    public class Transaction
    {
        public Transaction()
        {

        }
        public string poschpField { get; set; }
        public string poscdimField { get; set; }
        public string poschamField { get; set; }
        public string poscpField { get; set; }
        public string approvalCodeField { get; set; }
        public DateTime sysDateField { get; set; }
        public string revField { get; set; }
        public string orgItemIdField { get; set; }
        public short? itemSrcField { get; set; }
        public double? amtFeeField { get; set; }
        public string crdproductField { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ctxLocalDateField { get; set; }
        public long? ctxLocalTimeField { get; set; }
        public string aVSChkRsField { get; set; }
        public string threeDSecChkRsField { get; set; }
        public string actionCodeField { get; set; }
        public double? amtCashbackField { get; set; }

        [Display(Name = "ProductCode")]
        public string productCode { get; set; }
        public virtual double? currentBalance { get; set; }
        public virtual string description { get; set; }
        public virtual string balanceAdjustmentType { get; set; }
        public virtual string paymentType { get; set; }
        public virtual string paymentMethodType { get; set; }
        public virtual string merchantName { get; set; }

        [Display(Name = "City")]
        public virtual string city { get; set; }
        public string mCCSubFieldDescription { get; set; }

        [Display(Name = "PostCode")]
        public virtual string postCode { get; set; }
        public int mCCSubField { get; set; }
        public string crdAcptLocField { get; set; }

        [Display(Name = "TranasctionId")]
        public int messageIDField { get; set; }
        public string instCodeField { get; set; }
        public string txnTypeField { get; set; }
        public int msgTypeField { get; set; }
        public string tlogIdField { get; set; }
        public string orgTlogIDField { get; set; }
        public int? repeatField { get; set; }
        public int? timeoutField { get; set; }
        public string pANField { get; set; }

        [Display(Name = "CardId")]
        public string cardIDField { get; set; }
        public string accNoField { get; set; }
        public string curBillField { get; set; }
        public double? avlBalField { get; set; }
        public double? blkAmtField { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime localDateField { get; set; }
        public long localTimeField { get; set; }
        public double? amtTxnField { get; set; }
        public string curTxnField { get; set; }
        public double? billAmtField { get; set; }
        public double? billConvRateField { get; set; }
        public double? amtComField { get; set; }
        public double? amtPadField { get; set; }
        public int? txnCodeField { get; set; }
        public string termCodeField { get; set; }
        public string crdAcptIDField { get; set; }
        public int? mCCField { get; set; }
        public virtual string currencyCode { get; set; }
        public string mCCFieldDescription { get; set; }

        private double amount;

        public double TransAmount { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int TotalTransactions { get; set; }

        public double TotalSpent { get; set; }

        public int Total { get; set; }

        public string CardProxyId { get; set; }

        public string Status { get; set; }

        public decimal Balance { get; set; }

        public string Currency { get; set; }

        public string Product_ProductCode { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime ActivationDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime FirstTransactionDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ExpiryDate { get; set; }

        public string LoadType { get; set; }

        public string LoadCategory { get; set; }

        public string UserName { get; set; }

        public string CardholderId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CardOrderCreatedDate { get; set; }

        public int DaysSinceCardOrder { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CardCreationDate { get; set; }

        public int DaysSinceCardCreated { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }

        public string ProfileName { get; set; }

        public String TransactionTime { get; set; }

        public string CardholderEmail { get; set; }

        public string CardholderMobile { get; set; }

        public string SpendProfileName { get; set; }

        public string CreditProfileId { get; set; }

        public string CreditProfileName { get; set; }

        public string CardOrderCardholder { get; set; }

        public string TransactionCreatedDate { get; set; }

        public string UTID { get; set; }

        public string trn { get; set; }

        public string txnSubCode { get; set; }
    }
}
