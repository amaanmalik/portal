﻿using CorporatePortal.Api.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.User
{
    public class CardHolderDetailsPageModel
    {
        public CardHolderDetails CardHolder { get; set; }

        public List<ApiPrepaidCardModel> Cards { get; set; }  
        
        public SpendProfileResponseModel SpendProfile { get; set; }

        public IList<SelectListItem> PurchaseCategories { get; set; }
    }
}