﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.User
{
    public class CardHoldersListPageModel
    {
        public string UserId { get; set; }
        
        public string UserName { get; set; }
      
        public string FirstName { get; set; }
       
        public string LastName { get; set; }
                
        public string Email { get; set; }

        public List<string> Roles { get; set; }

        [Display(Name = "Is cardholder active")]
        public bool IsActive { get; set; }

        [Display(Name = "Has activated card")]
        public bool HasActiveCard { get; set; }

        public bool EmailConfirmed { get; set; }

        public bool? PassedKyc { get; set; }

        public bool? PassedPeps { get; set; }

        public bool? PassedSanctions { get; set; }

        public string SpendProfile { get; set; }

        public string CardProxyId { get; set; }

        public string CardStatus { get; set; }

        public string ProductCode { get; set; }

        public string CreditProfile { get; set; }

        public bool? AllowATM { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}