﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.User
{
    public class CreateNewCardholderPageModel
    {
        public List<SelectListItem> Countries { get; set; }

        //public List<SelectListItem> Products { get; set; }

        //[Required]
        //public string ProductCode { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Max length = 20 characters")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Max length = 20 characters")]
        public string LastName { get; set; }

        public string Mobile { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(64, MinimumLength = 1, ErrorMessage = "Max length = 64 characters")]
        [Display(Name = "EmailAddress")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Display(Name = "Confirm Email")]
        [System.ComponentModel.DataAnnotations.Compare("EmailAddress", ErrorMessage = "The email and confirmation do not match.")]
        public string ConfirmEmailAddress { get; set; }


        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$", ErrorMessage = "Password min length : 8. Must contain atleast one uppercase letter and a digit. No special characters allowed!")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation do not match.")]
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "House/Building #")]
        public string HouseNumberOrBuilding { get; set; }

        [Required]
        [StringLength(35, MinimumLength = 1, ErrorMessage = "Max length = 35 characters")]
        public string Address1 { get; set; }

        [StringLength(35, MinimumLength = 1, ErrorMessage = "Max length = 35 characters")]
        public string Address2 { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Max length = 20 characters")]
        public string City { get; set; }

        [Display(Name = "County")]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Max length = 20 characters")]
        public string State { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 1, ErrorMessage = "Max length = 10 characters")]
        public string PostCode { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [CustomDateAttribute(ErrorMessage = "Invalid year.")]
        public DateTime DateOfBirth { get; set; }

        public virtual string cardDisplayName { get; set; }

        public virtual string CardProxyId { get; set; }
    }
}