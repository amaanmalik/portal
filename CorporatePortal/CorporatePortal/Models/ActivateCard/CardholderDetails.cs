﻿using Newtonsoft.Json;
using CorporatePortal.Api.Responses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.ActivateCard
{
    public class CardholderDetails
    {
        public CardholderDetails()
        {
            Genders = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Value = "Male",
                    Text = "Male"
                },
                new SelectListItem
                {
                    Value = "Female",
                    Text = "Female"
                }
        };
        }

        public string Mobile { get; set; }

        public string LandlineTelephone { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        public string Language { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        public Gender? Gender { get; set; }

        public string SocialSecurityNumber { get; set; }

        public string IdCardNumber { get; set; }

        public string TaxIdCardNumber { get; set; }

        public string Title { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string SecondSurname { get; set; }


        public string Nationality { get; set; }

        //[Required]
        public string HouseNumberOrBuilding { get; set; }

        [Required]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        [Required]
        public string PostCode { get; set; }

        [Required]
        public string Country { get; set; }

        public IList<SelectListItem> Genders { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

