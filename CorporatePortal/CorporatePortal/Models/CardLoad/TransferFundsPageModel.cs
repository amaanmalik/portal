﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class TransferFundsPageModel
    {
        public string PAN { get; set; }

        public string cardProxyIdFrom { get; set; }

        public string cardProxyIdFromStatus { get; set; }

        public double cardProxyIdFromBalance { get; set; }

        public string productCode { get; set; }

        public string cardholderId { get; set; }

        public string cardProxyIdTo { get; set; }

        public double Amount { get; set; }

        public double cardProxyIdToBalance { get; set; }

    }
}