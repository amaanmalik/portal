﻿using CorporatePortal.Api.Responses;
using CorporatePortal.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class LoadModel
    {
        public string productCode { get; set; }
        public List<QcsProduct> Products { get; set; }
        public List<CorporateFundsSummary> AccountsList { get; set; }
    }
}