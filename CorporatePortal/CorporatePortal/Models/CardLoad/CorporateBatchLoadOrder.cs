﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class CorporateBatchLoadOrder
    {
        public decimal Amount { get; set; }
        public string CardProxyId { get; set; }
        public LoadStatus Status { get; set; }
        public int CorporateAccountId { get; set; }
        public string BatchLoadFileName { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string FileName { get; set; }
    }

    public enum LoadStatus
    {
        Pending = 1,
        Success = 2,
        Fail = 3
    }

    public class BatchLoadHistory
    {
        public int Id { get; set; }

        public string FileName { get; set; }

        public string FileStatus { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int TotalCards { get; set; }

        public double TotalAmount { get; set; }
    }
}