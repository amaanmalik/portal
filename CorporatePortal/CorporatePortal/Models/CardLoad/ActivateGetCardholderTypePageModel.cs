﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.CardLoad
{
    public class ActivateGetCardholderTypePageModel
    {
        public string cardProxyId { get; set; }

        public string productCode { get; set; }

        public string cardholderType { get; set; }

        public IList<SelectListItem> cardholderTypeList { get; set; }
    }
}
