﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.CardLoad
{
    public class LoadPageModel
    {
        public string productCode { get; set; }
        public string status { get; set; }
        public List<SelectListItem> ProductsList { get; set; }
        public List<SelectListItem> TypeList { get; set; }
        public IList<CorporatePortal.Models.CardLoad.CorporateCard> vcards { get; set; }
        public IList<CorporatePortal.Models.CardLoad.CorporateCard> activecards { get; set; }
        public IList<CorporatePortal.Models.CardLoad.CorporateCard> inactivecards { get; set; }
    }
}