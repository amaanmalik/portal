﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.CardLoad
{
    public class GetExistingCardholderPageModel
    {
        public string cardProxyId { get; set; }

        public string productCode { get; set; }

        [Required]
        public string searchType { get; set; }

        [Required]
        public string searchValue { get; set; }

        public IList<SelectListItem> searchTypes { get; set; }

    }

}