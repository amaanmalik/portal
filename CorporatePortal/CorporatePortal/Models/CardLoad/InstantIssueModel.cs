﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class InstantIssueModel
    {
        public string PAN { get; set; }

        public string cardProxyId { get; set; }

        public string productCode { get; set; }
    }
}