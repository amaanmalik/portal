﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class LoadCardVM
    {
        public int AccountId { get; set; }
        public List<CorporateAccount> CorporateAccounts { get; set; }
        public string CardProxyId { get; set; }
        public decimal Amount { get; set; }    
        public string productCode { get;set; }
        public string status { get; set; }
    }
}