﻿using CorporatePortal.Api.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class CheckCardAlreadyAssginedReponse : BaseApiResponse
    {
        public bool AlreadyAssigned { get; set; }

        public bool IsSuccess { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}