﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class BatchLoadHistoryPageModel
    {
        public int Id { get; set; }

        public string FileName { get; set; }

        public string FileStatus { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int TotalCards { get; set; }

        public double TotalAmount { get; set; }
        //public string FileName { get; set; }
        //public decimal TotalAmount { get; set; }
        //public int TotalCards { get; set; }
        //public List<CorporateBatchLoadOrder> List { get; set; }

        //[DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        //public DateTime CreatedDate { get; set; }
        //public string FileStatus { get; set; }
    }
}