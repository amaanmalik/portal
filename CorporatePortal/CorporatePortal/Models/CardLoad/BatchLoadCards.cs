﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class BatchLoadCards
    {
        [Required]
        [Display(Name = "Choose File")]
        public HttpPostedFileBase postedFile { get; set; }
    }
}