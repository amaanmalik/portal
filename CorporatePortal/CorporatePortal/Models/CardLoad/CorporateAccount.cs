﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class CorporateAccount
    {
        public int Id { get; set; }

        public string AccountName { get; set;  }

        public decimal Balance { get; set; }
       
        public string CurrencyCode { get; set; }

        public virtual FundingSource? FundingSource { get; set; }

        public virtual string CardId { get; set; }

        public string AccountDetails
        {
            get
            {
                return String.Format("AccountName: {0}. Balance: {1}. Currency: {2}", AccountName,Balance,CurrencyCode);
            }
        }
    }

    public enum FundingSource
    {
        Corporate = 1,
        Card = 2
    }
}