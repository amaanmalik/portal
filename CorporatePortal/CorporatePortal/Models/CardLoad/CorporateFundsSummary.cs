﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class CorporateFundsSummary
    {
        public int AccountId { get; set; }
        public decimal AccountBalance { get; set; }
        public string ProductCode { get; set; }
        public decimal TotalLoaded { get; set; }
    }
}