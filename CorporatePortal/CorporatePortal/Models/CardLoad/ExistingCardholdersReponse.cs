﻿using CorporatePortal.Api.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class ExistingCardholdersReponse : BaseApiResponse
    {
        public CardHolderDetails Cardholder { get; set; }

        public List<CardHolderDetails> Cardholders { get; set; }

        public bool IsSuccess { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

