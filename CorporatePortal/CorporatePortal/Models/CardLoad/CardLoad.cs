﻿using CorporatePortal.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardLoad
{
    public class CorporateCard
    {
        public string Name { get; set; }

        public decimal Balance { get; set; }

        public string Currency { get; set; }

        public string CardProxyId { get; set; }

        public string ProductCode { get; set; }

        public string CardholderId { get; set; }

        public UserModel CardHolder { get; set; }

        public string CardStatus { get; set; }

        public string FundsOwnership { get; set; }
    }
}