﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardOrders
{
    public class InvalidBatchOrder
    {
        public int? Id { get; set; }
        public string ProductCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}