﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.CardOrders
{
    public class OrderVirtualCardForExistingCardholdersPageModel
    {
        public List<SelectListItem> Countries { get; set; }

        public List<SelectListItem> Products { get; set; }

        [Required]
        public string ProductCode { get; set; }

    
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Mobile { get; set; }       
        
        [Display(Name = "House/Building #")]
        public string HouseNumberOrBuilding { get; set; }

     
        public string Address1 { get; set; }

        public string Address2 { get; set; }

  
        public string City { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        [Display(Name = "County")]
        public string Country { get; set; }

        
        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateOfBirth { get; set; }

        public virtual string cardDisplayName { get; set; }

        public virtual string CardProxyId { get; set; }

        public string CardHolderId { get; set; }
    }
}

