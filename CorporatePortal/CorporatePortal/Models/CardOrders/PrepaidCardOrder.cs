﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardOrders
{
    public class PrepaidCardOrder
    {
        public string ProductCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }

        [Display(Name = "County")]
        public string State { get; set; }
        public string PostCode { get; set; }
        public string ISOCountryCode { get; set; }
        public string RecipientEmail { get; set; }
        public string OrderStatus { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string Dob { get; set; }

        public int Id { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime UpdatedDate { get; set; }

        public string CardFileName { get; set; }

        public string NameOnCard { get; set; }

        public string Address3 { get; set; }

        public string CardDesign { get; set; }

        public string AdditionalCardEmbossData { get; set; }
    }
}