﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardOrders
{
    public class OrderHistory
    {
        public int OrderId { get; set; }
        public string ProductCode { get; set; }        
    }
}