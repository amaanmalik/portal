﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardOrders
{
    public class BatchOrderCards
    {
        [Display(Name = "Choose File")]
        public HttpPostedFileBase postedFile { get; set; }
    }
}