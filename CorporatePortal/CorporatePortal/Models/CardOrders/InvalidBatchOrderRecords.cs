﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardOrders
{
    public class InvalidBatchOrderRecords
    {
        public List<string> Records { get; set; }
    }
}