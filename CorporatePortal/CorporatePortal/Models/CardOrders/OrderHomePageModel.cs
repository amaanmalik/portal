﻿using CorporatePortal.Api.Responses;
using CorporatePortal.Models.CardLoad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardOrders
{
    public class OrderHomePageModel
    {
        public List<BatchOrderHistory> batchOrders { get; set; }
        public List<PrepaidCardOrder> individualOrders { get; set; }
        public List<CorporateFundsSummary> AccountsList { get; set; }
        public List<CorporateCard> VirtualCards { get; set; }
    }
}