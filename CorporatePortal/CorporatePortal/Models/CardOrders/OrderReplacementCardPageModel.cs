﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.CardOrders
{
    public class OrderReplacementCardPageModel
    {
        
        public string ProductCode { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of birth")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string FirstName { get; set; }


        [Required]
        public string EmailAddress { get; set; }

        public string Mobile { get; set; }

        // Address
        [Required]
        [Display(Name = "Country")]
        public string ISOCountryCode { get; set; }

        [Required]
        public string PostCode { get; set; }

        [Display(Name = "County")]
        public string State { get; set; }

        [Required]
        public string City { get; set; }

        public string Address2 { get; set; }

        [Required]
        public string Address1 { get; set; }

        public string cardProxyId { get; set; }

        public string cardholderId { get; set; }

        public string ReplacedCardProxyId { get; set; }

        public string AdditionalCardEmbossData { get; set; }

        public IList<SelectListItem> Products { get; set; }

        public IList<SelectListItem> Countries { get; set; }
    }
}