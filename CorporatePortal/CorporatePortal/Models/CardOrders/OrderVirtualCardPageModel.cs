﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.CardOrders
{
    public class OrderVirtualCardPageModel
    {
        public string cardholderType { get; set; }

        public IList<SelectListItem> cardholderTypeList { get; set; }
    }
}