﻿using CorporatePortal.Api.Requests;
using CorporatePortal.Api.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardOrders
{
    public class BatchOrder
    {
        public List<Api.Requests.CardOrder.OrderIndividualCardRequest> CardOrders { get; set; }
    }
}