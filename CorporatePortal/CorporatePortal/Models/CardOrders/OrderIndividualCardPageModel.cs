﻿using CorporatePortal.Api.Responses;
using CorporatePortal.Models.Product;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.CardOrders
{
    public class OrderIndividualCardPageModel
    {
        public OrderIndividualCardPageModel()
        {
            
        }

        public string CardholderId { get; set; }

        [Required]
        public string ProductCode { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date of birth")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [CustomDateAttribute(ErrorMessage = "Invalid year.")]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Delivery LastName")]
        [Required]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Max length = 20 characters")]
        public string LastName { get; set; }

        [Display(Name = "Delivery FirstName")]
        [Required]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Max length = 20 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(64, MinimumLength = 1, ErrorMessage = "Max length = 64 characters")]
        [Display(Name = "EmailAddress")]
        public string EmailAddress { get; set; }
        
        public string Mobile { get; set; }


        // Address
        [Display(Name = "Delivery Country")]
        [Required]
        public string ISOCountryCode { get; set; }

        [Display(Name = "Delivery PostCode")]
        [Required]
        [StringLength(10, MinimumLength = 1, ErrorMessage = "Max length = 10 characters")]
        public string PostCode { get; set; }

        [Display(Name = "Delivery County")]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Max length = 20 characters")]
        public string State { get; set; }

        [Display(Name = "Delivery City")]
        [Required]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Max length = 20 characters")]
        public string City { get; set; }

        [Display(Name = "Delivery Address2")]
        [StringLength(35, MinimumLength = 1, ErrorMessage = "Max length = 35 characters")]
        public string Address2 { get; set; }

        [Required]
        [Display(Name = "Name to appear on card")]
        [StringLength(21, MinimumLength = 1, ErrorMessage = "Max length = 21 characters")]
        public string NameOnCard { get; set; }

        [Display(Name = "Delivery Address1")]
        [Required]
        [StringLength(35, MinimumLength = 1, ErrorMessage = "Max length = 35 characters")]
        public string Address1 { get; set; }

        [Display(Name = "Addtional Card Emboss Data")]
        [StringLength(30, MinimumLength = 1, ErrorMessage = "Max length = 30 characters")]
        public string AdditionalCardEmbossData { get; set; }

        [Display(Name = "Delivery Address3")]
        [StringLength(35, MinimumLength = 1, ErrorMessage = "Max length = 35 characters")]
        public string Address3 { get; set; }

        [Display(Name = "Card Design")]
        public string CardDesign { get; set; }

        public IList<SelectListItem> Products { get; set; }

        public IList<SelectListItem> Countries { get; set; }
    }
}
