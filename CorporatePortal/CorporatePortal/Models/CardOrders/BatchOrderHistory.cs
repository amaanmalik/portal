﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.CardOrders
{
    public class BatchOrderHistory
    {
        public int? BatchOrderId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedDate { get; set; }
        public string OrderStatus { get; set; }
        public int Total { get; set; }
        public string ProductCode { get; set; }
        public string CardFileName { get; set; }
    }
}