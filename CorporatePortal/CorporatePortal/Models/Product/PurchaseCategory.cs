﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class PurchaseCategory
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public double? MaxSingleTxnValue { get; set; }

        public double? MaxDailyTxnTotalValue { get; set; }

        public double? MaxDailyTotalTxns { get; set; }

        public double? MinSingleTxnValue { get; set; }

        public double? MaxSpend4Days { get; set; }

        public double? MaxTotalTxns4Days { get; set; }

        public double? MaxWeeklyTxnValue { get; set; }
    }
}