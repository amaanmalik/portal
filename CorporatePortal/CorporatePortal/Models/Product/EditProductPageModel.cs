﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Product
{
    public class EditProductPageModel
    {
            [Required]
            [Display(Name = "Product Code")]
            public virtual string ProductCode { get; set; }

            [Required]
            [StringLength(3, MinimumLength = 3, ErrorMessage = "CurrencyCode must be 3 characters")]
            public virtual string Currency { get; set; }

            public IList<SelectListItem> Currencies { get; set; }

            [Display(Name = "Daily Balance")]
            public decimal? MaxDailyBalance { get; set; }

            [Display(Name = "Daily Load")]
            public decimal? MaxDailyLoad { get; set; }

            // Required for Ordering FISUK VirtualCard
            [Display(Name = "Program")]
            public string CardProgram { get; set; }

            [Display(Name = "Design")]
            public string CardDesign { get; set; }

            [Display(Name = "Product")]
            public string CardProduct { get; set; }

            [Display(Name = "Corporate Max Balance")]
            public decimal? CorporateLoadMaxBalance { get; set; }

            [Display(Name = "Corporate Max Balance")]
            public decimal? CorporateLoadMinLoadAmount { get; set; }

            [Display(Name = "Weekly")]
            public decimal? MaxWeeklyLoad { get; set; }

            [Display(Name = "Monthly")]
            public decimal? MaxMonthlyLoad { get; set; }

            [Display(Name = "Yearly")]
            public decimal? MaxYearlyLoad { get; set; }

            [Display(Name = "Reloadable")]
            public virtual bool? Reloadable { get; set; }

            [Display(Name = "Employee")]
            public virtual bool? ForEmployee { get; set; }

            [Display(Name = "Online")]
            public virtual bool? OnlineTransactions { get; set; }

            [Display(Name = "ATM")]
            public virtual bool? ATMTransactions { get; set; }

            [Display(Name = "Funds")]
            public virtual FundsOwnership? FundsOwnership { get; set; }

            [Display(Name = "Type")]
            public virtual string ProductType { get; set; }

            [Display(Name = "Active")]
            public virtual bool? IsActive { get; set; }

            public virtual bool? IsVirtual { get; set; }

            public virtual bool? KycCheck { get; set; }

            public virtual bool? PepsSanctionsCheck { get; set; }

            public double? DebitFreePercentage { get; set; }

            public double? CreditFreePercentage { get; set; }

            public bool? AutoTopupEnabled { get; set; }

            public double? AutoTopupDefaultAmount { get; set; }

            public double? AutoTopUpTopUpThreshold { get; set; }

            public bool? RewardsPointsEnabled { get; set; }

            public int? RewardsPointPerUnit { get; set; }

            public bool? TokeniseWithoutPaymentCardEnabled { get; set; }

            public double? MaxTopupValue { get; set; }

            public double? MinTopupValue { get; set; }



            public bool? AllowAnonymousPinReveal { get; set; }


        
    }
}