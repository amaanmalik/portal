﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class CreditProfileModel
    {
        public int CreditProfileId { get; set; }

        public string ProfileName { get; set; }

        public double CreditLimit { get; set; }

        public double AvailableBalance { get; set; }

        public bool? IsPrePaid { get; set; }

        public double? BalanceDue { get; set; }

        public double? AmountSpent { get; set; }

        public string PaymentFrequency { get; set; }

        public string PaymentTerms { get; set; }

        public string PaymentType { get; set; }

        public string SalesPersonCode { get; set; }

        public double NewCreditLimit { get; set; }

        public double TopUpAvailableBalance { get; set; }

        public string Comments { get; set; }
    }
}