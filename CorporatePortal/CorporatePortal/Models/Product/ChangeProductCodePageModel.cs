﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Product
{
    public class ChangeProductCodePageModel
    {
        public ChangeProductCodePageModel()
        {

        }

        public IList<SelectListItem> productCodes { get; set; }

        public string CurrentProductCode { get; set; }

        public string NewProductCode { get; set; }

        public string CardProxyId { get; set; }

        public string UserId { get; set; }

    }
}