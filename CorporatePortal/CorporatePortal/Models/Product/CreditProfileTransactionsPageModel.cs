﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class CreditProfileTransactionsPageModel
    {
        public int CreditProfileId { get; set; }

        public string CreditProfileName { get; set; }

        public double CreditLimit { get; set; }

        public double AvailableBalance { get; set; }

        public List<CreditProfileTransactionModel> Transactions { get; set; } = new List<CreditProfileTransactionModel>();

        public List<CreditProfileTransactionsModel> CreditProfileTransactions { get; set; } = new List<CreditProfileTransactionsModel>();
    }
}