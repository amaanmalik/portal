﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class SpendProfileOverride 
    {
        public double Amount { get; set; }
        public bool Active { get; set; }
        public string CardId { get; set; }
        public string UserId { get; set; }
    }
}