﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class CreditProfileTransactionModel
    {
        public int CreditProfileId { get; set; }

        public string UserId { get; set; }

        public string AdjustmentType { get; set; }

        public double PreviousAccountBalance { get; set; }

        public double CurrentAccountBalance { get; set; }

        public double PreviousCreditLimit { get; set; }

        public double CurrentCreditLimit { get; set; }

        public string TransactionType { get; set; }

        public double Amount { get; set; }

        public string Comments { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedDate { get; set; }
    }
}