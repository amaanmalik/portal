﻿using CorporatePortal.Api.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class SpendOverrideListResponseModel : BaseApiResponse
    {

        public List<SpendProfileOverride> SpendProfileOverrides { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}