﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class UpdateCreditProfileIsPrepaidRequest
    {
        public int CreditProfileId { get; set; }

        public bool? IsPrePaid { get; set; }
    }
}