﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class UpdateCreditProfileNameRequest
    {
        public int CreditProfileId { get; set; }

        public string CreditProfileName { get; set; }
    }
}