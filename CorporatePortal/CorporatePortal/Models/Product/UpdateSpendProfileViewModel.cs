﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Product
{
    public class UpdateSpendProfileViewModel
    {
        public SpendProfileModel SpendProfile { get; set; }

        public IList<SelectListItem> PurchaseCategories { get; set; }
    }
   
}