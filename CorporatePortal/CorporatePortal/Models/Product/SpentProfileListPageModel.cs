﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class SpentProfileListPageModel
    {
        public List<SpendProfileModel> ProfileList { get; set; }
    }
}