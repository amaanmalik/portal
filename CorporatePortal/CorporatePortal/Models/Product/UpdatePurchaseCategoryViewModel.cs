﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Product
{
    public class UpdatePurchaseCategoryViewModel
    {
        public PurchaseCategory PurchaseCategory { get; set; }

        public IList<SelectListItem> Mcc { get; set; }
    }
}