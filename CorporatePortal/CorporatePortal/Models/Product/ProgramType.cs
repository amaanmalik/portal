﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public enum ProgramType
    {
        CustomerIncentive = 0,
        EmployeeIncentive = 1,
        Payroll = 2,
        EmployeeExpenses = 3,
        SubContractorExpenses = 4,
        CustomerExpenses = 5,
        InsurancePayout = 6
    }
}