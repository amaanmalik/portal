﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class CreateSpendProfileOverridePageModel
    {        
        public double Amount { get; set; }
        public string CardId { get; set; }
        public string UserId { get; set; }
    }
}