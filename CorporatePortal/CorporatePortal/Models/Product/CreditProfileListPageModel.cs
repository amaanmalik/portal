﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Product
{
    public class CreditProfileListPageModel
    {
        public List<CreditProfileModel> ProfileList { get; set; }
    }
}