﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Product
{
    public class QcsProduct
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Product Code")]
        public virtual string ProductCode { get; set; }

        [Required]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "CurrencyCode must be 3 characters")]
        public virtual string Currency { get; set; }

        [Display(Name = "Active")]
        public  bool? IsActive { get; set; }

        public  bool? IsVirtual { get; set; }

        public  bool? KycCheck { get; set; }

        public bool? PepsSanctionsCheck { get; set; }
                     


        [Display(Name = "Daily Balance")]
        public decimal? MaxDailyBalance { get; set; }

        [Display(Name = "Daily Load")]
        public decimal? MaxDailyLoad { get; set; }

        // Required for Ordering FISUK VirtualCard
        [Display(Name = "(FIS) Virtual card program")]
        public string CardProgram { get; set; }

        [Display(Name = "(FIS) Virtual card design")]
        public string CardDesign { get; set; }

        [Display(Name = "(FIS) Virtual card product")]
        public string CardProduct { get; set; }

        [Display(Name = "Max Balance")]
        public decimal? CorporateLoadMaxBalance { get; set; }

        [Display(Name = "Corporate Min Balance")]
        public decimal? CorporateLoadMinLoadAmount { get; set; }

        [Display(Name = "Weekly")]
        public decimal? MaxWeeklyLoad { get; set; }

        [Display(Name = "Monthly")]
        public decimal? MaxMonthlyLoad { get; set; }

        [Display(Name = "Yearly")]
        public decimal? MaxYearlyLoad { get; set; }

        [Display(Name = "Reloadable")]
        public virtual bool? Reloadable { get; set; }

        [Display(Name = "Employee")]
        public virtual bool? ForEmployee { get; set; }

        [Display(Name = "Online")]
        public virtual bool? OnlineTransactions { get; set; }

        [Display(Name = "ATM")]
        public virtual bool? ATMTransactions { get; set; }

        [Display(Name = "Funds")]
        public virtual FundsOwnership? FundsOwnership { get; set; }

        [Display(Name = "Type")]
        public virtual string ProductType { get; set; }      


        public double? DebitFeePercentage { get; set; } 

        public double? CreditFeePercentage { get; set; } 

        public bool? AutoTopupEnabled { get; set; } 

        public double? AutoTopupDefaultAmount { get; set; } 

        public double? AutoTopUpTopUpThreshold { get; set; } 

        public bool? TokeniseWithoutPaymentCard { get; set; } 

        public double? MaxTopupValue { get; set; } 

        public double? MinTopupValue { get; set; } 

        public bool? RewardsPointsEnabled { get; set; }

        public int? RewardsPointPerUnit { get; set; } 

        public bool? AllowAnonymousPinReveal { get; set; } 

        public int? SpendProfileId { get; set; }

        public string SpendProfileName { get; set; }
        
        public IList<SelectListItem> SpendProfiles { get; set; }

        public int? CreditProfileId { get; set; }

        public string CreditProfileName { get; set; }

        public IList<SelectListItem> CreditProfiles { get; set; }

        [Display(Name = "Funding Source")]
        public virtual FundingSource? FundingSource { get; set; }

        public bool? TransactionAlerts { get; set; }

        public virtual int? ProductFeeId { get; set; }

        public string ProductFeeName { get; set; }

        public IList<SelectListItem> ProductFees { get; set; }
    }

    public enum FundsOwnership
    {
        CardHolder = 0,
        Company = 1
    }

    public enum FundingSource
    {
        
        Corporate_Load_Pot = 1,
        Master_Card_Account = 2
    }
}