﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Transactions
{
    public class Transaction
    {
        public string poschpField { get; set; }
        public string poscdimField { get; set; }
        public string poschamField { get; set; }
        public string poscpField { get; set; }
        public string approvalCodeField { get; set; }
        public DateTime sysDateField { get; set; }
        public string revField { get; set; }
        public string orgItemIdField { get; set; }
        public short? itemSrcField { get; set; }
        public double? amtFeeField { get; set; }
        public string crdproductField { get; set; }
        public DateTime? ctxLocalDateField { get; set; }
        public long? ctxLocalTimeField { get; set; }
        public string aVSChkRsField { get; set; }
        public string threeDSecChkRsField { get; set; }
        public string actionCodeField { get; set; }
        public double? amtCashbackField { get; set; }
        public string productCode { get; set; }
        public virtual double? currentBalance { get; set; }
        public virtual string description { get; set; }
        public virtual string balanceAdjustmentType { get; set; }
        public virtual string paymentType { get; set; }
        public virtual string paymentMethodType { get; set; }
        public virtual string merchantName { get; set; }
        public virtual string city { get; set; }
        public string mCCSubFieldDescription { get; set; }
        public virtual string postCode { get; set; }
        public int mCCSubField { get; set; }
        public string crdAcptLocField { get; set; }
        public int messageIDField { get; set; }
        public string instCodeField { get; set; }
        public string txnTypeField { get; set; }
        public int msgTypeField { get; set; }
        public string tlogIdField { get; set; }
        public string orgTlogIDField { get; set; }
        public int? repeatField { get; set; }
        public int? timeoutField { get; set; }
        public string pANField { get; set; }
        public string cardIDField { get; set; }
        public string accNoField { get; set; }
        public string curBillField { get; set; }
        public double? avlBalField { get; set; }
        public double? blkAmtField { get; set; }
        public DateTime localDateField { get; set; }
        public long localTimeField { get; set; }
        public double? amtTxnField { get; set; }
        public string curTxnField { get; set; }
        public double? billAmtField { get; set; }
        public double? billConvRateField { get; set; }
        public double? amtComField { get; set; }
        public double? amtPadField { get; set; }
        public int? txnCodeField { get; set; }
        public string termCodeField { get; set; }
        public string crdAcptIDField { get; set; }
        public int? mCCField { get; set; }
        public virtual string currencyCode { get; set; }
    }
}