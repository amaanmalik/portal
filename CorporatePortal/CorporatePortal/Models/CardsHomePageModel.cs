﻿using CorporatePortal.Models.CardLoad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models
{
    public class CardsHomePageModel
    {
        public List<CorporateFundsSummary> AccountsList { get; set; }
    }
}