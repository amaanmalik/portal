﻿using CorporatePortal.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Report
{
    public class GroupedTransactionsPageModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ProductCode { get; set; }
        public List<SelectListItem> Products { get; set; }
        public IList<Transaction> Transactions { get; set; }
    }
}