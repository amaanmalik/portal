﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Report
{
    public class CreditProfileTransForReportModel
    {
        public int CreditProfileId { get; set; }
        public int AdjustmentType { get; set; }
        public string Type { get; set; }
        public double PreviousAccountBalance { get; set; }
        public double CurrentAccountBalance { get; set; }
        public double PreviousCreditLimit { get; set; }
        public double CurrentCreditLimit { get; set; }
        public int TransactionType { get; set; }
        public double Amount { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}