﻿using CorporatePortal.Api.Responses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Report
{
    public class MasterAccountTransactionsPageModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CardProxyId { get; set; }
        public string AccountName { get; set; }
        public List<SelectListItem> Accounts { get; set; }
        public IList<ApiTransaction> Transactions { get; set; }
    }
}