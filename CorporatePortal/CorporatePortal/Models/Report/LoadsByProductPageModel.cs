﻿using CorporatePortal.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Report
{
    public class LoadsByProductPageModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ProductCode { get; set; }
        public List<SelectListItem> Products { get; set; }
        public IList<Transaction> Transactions { get; set; }
    }
}