﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Report
{
    public class PrepaidCardOrders
    {
        public int Id { get; set; }
        public string OrderStatus { get; set; }
        public int? BatchOrderId { get; set; }
        public string SecondSurName { get; set; }
        public string ReplacedCardProxyId { get; set; }
        public string Language { get; set; }
        public string Dob { get; set; }
        public string RecipientEmail { get; set; }
        public string PaymentRef { get; set; }
        public bool? Paid { get; set; }
        public string ISOCountryCode { get; set; }
        public string PostCode { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address2 { get; set; }
        public string Address1 { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Title { get; set; }      
        public string ProductCode { get; set; }
        public string cardFileName { get; set; }
                
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public virtual DateTime CreatedDate { get; set; }

        public string CardDesign { get; set; }

        public string NameOnCard { get; set; }

        public string AdditionalCardEmbossData { get; set; }

        public string Mobile { get; set; }

        public string Address3 { get; set; }
            
    }
}