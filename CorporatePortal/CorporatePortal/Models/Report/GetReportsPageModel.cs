﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Report
{
    public class GetReportsPageModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ReportType { get; set; }
        public List<SelectListItem> ReportTypes { get; set; }
    }

    public class vm
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ReportType { get; set; }
        public List<SelectListItem> ReportTypes { get; set; }
        public List<PrepaidCardOrders> Orders { get; set; }
    }

    public enum ReportType
    {
        OrdersPlaced = 0,
        CardsActivated = 1,
        LostStolenCards = 2,
        CardsIssuedCardHolders = 3,
        IndividualCardTransactionHistory = 4,
        ProductTransactionHistory = 5,
        IndividualCardBalances = 6
    }
}