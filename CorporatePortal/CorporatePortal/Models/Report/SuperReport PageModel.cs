﻿using CorporatePortal.Models.Product;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Report
{
    public class SuperReportPageModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CreditProfileId { get; set; }
        public string CreditProfileName { get; set; }
        public List<SelectListItem> CreditProfiles { get; set; }
        public IList<CreditProfileTransactionsModel> Transactions { get; set; }
    }
}