﻿using CorporatePortal.Api.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Report
{
    public class OrdersResponse : BaseApiResponse
    {
        public List<PrepaidCardOrders> Orders { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

