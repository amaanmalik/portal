﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Report
{
    public class OrderReportHomePageModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [Required]
        public string ReportType { get; set; }
        public List<SelectListItem> OrderReportTypes { get; set; }
        public List<PrepaidCardOrders> Orders { get; set; }

    }

    public enum OrderReportType
    {
        AllOrders = 0,
        IndividualCardOrders = 1,
        BatchCardOrders = 2,
        VirtualCardOrders = 3,
        BatchOrderFile = 4,
        ReplacementCardOrders = 5
    }
}