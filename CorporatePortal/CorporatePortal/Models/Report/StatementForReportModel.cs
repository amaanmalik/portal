﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Report
{
    public class StatementForReportModel
    {
        public string BusinessCentralId { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public bool Paid { get; set; }
        public double TotalNet { get; set; }
        public double TotalGross { get; set; }
        public double FXFeeTotal { get; set; }
        public double FeeVatPercentage { get; set; }
        public double CardIssueFeeNet { get; set; }
        public double CardIssueFeeGross { get; set; }
        public double CardFeeNet { get; set; }
        public double CardFeeGross { get; set; }
        public double FinanceFeeTotal { get; set; }
        public double TotalSpentNonGBP { get; set; }
        public double TotalSpentGBP { get; set; }
        public double TotalSpent { get; set; }
        public string TimeActioned { get; set; }
        public string StatementMonth { get; set; }
        public string StatementYear { get; set; }
        public string CreditProfileName { get; set; }
        public int CreditProfileId { get; set; }
        public string StatementType { get; set; }
        public virtual DateTime UpdatedDate { get; set; }
        public virtual DateTime CreatedDate { get; set; }
   
        public int StatementId { get; set; }
        public int WeekNumber { get; set; }
        public string Frequency { get; set; }
    }
}