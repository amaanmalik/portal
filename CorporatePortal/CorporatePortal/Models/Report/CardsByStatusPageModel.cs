﻿using CorporatePortal.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models.Report
{
    public class CardsByStatusPageModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CardStatus { get; set; }
        public List<SelectListItem> CardStatusList { get; set; }
        public IList<Transaction> Transactions { get; set; }
    }
}
