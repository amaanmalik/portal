﻿using CorporatePortal.Models.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Models
{
    public class CorporateStaff
    {
        public CorporateStaff()
        {
        }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        public string UserName { get; set; }

        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$", ErrorMessage = "Password min length : 8. Must contain atleast one uppercase letter and a digit. No special characters allowed!")]
        [DataType(DataType.Password)]
        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "EmailAddress")]
        public string EmailAddress { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Confirm Email")]
        [System.ComponentModel.DataAnnotations.Compare("EmailAddress", ErrorMessage = "The email and confirmation do not match.")]
        public string ConfirmEmailAddress { get; set; }

        [Required]
        public UserRole Role { get; set; }

        public IList<SelectListItem> Roles { get; set; }

        [Display(Name = "Active User")]
        public bool IsActive { get; set; }

        public bool EmailConfirmed { get; set; }

        public string UserId { get; set; }
    }
}