﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Account
{

    public class PasswordResetForm
    {
        public string UserId { get; set; }

        public string ResetToken { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}