﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Account
{
    public enum UserRole
    {
        Admin = 0,
        User = 1,
        Parent = 2,
        Child = 3,
        CustomerService = 4,
        WebApi = 5,
        Accounts = 6,
        CorporateAdmin = 7,
        CorporateManager = 8,
        CorporateEmployee = 9,
        CorporateUser = 10,
        SuperAdmin = 13
        
    }
}