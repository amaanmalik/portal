﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Models.Account
{
    public class GenerateConfirmationTokenForm
    {
        public string EmailAddress { get; set; }
    }
}