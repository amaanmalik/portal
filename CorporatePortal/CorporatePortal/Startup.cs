﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using CorporatePortal.Api;
using CorporatePortal.Controllers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Newtonsoft.Json;
using Owin;
using Qcs.EmailProvider;
using Qcs.EmailProvider.Smtp;

[assembly: OwinStartup(typeof(CorporatePortal.Startup))]

namespace CorporatePortal
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API and MVC controllers.
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // Local
            //var apiClientConfig = new ApiConfig(
            //   ConfigurationManager.AppSettings["Local.WebApi.Url"],
            //   ConfigurationManager.AppSettings["Local.WebApi.Username"],
            //   ConfigurationManager.AppSettings["Local.WebApi.Password"]
            //   );

            // Staging
            //var apiClientConfig = new ApiConfig(
            //  ConfigurationManager.AppSettings["Staging.WebApi.Url"],
            //  ConfigurationManager.AppSettings["Staging.WebApi.Username"],
            //  ConfigurationManager.AppSettings["Staging.WebApi.Password"]
            //  );

            // Fuelmate
            //var apiClientConfig = new ApiConfig(
            //ConfigurationManager.AppSettings["Fuelmate.WebApi.Url"],
            //ConfigurationManager.AppSettings["Fuelmate.WebApi.Username"],
            //ConfigurationManager.AppSettings["Fuelmate.WebApi.Password"]
            //);

            //Buttles
            //var apiClientConfig = new ApiConfig(
            //  ConfigurationManager.AppSettings["Buttles.WebApi.Url"],
            //  ConfigurationManager.AppSettings["Buttles.WebApi.Username"],
            //  ConfigurationManager.AppSettings["Buttles.WebApi.Password"]
            //  );

            //Buttles
            //var apiClientConfig = new ApiConfig(
            //  ConfigurationManager.AppSettings["ButtlesQBC.WebApi.Url"],
            //  ConfigurationManager.AppSettings["ButtlesQBC.WebApi.Username"],
            //  ConfigurationManager.AppSettings["ButtlesQBC.WebApi.Password"]
            //  );

            //Hyundai
            //var apiClientConfig = new ApiConfig(
            //   ConfigurationManager.AppSettings["Hyundai.WebApi.Url"],
            //   ConfigurationManager.AppSettings["Hyundai.WebApi.Username"],
            //   ConfigurationManager.AppSettings["Hyundai.WebApi.Password"]
            //   );

            //Dev Dtc Staging
            //var apiClientConfig = new ApiConfig(
            //   ConfigurationManager.AppSettings["DtcStaging.WebApi.Url"],
            //   ConfigurationManager.AppSettings["DtcStaging.WebApi.Username"],
            //   ConfigurationManager.AppSettings["DtcStaging.WebApi.Password"]
            //   );

            //Dev Dtc Staging Api
            //var apiClientConfig = new ApiConfig(
            //   ConfigurationManager.AppSettings["Dtc.WebApi.Url"],
            //   ConfigurationManager.AppSettings["Dtc.WebApi.Username"],
            //   ConfigurationManager.AppSettings["Dtc.WebApi.Password"]
            //   );

            // MyCardServices QcsProdQls
            //var apiClientConfig = new ApiConfig(
            //   ConfigurationManager.AppSettings["QcsProdQls.WebApi.Url"],
            //   ConfigurationManager.AppSettings["QcsProdQls.WebApi.Username"],
            //   ConfigurationManager.AppSettings["QcsProdQls.WebApi.Password"]
            //   );

            // MyCardServices QcsProd
            //var apiClientConfig = new ApiConfig(
            //   ConfigurationManager.AppSettings["QcsProd.WebApi.Url"],
            //   ConfigurationManager.AppSettings["QcsProd.WebApi.Username"],
            //   ConfigurationManager.AppSettings["QcsProd.WebApi.Password"]
            //   );

            // Fuelmate PreLives
            var apiClientConfig = new ApiConfig(
               ConfigurationManager.AppSettings["FuelmatePrelive.WebApi.Url"],
               ConfigurationManager.AppSettings["FuelmatePrelive.WebApi.Username"],
               ConfigurationManager.AppSettings["FuelmatePrelive.WebApi.Password"]
               );

            builder.RegisterType<ApiClient>()
                .As<IApiService>()
                .WithParameter(new NamedParameter("config", apiClientConfig));

            ////builder.RegisterType<MockApiService>()
            ////  .As<IApiService>();

            builder.RegisterType<SmtpEmailProvider>().As<IEmailProvider>().WithParameter(
              new NamedParameter("config", new SmtpConfig()
              {
                  Host = ConfigurationManager.AppSettings["SendGridEmail.Host"] as string,
                  Port = int.Parse(ConfigurationManager.AppSettings["SendGridEmail.Port"] as string),
                  Username = ConfigurationManager.AppSettings["SendGridEmail.UserName"] as string,
                  Password = ConfigurationManager.AppSettings["SendGridEmail.Password"] as string,
              }))
              .InstancePerRequest();

            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerLifetimeScope();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // Changes json setting DateTime format from UTC to Local...
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;

            // Register the Autofac middleware FIRST. This also adds
            // Autofac-injected middleware registered with the container.
            app.UseAutofacMiddleware(container);

            // OWIN WEB API SETUP:

            // Register the Autofac middleware FIRST, then the Autofac Web API middleware,
            // and finally the standard Web API middleware.
            //app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
            //app.UseWebApi(config); // Configures WebApi to run on top of Owin - REMOVED
            app.UseAutofacMvc();

            ConfigureAuth(app);
        }

        private void ConfigureAuth(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider(),
                CookieHttpOnly = true,
                CookieSecure = CookieSecureOption.SameAsRequest,
                CookieName = "QPayPortal",
                ExpireTimeSpan = TimeSpan.FromMinutes(20)
            });
        }
    }
}
