﻿using CorporatePortal.Api;
using CorporatePortal.Api.Responses;
using CorporatePortal.Models.Account;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Qcs.EmailProvider;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    public class AccountController : Controller
    {
        private readonly IApiService _apiService;
        private readonly IEmailProvider _emailService;

        public AccountController(IApiService apiService, IEmailProvider emailService)
        {
            _apiService = apiService;

            _emailService = emailService;
        }

        // GET: /Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = _apiService.CheckUserCredentials(model.Username, model.Password);

            if (user.UserInformation == null)
            {
                ModelState.AddModelError("", "Invalid Username or Password.");
                TempData["FailureMessage"] = "Invalid Username or Password!";
                return View(model);
            }

            if (user.UserInformation != null && !user.UserInformation.IsActive)
            {
                ModelState.AddModelError("", "User account not active.");
                TempData["FailureMessage"] = "User account not active!";
                return View(model);
            }

            if (user.UserInformation != null && user.UserInformation.IsActive && !user.UserInformation.EmailConfirmed)
            {
                ModelState.AddModelError("", "User account email not confirmed.Please check your email for confirmation link!");
                TempData["FailureMessage"] = "User account email not confirmed!";
                return View(model);
            }

            var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserInformation.FirstName));
            identity.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "user.UserInformation.FirstName"));
            foreach (var role in user.UserInformation.Roles)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, role));
            }
            IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, identity);

            return RedirectToAction("Index", "Home");

        }
        
        #region Helper
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Home", "Home");
        }
        #endregion
        private bool Login(string username, string password)
        {
            //Logger.Debug("Login(username = {0}, password = xxx", username);

            var user = _apiService.CheckUserCredentials(username, password);

            //Logger.Debug("Login user = {0}", user);

            //Session["user-creds"] = new { Username = username, Password = password }; //TODO: This isn't the best way to do this
            if (user.UserInformation == null)
            {
                //Logger.Debug("User not found");
                return false;
            }

            if (!user.UserInformation.IsActive)
            {
                return false;
            }

            var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.UserInformation.FirstName));
            identity.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", user.UserInformation.FirstName));

            IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, identity);

            //Session["user-details"] = user.UserInformation;

            //var IsParent = _apiService.CheckUserRolw(user.UserInformation.CardholderId, "Parent").IsInRole;

            //Session["IsParent"] = IsParent ? true : false;

            return true;
        }

        public ActionResult Logout()
        {
            IAuthenticationManager authenticationManager = HttpContext.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        public ActionResult ConfirmEmail(string Token, string Email)
        {
            if (Token == null || Email == null)
            {
                return View("Error");
            }
            //var result = await UserManager.ConfirmEmailAsync(userId, code);

            var response = _apiService.ConfirmEmail(Token, new CardHolderDetails { IsActive = true });

            return View(response.IsSuccess ? "ConfirmEmail" : "Error");

        }      

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotUsername()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotUsername(ForgotUsernameViewModel form)
        {
            if (ModelState.IsValid)
            {
                var tokenResponse = _apiService.GenerateForgottenUsernameToken(form.EmailAddress);
                var resetToken = HttpUtility.UrlEncode(tokenResponse.Token);
                
                if (tokenResponse.IsSuccess && tokenResponse.Username != null)
                {
                    SendUsernameEmail(tokenResponse.Username, form.EmailAddress);
                    TempData["SuccessMessage"] = "Username reminder sent successfully";
                    return View("UsernameReminderConfirmation");
                }

                if (!tokenResponse.IsSuccess)
                {
                    ModelState.AddModelError("", "No user found with the email provided!");
                    TempData["FailureMessage"] = "No user found with the email provided!";
                    return View(form);
                }

            }

            TempData["FailureMessage"] = "Invalid data!";
            return View(form);
        }

        private void SendUsernameEmail(string username, string emailAddress)
        {
            string emailProduct = ConfigurationManager.AppSettings["EmailProduct"];
            string usernameReminderEmailSubject = string.Format(ConfigurationManager.AppSettings["UsernameReminderEmailSubject"],emailProduct);


            string emailBody = string.Format("{0}{1}Please <a href = {2}>{3}</a> {4}",
                               "Hello, <br/> You recently requested a username reminder for the QPAY " + emailProduct + " portal.<br/><br/>",
                               "Your username is : <b>" + username + "</b>.<br/><br/>",
                               Url.Action("Login", "Account", new { }, Request.Url.Scheme),
                               "click here to Login.",
                             "<br/><br/> Thanks,<br/> QPay");

            _emailService.SendEmail(new EmailMessage
            {
                From = new EmailAddress
                {
                    Address = "contactus@qcs-uk.com"
                },
                To = new List<EmailAddress>(){
                    new EmailAddress
                    {
                        Address = emailAddress
                    }
                },
                Body = emailBody,
                EmailType = EmailType.Html,
                Subject = usernameReminderEmailSubject
            });
        }
        
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePassword(UpdatePasswordForm form)
        {
            if (!ModelState.IsValid)
            {

            }

            bool isCurrentPasswordCorrect = ((dynamic)Session["user-creds"]).Password.Equals(form.CurrentPassword);
            if (!isCurrentPasswordCorrect)
            {

            }

            //var apiUser = Session["user-details"] as ApiUser;
            //var apiResponse = _apiService.ChangePassword(apiUser.CardholderId, form.CurrentPassword, form.NewPassword);
            //if (!apiResponse.IsSuccess)
            //{
            //    //TempData[UmbracoMessageKeys.MESSAGE_SUCCCESS_KEY] = false;
            //    //TempData[UmbracoMessageKeys.MESSAGE_KEY] = UmbracoMessageKeys.PASSWORD_UPDATE_FAILURE;
            //    //return CurrentUmbracoPage();
            //}

            //TempData[UmbracoMessageKeys.MESSAGE_SUCCCESS_KEY] = true;
            //TempData[UmbracoMessageKeys.MESSAGE_KEY] = UmbracoMessageKeys.PASSWORD_UPDATE_SUCCESS;

            return Redirect("");
        }
        
        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ConfirmPasswordReset(string Token, string CardholderId)
        {
            if (Token == null || CardholderId == null)
            {
                return View("Error");
            }

            var model = new PasswordResetModel
            {
                UserId = CardholderId,
                ResetToken = Token
            };

            return View("ResetPassword", model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GenerateForgottenPasswordToken(ForgotPasswordViewModel form)
        {
            if (ModelState.IsValid)
            {                
                var tokenResponse = _apiService.GenerateForgottenPasswordToken(form.EmailAddress);
                var resetToken = HttpUtility.UrlEncode(tokenResponse.Token);

                if (tokenResponse.IsSuccess && tokenResponse.CardholderId != null)
                {
                    SendPasswordResetEmail(tokenResponse.Token, tokenResponse.CardholderId, form.EmailAddress);
                    TempData["SuccessMessage"] = "Password reminder sent successfully";
                    return View("ResetPasswordReminderConfimation");
                }

                if (!tokenResponse.IsSuccess)
                {
                    ModelState.AddModelError("", "No user found with the email provided!");
                    TempData["FailureMessage"] = "No user found with the email provided!";
                     return View("ForgotPassword", form);
                }

            }

            TempData["FailureMessage"] = "Invalid data!";
            return View("ForgotPassword", form);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PasswordReset(PasswordResetModel form)
        {
            if (ModelState.IsValid)
            {
                var resetPasswordResponse = _apiService.ResetPasswordWithToken(form.UserId, form.ResetToken, form.Password);
                TempData["SuccessMessage"] = "Password reset successfully";
                return View("ResetPasswordConfirmation");
            }

            TempData["FailureMessage"] = "Invalid data!";
            return View("ResetPassword", form);     
        }
                     
        private void SendPasswordResetEmail(string token, string cardholderId, string emailAddress, bool passwordupdated = false)
        {
            string emailProduct = ConfigurationManager.AppSettings["EmailProduct"];
            string passwordReminderEmailSubject = string.Format(ConfigurationManager.AppSettings["PasswordResetEmailSubject"], emailProduct);


            string emailBody = string.Format("{0}<a href = {1}>{2}</a> {3}",
                              "Hello, <br/> You recently requested a password reset for the QPAY " + emailProduct + " portal.<br/><br/>",
                              Url.Action("ConfirmPasswordReset", "Account", new { Token = token, CardholderId = cardholderId }, Request.Url.Scheme),
                              "Click here to reset password.",
                              "If you didn’t mean to reset your password, then you can just ignore this email; your password will not change.<br/><br/> Thanks,<br/> QPay");

            _emailService.SendEmail(new EmailMessage
            {
                From = new EmailAddress
                {
                    Address = "contactus@qcs-uk.com"
                },
                To = new List<EmailAddress>(){
                    new EmailAddress
                    {
                        Address = emailAddress
                    }
                },
                Body = emailBody,
                EmailType = EmailType.Html,
                Subject = passwordReminderEmailSubject
            });            
        }
    }  

}