﻿using CorporatePortal.Api;
using CorporatePortal.Api.Requests;
using CorporatePortal.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    [Authorize(Roles ="Admin,CorporateAdmin,CorporateManager")]
    public class CreditProfileController : Controller
    {
        IApiService _apiService;

        public CreditProfileController(IApiService apiService)
        {
            _apiService = apiService;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager")]    
        public ActionResult Index()
        {
            var response = _apiService.GetCreditProfilesList();

            var model = new CreditProfileListPageModel
            {
                ProfileList = response.CreditProfileList
            };

            return View(model);
        }

        [Authorize(Roles = "Admin,CorporateAdmin")]
        public ActionResult Create()
        {
            var model = new CreditProfileModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CreditProfileModel model)
        {
            var response = _apiService.CreateCreditProfile(model);

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin,CorporateAdmin")]
        public ActionResult Update(int creditProfileId)
        {
            var creditProfileResponse = _apiService.GetCreditProfileById(creditProfileId);

            var model = creditProfileResponse.CreditProfile;

            return View(model);
        }

        //[HttpPost]
        //public ActionResult Update(CreditProfileModel model)
        //{
        //    var request = new UpdateCreditProfileRequest
        //    {
        //        CreditProfile = model
        //    };

        //    var response = _apiService.UpdateCreditProfile(request);

        //    return RedirectToAction("Index");
        //}

        [Authorize(Roles = "Admin,CorporateAdmin")]
        public ActionResult UpdateCreditLimit(int creditProfileId)
        {
            var creditProfileResponse = _apiService.GetCreditProfileById(creditProfileId);

            var model = creditProfileResponse.CreditProfile;

            return View(model);
        }

        [Authorize(Roles = "Admin,CorporateAdmin")]
        public ActionResult UpdateAvailableBalance(int creditProfileId)
        {
            var creditProfileResponse = _apiService.GetCreditProfileById(creditProfileId);

            var profile = creditProfileResponse.CreditProfile;

            var model = new CreditProfileModel
            {
                CreditProfileId = profile.CreditProfileId,
                AvailableBalance = Convert.ToDouble(profile.AvailableBalance.ToString("0.00")),
                TopUpAvailableBalance = profile.TopUpAvailableBalance,
                CreditLimit = profile.CreditLimit,
                NewCreditLimit = profile.NewCreditLimit,
                ProfileName = profile.ProfileName,
                Comments = profile.Comments
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateCreditLimit(CreditProfileModel model)
        {
            var request = new UpdateCreditProfileCreditLimitRequest
            {
                CreditProfileId = model.CreditProfileId,
                CreditLimit = model.NewCreditLimit,
                CreditProfileName = model.ProfileName
            };

            var response = _apiService.UpdateCreditProfileCreditLimit(request);
            
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UpdateAvailableBalance(CreditProfileModel model)
        {
            var request = new UpdateCreditProfileAvailableBalanceRequest
            {
                CreditProfileId = model.CreditProfileId,
                AvailableBalance = model.TopUpAvailableBalance,
                CreditProfileName = model.ProfileName,
                Comments = model.Comments
            };

            var response = _apiService.UpdateCreditProfileAvailableBalance(request);

            if(response.IsSuccess == false)
            {
                if (response.ErrorCodes[0] == "19000001")
                {
                    ModelState.AddModelError("", "Balance not updated. Error : Available balance cannot exceed credit limit.");
                    TempData["FailureMessage"] = "Balance not updated. Error : Available balance cannot exceed credit limit";

                    var creditProfileResponse = _apiService.GetCreditProfileById(request.CreditProfileId.Value);

                    var vm = creditProfileResponse.CreditProfile;

                    return View(vm);
                }
                else
                {
                    ModelState.AddModelError("", "Balance not updated.ErrorCode : " + response.ErrorCodes[0].ToString());
                    TempData["FailureMessage"] = "Balance not updated.ErrorCode : " + response.ErrorCodes[0].ToString();

                    var creditProfileResponse = _apiService.GetCreditProfileById(request.CreditProfileId.Value);

                    var vm = creditProfileResponse.CreditProfile;

                    return View(vm);
                }
            }
            

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager")]
        [HttpGet]
        public ActionResult CreditProfileTransactions(int creditProfileId, string creditProfileName, double creditLimit, double availableBalance)
        {   
            var response = _apiService.GetCreditProfileTransactions(creditProfileId);

            var list = response.Transactions;

            var model = new CreditProfileTransactionsPageModel();

            foreach (var trans in list)
            {
                model.Transactions.Add(new CreditProfileTransactionModel
                {
                    CreditProfileId = trans.CreditProfileId,
                    Amount = Math.Round(trans.Amount,2),
                    AdjustmentType = trans.AdjustmentType ,
                    CurrentAccountBalance = trans.CurrentAccountBalance,
                    TransactionType = trans.TransactionType,
                    PreviousAccountBalance = trans.PreviousAccountBalance,
                    CurrentCreditLimit = trans.CurrentCreditLimit,
                    PreviousCreditLimit = trans.PreviousCreditLimit,
                    CreatedDate = trans.CreatedDate,
                    UserId = trans.UserId,
                    Comments = trans.Comments
                });
            }

            model.CreditProfileId = creditProfileId;
            model.CreditProfileName = creditProfileName;
            model.CreditLimit = creditLimit;
            model.AvailableBalance = availableBalance;

            return View(model);
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager")]
        [HttpGet]
        public ActionResult DeleteCreditProfile(int creditProfileId)
        {
            var response = _apiService.DeleteCreditProfile(creditProfileId);
            
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin,CorporateAdmin")]
        public ActionResult UpdateCreditProfileName(int creditProfileId)
        {
            var creditProfileResponse = _apiService.GetCreditProfileById(creditProfileId);

            var model = creditProfileResponse.CreditProfile;

            return View(model);
        }
        
        [HttpPost]
        public ActionResult UpdateCreditProfileName(CreditProfileModel req)
        {
            var request = new UpdateCreditProfileNameRequest
            {
                CreditProfileId = req.CreditProfileId,
                CreditProfileName = req.ProfileName
            };

            var creditProfileResponse = _apiService.UpdateCreditProfileName(request);

            var model = creditProfileResponse.CreditProfile;

            return RedirectToAction("Index");
        }

        //[Authorize(Roles = "Admin,CorporateAdmin")]
        //public ActionResult UpdateCreditProfileIsPrePaid(int creditProfileId)
        //{
        //    var creditProfileResponse = _apiService.GetCreditProfileById(creditProfileId);

        //    var model = creditProfileResponse.CreditProfile;

        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult UpdateCreditProfileIsPrePaid(CreditProfileModel req)
        //{
        //    var request = new UpdateCreditProfileIsPrepaidRequest
        //    {
        //        CreditProfileId = req.CreditProfileId,
        //        IsPrePaid = req.IsPrePaid
        //    };

        //    var creditProfileResponse = _apiService.UpdateCreditProfileIsPrePaid(request);

        //    var model = creditProfileResponse.CreditProfile;

        //    return RedirectToAction("Index");
        //}
    }
}