﻿using CorporatePortal.Api;
using CorporatePortal.Api.Requests;
using CorporatePortal.Models.ProductFee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    public class ProductFeeController : Controller
    {
        IApiService _apiService;

        public ProductFeeController(IApiService apiService)
        {
            _apiService = apiService;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }
        // GET: ProductFee
        public ActionResult Index()
        {
            var response = _apiService.GetProductFeesList();

            var model = new ProductFeesListPageModel
            {
                ProductFeeList = response.ProductFees
            };

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new CreateProductFeeModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CreateProductFeeModel model)
        {
            if (!ModelState.IsValid)
            {                
                return View(model);
            }

            ProductFeeModel productFee = new ProductFeeModel
            {
                Name = model.Name,
                CardFee = model.CardFee,
                CardIsueeFee = model.CardIsueeFee,
                FinanceFee = model.FinanceFee,
                FXFee = model.FXFee
            };

            var response = _apiService.CreateProductFee(productFee);

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager")]
        [HttpGet]
        public ActionResult DeleteProductFee(int productFeeId)
        {
            var response = _apiService.DeleteProductFee(productFeeId);

            return RedirectToAction("Index");
        }

        public ActionResult Update(int productFeeId)
        {
            var response = _apiService.GetProductFeeById(productFeeId);

            var model = new UpdateProductFeeModel
            {
                ProductFeeId = response.ProductFee.ProductFeeId,
                CardFee = response.ProductFee.CardFee,
                CardIsueeFee = response.ProductFee.CardIsueeFee,
                FinanceFee = response.ProductFee.FinanceFee,
                FXFee = response.ProductFee.FXFee,
                Name = response.ProductFee.Name
            };
                                   
            return View(model);
        }

        [HttpPost]
        public ActionResult Update(UpdateProductFeeModel model)
        {            
            var request = new UpdateProductFeeRequest
            {
                Name = model.Name,
                CardFee = model.CardFee,
                CardIsueeFee = model.CardIsueeFee,
                FinanceFee = model.FinanceFee,
                FXFee = model.FXFee
            };

            var response = _apiService.UpdateProductFee(model.ProductFeeId,request);

            return RedirectToAction("Index");
        }
    }
}