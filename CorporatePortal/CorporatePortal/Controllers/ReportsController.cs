﻿using CorporatePortal.Api;
using CorporatePortal.Api.Responses;
using CorporatePortal.Models.CardLoad;
using CorporatePortal.Models.Report;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager")]
    public class ReportsController : Controller
    {
        private readonly IApiService _apiService;

        public ReportsController(IApiService apiService)
        {
            _apiService = apiService;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }
        // GET: Reports
        public ActionResult Index()
        {
            var model = new GetReportsPageModel
            {
                ReportTypes = GetReportTypeDdl()
            };


            return View(model);

        }

        public ActionResult OrdersHome()
        {
            var model = new OrderReportHomePageModel
            {
                OrderReportTypes = GetOrderReportTypes()
            };

            return View(model);
        }

        private static List<SelectListItem> GetOrderReportTypes()
        {
            List<SelectListItem> OrderReportTypes = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Text = "AllOrders",
                    Value = OrderReportType.AllOrders.ToString()
                },
               new SelectListItem
                {
                    Text = "IndividualCardOrders",
                    Value = OrderReportType.IndividualCardOrders.ToString()
                },
               new SelectListItem
                {
                    Text = "BatchCardOrders",
                    Value = OrderReportType.BatchCardOrders.ToString()
                },
               new SelectListItem
                {
                    Text = "ReplacementCardOrders",
                    Value = OrderReportType.ReplacementCardOrders.ToString()
                }
            };

            OrderReportTypes.Insert(0, new SelectListItem { Text = "--Select--", Value = "", Disabled = false, Selected = true });
            return OrderReportTypes.ToList();
        }

        [HttpPost]
        public ActionResult GetOrderReport(OrderReportHomePageModel model)
        {
            var ordersResponse = _apiService.GetOrders();

            var orders = ordersResponse.Orders.Where(o => o.CreatedDate.Date >= model.StartDate && o.CreatedDate.Date <= model.EndDate).ToList();

            if (model.ReportType == OrderReportType.IndividualCardOrders.ToString())
            {
                orders = orders.Where(o => o.BatchOrderId.HasValue == false).Where(x => string.IsNullOrEmpty(x.ReplacedCardProxyId)).ToList();
            }

            if (model.ReportType == OrderReportType.BatchCardOrders.ToString())
            {
                orders = orders.Where(o => o.BatchOrderId.HasValue == true).ToList();
            }

            if (model.ReportType == OrderReportType.ReplacementCardOrders.ToString())
            {
                orders = orders.Where(x => !string.IsNullOrEmpty(x.ReplacedCardProxyId)).ToList();
            }

            var reponseModel = new OrderReportHomePageModel
            {
                ReportType = model.ReportType,
                OrderReportTypes = GetOrderReportTypes(),
                Orders = orders,
                StartDate = model.StartDate,
                EndDate = model.EndDate
            };

            TempData["ReportType"] = model.ReportType;
            return View("OrdersHome", reponseModel);
        }


        [HttpPost]
        public ActionResult Index(GetReportsPageModel model)
        {
            var ordersResponse = _apiService.GetOrders();

            var orders = ordersResponse.Orders;

            var transactionsReponse = _apiService.GetTransactionByProductSummary();


            return View("Orders", orders);

        }

        private static List<SelectListItem> GetReportTypeDdl()
        {
            List<SelectListItem> Roles = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Text = "Orders Placed",
                    Value = ReportType.OrdersPlaced.ToString()
                },
               new SelectListItem
                {
                    Text = "Cards Activated",
                    Value = ReportType.CardsActivated.ToString()
                },
               new SelectListItem
                {
                    Text = "Lost / Stolen Cards",
                    Value = ReportType.LostStolenCards.ToString()
                },
                 new SelectListItem
                {
                    Text = "Cards Issued CardHolders",
                    Value = ReportType.CardsIssuedCardHolders.ToString()
                }
            };

            Roles.Insert(0, new SelectListItem { Text = "--Select Role--", Value = "", Disabled = false, Selected = true });
            return Roles.ToList();
        }

        public ActionResult Orders()
        {
            var orders = _apiService.GetOrders();

            return View();
        }

        public ActionResult TransactionsByProduct()
        {
            var response = _apiService.GetProducts();

            var products = response.ProductList.ToList();

            var model = new TransactionsByProductPageModel();

            model.Products = GetProductsForTransactionsDdl(products);

            return View(model);
        }

        [HttpPost]
        public ActionResult TransactionsByProduct(TransactionsByProductPageModel model)
        {
            AccountTransactionHistoryResponseModel response = new AccountTransactionHistoryResponseModel();

            if (model.ProductCode == "All Products")
            {
                response = _apiService.GetAllTransactions();
                response.Transactions = response.Transactions.Where(p => p.actionCodeField == "000").ToList();
            }
            else
            {
                response = _apiService.GetTransactionByProductCode(model.ProductCode);
            }


            model.Products = GetProductsForTransactionsDdl(_apiService.GetProducts().ProductList.ToList());

            var selected = model.Products.Where(x => x.Text == model.ProductCode).First();
            selected.Selected = true;

            foreach (var item in response.Transactions)
            {
                item.TransAmount = ((item.billAmtField.HasValue ? item.billAmtField.Value : 0) + (item.billAmtField.HasValue ? item.amtFeeField.Value : 0));
            }

            model.Transactions = response.Transactions.Where(o => o.txnTypeField == "19" || o.txnTypeField == "66").Where(o => o.CreatedDate.Date >= model.StartDate && o.CreatedDate.Date <= model.EndDate).ToList();

            return View(model);
        }

        public ActionResult GetCardsByStatus()
        {
            CardsByStatusPageModel model = new CardsByStatusPageModel();
            model.CardStatusList = GetCardStatusList();

            return View(model);
        }

        [HttpPost]
        public ActionResult GetCardsByStatus(CardsByStatusPageModel model)
        {

            //var test1 = _apiService.GetTransactionsByCardBalances();

            //var test2 = _apiService.GetGroupedTransactions(new GetGroupedTransactionsRequestModel { ProductCode = "", StartDate = DateTime.Now.AddDays(-30).Date, EndDate = DateTime.Now.Date });

            var response = _apiService.GetTransactionsByCardStatus(model.CardStatus);

            model.CardStatusList = GetCardStatusList();

            model.Transactions = response.Transactions.Where(o => o.localDateField.Date >= model.StartDate && o.localDateField.Date <= model.EndDate).ToList();


            return View(model);
        }

        public ActionResult ActivatedCardsList()
        {
            // var response = _apiService.GetTransactionsByCardBalances();

            //var model = response.Transactions;

            CardsByStatusPageModel model = new CardsByStatusPageModel();
            model.CardStatus = "Activated";
            return View(model);
        }

        [HttpPost]
        public ActionResult ActivatedCardsList(CardsByStatusPageModel model)
        {

            //var test1 = _apiService.GetTransactionsByCardBalances();

            //var test2 = _apiService.GetGroupedTransactions(new GetGroupedTransactionsRequestModel { ProductCode = "", StartDate = DateTime.Now.AddDays(-30).Date, EndDate = DateTime.Now.Date });

            var response = _apiService.GetTransactionsByCardStatus(model.CardStatus);

            model.CardStatusList = GetCardStatusList();

            model.Transactions = response.Transactions.Where(o => o.localDateField.Date >= model.StartDate && o.localDateField.Date <= model.EndDate).ToList();

            return View(model);
        }

        public ActionResult BlockedCardsList()
        {
            // var response = _apiService.GetTransactionsByCardBalances();

            //var model = response.Transactions;

            CardsByStatusPageModel model = new CardsByStatusPageModel();
            model.CardStatus = "Blocked";
            return View(model);
        }

        [HttpPost]
        public ActionResult BlockedCardsList(CardsByStatusPageModel model)
        {

            //var test1 = _apiService.GetTransactionsByCardBalances();

            //var test2 = _apiService.GetGroupedTransactions(new GetGroupedTransactionsRequestModel { ProductCode = "", StartDate = DateTime.Now.AddDays(-30).Date, EndDate = DateTime.Now.Date });

            var response = _apiService.GetTransactionsByCardStatus(model.CardStatus);

            model.CardStatusList = GetCardStatusList();

            model.Transactions = response.Transactions.Where(o => o.localDateField.Date >= model.StartDate && o.localDateField.Date <= model.EndDate).ToList();

            return View(model);
        }

        public ActionResult SuspendedCardsList()
        {
            // var response = _apiService.GetTransactionsByCardBalances();

            //var model = response.Transactions;

            CardsByStatusPageModel model = new CardsByStatusPageModel();
            model.CardStatus = "Suspended";
            return View(model);
        }

        [HttpPost]
        public ActionResult SuspendedCardsList(CardsByStatusPageModel model)
        {

            //var test1 = _apiService.GetTransactionsByCardBalances();

            //var test2 = _apiService.GetGroupedTransactions(new GetGroupedTransactionsRequestModel { ProductCode = "", StartDate = DateTime.Now.AddDays(-30).Date, EndDate = DateTime.Now.Date });

            var response = _apiService.GetTransactionsByCardStatus(model.CardStatus);

            model.CardStatusList = GetCardStatusList();

            model.Transactions = response.Transactions.Where(o => o.localDateField.Date >= model.StartDate && o.localDateField.Date <= model.EndDate).ToList();

            return View(model);
        }

        public ActionResult LostStolenCardsList()
        {
            // var response = _apiService.GetTransactionsByCardBalances();

            //var model = response.Transactions;

            CardsByStatusPageModel model = new CardsByStatusPageModel();
            model.CardStatus = "Lost or Stolen";
            return View(model);
        }

        [HttpPost]
        public ActionResult LostStolenCardsList(CardsByStatusPageModel model)
        {

            //var test1 = _apiService.GetTransactionsByCardBalances();

            //var test2 = _apiService.GetGroupedTransactions(new GetGroupedTransactionsRequestModel { ProductCode = "", StartDate = DateTime.Now.AddDays(-30).Date, EndDate = DateTime.Now.Date });

            var response = _apiService.GetTransactionsByCardStatus(model.CardStatus);

            model.CardStatusList = GetCardStatusList();

            model.Transactions = response.Transactions.Where(o => o.localDateField.Date >= model.StartDate && o.localDateField.Date <= model.EndDate).ToList();

            return View(model);
        }

        public ActionResult GetCardsBalanceList()
        {
            var response = _apiService.GetTransactionsByCardBalances();

            var model = response.Transactions;

            return View(model);
        }

        public ActionResult InactiveCardsList()
        {
            var response = _apiService.GetTransactionsByCardStatus("Inactive");

            var model = response.Transactions;

            return View(model);
        }

        public ActionResult ExpiredCardsList()
        {
            var response = _apiService.GetTransactionsByCardStatus("Expired");

            var model = response.Transactions;

            return View(model);
        }

        public ActionResult CreditProfileTransForReport()
        {
            var response = _apiService.GetCreditProfileTransForReport();

            var model = response.Transactions;

            return View(model);
        }

        public ActionResult CardaActivationFirstTransaction()
        {
            var response = _apiService.CardsActivationFirstTransaction();

            var model = response.Transactions;

            return View(model);
        }

        public ActionResult GroupedTransactions()
        {
            var response = _apiService.GetProducts();

            var products = response.ProductList.ToList();

            var model = new GroupedTransactionsPageModel();

            model.Products = GetProductsDdlForGroupedTransactions(products);

            return View(model);
        }

        [HttpPost]
        public ActionResult GroupedTransactions(GroupedTransactionsPageModel model)
        {
            var request = new GetGroupedTransactionsRequestModel
            {
                ProductCode = model.ProductCode,
                StartDate = model.StartDate,
                EndDate = model.EndDate
            };

            AccountTransactionHistoryResponseModel response = new AccountTransactionHistoryResponseModel();

            if (model.ProductCode == "All Products")
            {
                response = _apiService.GetGroupedTransactions(request);
            }
            else
            {
                response = _apiService.GetGroupedTransactionsByProduct(request);
            }


            model.Transactions = response.Transactions;

            double totalSpent = 0;
            double totalTransaction = 0;

            foreach (var item in model.Transactions)
            {
                totalSpent += Math.Round(item.TotalSpent, 2);
                totalTransaction += item.TotalTransactions;
            }

            model.Products = GetProductsDdlForGroupedTransactions(_apiService.GetProducts().ProductList.ToList());

            return View(model);
        }

        public ActionResult LoadsByProduct()
        {
            var response = _apiService.GetProducts();

            var products = response.ProductList.ToList();

            var model = new LoadsByProductPageModel();

            model.Products = GetProductsDdl(products);

            return View(model);
        }

        [HttpPost]
        public ActionResult LoadsByProduct(LoadsByProductPageModel model)
        {
            var response = _apiService.GetLoadsByProductCode(model.ProductCode);
            model.Products = GetProductsDdl(_apiService.GetProducts().ProductList.ToList());


            model.Transactions = response.Transactions.Where(o => o.localDateField.Date >= model.StartDate && o.localDateField.Date <= model.EndDate).ToList();

            return View(model);
        }

        public ActionResult TransactionsWithCardholders()
        {
            var response = _apiService.GetTransactionsWithCardholder();

            var model = response.Transactions.Where(o => (o.txnTypeField == "19" || o.txnTypeField == "66") && o.actionCodeField == "000").ToList();

            for (int i = 0; i < model.Count; i++)
            {
                //model[i].TransactionTime = DateTime.ParseExact(model[i].ctxLocalTimeField.Value.ToString().PadLeft(6, '0'), "HHmmss", null);
               
                //poshchp
                if (model[i].poschpField == "0") { model[i].poschpField = "present"; }
                if (model[i].poschpField == "1") { model[i].poschpField = "not present, unspecified"; }
                if (model[i].poschpField == "2") { model[i].poschpField = "not present, mail order"; }
                if (model[i].poschpField == "3") { model[i].poschpField = "not present, telephone order"; }
                if (model[i].poschpField == "4") { model[i].poschpField = "not present, standing order"; }
                if (model[i].poschpField == "5") { model[i].poschpField = "not present, electronic order"; }
                if (model[i].poschpField == "6") { model[i].poschpField = "present, standing order"; }

                //poshdim
                if (model[i].poscdimField == "0") { model[i].poscdimField = "unspecified"; }
                if (model[i].poscdimField == "1") { model[i].poscdimField = "manual, no terminal"; }
                if (model[i].poscdimField == "2") { model[i].poscdimField = "mag stripe read"; }
                if (model[i].poscdimField == "3") { model[i].poscdimField = "bar code read"; }
                if (model[i].poscdimField == "4") { model[i].poscdimField = "OCR"; }
                if (model[i].poscdimField == "5") { model[i].poscdimField = "ICC read"; }
                if (model[i].poscdimField == "6") { model[i].poscdimField = "manually keyed"; }
                if (model[i].poscdimField == "7") { model[i].poscdimField = "contactless ICC"; }
                if (model[i].poscdimField == "8") { model[i].poscdimField = "ICC, data not reliable"; }
                if (model[i].poscdimField == "9") { model[i].poscdimField = "Full track read"; }
                if (model[i].poscdimField == "A") { model[i].poscdimField = "As S and chip cryptogram used"; }
                if (model[i].poscdimField == "B") { model[i].poscdimField = "As T and chip cryptogram used"; }
                if (model[i].poscdimField == "C") { model[i].poscdimField = "As V and chip cryptogram used"; }
                if (model[i].poscdimField == "D") { model[i].poscdimField = "As V but merchant is caable of supporting SET"; }
                if (model[i].poscdimField == "E") { model[i].poscdimField = "Contactless mag stripe"; }
                if (model[i].poscdimField == "S") { model[i].poscdimField = "Merchant supports 3D secure"; }
                if (model[i].poscdimField == "T") { model[i].poscdimField = "Full 3D secure transaction"; }
                if (model[i].poscdimField == "U") { model[i].poscdimField = "E-Commerce no security"; }
                if (model[i].poscdimField == "V") { model[i].poscdimField = "-Commerce with channel encryption"; }
                if (model[i].poscdimField == "R") { model[i].poscdimField = "Refund"; }
                //poshcam
                if (model[i].poschamField == "0") { model[i].poschamField = "not authenticated"; }
                if (model[i].poschamField == "1") { model[i].poschamField = "PIN"; }
                if (model[i].poschamField == "2") { model[i].poschamField = "electronic signature analysis"; }
                if (model[i].poschamField == "3") { model[i].poschamField = "biometrics"; }
                if (model[i].poschamField == "4") { model[i].poschamField = "biographic"; }
                if (model[i].poschamField == "5") { model[i].poschamField = "manual signature verification"; }
                if (model[i].poschamField == "6") { model[i].poschamField = "manual (other)"; }
                if (model[i].poschamField == "8") { model[i].poschamField = "unknown"; }
                if (model[i].poschamField == "9") { model[i].poschamField = "e-commerce transaction"; }

            }
            return View(model);
        }

        public ActionResult DeclinedTransactions()
        {
            var response = _apiService.GetTransactionsWithCardholder();

            var model = response.Transactions.Where(o => o.txnTypeField == "19" && o.actionCodeField != "000").ToList();

            for (int i = 0; i < model.Count; i++)
            {
                if (model[i].actionCodeField == "800") { model[i].actionCodeField = "Cash declined on KYC"; }
                if (model[i].actionCodeField == "801") { model[i].actionCodeField = "E-Comm or ATM declined product restriction"; }
                if (model[i].actionCodeField == "802") { model[i].actionCodeField = "Declined Product Is Inactive"; }
                if (model[i].actionCodeField == "803") { model[i].actionCodeField = "Credit Profile limits exceeded"; }
                if (model[i].actionCodeField == "804") { model[i].actionCodeField = "Weekly spend limits exceeded"; }
                if (model[i].actionCodeField == "805") { model[i].actionCodeField = "Daily spend limits exceeded"; }
                if (model[i].actionCodeField == "806") { model[i].actionCodeField = "Spend Profile restriction.Merchant not allowed"; }
                if (model[i].actionCodeField == "899") { model[i].actionCodeField = "Internal Server Error"; }
                if (model[i].actionCodeField == "101") { model[i].actionCodeField = "FIS Decline. Card expired, deny "; }
                if (model[i].actionCodeField == "104") { model[i].actionCodeField = "FIS Decline. Restricted card, deny "; }
                if (model[i].actionCodeField == "105") { model[i].actionCodeField = "FIS Decline. Call acquirer security, deny"; }
                if (model[i].actionCodeField == "106") { model[i].actionCodeField = "FIS Decline. PIN tries exceeded, deny "; }
                if (model[i].actionCodeField == "107") { model[i].actionCodeField = "FIS Decline. Refer to issuer, deny "; }
                if (model[i].actionCodeField == "116") { model[i].actionCodeField = "FIS Decline. Insufficient funds, deny "; }
                if (model[i].actionCodeField == "118") { model[i].actionCodeField = "FIS Decline. No card record, deny "; }
                if (model[i].actionCodeField == "119") { model[i].actionCodeField = "FIS Decline. Transaction not allowed to cardholder, deny"; }
                if (model[i].actionCodeField == "121") { model[i].actionCodeField = "FIS Decline. Amount limits exceeded or outside valid load range, deny "; }
                if (model[i].actionCodeField == "123") { model[i].actionCodeField = "FIS Decline. Frequency limits exceeded, deny "; }
                if (model[i].actionCodeField == "125") { model[i].actionCodeField = "FIS Decline. Card not effective, deny"; }
                if (model[i].actionCodeField == "126") { model[i].actionCodeField = "FIS Decline. Invalid PIN, deny"; }
                if (model[i].actionCodeField == "167") { model[i].actionCodeField = "FIS Decline. No PIN assigned, deny"; }
                if (model[i].actionCodeField == "168") { model[i].actionCodeField = "FIS Decline. PIN already present, deny"; }
                if (model[i].actionCodeField == "169") { model[i].actionCodeField = "FIS Decline. No-PIN card, deny "; }
                if (model[i].actionCodeField == "184") { model[i].actionCodeField = "FIS Decline. PIN confirmation failed, deny "; }
                if (model[i].actionCodeField == "200") { model[i].actionCodeField = "FIS Decline. Card closed, deny & pickup "; }
                if (model[i].actionCodeField == "202") { model[i].actionCodeField = "FIS Decline. Fraudulent use, deny & pickup"; }
                if (model[i].actionCodeField == "204") { model[i].actionCodeField = "FIS Decline. Restricted card, deny & pickup"; }
                if (model[i].actionCodeField == "205") { model[i].actionCodeField = "FIS Decline. Call acquirer security, deny & pickup"; }
                if (model[i].actionCodeField == "206") { model[i].actionCodeField = "FIS Decline. PIN tries exceeded, deny & pickup "; }
                if (model[i].actionCodeField == "207") { model[i].actionCodeField = "FIS Decline. Special conditions, deny & pickup"; }
                if (model[i].actionCodeField == "208") { model[i].actionCodeField = "FIS Decline. Card lost, deny & pickup"; }
                if (model[i].actionCodeField == "209") { model[i].actionCodeField = "FIS Decline. Card stolen, deny & pickup"; }
                if (model[i].actionCodeField == "908") { model[i].actionCodeField = "FIS Decline. System malfunction, deny"; }
                if (model[i].actionCodeField == "909") { model[i].actionCodeField = "FIS Decline. System malfunction, deny"; }

                //poshchp
                if (model[i].poschpField == "0") { model[i].poschpField = "present"; }
                if (model[i].poschpField == "1") { model[i].poschpField = "not present, unspecified"; }
                if (model[i].poschpField == "2") { model[i].poschpField = "not present, mail order"; }
                if (model[i].poschpField == "3") { model[i].poschpField = "not present, telephone order"; }
                if (model[i].poschpField == "4") { model[i].poschpField = "not present, standing order"; }
                if (model[i].poschpField == "5") { model[i].poschpField = "not present, electronic order"; }
                if (model[i].poschpField == "6") { model[i].poschpField = "present, standing order"; }

                //poshdim
                if (model[i].poscdimField == "0") { model[i].poscdimField = "unspecified"; }
                if (model[i].poscdimField == "1") { model[i].poscdimField = "manual, no terminal"; }
                if (model[i].poscdimField == "2") { model[i].poscdimField = "mag stripe read"; }
                if (model[i].poscdimField == "3") { model[i].poscdimField = "bar code read"; }
                if (model[i].poscdimField == "4") { model[i].poscdimField = "OCR"; }
                if (model[i].poscdimField == "5") { model[i].poscdimField = "ICC read"; }
                if (model[i].poscdimField == "6") { model[i].poscdimField = "manually keyed"; }
                if (model[i].poscdimField == "7") { model[i].poscdimField = "contactless ICC"; }
                if (model[i].poscdimField == "8") { model[i].poscdimField = "ICC, data not reliable"; }
                if (model[i].poscdimField == "9") { model[i].poscdimField = "Full track read"; }
                if (model[i].poscdimField == "A") { model[i].poscdimField = "As S and chip cryptogram used"; }
                if (model[i].poscdimField == "B") { model[i].poscdimField = "As T and chip cryptogram used"; }
                if (model[i].poscdimField == "C") { model[i].poscdimField = "As V and chip cryptogram used"; }
                if (model[i].poscdimField == "D") { model[i].poscdimField = "As V but merchant is caable of supporting SET"; }
                if (model[i].poscdimField == "E") { model[i].poscdimField = "Contactless mag stripe"; }
                if (model[i].poscdimField == "H") { model[i].poscdimField = "PAN/account number on file"; }
                if (model[i].poscdimField == "S") { model[i].poscdimField = "Merchant supports 3D secure"; }
                if (model[i].poscdimField == "T") { model[i].poscdimField = "Full 3D secure transaction"; }
                if (model[i].poscdimField == "U") { model[i].poscdimField = "E-Commerce no security"; }
                if (model[i].poscdimField == "V") { model[i].poscdimField = "-Commerce with channel encryption"; }

                //poshcam
                if (model[i].poschamField == "0") { model[i].poschamField = "not authenticated"; }
                if (model[i].poschamField == "1") { model[i].poschamField = "PIN"; }
                if (model[i].poschamField == "2") { model[i].poschamField = "electronic signature analysis"; }
                if (model[i].poschamField == "3") { model[i].poschamField = "biometrics"; }
                if (model[i].poschamField == "4") { model[i].poschamField = "biographic"; }
                if (model[i].poschamField == "5") { model[i].poschamField = "manual signature verification"; }
                if (model[i].poschamField == "6") { model[i].poschamField = "manual (other)"; }
                if (model[i].poschamField == "8") { model[i].poschamField = "unknown"; }
                if (model[i].poschamField == "9") { model[i].poschamField = "e-commerce transaction"; }
            }

            return View(model);
        }

        public ActionResult TransactionsByDate()
        {
            var model = new TransactionsByDatePageModel();

            var response = _apiService.GetProducts();

            var products = response.ProductList.ToList();

            model.Products = GetProductsForTransactionsDdl(products);

            return View(model);
        }

        [HttpPost]
        public ActionResult TransactionsByDate(TransactionsByDatePageModel model)
        {
            AccountTransactionHistoryResponseModel response = new AccountTransactionHistoryResponseModel();

            if (model.ProductCode == "All Products")
            {
                var AllTransactionsrequest = new TransactionsByDateRequestModel
                {
                    StartDate = model.StartDate,
                    EndDate = model.EndDate
                };

                response = _apiService.GetTransactionsByDate(AllTransactionsrequest);
            }
            else
            {
                var AllTransactionsByProductcoderequest = new TransactionsByDateRequestModel
                {
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    ProductCode = model.ProductCode
                };
                response = _apiService.GetTransactionsByDateAndProductCode(AllTransactionsByProductcoderequest);
            }


            model.Products = GetProductsForTransactionsDdl(_apiService.GetProducts().ProductList.ToList());

            var selected = model.Products.Where(x => x.Text == model.ProductCode).First();
            selected.Selected = true;

            model.Transactions = response.Transactions.ToList();
            //var request = new TransactionsByDateRequestModel
            //{
            //    StartDate = model.StartDate,
            //    EndDate = model.EndDate
            //};

            //var response = _apiService.GetTransactionsByDate(request);

            //model.Transactions = response.Transactions.ToList();

            for (int i = 0; i < model.Transactions.Count; i++)
            {
                model.Transactions[i].TransactionTime = model.Transactions[i].CreatedDate.ToString("HH:mm");
                model.Transactions[i].TransactionCreatedDate = model.Transactions[i].CreatedDate.ToShortDateString();
                //poshchp
                if (model.Transactions[i].poschpField == "0") { model.Transactions[i].poschpField = "present"; }
                if (model.Transactions[i].poschpField == "1") { model.Transactions[i].poschpField = "not present, unspecified"; }
                if (model.Transactions[i].poschpField == "2") { model.Transactions[i].poschpField = "not present, mail order"; }
                if (model.Transactions[i].poschpField == "3") { model.Transactions[i].poschpField = "not present, telephone order"; }
                if (model.Transactions[i].poschpField == "4") { model.Transactions[i].poschpField = "not present, standing order"; }
                if (model.Transactions[i].poschpField == "5") { model.Transactions[i].poschpField = "not present, electronic order"; }
                if (model.Transactions[i].poschpField == "6") { model.Transactions[i].poschpField = "present, standing order"; }

                //poshdim
                if (model.Transactions[i].poscdimField == "0") { model.Transactions[i].poscdimField = "unspecified"; }
                if (model.Transactions[i].poscdimField == "1") { model.Transactions[i].poscdimField = "manual, no terminal"; }
                if (model.Transactions[i].poscdimField == "2") { model.Transactions[i].poscdimField = "mag stripe read"; }
                if (model.Transactions[i].poscdimField == "3") { model.Transactions[i].poscdimField = "bar code read"; }
                if (model.Transactions[i].poscdimField == "4") { model.Transactions[i].poscdimField = "OCR"; }
                if (model.Transactions[i].poscdimField == "5") { model.Transactions[i].poscdimField = "ICC read"; }
                if (model.Transactions[i].poscdimField == "6") { model.Transactions[i].poscdimField = "manually keyed"; }
                if (model.Transactions[i].poscdimField == "7") { model.Transactions[i].poscdimField = "contactless ICC"; }
                if (model.Transactions[i].poscdimField == "8") { model.Transactions[i].poscdimField = "ICC, data not reliable"; }
                if (model.Transactions[i].poscdimField == "9") { model.Transactions[i].poscdimField = "Full track read"; }
                if (model.Transactions[i].poscdimField == "A") { model.Transactions[i].poscdimField = "As S and chip cryptogram used"; }
                if (model.Transactions[i].poscdimField == "B") { model.Transactions[i].poscdimField = "As T and chip cryptogram used"; }
                if (model.Transactions[i].poscdimField == "C") { model.Transactions[i].poscdimField = "As V and chip cryptogram used"; }
                if (model.Transactions[i].poscdimField == "D") { model.Transactions[i].poscdimField = "As V but merchant is caable of supporting SET"; }
                if (model.Transactions[i].poscdimField == "E") { model.Transactions[i].poscdimField = "Contactless mag stripe"; }
                if (model.Transactions[i].poscdimField == "H") { model.Transactions[i].poscdimField = "PAN/account number on file"; }
                if (model.Transactions[i].poscdimField == "S") { model.Transactions[i].poscdimField = "Merchant supports 3D secure"; }
                if (model.Transactions[i].poscdimField == "T") { model.Transactions[i].poscdimField = "Full 3D secure transaction"; }
                if (model.Transactions[i].poscdimField == "U") { model.Transactions[i].poscdimField = "E-Commerce no security"; }
                if (model.Transactions[i].poscdimField == "V") { model.Transactions[i].poscdimField = "-Commerce with channel encryption"; }
                //if (model.Transactions[i].poscdimField == "R" && model.Transactions[i].txnCodeField.Value == 20) { model.Transactions[i].poscdimField = "Refund"; }
                //poshcam
                if (model.Transactions[i].poschamField == "0") { model.Transactions[i].poschamField = "not authenticated"; }
                if (model.Transactions[i].poschamField == "1") { model.Transactions[i].poschamField = "PIN"; }
                if (model.Transactions[i].poschamField == "2") { model.Transactions[i].poschamField = "electronic signature analysis"; }
                if (model.Transactions[i].poschamField == "3") { model.Transactions[i].poschamField = "biometrics"; }
                if (model.Transactions[i].poschamField == "4") { model.Transactions[i].poschamField = "biographic"; }
                if (model.Transactions[i].poschamField == "5") { model.Transactions[i].poschamField = "manual signature verification"; }
                if (model.Transactions[i].poschamField == "6") { model.Transactions[i].poschamField = "manual (other)"; }
                if (model.Transactions[i].poschamField == "8") { model.Transactions[i].poschamField = "unknown"; }
                if (model.Transactions[i].poschamField == "9") { model.Transactions[i].poschamField = "e-commerce transaction"; }

                if (model.Transactions[i].txnCodeField.Value == 20)
                {
                    if (model.Transactions[i].poscdimField == "R")
                    {
                        { model.Transactions[i].poscdimField = "Refund"; }
                    }
                    else
                    { model.Transactions[i].poscdimField = "Auth Refund"; }
                };

            }

            return View(model);
        }


        public ActionResult DeclinedTransactionsByDate()
        {
            var response = _apiService.GetProducts();

            var products = response.ProductList.ToList();

            var model = new DeclinedTransactionsByDatePageModel();

            model.Products = GetProductsForTransactionsDdl(products);


            return View(model);
        }

        [HttpPost]
        public ActionResult DeclinedTransactionsByDate(DeclinedTransactionsByDatePageModel model)
        {
            AccountTransactionHistoryResponseModel response = new AccountTransactionHistoryResponseModel();

            if (model.ProductCode == "All Products")
            {
                var AlldeclinedTransactionsrequest = new TransactionsByDateRequestModel
                {
                    StartDate = model.StartDate,
                    EndDate = model.EndDate
                };

                response = _apiService.GetDeclinedTransactionsByDate(AlldeclinedTransactionsrequest);
            }
            else
            {
                var AlldeclinedTransactionsByProductcoderequest = new TransactionsByDateRequestModel
                {
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    ProductCode = model.ProductCode
                };
                response = _apiService.GetDeclinedTransactionsByDateAndProductCode(AlldeclinedTransactionsByProductcoderequest);
            }


            model.Products = GetProductsForTransactionsDdl(_apiService.GetProducts().ProductList.ToList());

            var selected = model.Products.Where(x => x.Text == model.ProductCode).First();
            selected.Selected = true;

            model.Transactions = response.Transactions.ToList();

            for (int i = 0; i < model.Transactions.Count; i++)
            {
                if (model.Transactions[i].actionCodeField == "800") { model.Transactions[i].actionCodeField = "Cash declined on KYC"; }
                if (model.Transactions[i].actionCodeField == "801") { model.Transactions[i].actionCodeField = "E-Comm or ATM declined product restriction"; }
                if (model.Transactions[i].actionCodeField == "802") { model.Transactions[i].actionCodeField = "Declined Product Is Inactive"; }
                if (model.Transactions[i].actionCodeField == "803") { model.Transactions[i].actionCodeField = "Credit Profile limits exceeded"; }
                if (model.Transactions[i].actionCodeField == "804") { model.Transactions[i].actionCodeField = "Weekly spend limits exceeded"; }
                if (model.Transactions[i].actionCodeField == "805") { model.Transactions[i].actionCodeField = "Daily spend limits exceeded"; }
                if (model.Transactions[i].actionCodeField == "806") { model.Transactions[i].actionCodeField = "Spend Profile restriction.Merchant not allowed"; }
                if (model.Transactions[i].actionCodeField == "899") { model.Transactions[i].actionCodeField = "Internal Server Error"; }
                if (model.Transactions[i].actionCodeField == "101") { model.Transactions[i].actionCodeField = "FIS Decline. Card expired, deny "; }
                if (model.Transactions[i].actionCodeField == "104") { model.Transactions[i].actionCodeField = "FIS Decline. Restricted card, deny "; }
                if (model.Transactions[i].actionCodeField == "105") { model.Transactions[i].actionCodeField = "FIS Decline. Call acquirer security, deny"; }
                if (model.Transactions[i].actionCodeField == "106") { model.Transactions[i].actionCodeField = "FIS Decline. PIN tries exceeded, deny "; }
                if (model.Transactions[i].actionCodeField == "107") { model.Transactions[i].actionCodeField = "FIS Decline. Refer to issuer, deny "; }
                if (model.Transactions[i].actionCodeField == "116") { model.Transactions[i].actionCodeField = "FIS Decline. Insufficient funds, deny "; }
                if (model.Transactions[i].actionCodeField == "118") { model.Transactions[i].actionCodeField = "FIS Decline. No card record, deny "; }
                if (model.Transactions[i].actionCodeField == "119") { model.Transactions[i].actionCodeField = "FIS Decline. Transaction not allowed to cardholder, deny"; }
                if (model.Transactions[i].actionCodeField == "121") { model.Transactions[i].actionCodeField = "FIS Decline. Amount limits exceeded or outside valid load range, deny "; }
                if (model.Transactions[i].actionCodeField == "123") { model.Transactions[i].actionCodeField = "FIS Decline. Frequency limits exceeded, deny "; }
                if (model.Transactions[i].actionCodeField == "125") { model.Transactions[i].actionCodeField = "FIS Decline. Card not effective, deny"; }
                if (model.Transactions[i].actionCodeField == "126") { model.Transactions[i].actionCodeField = "FIS Decline. Invalid PIN, deny"; }
                if (model.Transactions[i].actionCodeField == "167") { model.Transactions[i].actionCodeField = "FIS Decline. No PIN assigned, deny"; }
                if (model.Transactions[i].actionCodeField == "168") { model.Transactions[i].actionCodeField = "FIS Decline. PIN already present, deny"; }
                if (model.Transactions[i].actionCodeField == "169") { model.Transactions[i].actionCodeField = "FIS Decline. No-PIN card, deny "; }
                if (model.Transactions[i].actionCodeField == "184") { model.Transactions[i].actionCodeField = "FIS Decline. PIN confirmation failed, deny "; }
                if (model.Transactions[i].actionCodeField == "200") { model.Transactions[i].actionCodeField = "FIS Decline. Card closed, deny & pickup "; }
                if (model.Transactions[i].actionCodeField == "202") { model.Transactions[i].actionCodeField = "FIS Decline. Fraudulent use, deny & pickup"; }
                if (model.Transactions[i].actionCodeField == "204") { model.Transactions[i].actionCodeField = "FIS Decline. Restricted card, deny & pickup"; }
                if (model.Transactions[i].actionCodeField == "205") { model.Transactions[i].actionCodeField = "FIS Decline. Call acquirer security, deny & pickup"; }
                if (model.Transactions[i].actionCodeField == "206") { model.Transactions[i].actionCodeField = "FIS Decline. PIN tries exceeded, deny & pickup "; }
                if (model.Transactions[i].actionCodeField == "207") { model.Transactions[i].actionCodeField = "FIS Decline. Special conditions, deny & pickup"; }
                if (model.Transactions[i].actionCodeField == "208") { model.Transactions[i].actionCodeField = "FIS Decline. Card lost, deny & pickup"; }
                if (model.Transactions[i].actionCodeField == "209") { model.Transactions[i].actionCodeField = "FIS Decline. Card stolen, deny & pickup"; }
                if (model.Transactions[i].actionCodeField == "908") { model.Transactions[i].actionCodeField = "FIS Decline. System malfunction, deny"; }
                if (model.Transactions[i].actionCodeField == "909") { model.Transactions[i].actionCodeField = "FIS Decline. System malfunction, deny"; }

                if (model.Transactions[i].actionCodeField == "807") { model.Transactions[i].actionCodeField = "Monthly Spend Limit exceeded"; }
                if (model.Transactions[i].actionCodeField == "808") { model.Transactions[i].actionCodeField = "Spend Profile override active on card but transaction amount is greater than override"; }
                if (model.Transactions[i].actionCodeField == "809") { model.Transactions[i].actionCodeField = "Recurring payment declined"; }

                if (model.Transactions[i].actionCodeField == "002") { model.Transactions[i].actionCodeField = "Approved for partial amount "; }
                if (model.Transactions[i].actionCodeField == "102") { model.Transactions[i].actionCodeField = "Suspected Fraud"; }
                if (model.Transactions[i].actionCodeField == "108") { model.Transactions[i].actionCodeField = "Refer to issuer"; }

                if (model.Transactions[i].actionCodeField == "109") { model.Transactions[i].actionCodeField = "Invalid merchant "; }
                if (model.Transactions[i].actionCodeField == "110") { model.Transactions[i].actionCodeField = "Invalid amount"; }
                if (model.Transactions[i].actionCodeField == "111") { model.Transactions[i].actionCodeField = "Invalid card number"; }
                if (model.Transactions[i].actionCodeField == "112") { model.Transactions[i].actionCodeField = "PIN data required"; }
                if (model.Transactions[i].actionCodeField == "114") { model.Transactions[i].actionCodeField = "No account of type requested"; }
                if (model.Transactions[i].actionCodeField == "115") { model.Transactions[i].actionCodeField = "Requested function not supported"; }
                if (model.Transactions[i].actionCodeField == "117") { model.Transactions[i].actionCodeField = "Incorrect PIN entered (at ATM)"; }

                if (model.Transactions[i].actionCodeField == "120") { model.Transactions[i].actionCodeField = "Transaction Not Permitted to Terminal "; }
                if (model.Transactions[i].actionCodeField == "122") { model.Transactions[i].actionCodeField = "Exceeds Withdrawal Amount Limit "; }
                if (model.Transactions[i].actionCodeField == "124") { model.Transactions[i].actionCodeField = "Violation of law"; }
                if (model.Transactions[i].actionCodeField == "127") { model.Transactions[i].actionCodeField = "PIN length error "; }
                if (model.Transactions[i].actionCodeField == "128") { model.Transactions[i].actionCodeField = "PIN key synchronization error "; }
                if (model.Transactions[i].actionCodeField == "129") { model.Transactions[i].actionCodeField = "Suspected counterfeit card – card expiry date mismatch"; }

                if (model.Transactions[i].actionCodeField == "170") { model.Transactions[i].actionCodeField = "Cardholder contact Issuer "; }
                if (model.Transactions[i].actionCodeField == "171") { model.Transactions[i].actionCodeField = "PIN not changed "; }
                if (model.Transactions[i].actionCodeField == "173") { model.Transactions[i].actionCodeField = "Card Not Present (CNP) soft decline. Cardholder SCA (strong customer authentication) required via 3DS check"; }
                if (model.Transactions[i].actionCodeField == "174") { model.Transactions[i].actionCodeField = "Card Present (CP) soft decline. Cardholder to enter PIN"; }
                if (model.Transactions[i].actionCodeField == "177") { model.Transactions[i].actionCodeField = "Invalid currency code"; }
                if (model.Transactions[i].actionCodeField == "178") { model.Transactions[i].actionCodeField = "ICC decline, after card-initiated referral "; }
                if (model.Transactions[i].actionCodeField == "179") { model.Transactions[i].actionCodeField = "PIN change declined (new PIN unsafe) "; }
                if (model.Transactions[i].actionCodeField == "183") { model.Transactions[i].actionCodeField = "SNo chequing account "; }

                if (model.Transactions[i].actionCodeField == "185") { model.Transactions[i].actionCodeField = "CVV VALIDATION ERROR, Invalid CVC"; }
                if (model.Transactions[i].actionCodeField == "188") { model.Transactions[i].actionCodeField = "Restricted/No Funds"; }
                if (model.Transactions[i].actionCodeField == "189") { model.Transactions[i].actionCodeField = "Forex not available "; }
                if (model.Transactions[i].actionCodeField == "190") { model.Transactions[i].actionCodeField = "Transaction Count Limit Exceeded "; }
                if (model.Transactions[i].actionCodeField == "197") { model.Transactions[i].actionCodeField = "ICC card authentication failed "; }

                if (model.Transactions[i].actionCodeField == "201") { model.Transactions[i].actionCodeField = "Expired card"; }
                if (model.Transactions[i].actionCodeField == "285") { model.Transactions[i].actionCodeField = "CVV Validation Error"; }
                if (model.Transactions[i].actionCodeField == "902") { model.Transactions[i].actionCodeField = "Invalid transaction"; }
                if (model.Transactions[i].actionCodeField == "905") { model.Transactions[i].actionCodeField = "Acquirer Not Supported by Switch"; }

                if (model.Transactions[i].actionCodeField == "907") { model.Transactions[i].actionCodeField = "Card Issuer or Switch Inoperative "; }
                if (model.Transactions[i].actionCodeField == "912") { model.Transactions[i].actionCodeField = "Issuer unavailable: failed to connect/get a response from issuer"; }
                if (model.Transactions[i].actionCodeField == "919") { model.Transactions[i].actionCodeField = "Encryption Key Sync. Error"; }



                //poshchp
                if (model.Transactions[i].poschpField == "0") { model.Transactions[i].poschpField = "present"; }
                if (model.Transactions[i].poschpField == "1") { model.Transactions[i].poschpField = "not present, unspecified"; }
                if (model.Transactions[i].poschpField == "2") { model.Transactions[i].poschpField = "not present, mail order"; }
                if (model.Transactions[i].poschpField == "3") { model.Transactions[i].poschpField = "not present, telephone order"; }
                if (model.Transactions[i].poschpField == "4") { model.Transactions[i].poschpField = "not present, standing order"; }
                if (model.Transactions[i].poschpField == "5") { model.Transactions[i].poschpField = "not present, electronic order"; }
                if (model.Transactions[i].poschpField == "6") { model.Transactions[i].poschpField = "present, standing order"; }

                //poshdim
                if (model.Transactions[i].poscdimField == "0") { model.Transactions[i].poscdimField = "unspecified"; }
                if (model.Transactions[i].poscdimField == "1") { model.Transactions[i].poscdimField = "manual, no terminal"; }
                if (model.Transactions[i].poscdimField == "2") { model.Transactions[i].poscdimField = "mag stripe read"; }
                if (model.Transactions[i].poscdimField == "3") { model.Transactions[i].poscdimField = "bar code read"; }
                if (model.Transactions[i].poscdimField == "4") { model.Transactions[i].poscdimField = "OCR"; }
                if (model.Transactions[i].poscdimField == "5") { model.Transactions[i].poscdimField = "ICC read"; }
                if (model.Transactions[i].poscdimField == "6") { model.Transactions[i].poscdimField = "manually keyed"; }
                if (model.Transactions[i].poscdimField == "7") { model.Transactions[i].poscdimField = "contactless ICC"; }
                if (model.Transactions[i].poscdimField == "8") { model.Transactions[i].poscdimField = "ICC, data not reliable"; }
                if (model.Transactions[i].poscdimField == "9") { model.Transactions[i].poscdimField = "Full track read"; }
                if (model.Transactions[i].poscdimField == "A") { model.Transactions[i].poscdimField = "As S and chip cryptogram used"; }
                if (model.Transactions[i].poscdimField == "B") { model.Transactions[i].poscdimField = "As T and chip cryptogram used"; }
                if (model.Transactions[i].poscdimField == "C") { model.Transactions[i].poscdimField = "As V and chip cryptogram used"; }
                if (model.Transactions[i].poscdimField == "D") { model.Transactions[i].poscdimField = "As V but merchant is caable of supporting SET"; }
                if (model.Transactions[i].poscdimField == "E") { model.Transactions[i].poscdimField = "Contactless mag stripe"; }
                if (model.Transactions[i].poscdimField == "S") { model.Transactions[i].poscdimField = "Merchant supports 3D secure"; }
                if (model.Transactions[i].poscdimField == "T") { model.Transactions[i].poscdimField = "Full 3D secure transaction"; }
                if (model.Transactions[i].poscdimField == "U") { model.Transactions[i].poscdimField = "E-Commerce no security"; }
                if (model.Transactions[i].poscdimField == "V") { model.Transactions[i].poscdimField = "-Commerce with channel encryption"; }
                if (model.Transactions[i].poscdimField == "H") { model.Transactions[i].poscdimField = "PAN/account number on file"; }

                //poshcam
                if (model.Transactions[i].poschamField == "0") { model.Transactions[i].poschamField = "not authenticated"; }
                if (model.Transactions[i].poschamField == "1") { model.Transactions[i].poschamField = "PIN"; }
                if (model.Transactions[i].poschamField == "2") { model.Transactions[i].poschamField = "electronic signature analysis"; }
                if (model.Transactions[i].poschamField == "3") { model.Transactions[i].poschamField = "biometrics"; }
                if (model.Transactions[i].poschamField == "4") { model.Transactions[i].poschamField = "biographic"; }
                if (model.Transactions[i].poschamField == "5") { model.Transactions[i].poschamField = "manual signature verification"; }
                if (model.Transactions[i].poschamField == "6") { model.Transactions[i].poschamField = "manual (other)"; }
                if (model.Transactions[i].poschamField == "8") { model.Transactions[i].poschamField = "unknown"; }
                if (model.Transactions[i].poschamField == "9") { model.Transactions[i].poschamField = "e-commerce transaction"; }
            }

            return View(model);
        }

        public ActionResult CreditProfileTransactions()
        {
            var creditProfilesResponse = _apiService.GetCreditProfilesList();

            var profiles = GetCreditProfileDdl(creditProfilesResponse.CreditProfileList);

            var model = new CreditProfileTransactionsPageModel();

            model.CreditProfiles = profiles;

            return View(model);
        }

        [HttpPost]
        public ActionResult CreditProfileTransactions(CreditProfileTransactionsPageModel model)
        {
            if (model.CreditProfileId == "999")
            {
                return RedirectToAction("CreditProfileTransactions");
            }

            var creditProfile = _apiService.GetCreditProfileById(Convert.ToInt16(model.CreditProfileId));

            var response = _apiService.GetCreditProfileTransactionsById(Convert.ToInt16(model.CreditProfileId));

            //for (int i = 0; i < response.Transactions.Count; i++)
            //{
            //    if (response.Transactions[i].description == "Authorisation Advice")
            //    {
            //        response.Transactions.Remove(response.Transactions[i]);
            //    }
            //}

            var creditProfilesResponse = _apiService.GetCreditProfilesList();

            var profiles = GetCreditProfileSelectedDdl(creditProfilesResponse.CreditProfileList, model.CreditProfileId);

            model.Transactions = response.Transactions.Where(o => o.CreatedDate.Date >= model.StartDate && o.CreatedDate.Date <= model.EndDate).ToList();

            model.CreditProfiles = profiles;

            model.CreditProfileName = creditProfile.CreditProfile.ProfileName;

            return View(model);
        }

        public ActionResult Reconciliation()
        {
            var model = new SuperReportPageModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult Reconciliation(SuperReportPageModel model)
        {
            var startDate = model.StartDate;
            var endDate = model.EndDate;

            var request = new SuperReportRequestModel
            {
                StartDate = startDate,
                EndDate = endDate
            };

            var response = _apiService.SuperReportTransactions(request);

            model.Transactions = response.Transactions.ToList();

            for (int i = 0; i < model.Transactions.Count; i++)
            {      
                //poshdim
                if (model.Transactions[i].poscdimField == "0") { model.Transactions[i].poscdimField = "unspecified"; }
                if (model.Transactions[i].poscdimField == "1") { model.Transactions[i].poscdimField = "manual, no terminal"; }
                if (model.Transactions[i].poscdimField == "2") { model.Transactions[i].poscdimField = "mag stripe read"; }
                if (model.Transactions[i].poscdimField == "3") { model.Transactions[i].poscdimField = "bar code read"; }
                if (model.Transactions[i].poscdimField == "4") { model.Transactions[i].poscdimField = "OCR"; }
                if (model.Transactions[i].poscdimField == "5") { model.Transactions[i].poscdimField = "ICC read"; }
                if (model.Transactions[i].poscdimField == "6") { model.Transactions[i].poscdimField = "manually keyed"; }
                if (model.Transactions[i].poscdimField == "7") { model.Transactions[i].poscdimField = "contactless ICC"; }
                if (model.Transactions[i].poscdimField == "8") { model.Transactions[i].poscdimField = "ICC, data not reliable"; }
                if (model.Transactions[i].poscdimField == "9") { model.Transactions[i].poscdimField = "Full track read"; }
                if (model.Transactions[i].poscdimField == "A") { model.Transactions[i].poscdimField = "As S and chip cryptogram used"; }
                if (model.Transactions[i].poscdimField == "B") { model.Transactions[i].poscdimField = "As T and chip cryptogram used"; }
                if (model.Transactions[i].poscdimField == "C") { model.Transactions[i].poscdimField = "As V and chip cryptogram used"; }
                if (model.Transactions[i].poscdimField == "D") { model.Transactions[i].poscdimField = "As V but merchant is caable of supporting SET"; }
                if (model.Transactions[i].poscdimField == "E") { model.Transactions[i].poscdimField = "Contactless mag stripe"; }
                if (model.Transactions[i].poscdimField == "S") { model.Transactions[i].poscdimField = "Merchant supports 3D secure"; }
                if (model.Transactions[i].poscdimField == "T") { model.Transactions[i].poscdimField = "Full 3D secure transaction"; }
                if (model.Transactions[i].poscdimField == "U") { model.Transactions[i].poscdimField = "E-Commerce no security"; }
                if (model.Transactions[i].poscdimField == "V") { model.Transactions[i].poscdimField = "-Commerce with channel encryption"; }
                if (model.Transactions[i].poscdimField == "R") { model.Transactions[i].poscdimField = "Refund"; }

            }


            return View(model);

        }

        public ActionResult MasterAccountTransactions()
        {
            var response = _apiService.GetAccountsList();

            var accountsList = response.AccountsList.ToList();

            var model = new MasterAccountTransactionsPageModel();

            model.Accounts = GetMasterAccountsDdl(accountsList);

            return View(model);
        }

        [HttpPost]
        public ActionResult MasterAccountTransactions(MasterAccountTransactionsPageModel model)
        {
            var response = _apiService.GetCorporaeAccountCardTransactionHistory(model.CardProxyId, DateTime.Now, 24);

            model.Transactions = response.Transactions.Where(o => o.AuthDate.Date >= model.StartDate && o.AuthDate.Date <= model.EndDate).ToList();

            model.Accounts = GetMasterAccountsDdl(_apiService.GetAccountsList().AccountsList.ToList());

            var selected = model.Accounts.Where(x => x.Value == model.CardProxyId).First();
            selected.Selected = true;
            //var model = response.Transactions;

            //ViewBag.AccountId = accountId;
            ViewBag.AccountName = model.AccountName;
            return View(model);
        }

        public ActionResult Cardholders()
        {
            var model = new CardholderBySpendProfilePageModel();

            var response = _apiService.GetSpendProfileList();

            var profiles = response.SpendProfileList.ToList();

            model.SpendProfiles = GetSpendProfilesDdl(profiles);

            return View(model);
        }

        [HttpPost]
        public ActionResult Cardholders(CardholderBySpendProfilePageModel model)
        {
            AccountTransactionHistoryResponseModel response = new AccountTransactionHistoryResponseModel();

            if (model.SpendProfileName == "All Profiles")
            {
                var AllTransactionsrequest = new CardholdersBySpendProfilesRequestModel
                {
                    StartDate = DateTime.Now.AddYears(-5),
                    EndDate = DateTime.Now
                };

                response = _apiService.GetCardholdersBySpendProfileDate(AllTransactionsrequest);
            }
            else
            {
                var AllTransactionsByProductcoderequest = new CardholdersBySpendProfilesRequestModel
                {
                    StartDate = DateTime.Now.AddYears(-5),
                    EndDate = DateTime.Now,
                    SpendProfileId = model.SpendProfileId
                };
                response = _apiService.GetCardholdersBySpendProfileDateAndProfile(AllTransactionsByProductcoderequest);
            }


            model.SpendProfiles = GetSpendProfilesDdl(_apiService.GetSpendProfileList().SpendProfileList.ToList());

            var selected = model.SpendProfiles.Where(x => x.Value == model.SpendProfileId).First();
            selected.Selected = true;

            model.Transactions = response.Transactions.ToList();
            //var request = new TransactionsByDateRequestModel
            //{
            //    StartDate = model.StartDate,
            //    EndDate = model.EndDate
            //};

            //var response = _apiService.GetTransactionsByDate(request);

            //model.Transactions = response.Transactions.ToList();



            return View(model);
        }


        public ActionResult CreditProfileTransForFeeReport()
        {
            var response = _apiService.GetCreditProfileTransForReport();

            for (int i = 0; i < response.Transactions.Count; i++)
            {
                //poshdim
                if (response.Transactions[i].AdjustmentType == 5) { response.Transactions[i].Type = "Order Fee"; }
                if (response.Transactions[i].AdjustmentType == 6) { response.Transactions[i].Type = "Subscription Fee"; }
                if (response.Transactions[i].AdjustmentType == 7) { response.Transactions[i].Type = "FX Fee"; }
                if (response.Transactions[i].AdjustmentType == 8) { response.Transactions[i].Type = "Finance Fee"; }

            }
            return View(response);
        }

        public ActionResult StatementsForFeeReport()
        {
            var response = _apiService.GetStatementsForReport();

            //for (int i = 0; i < response.Transactions.Count; i++)
            //{
            //    //poshdim
            //    if (response.Transactions[i].AdjustmentType == 5) { response.Transactions[i].Type = "Order Fee"; }
            //    if (response.Transactions[i].AdjustmentType == 6) { response.Transactions[i].Type = "Subscription Fee"; }
            //    if (response.Transactions[i].AdjustmentType == 7) { response.Transactions[i].Type = "FX Fee"; }
            //    if (response.Transactions[i].AdjustmentType == 8) { response.Transactions[i].Type = "Finance Fee"; }

            //}

            return View(response);
        }


        private static List<SelectListItem> GetProductsDdl(List<Models.Product.QcsProduct> productList)
        {
            List<SelectListItem> products = (from p in productList.AsEnumerable()
                                             select new SelectListItem
                                             {
                                                 Text = p.ProductCode,
                                                 Value = p.ProductCode
                                             }).ToList();



            products.Insert(0, new SelectListItem { Text = "--Select Product--", Value = "", Disabled = true, Selected = true });
            return products;
        }

        private static List<SelectListItem> GetProductsForTransactionsDdl(List<Models.Product.QcsProduct> productList)
        {
            List<SelectListItem> products = (from p in productList.AsEnumerable().Where(p => p.FundsOwnership.Value.ToString() == "Company")
                                             select new SelectListItem
                                             {
                                                 Text = p.ProductCode,
                                                 Value = p.ProductCode
                                             }).ToList();



            products.Insert(0, new SelectListItem { Text = "--Select Product--", Value = "", Disabled = true, Selected = true });
            products.Insert(1, new SelectListItem { Text = "All Products", Value = "AllProducts" });
            return products;
        }

        private static List<SelectListItem> GetProductsDdlForGroupedTransactions(List<Models.Product.QcsProduct> productList)
        {
            List<SelectListItem> products = (from p in productList.AsEnumerable()
                                             select new SelectListItem
                                             {
                                                 Text = p.ProductCode,
                                                 Value = p.ProductCode
                                             }).ToList();


            products.Insert(0, new SelectListItem { Text = "All Products", Value = "AllProducts", Disabled = false, Selected = false });
            products.Insert(0, new SelectListItem { Text = "--Select Product--", Value = "", Disabled = true, Selected = true });
            return products;
        }

        private static List<SelectListItem> GetCardStatusList()
        {
            List<SelectListItem> cardStatusList = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "Activated",
                                                 Value = "Activated"
                                             },
                new SelectListItem
                                             {
                                                Text = "Lost or Stolen",
                                                 Value = "Lost"
                                             },
                new SelectListItem
                                             {
                                                Text = "Blocked",
                                                 Value = "Block"
                                             },
                new SelectListItem
                                             {
                                                Text = "Suspended",
                                                 Value = "Suspended"
                                             }
            }.ToList();

            cardStatusList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            return cardStatusList;
        }

        private static List<SelectListItem> GetCreditProfileDdl(List<Models.Product.CreditProfileModel> creditProfilesList)
        {
            //List<SelectListItem> profiles = (from p in creditProfilesList.AsEnumerable()
            //                                 select new SelectListItem
            //                                 {
            //                                     Text = p.ProfileName,
            //                                     Value = p.CreditProfileId.ToString()
            //                                 }).ToList();



            //profiles.Insert(0, new SelectListItem { Text = "--Select Credit Profile--", Value = "", Disabled = true, Selected = true });

            //return profiles;

            List<SelectListItem> profiles = new List<SelectListItem>();


            foreach (var item in creditProfilesList)
            {
                profiles.Add(new SelectListItem
                {
                    Text = item.ProfileName,
                    Value = item.CreditProfileId.ToString()
                });
            }
            profiles.Insert(0, new SelectListItem { Text = "--Select--", Value = "999", Disabled = false, Selected = true });

            return profiles;
        }

        private static List<SelectListItem> GetCreditProfileSelectedDdl(List<Models.Product.CreditProfileModel> creditProfilesList, string selectedProfileId)
        {
            //List<SelectListItem> profiles = (from p in creditProfilesList.AsEnumerable()
            //                                 select new SelectListItem
            //                                 {
            //                                     Text = p.ProfileName,
            //                                     Value = p.CreditProfileId.ToString()
            //                                 }).ToList();



            //profiles.Insert(0, new SelectListItem { Text = "--Select Credit Profile--", Value = "", Disabled = true, Selected = true });

            //return profiles;

            List<SelectListItem> profiles = new List<SelectListItem>();


            foreach (var item in creditProfilesList)
            {
                profiles.Add(new SelectListItem
                {
                    Text = item.ProfileName,
                    Value = item.CreditProfileId.ToString()
                });
            }
            profiles.Insert(0, new SelectListItem { Text = "--Select--", Value = "999", Disabled = false, Selected = false });


            var selected = profiles.Where(x => x.Value == selectedProfileId).First();
            selected.Selected = true;

            return profiles;
        }

        private static List<SelectListItem> GetSpendProfilesDdl(List<Models.Product.SpendProfileModel> profileList)
        {
            List<SelectListItem> products = (from p in profileList.AsEnumerable()
                                             select new SelectListItem
                                             {
                                                 Text = p.ProfileName,
                                                 Value = p.SpendProfileId.ToString()
                                             }).ToList();

            products.Insert(0, new SelectListItem { Text = "--Select Profile--", Value = "", Disabled = true, Selected = true });
            products.Insert(1, new SelectListItem { Text = "All Profiles", Value = "AllProfiles" });
            return products;
        }

        private static List<SelectListItem> GetMasterAccountsDdl(List<CorporateAccount> accountsList)
        {
            List<SelectListItem> accounts = new List<SelectListItem>();


            foreach (var a in accountsList)
            {
                accounts.Add(new SelectListItem
                {
                    Text = a.AccountName,
                    Value = a.CardId
                });
            }
            accounts.Insert(0, new SelectListItem { Text = "--Select--", Value = "999", Disabled = false, Selected = true });

            return accounts;
        }


    }
}