﻿using CorporatePortal.Api;
using CorporatePortal.Api.Requests;
using CorporatePortal.Api.Responses;
using CorporatePortal.Models;
using CorporatePortal.Models.Account;
using CorporatePortal.Models.ChangeCardStatus;
using CorporatePortal.Models.Product;
using CorporatePortal.Models.User;
using Qcs.EmailProvider;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager,CorporateEmployee,CorporateClient")]
    public class UserController : Controller
    {
        private readonly IApiService _apiService;
        private readonly IEmailProvider _emailService;

        public UserController(IApiService apiService, IEmailProvider emailService)
        {
            _apiService = apiService;
            _emailService = emailService;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateClient")]
        public ActionResult CorporateStaff()
        {         
            var response = _apiService.GetUsersWithRoles();

            var usersList = new List<CorporateStaffListPageModel>();

            if (response.IsSuccess)
            {
                if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
                {
                    foreach (var user in response.Users)
                    {
                        usersList.Add(new CorporateStaffListPageModel
                        {
                            UserId = user.UserId,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Email = user.Email,
                            IsActive = user.IsActive,
                            UserName = user.UserName,
                            EmailConfirmed = user.EmailConfirmed,
                            CreatedDate = user.CreatedDate,
                            Roles = user.Roles
                        });
                    }
                }
                else
                {
                    foreach (var user in response.Users.Where(u=>u.Roles.Contains("CorporateAdmin") || u.Roles.Contains("CorporateManager") || u.Roles.Contains("CorporateEmployee")))
                    {
                        usersList.Add(new CorporateStaffListPageModel
                        {
                            UserId = user.UserId,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            Email = user.Email,
                            IsActive = user.IsActive,
                            UserName = user.UserName,
                            EmailConfirmed = user.EmailConfirmed,
                            CreatedDate = user.CreatedDate,
                            Roles = user.Roles
                        });
                    }
                }               
            }

            for (int i = 0; i < usersList.Count; i++)
            {
                if (usersList[i].Roles.Contains("SuperAdmin"))
                {
                    usersList[i].Roles.Remove("CorporateAdmin");
                }
            }

            var uList = usersList.OrderByDescending(c => c.CreatedDate).ToList();

            return View(uList);
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateClient")]
        public ActionResult NewCorporateStaff()
        {
            var model = new CorporateStaff();
            model.Roles = GetUserRolesDdl();
            return View(model);
        }

        private static List<SelectListItem> GetUserRolesDdl()
        {
            List<SelectListItem> Roles = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Text = UserRole.CorporateAdmin.ToString(),
                    Value = ((int) UserRole.CorporateAdmin).ToString()
                },
               new SelectListItem
                {
                    Text = UserRole.CorporateEmployee.ToString(),
                    Value = ((int) UserRole.CorporateEmployee).ToString()
                },
               new SelectListItem
                {
                    Text = UserRole.CorporateManager.ToString(),
                    Value = ((int) UserRole.CorporateManager).ToString()
                },
                new SelectListItem
                {
                    Text = UserRole.SuperAdmin.ToString(),
                    Value = ((int) UserRole.SuperAdmin).ToString()
                }
            };            

            Roles.Insert(0, new SelectListItem { Text = "--Select Role--", Value = "", Disabled = false, Selected = true });
            return Roles.ToList();
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateClient")]
        [HttpPost]
        public ActionResult NewCorporateStaff(CorporateStaff user)
        {
            if (!ModelState.IsValid)
            {
                user.Roles = GetUserRolesDdl();
                return View(user);
            }

            var req = new NewCorporateStaffRequest
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Password = user.Password,
                EmailAddress = user.EmailAddress,
                Role = user.Role,
                IsActive = false
            };

            var response = _apiService.NewUser(req);

            if (!response.IsSuccess)
            {
                if (response.ErrorCodes[0] == "30000016")
                {
                    ModelState.AddModelError("", "Username is already taken. Please try a different username");
                    TempData["FailureMessage"] = "Create cardholder failed.Username already in use!";
                }
                if (response.ErrorCodes[0] == "30000017")
                {
                    ModelState.AddModelError("", "Email is already taken. Please try a different email");
                    TempData["FailureMessage"] = "Create cardholder failed.Email already in use!";
                }
                if (response.ErrorCodes[0] == "30000025")
                {
                    ModelState.AddModelError("", "Create cardholder failed!");
                    TempData["FailureMessage"] = "Create cardholder failed!";
                }

                user.Roles = GetUserRolesDdl();
                return View(user);
            }

          
            SendActivationEmail(response);

            TempData["SuccessMessage"] = "New user created successfully!";
            return RedirectToAction("CorporateStaff", "User");            
           
        }

        private void SendActivationEmail(NewCorporateUserResponse response)
        {
            string emailProduct = ConfigurationManager.AppSettings["EmailProduct"];
            string accountConfirmationEmailSubject = string.Format(ConfigurationManager.AppSettings["AccountConfirmationEmailSubject"],emailProduct);
            

            string emailBody = string.Format("{0}{1}<br/> Username : {2} <br/>Password : {3} <br/><br/> Please <a href = {4}> {5} </a> {6}",
                               "Hello, <br/> A user account has been created for you in the QPAY Portal for ",
                               emailProduct + ".<br/>",
                               response.User.UserName,
                               response.User.Password,
                               Url.Action("ConfirmEmail", "Account", new { Token = response.User.Id, Email = response.User.EmailAddress }, Request.Url.Scheme),
                               "click here to Confirm",
                               "your details and login with the new credentials. <br/><br/>If you want to change your password, click ‘Forgotten Password’ on the login screen and follow to instructions to set up a new password. <br/><br/> Thanks,<br/> QPay");

            _emailService.SendEmail(new EmailMessage
            {
                From = new EmailAddress
                {
                    Address = "contactus@qcs-uk.com"
                },
                To = new List<EmailAddress>(){
                    new EmailAddress
                    {
                        Address = response.User.EmailAddress
                    }
                },
                Body = emailBody,
                EmailType = EmailType.Html,
                Subject = accountConfirmationEmailSubject
            });
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateClient")]
        public ActionResult EditCorporateStaff(string userId)
        {
            var user = _apiService.GetUsersWithRoles().Users.Where(u => u.UserId == userId).FirstOrDefault();

            if (user.Roles.Contains("SuperAdmin"))
            {
                user.Roles.Remove("CorporateAdmin");
            }

            var model = new CorporateStaff
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                EmailAddress = user.Email,
                Role = (Models.Account.UserRole)Enum.Parse(typeof(Models.Account.UserRole), user.Roles.FirstOrDefault()),
                IsActive = user.IsActive,
                UserId = user.UserId,
                EmailConfirmed = user.EmailConfirmed,
                Roles = GetUserRolesDdl()
            };

            return View(model);
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateClient")]
        [HttpPost]
        public ActionResult EditCorporateStaff(CorporateStaff user)
        {
            var userToUpdate = new CardHolderDetails
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                EmailAddress = user.EmailAddress,
                IsActive = user.IsActive,
                EmailConfirmed = user.EmailConfirmed
            };

            var response = _apiService.UpdateStaffDetails(user.UserId, userToUpdate);

            if (!response.IsSuccess)
            {
                return Content("oops");
            }

            TempData["SuccessMessage"] = "Staff details updated successfully!";
            return RedirectToAction("CorporateStaff");
        }

        public ActionResult CorporateCardholders()
        {
            var response = _apiService.GetAllCardHoldersWithSpendProfiles();
            //var products = _apiService.GetProducts();
            //ViewBag.products = GetProductsDdl(products.ProductList);
            var cardHolders = new List<CardHoldersListPageModel>();
            foreach (var cardHolder in response.Users)
            {
                cardHolders.Add(new CardHoldersListPageModel
                {
                    UserId = cardHolder.UserId,
                    FirstName = cardHolder.FirstName,
                    LastName = cardHolder.LastName,
                    UserName = cardHolder.UserName,
                    Email = cardHolder.Email,
                    IsActive = cardHolder.IsActive,
                    HasActiveCard = cardHolder.HasActiveCard,
                    EmailConfirmed = cardHolder.EmailConfirmed,
                    PassedKyc = cardHolder.PassedKyc,
                    PassedPeps=cardHolder.PassedPeps,
                    PassedSanctions = cardHolder.PassedSanctions,
                    SpendProfile = cardHolder.SpendProfile,
                    CardProxyId = cardHolder.CardProxyId,
                    CardStatus = cardHolder.CardStatus,
                    ProductCode = cardHolder.ProductCode,
                    CreditProfile = cardHolder.CreditProfile,
                    CreatedDate = cardHolder.CreatedDate.Date,
                    AllowATM = cardHolder.AllowATM
                    
                });
            }

            if (!response.IsSuccess)
            {

            }

            var uList = cardHolders.OrderByDescending(c => c.CreatedDate).ToList();

            return View(uList);
        }

        public ActionResult CreateCardholder()
        {
            var model = new CreateNewCardholderPageModel();
            model.Countries = GetCountrySelectListItems();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateCardholder(CreateNewCardholderPageModel model)
        {
            //var productsList = _apiService.GetProducts().ProductList.Where(p => p.IsVirtual == true).ToList();
            model.Countries = GetCountrySelectListItems();
            //model.Products = GetProductsDdl(productsList);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (ModelState.IsValid)
            {
                var request = new CreateCardholderRequest();
                request.FirstName = model.FirstName;
                request.LastName = model.LastName;
                request.Mobile = model.Mobile;
                request.Email = model.EmailAddress;
                request.Address1 = model.Address1;
                request.Address2 = model.Address2;
                request.City = model.City;
                request.PostCode = model.PostCode;
                request.Country = model.Country;
                request.DateOfBirth = model.DateOfBirth;
                request.UserName = model.UserName;
                request.Password = model.Password;
                request.HouseNumberOrBuilding = model.HouseNumberOrBuilding;
                request.State = model.State;



                //var prodCurrency = productsList.Where(p => p.ProductCode == model.ProductCode).FirstOrDefault().Currency;
               // var country = _apiService.GetCountryInfo(model.Country);
               // var allowed = country.CardLoadInfos.Any(c => c.Currency == prodCurrency);

                //if (!allowed)
                //{
                //    ModelState.AddModelError("", "Product not available for cardholder country!");
                //    TempData["FailureMessage"] = "Product not available for cardholder country!";
                //    return View(model);
                //}

                var response = _apiService.CreateCardholder(request);

                if (!response.IsSuccess)
                {
                    if (response.ErrorCodes[0] == "30000016")
                    {
                        ModelState.AddModelError("", "Username is already taken. Please try a different username");
                        TempData["FailureMessage"] = "Create cardholder failed.Username already in use!";
                    }
                    if (response.ErrorCodes[0] == "30000017")
                    {
                        ModelState.AddModelError("", "Email is already taken. Please try a different email");
                        TempData["FailureMessage"] = "Create cardholder failed.Email already in use!";
                    }
                    if (response.ErrorCodes[0] == "30000025")
                    {
                        ModelState.AddModelError("", "Create cardholder failed!");
                        TempData["FailureMessage"] = "Create cardholder failed!";
                    }

                    return View(model);
                }

                //var orderResponse = _apiService.OrderVirtualCard(new OrderVirtualCardRequest
                //{
                //    CardHolderId = response.CardHolderId,
                //    ProductCode = model.ProductCode
                //});

                //if (!orderResponse.IsSuccess)
                //{
                //    if (orderResponse.ErrorCodes[0] == "18000002")
                //    {
                //        ModelState.AddModelError("", "Cardholder created. Card could not be ordered.");
                //        TempData["FailureMessage"] = "Cardholder created. Card could not be ordered!";
                //    }

                //    return View(model);
                //}

                //TempData["Success"] = String.Format("CardId : {0} : ordered successfully for {1} {2}", orderResponse.CardProxyId, request.FirstName, request.LastName);
                //return RedirectToAction("VirtualCardOrderHistory");
            }

            TempData["SuccessMessage"] = "New user created successfully!";
            return RedirectToAction("CorporateCardholders","User");
          
        }

        public List<SelectListItem> GetCountrySelectListItems()
        {
            var countries = GetCountries().Select(s => new SelectListItem
            {
                Value = s.Alpha3,
                Text = s.Name
            }).ToList();

            countries.Insert(0, new SelectListItem { Text = "--Select Country--", Value = "", Disabled = true, Selected = true });
            return countries;

        }

        private static List<SelectListItem> GetProductsDdl(List<Models.Product.QcsProduct> productList)
        {
            List<SelectListItem> products = (from p in productList.AsEnumerable()
                                             select new SelectListItem
                                             {
                                                 Text = p.ProductCode,
                                                 Value = p.ProductCode
                                             }).ToList();



            products.Insert(0, new SelectListItem { Text = "--Select Product--", Value = "", Disabled = true, Selected = true });
            return products;
        }

        public ActionResult CardholderDetails(string UserId)
        {
            var getCardholderResponse = _apiService.GetCardHolderDetails(UserId);

            var getCardholderCardsResponse = _apiService.GetPrepaidCards(UserId);

            if(getCardholderCardsResponse.Cards.Count < 1)
            {
                return View(new CardHolderDetailsPageModel
                {
                    CardHolder = getCardholderResponse.User,
                    Cards = getCardholderCardsResponse.Cards,
                    SpendProfile = null,
                    PurchaseCategories = new List<SelectListItem>()
                });
            }
            foreach (var item in getCardholderCardsResponse.Cards)
            {            
                item.FundsOwnership = _apiService.GetProductDetails(item.ProductCode).Product.FundsOwnership.ToString();
            }

            foreach (var item in getCardholderCardsResponse.Cards)
            {                
                var spendDet = _apiService.GetSpendDetails(item.CardProxyId);

                if(spendDet.IsSuccess != false)
                {
                    if (spendDet != null)
                    {
                        item.SpendDetail = new PrepaidCardSpendDetails
                        {
                            DailySpend = spendDet.DailySpend,
                            WeeklySpend = spendDet.WeeklySpend,
                            MonthlySpend = spendDet.MonthlySpend,
                            DailyLimit = spendDet.DailyLimit.ToString(),
                            WeeklyLimit = spendDet.WeeklyLimit.ToString(),
                            MonthlyLimit = spendDet.MonthlyLimit.ToString(),
                            DailyLeftToSpend = spendDet.DailyLeftToSpend.ToString(),
                            WeeklyLeftToSpend = spendDet.WeeklyLeftToSpend.ToString(),
                            MonthlyLeftToSpend = spendDet.MonthlyLeftToSpend.ToString(),
                            LeftToSpend = spendDet.LeftToSpend
                        };
                    }
                }
                else
                {
                    item.SpendDetail = new PrepaidCardSpendDetails();
                }
               
                           
            }

            SpendProfileResponseModel spendProfile = new SpendProfileResponseModel();

            foreach (var item in getCardholderCardsResponse.Cards)
            {
                spendProfile = _apiService.GetSpendProfileByCardId(item.CardProxyId);
            }

            var purchaseCategoriesResponse = _apiService.GetPurchaseCategoriesBySpendProfile(spendProfile.SpendProfile.SpendProfileId);

            var allPurchaseCategoriesResponse = _apiService.GetPurchaseCategories();

            var allCats = allPurchaseCategoriesResponse.Categories;
            var selectedCats = purchaseCategoriesResponse.Categories;

            var categories = new List<SelectListItem>();

            foreach (var allCategories in allCats.ToList())
            {
                foreach (var selectedCategories in selectedCats.ToList())
                {
                    if (allCategories.Id == selectedCategories.Id)
                    {
                        categories.Add(new SelectListItem
                        {
                            Text = allCategories.Name,
                            Value = allCategories.Id.ToString(),
                            Selected = true
                        });

                        allCats.Remove(allCategories);
                    }
                }
            }

            foreach (var cats in allCats)
            {
                categories.Add(new SelectListItem
                {
                    Text = cats.Name,
                    Value = cats.Id.ToString(),
                    Selected = false
                });
            }



            var model = new CardHolderDetailsPageModel
            {
                CardHolder = getCardholderResponse.User,
                Cards = getCardholderCardsResponse.Cards,
                SpendProfile = spendProfile,
                PurchaseCategories = categories
            };
            
            return View(model);
        }



        [Authorize(Roles = "Admin,CorporateAdmin")]
        public ActionResult EditCardholder(string UserId)
        {
            var getCardholderResponse = _apiService.GetCardHolderDetails(UserId);

            getCardholderResponse.User.AutoRenew = "1";

            if (!getCardholderResponse.IsSuccess)
            {

            }

            var model = new CardHolderDetailsPageModel
            {
                CardHolder = getCardholderResponse.User
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult EditCardholder(CardHolderDetailsPageModel user)
        {
            var cardHolder = user.CardHolder;
            var response = _apiService.UpdateUserDetails(cardHolder.Id, cardHolder);

            if (!response.IsSuccess)
            {

            }

            TempData["SuccessMessage"] = "Cardholder details updated successfully!";
            return RedirectToAction("CorporateCardholders");
        }

        public ActionResult GetTransactions(string cardProxyId, string userId)
        {
            var transactionList = _apiService.GetTransactions(cardProxyId, DateTime.Now, 6);

            var model = new UserTransactionHistoryVM
            {
                UserId = userId,
                CardProxyId = cardProxyId,
                Transactions = transactionList.Transactions
            };
                       
            return View(model);
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager")]
        public ActionResult ChangeCardStatus(string cardProxyId,string userId, string currentStatus)
        {
            var model = new ChangeCardStatusPageModel
            {
                CurrentStatus = currentStatus,
                UserId=userId,
                cardStatusList = GetStatusList(currentStatus)
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateCardStatus(ChangeCardStatusPageModel model)
        {
            BaseApiResponse response = new BaseApiResponse();

            if(model.CurrentStatus == "Inactive")
            {
                response = _apiService.ChangeInactiveCardStatus(model.CardProxyId, model.Status);
            }
            else
            {
                response = _apiService.ChangeCardStatus(model.CardProxyId, model.Status);
            }

            if (!response.IsSuccess)
            {
                var mod = new ChangeCardStatusPageModel
                {
                    CurrentStatus = model.CurrentStatus,
                    UserId = model.UserId,
                    cardStatusList = GetStatusList(model.Status)
                };
                
                TempData["FailureMessage"] = "There was a problem updating card status. Please try again!";
                return RedirectToAction("ChangeCardStatus",new { cardProxyId = model.CardProxyId,userId=model.UserId,currentStatus=model.CurrentStatus});
            }

            if (model.CurrentStatus == "Inactive")
            {
                TempData["SuccessMessage"] = "Card status updated successfully!";
                return RedirectToAction("Home", "Manage");
            }

            var cardHolderId = _apiService.GetCardholderFromProxy(model.CardProxyId).Card.CardholderId;

            TempData["SuccessMessage"] = "Card status updated successfully!";
            return RedirectToAction("CardHolderDetails",new { UserId = cardHolderId });
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager")]
        public ActionResult ChangeProductCode(string cardProxyId, string productCode, string userId)
        {
            var response = _apiService.GetProducts();

            var products = response.ProductList.Where(p => p.IsVirtual != true).ToList();

             

            var model = new ChangeProductCodePageModel
            {
                CurrentProductCode = productCode,
                CardProxyId = cardProxyId,
                UserId = userId,
                productCodes =  GetProductsDdl(products)
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeProductCode(ChangeProductCodePageModel model)
        {
            BaseApiResponse response = new BaseApiResponse();

           
            var res = _apiService.ChangeProductCode(model.CardProxyId, model.NewProductCode);
            

            if (!res.IsSuccess)
            {
                var mod = new ChangeProductCodePageModel
                {
                    CurrentProductCode = model.CurrentProductCode,
                    productCodes = (model.productCodes)
                };

                TempData["FailureMessage"] = "There was a problem updating card product. Credit profiles must match for both products!!";
                return RedirectToAction("ChangeProductCode", new { cardProxyId = model.CardProxyId, productCode = model.CurrentProductCode });
            }


            TempData["SuccessMessage"] = "Card Product updated successfully!";
            return RedirectToAction("CardHolderDetails", new { UserId = model.UserId });
        }

        private static List<SelectListItem> GetCardStatusList()
        {
            var cardStatusList = Enum.GetValues(typeof(CardStatus)).Cast<CardStatus>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = v.ToString()
            }).ToList();

            cardStatusList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });

            return cardStatusList;
        }

        private static List<SelectListItem> GetStatusList(string status = null)
        {
            List<SelectListItem> statusList = new List<SelectListItem>();

            if (status == "Block" || status == "Suspend")
            {
                statusList.Add(new SelectListItem
                {
                    Text = "Normal",
                    Value = "Normal"
                });

                statusList.Add(new SelectListItem
                {
                    Text = "Lost",
                    Value = "Lost"
                });

                statusList.Add(new SelectListItem
                {
                    Text = "Stolen",
                    Value = "Stolen"
                });
            }

            if (status == "Normal")
            {
                statusList.Add(new SelectListItem
                {
                    Text = "Normal",
                    Value = "Normal"
                });

                statusList.Add(new SelectListItem
                {
                    Text = "Lost",
                    Value = "Lost"
                });

                statusList.Add(new SelectListItem
                {
                    Text = "Stolen",
                    Value = "Stolen"
                });

                statusList.Add(new SelectListItem
                {
                    Text = "Block",
                    Value = "Block"
                });

                statusList.Add(new SelectListItem
                {
                    Text = "Suspend",
                    Value = "Suspend"
                });
            }

            if (status == "Inactive")
            {               
                statusList.Add(new SelectListItem
                {
                    Text = "Lost",
                    Value = "Lost"
                });

                statusList.Add(new SelectListItem
                {
                    Text = "Stolen",
                    Value = "Stolen"
                });
            }

            //Add Default Item at First Position.
            statusList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            return statusList;
        }

        private IEnumerable<Country> GetCountries()
        {
            return new List<Country>
            {
                new Country("United Kingdom of Great Britain", "GB", "GBR", 826),


                new Country("Afghanistan", "AF", "AFG", 4),
                new Country("Åland Islands", "AX", "ALA", 248),
                new Country("Albania", "AL", "ALB", 8),
                new Country("Algeria", "DZ", "DZA", 12),
                new Country("American Samoa", "AS", "ASM", 16),
                new Country("Andorra", "AD", "AND", 20),
                new Country("Angola", "AO", "AGO", 24),
                new Country("Anguilla", "AI", "AIA", 660),
                new Country("Antarctica", "AQ", "ATA", 10),
                new Country("Antigua and Barbuda", "AG", "ATG", 28),
                new Country("Argentina", "AR", "ARG", 32),
                new Country("Armenia", "AM", "ARM", 51),
                new Country("Aruba", "AW", "ABW", 533),
                new Country("Australia", "AU", "AUS", 36),
                new Country("Austria", "AT", "AUT", 40),
                new Country("Azerbaijan", "AZ", "AZE", 31),
                new Country("Bahamas", "BS", "BHS", 44),
                new Country("Bahrain", "BH", "BHR", 48),
                new Country("Bangladesh", "BD", "BGD", 50),
                new Country("Barbados", "BB", "BRB", 52),
                new Country("Belarus", "BY", "BLR", 112),
                new Country("Belgium", "BE", "BEL", 56),
                new Country("Belize", "BZ", "BLZ", 84),
                new Country("Benin", "BJ", "BEN", 204),
                new Country("Bermuda", "BM", "BMU", 60),
                new Country("Bhutan", "BT", "BTN", 64),
                new Country("Bolivia (Plurinational State of)", "BO", "BOL", 68),
                new Country("Bonaire, Sint Eustatius and Saba", "BQ", "BES", 535),
                new Country("Bosnia and Herzegovina", "BA", "BIH", 70),
                new Country("Botswana", "BW", "BWA", 72),
                new Country("Bouvet Island", "BV", "BVT", 74),
                new Country("Brazil", "BR", "BRA", 76),
                new Country("British Indian Ocean Territory", "IO", "IOT", 86),
                new Country("Brunei Darussalam", "BN", "BRN", 96),
                new Country("Bulgaria", "BG", "BGR", 100),
                new Country("Burkina Faso", "BF", "BFA", 854),
                new Country("Burundi", "BI", "BDI", 108),
                new Country("Cabo Verde", "CV", "CPV", 132),
                new Country("Cambodia", "KH", "KHM", 116),
                new Country("Cameroon", "CM", "CMR", 120),
                new Country("Canada", "CA", "CAN", 124),
                new Country("Cayman Islands", "KY", "CYM", 136),
                new Country("Central African Republic", "CF", "CAF", 140),
                new Country("Chad", "TD", "TCD", 148),
                new Country("Chile", "CL", "CHL", 152),
                new Country("China", "CN", "CHN", 156),
                new Country("Christmas Island", "CX", "CXR", 162),
                new Country("Cocos (Keeling) Islands", "CC", "CCK", 166),
                new Country("Colombia", "CO", "COL", 170),
                new Country("Comoros", "KM", "COM", 174),
                new Country("Congo", "CG", "COG", 178),
                new Country("Congo (Democratic Republic of the)", "CD", "COD", 180),
                new Country("Cook Islands", "CK", "COK", 184),
                new Country("Costa Rica", "CR", "CRI", 188),
                new Country("Côte d'Ivoire", "CI", "CIV", 384),
                new Country("Croatia", "HR", "HRV", 191),
                new Country("Cuba", "CU", "CUB", 192),
                new Country("Curaçao", "CW", "CUW", 531),
                new Country("Cyprus", "CY", "CYP", 196),
                new Country("Czech Republic", "CZ", "CZE", 203),
                new Country("Denmark", "DK", "DNK", 208),
                new Country("Djibouti", "DJ", "DJI", 262),
                new Country("Dominica", "DM", "DMA", 212),
                new Country("Dominican Republic", "DO", "DOM", 214),
                new Country("Ecuador", "EC", "ECU", 218),
                new Country("Egypt", "EG", "EGY", 818),
                new Country("El Salvador", "SV", "SLV", 222),
                new Country("Equatorial Guinea", "GQ", "GNQ", 226),
                new Country("Eritrea", "ER", "ERI", 232),
                new Country("Estonia", "EE", "EST", 233),
                new Country("Ethiopia", "ET", "ETH", 231),
                new Country("Falkland Islands (Malvinas)", "FK", "FLK", 238),
                new Country("Faroe Islands", "FO", "FRO", 234),
                new Country("Fiji", "FJ", "FJI", 242),
                new Country("Finland", "FI", "FIN", 246),
                new Country("France", "FR", "FRA", 250),
                new Country("French Guiana", "GF", "GUF", 254),
                new Country("French Polynesia", "PF", "PYF", 258),
                new Country("French Southern Territories", "TF", "ATF", 260),
                new Country("Gabon", "GA", "GAB", 266),
                new Country("Gambia", "GM", "GMB", 270),
                new Country("Georgia", "GE", "GEO", 268),
                new Country("Germany", "DE", "DEU", 276),
                new Country("Ghana", "GH", "GHA", 288),
                new Country("Gibraltar", "GI", "GIB", 292),
                new Country("Greece", "GR", "GRC", 300),
                new Country("Greenland", "GL", "GRL", 304),
                new Country("Grenada", "GD", "GRD", 308),
                new Country("Guadeloupe", "GP", "GLP", 312),
                new Country("Guam", "GU", "GUM", 316),
                new Country("Guatemala", "GT", "GTM", 320),
                new Country("Guernsey", "GG", "GGY", 831),
                new Country("Guinea", "GN", "GIN", 324),
                new Country("Guinea-Bissau", "GW", "GNB", 624),
                new Country("Guyana", "GY", "GUY", 328),
                new Country("Haiti", "HT", "HTI", 332),
                new Country("Heard Island and McDonald Islands", "HM", "HMD", 334),
                new Country("Holy See", "VA", "VAT", 336),
                new Country("Honduras", "HN", "HND", 340),
                new Country("Hong Kong", "HK", "HKG", 344),
                new Country("Hungary", "HU", "HUN", 348),
                new Country("Iceland", "IS", "ISL", 352),
                new Country("India", "IN", "IND", 356),
                new Country("Indonesia", "ID", "IDN", 360),
                new Country("Iran (Islamic Republic of)", "IR", "IRN", 364),
                new Country("Iraq", "IQ", "IRQ", 368),
                new Country("Ireland", "IE", "IRL", 372),
                new Country("Isle of Man", "IM", "IMN", 833),
                new Country("Israel", "IL", "ISR", 376),
                new Country("Italy", "IT", "ITA", 380),
                new Country("Jamaica", "JM", "JAM", 388),
                new Country("Japan", "JP", "JPN", 392),
                new Country("Jersey", "JE", "JEY", 832),
                new Country("Jordan", "JO", "JOR", 400),
                new Country("Kazakhstan", "KZ", "KAZ", 398),
                new Country("Kenya", "KE", "KEN", 404),
                new Country("Kiribati", "KI", "KIR", 296),
                new Country("Korea (Democratic People's Republic of)", "KP", "PRK", 408),
                new Country("Korea (Republic of)", "KR", "KOR", 410),
                new Country("Kuwait", "KW", "KWT", 414),
                new Country("Kyrgyzstan", "KG", "KGZ", 417),
                new Country("Lao People's Democratic Republic", "LA", "LAO", 418),
                new Country("Latvia", "LV", "LVA", 428),
                new Country("Lebanon", "LB", "LBN", 422),
                new Country("Lesotho", "LS", "LSO", 426),
                new Country("Liberia", "LR", "LBR", 430),
                new Country("Libya", "LY", "LBY", 434),
                new Country("Liechtenstein", "LI", "LIE", 438),
                new Country("Lithuania", "LT", "LTU", 440),
                new Country("Luxembourg", "LU", "LUX", 442),
                new Country("Macao", "MO", "MAC", 446),
                new Country("Macedonia (the former Yugoslav Republic of)", "MK", "MKD", 807),
                new Country("Madagascar", "MG", "MDG", 450),
                new Country("Malawi", "MW", "MWI", 454),
                new Country("Malaysia", "MY", "MYS", 458),
                new Country("Maldives", "MV", "MDV", 462),
                new Country("Mali", "ML", "MLI", 466),
                new Country("Malta", "MT", "MLT", 470),
                new Country("Marshall Islands", "MH", "MHL", 584),
                new Country("Martinique", "MQ", "MTQ", 474),
                new Country("Mauritania", "MR", "MRT", 478),
                new Country("Mauritius", "MU", "MUS", 480),
                new Country("Mayotte", "YT", "MYT", 175),
                new Country("Mexico", "MX", "MEX", 484),
                new Country("Micronesia (Federated States of)", "FM", "FSM", 583),
                new Country("Moldova (Republic of)", "MD", "MDA", 498),
                new Country("Monaco", "MC", "MCO", 492),
                new Country("Mongolia", "MN", "MNG", 496),
                new Country("Montenegro", "ME", "MNE", 499),
                new Country("Montserrat", "MS", "MSR", 500),
                new Country("Morocco", "MA", "MAR", 504),
                new Country("Mozambique", "MZ", "MOZ", 508),
                new Country("Myanmar", "MM", "MMR", 104),
                new Country("Namibia", "NA", "NAM", 516),
                new Country("Nauru", "NR", "NRU", 520),
                new Country("Nepal", "NP", "NPL", 524),
                new Country("Netherlands", "NL", "NLD", 528),
                new Country("New Caledonia", "NC", "NCL", 540),
                new Country("New Zealand", "NZ", "NZL", 554),
                new Country("Nicaragua", "NI", "NIC", 558),
                new Country("Niger", "NE", "NER", 562),
                new Country("Nigeria", "NG", "NGA", 566),
                new Country("Niue", "NU", "NIU", 570),
                new Country("Norfolk Island", "NF", "NFK", 574),
                new Country("Northern Mariana Islands", "MP", "MNP", 580),
                new Country("Norway", "NO", "NOR", 578),
                new Country("Oman", "OM", "OMN", 512),
                new Country("Pakistan", "PK", "PAK", 586),
                new Country("Palau", "PW", "PLW", 585),
                new Country("Palestine, State of", "PS", "PSE", 275),
                new Country("Panama", "PA", "PAN", 591),
                new Country("Papua New Guinea", "PG", "PNG", 598),
                new Country("Paraguay", "PY", "PRY", 600),
                new Country("Peru", "PE", "PER", 604),
                new Country("Philippines", "PH", "PHL", 608),
                new Country("Pitcairn", "PN", "PCN", 612),
                new Country("Poland", "PL", "POL", 616),
                new Country("Portugal", "PT", "PRT", 620),
                new Country("Puerto Rico", "PR", "PRI", 630),
                new Country("Qatar", "QA", "QAT", 634),
                new Country("Réunion", "RE", "REU", 638),
                new Country("Romania", "RO", "ROU", 642),
                new Country("Russian Federation", "RU", "RUS", 643),
                new Country("Rwanda", "RW", "RWA", 646),
                new Country("Saint Barthélemy", "BL", "BLM", 652),
                new Country("Saint Helena, Ascension and Tristan da Cunha", "SH", "SHN", 654),
                new Country("Saint Kitts and Nevis", "KN", "KNA", 659),
                new Country("Saint Lucia", "LC", "LCA", 662),
                new Country("Saint Martin (French part)", "MF", "MAF", 663),
                new Country("Saint Pierre and Miquelon", "PM", "SPM", 666),
                new Country("Saint Vincent and the Grenadines", "VC", "VCT", 670),
                new Country("Samoa", "WS", "WSM", 882),
                new Country("San Marino", "SM", "SMR", 674),
                new Country("Sao Tome and Principe", "ST", "STP", 678),
                new Country("Saudi Arabia", "SA", "SAU", 682),
                new Country("Senegal", "SN", "SEN", 686),
                new Country("Serbia", "RS", "SRB", 688),
                new Country("Seychelles", "SC", "SYC", 690),
                new Country("Sierra Leone", "SL", "SLE", 694),
                new Country("Singapore", "SG", "SGP", 702),
                new Country("Sint Maarten (Dutch part)", "SX", "SXM", 534),
                new Country("Slovakia", "SK", "SVK", 703),
                new Country("Slovenia", "SI", "SVN", 705),
                new Country("Solomon Islands", "SB", "SLB", 90),
                new Country("Somalia", "SO", "SOM", 706),
                new Country("South Africa", "ZA", "ZAF", 710),
                new Country("South Georgia and the South Sandwich Islands", "GS", "SGS", 239),
                new Country("South Sudan", "SS", "SSD", 728),
                new Country("Spain", "ES", "ESP", 724),
                new Country("Sri Lanka", "LK", "LKA", 144),
                new Country("Sudan", "SD", "SDN", 729),
                new Country("Suriname", "SR", "SUR", 740),
                new Country("Svalbard and Jan Mayen", "SJ", "SJM", 744),
                new Country("Swaziland", "SZ", "SWZ", 748),
                new Country("Sweden", "SE", "SWE", 752),
                new Country("Switzerland", "CH", "CHE", 756),
                new Country("Syrian Arab Republic", "SY", "SYR", 760),
                new Country("Taiwan, Province of China[a]", "TW", "TWN", 158),
                new Country("Tajikistan", "TJ", "TJK", 762),
                new Country("Tanzania, United Republic of", "TZ", "TZA", 834),
                new Country("Thailand", "TH", "THA", 764),
                new Country("Timor-Leste", "TL", "TLS", 626),
                new Country("Togo", "TG", "TGO", 768),
                new Country("Tokelau", "TK", "TKL", 772),
                new Country("Tonga", "TO", "TON", 776),
                new Country("Trinidad and Tobago", "TT", "TTO", 780),
                new Country("Tunisia", "TN", "TUN", 788),
                new Country("Turkey", "TR", "TUR", 792),
                new Country("Turkmenistan", "TM", "TKM", 795),
                new Country("Turks and Caicos Islands", "TC", "TCA", 796),
                new Country("Tuvalu", "TV", "TUV", 798),
                new Country("Uganda", "UG", "UGA", 800),
                new Country("Ukraine", "UA", "UKR", 804),
                new Country("United Arab Emirates", "AE", "ARE", 784),
                new Country("United States of America", "US", "USA", 840),
                new Country("United States Minor Outlying Islands", "UM", "UMI", 581),
                new Country("Uruguay", "UY", "URY", 858),
                new Country("Uzbekistan", "UZ", "UZB", 860),
                new Country("Vanuatu", "VU", "VUT", 548),
                new Country("Venezuela (Bolivarian Republic of)", "VE", "VEN", 862),
                new Country("Viet Nam", "VN", "VNM", 704),
                new Country("Virgin Islands (British)", "VG", "VGB", 92),
                new Country("Virgin Islands (U.S.)", "VI", "VIR", 850),
                new Country("Wallis and Futuna", "WF", "WLF", 876),
                new Country("Western Sahara", "EH", "ESH", 732),
                new Country("Yemen", "YE", "YEM", 887),
                new Country("Zambia", "ZM", "ZMB", 894),
                new Country("Zimbabwe", "ZW", "ZWE", 716)
            };
        }


    }
}