﻿using CorporatePortal.Api;
using CorporatePortal.Api.Requests;
using CorporatePortal.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace CorporatePortal.Controllers
{
    [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager,CorporateEmployee,CorporateClient")]
    public class ProductController : Controller
    {
        IApiService _apiService;

        public ProductController(IApiService service)
        {
            _apiService = service;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }
        // GET: Products
        public ActionResult Index()
        {
            var response = _apiService.GetPortalProducts();

            if (!response.IsSuccess)
            {

            }

            var products = response.ProductList;

            //for (int i = 0; i < products.Count; i++)
            //{
            //    if (products[i].SpendProfileId.HasValue)
            //    {
            //        var spendProfile = _apiService.GetSpendProfileById(products[i].SpendProfileId.Value);
            //        if(spendProfile != null)
            //        {
            //            products[i].SpendProfileName = spendProfile.SpendProfile.ProfileName;
            //        }
            //    }

            //    if (products[i].CreditProfileId.HasValue)
            //    {
            //        var creditProfile = _apiService.GetCreditProfileById(products[i].CreditProfileId.Value);
            //        if(creditProfile != null)
            //        {
            //            products[i].CreditProfileName = creditProfile.CreditProfile.ProfileName;
            //        }
                   
            //    }

            //    if (products[i].ProductFeeId.HasValue)
            //    {
            //        var productFee = _apiService.GetProductFeeById(products[i].ProductFeeId.Value);
            //        if(productFee != null)
            //        {
            //            products[i].ProductFeeName = productFee.ProductFee.Name;
            //        }                    
            //    }
            //}

            return View(products);
        }

        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult Create()
        {
            var model = new CreateProductPageModel();
            model.Currencies = GetCurrenciesDdl();
            model.ProgramTypes = GetProgramTypesDdl();
            model.FundingSources = GetFundingSourceDdl();

            return View(model);
        }

        [Authorize(Roles = "Admin,SuperAdmin")]
        [HttpPost]
        public ActionResult Create(CreateProductPageModel p)
        {
            if (!ModelState.IsValid)
            {
                p.Currencies = GetCurrenciesDdl();
                p.ProgramTypes = GetProgramTypesDdl();
                p.FundingSources = GetFundingSourceDdl();
                return View(p);
            }

            var req = new NewProductRequest
            {
                ProductCode = p.ProductCode,
                Currency = p.Currency,
                IsActive = p.IsActive,
                IsVirtual = p.IsVirtual,
                CardDesign = p.CardDesign,
                CardProduct = p.CardProduct,
                CardProgram = p.CardProgram,
                DebitFeePercentage = p.DebitFeePercentage,
                CreditFeePercentage = p.CreditFeePercentage,
                KycCheck = p.KycCheck,
                PepsSanctionsCheck = p.PepsSanctionsCheck,
                AutoTopupEnabled = p.AutoTopupEnabled,
                AutoTopupDefaultAmount = p.AutoTopupDefaultAmount,
                AutoTopUpTopUpThreshold = p.AutoTopUpTopUpThreshold,
                RewardsPointPerUnit = p.RewardsPointPerUnit,
                RewardsPointsEnabled = p.RewardsPointsEnabled,
                AllowAnonymousPinReveal = p.AllowAnonymousPinReveal,
                TokeniseWithoutPaymentCardEnabled = p.TokeniseWithoutPaymentCard,
                ProductType = p.ProductType,
                Reloadable = p.Reloadable,
                ATMTransactions = p.ATMTransactions,
                OnlineTransactions = p.OnlineTransactions,
                ForEmployee = p.ForEmployee,
                MaxDailyBalance = p.MaxDailyBalance,
                MaxDailyLoad = p.MaxDailyLoad,
                CorporateLoadMaxBalance = p.CorporateLoadMaxBalance,
                CorporateLoadMinLoadAmount = p.CorporateLoadMinLoadAmount,
                MaxWeeklyLoad = p.MaxWeeklyLoad,
                MaxMonthlyLoad = p.MaxMonthlyLoad,
                MaxYearlyLoad = p.MaxYearlyLoad,
                MaxTopupValue = p.MaxTopupValue,
                MinTopupValue = p.MinTopupValue,
                FundsOwnership = p.FundsOwnership,
                FundingSource = p.FundingSource,
                TransactionAlerts = p.TransactionAlerts
                
                

            };


            var response = _apiService.AddNewProduct(req);

            if (!response.IsSuccess)
            {

            }

            return RedirectToAction("ProductDetails", new { productCode = p.ProductCode });
        }

        private static List<SelectListItem> GetCurrenciesDdl()
        {

            List<SelectListItem> Currencies = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "GBP",
                                                 Value = "GBP"
                                             },
                new SelectListItem
                                             {
                                                Text = "EUR",
                                                 Value = "EUR"
                                             }
            }.ToList();

            //Add Default Item at First Position.
            Currencies.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            return Currencies;
        }

        private static List<SelectListItem> GetProgramTypesDdl()
        {
          var productTypes =  Enum.GetValues(typeof(ProgramType)).Cast<ProgramType>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = v.ToString()
            }).ToList();

            productTypes.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });

            return productTypes;
        }

        private static List<SelectListItem> GetFundingSourceDdl()
        {
            var fundingSources = Enum.GetValues(typeof(Models.Product.FundingSource)).Cast<FundingSource>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = v.ToString()
            }).ToList();

            fundingSources.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });

            return fundingSources;
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager,CorporateEmployee,CorporateClient")]      
        public ActionResult ProductDetails(string productCode)
        {
            var response = _apiService.GetProductDetails(productCode);
            var model = response.Product;

            if (response.Product.SpendProfileId.HasValue)
            {
              var spendProfile = _apiService.GetSpendProfileById(response.Product.SpendProfileId.Value);
                if(spendProfile != null)
                {
                    model.SpendProfileName = spendProfile.SpendProfile.ProfileName;
                }                
            }

            if (response.Product.CreditProfileId.HasValue)
            {
                var creditProfile = _apiService.GetCreditProfileById(response.Product.CreditProfileId.Value);
                if(creditProfile != null)
                {
                    model.CreditProfileName = creditProfile.CreditProfile.ProfileName;
                }                
            }

            if (response.Product.ProductFeeId.HasValue)
            {
                var productFee = _apiService.GetProductFeeById(response.Product.ProductFeeId.Value);
                if (productFee != null)
                {
                    model.ProductFeeName = productFee.ProductFee.Name;
                }
            }

            return View(model);
        }

        [Authorize(Roles = "Admin,CorporateAdmin")]
        public ActionResult UpdateProductDetails(string productCode)
        {
            var response = _apiService.GetProductDetails(productCode);

            var model = response.Product;

            model.SpendProfiles = GetSpendProfilesDdl();
            model.CreditProfiles = GetCreditProfilesDdl();
            model.ProductFees = GetProductFeesDdl();

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult UpdateLimits(string cardProxyId, string UserId)
        {
            string productCode = "somecode";
            var response = _apiService.GetProductDetails(productCode);

            return View(response.Product);
        }

        [Authorize(Roles = "Admin,CorporateAdmin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateProductDetails(QcsProduct qcsProduct)
        {
            if (ModelState.IsValid)
            {
                var response = _apiService.UpdateProduct(qcsProduct.ProductCode, qcsProduct);

                return RedirectToAction("ProductDetails", "Product", new { productCode = qcsProduct.ProductCode });
            }

            return View(qcsProduct);
        }

        [Authorize(Roles = "Admin,CorporateAdmin")]
        public ActionResult GetSpendProfileOverrideList()
        {
            var model = new List<SpendProfileOverride>();

            var response = _apiService.GetSpendProfileOverridList();

            if (response.IsSuccess)
            {
                model = response.SpendProfileOverrides;
            }

            return View(model);
        }

        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult CreateSpendProfileOverride()
        {
            var model = new CreateSpendProfileOverridePageModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult CreateSpendProfileOverride(CreateSpendProfileOverridePageModel model)
        {
            var response = _apiService.CreateSpendProfileOverride(new CreateSpendProfileOverrideRequest { Amount = model.Amount, CardId = model.CardId });

            if (response.IsSuccess)
            {

                TempData["SuccessMessage"] = "Created successfully";

                return RedirectToAction("GetSpendProfileOverrideList");
            }
            else if (response.ErrorCodes[0] == "19000002")
            {
                TempData["FailureMessage"] = "Not created. Balance lower then amount. ErrorCode : " + response.ErrorCodes[0].ToString();
            }
            else
            {
                TempData["FailureMessage"] = "Not created.";
            }

            return View(model);
        }

        private List<SelectListItem> GetSpendProfilesDdl()
        {
            var list = _apiService.GetSpendProfileList();
            
            List<SelectListItem> spendProfilesList = (from p in list.SpendProfileList.AsEnumerable()
                                             select new SelectListItem
                                             {
                                                 Text = p.ProfileName,
                                                 Value = p.SpendProfileId.ToString()
                                             }).ToList();



            spendProfilesList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            spendProfilesList.Insert(1, new SelectListItem { Text = "--None --", Value = "", Disabled = false, Selected = false });
            return spendProfilesList;
        }

        private List<SelectListItem> GetCreditProfilesDdl()
        {
            var list = _apiService.GetCreditProfilesList();

            List<SelectListItem> creditProfilesList = (from p in list.CreditProfileList.AsEnumerable()
                                                      select new SelectListItem
                                                      {
                                                          Text = p.ProfileName,
                                                          Value = p.CreditProfileId.ToString()
                                                      }).ToList();



            creditProfilesList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            creditProfilesList.Insert(1, new SelectListItem { Text = "--None --", Value = "", Disabled = false, Selected = false });
            return creditProfilesList;
        }

        private List<SelectListItem> GetProductFeesDdl()
        {
            var list = _apiService.GetProductFeesList();

            List<SelectListItem> productFeesList = (from p in list.ProductFees.AsEnumerable()
                                                       select new SelectListItem
                                                       {
                                                           Text = p.Name,
                                                           Value = p.ProductFeeId.ToString()
                                                       }).ToList();



            productFeesList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            productFeesList.Insert(1, new SelectListItem { Text = "--None --", Value = "", Disabled = false, Selected = false });
            return productFeesList;
        }

        #region SpendProfile



        #endregion
    }
}