﻿using CorporatePortal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager,CorporateEmployee,CorporateClient")]
    public class HomeController : Controller
    {
        private readonly IApiService _apiService;

        public HomeController(IApiService service)
        {
            _apiService = service;
            var model = _apiService.GetPortalSummary();         
            ViewBag.TopTiles = model.Summary;
        }

        public ActionResult Index()
        {

            //var model = _service.GetPortalSummary();
            //ViewBag.TopTiles = model.Summary;

            var accounts = _apiService.GetAccountsList();
            ViewBag.Accounts = accounts.AccountsList;

            var products = _apiService.GetProducts();
            ViewBag.products = products.ProductList;
            return View();
        }

        public ActionResult AccountLoadHistory(int accountId , string accountName)
        {
            var response = _apiService.GetCorporateAccountLoadHistory(accountId);

            ViewBag.AccountId = accountId;
            ViewBag.AccountName = accountName;

            var model = response.AccountLoads;

            return View(model);
        }

        public ActionResult AccountTransactionHistory(string cardProxyId,string accountId, string accountName)
        {
            var response = _apiService.GetCorporaeAccountCardTransactionHistory(cardProxyId,DateTime.Now,2);

            var model = response.Transactions;

            ViewBag.AccountId = accountId;
            ViewBag.AccountName = accountName;
            return View(model);
        }

        [HttpGet]
        public JsonResult TransactionByProductChart()
        {
            decimal totalTransactions = 0;
            var response = _apiService.GetTransactionByProductSummary();
            var model = response.Summary;
            var list = new List<ds>();
            var random = new Random();
           
            foreach (var item in model)
            {
                totalTransactions += item.Total;
                var ds = new ds
                {
                    label = item.ProductCode,
                    backgroundColor = String.Format("#{0:X6}", random.Next(0x1000000)),
                   data = new List<long> { Convert.ToInt64(item.Total) }
                };
                list.Add(ds);
            }

            for(int i =0;i<list.Count;i++)
            {
                //Portal products
                if (list[i].label == "Limit tester") { list[i].backgroundColor = "#b3ffb3"; }

                if (list[i].label == "Product4") { list[i].backgroundColor = "#9999e6"; }

                if (list[i].label == "Product5") { list[i].backgroundColor = "#ccccff"; }

                if (list[i].label == "Product6") { list[i].backgroundColor = "#ff99c2"; }

                if (list[i].label == "QPay V Test") { list[i].backgroundColor = "#b3b3cc"; }

                if (list[i].label == "QPayV2") { list[i].backgroundColor = "#ccffff"; }

                if (list[i].label == "QPayV3") { list[i].backgroundColor = "#ffccdd"; }

                if (list[i].label == "TestProd1") { list[i].backgroundColor = "#ecc6d9"; }

                //Buttle products
                if (list[i].label == "BUTTLE") { list[i].backgroundColor = "#ccccff"; }

                //Hyundai products
                if (list[i].label == "Hyundai") { list[i].backgroundColor = "#990000"; }

                //GiftCloud products
                if (list[i].label == "DECISION") { list[i].backgroundColor = "#b3ffb3"; }

                if (list[i].label == "GENIE") { list[i].backgroundColor = "#9999e6"; }

                if (list[i].label == "HYPER") { list[i].backgroundColor = "#ccccff"; }

                //Fuelmate products
                if (list[i].label == "50000011") { list[i].backgroundColor = "#6D1704"; }

                if (list[i].label == "50000012") { list[i].backgroundColor = "#C06A57"; }

                if (list[i].label == "50000013") { list[i].backgroundColor = "#DCBDB6"; }

                if (list[i].label == "50000032") { list[i].backgroundColor = "#074B9D"; }

                if (list[i].label == "50000042") { list[i].backgroundColor = "#666D75"; }

                if (list[i].label == "50000171") { list[i].backgroundColor = "#0674F2"; }

                if (list[i].label == "50000211") { list[i].backgroundColor = "#0CC856"; }

                if (list[i].label == "50000231") { list[i].backgroundColor = "#12F7D7"; }

                if (list[i].label == "60000011") { list[i].backgroundColor = "#BB0B81"; }

                if (list[i].label == "60000021") { list[i].backgroundColor = "#E4F705"; }

                if (list[i].label == "60000022") { list[i].backgroundColor = "#A00692"; }


            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AmountSpentByProductChart()
        {
            var response = _apiService.GetAmountByProductSummary();
            decimal totalAmountSpent = 0;
            var model = response.Summary;
            var list = new List<ds>();
            var random = new Random();

            foreach (var item in model)
            {
                totalAmountSpent += item.Total;
                var ds = new ds
                {
                    label = item.ProductCode,
                    backgroundColor = String.Format("#{0:X6}", random.Next(0x1000000)),
                    data = new List<long> { Convert.ToInt64(item.Total) }
                };
                list.Add(ds);
            }

            for (int i = 0; i < list.Count; i++)
            {
                //Portal products
                if (list[i].label == "Limit tester") { list[i].backgroundColor = "#b3ffb3"; }

                if (list[i].label == "Product4") { list[i].backgroundColor = "#9999e6"; }

                if (list[i].label == "Product5") { list[i].backgroundColor = "#ccccff"; }

                if (list[i].label == "Product6") { list[i].backgroundColor = "#ff99c2"; }

                if (list[i].label == "QPay V Test") { list[i].backgroundColor = "#b3b3cc"; }

                if (list[i].label == "QPayV2") { list[i].backgroundColor = "#ccffff"; }

                if (list[i].label == "QPayV3") { list[i].backgroundColor = "#ffccdd"; }

                if (list[i].label == "TestProd1") { list[i].backgroundColor = "#ecc6d9"; }

                //Buttle products
                if (list[i].label == "BUTTLE") { list[i].backgroundColor = "#ccccff"; }

                //Hyundai products
                if (list[i].label == "Hyundai") { list[i].backgroundColor = "#990000"; }

                //GiftCloud products
                if (list[i].label == "DECISION") { list[i].backgroundColor = "#b3ffb3"; }

                if (list[i].label == "GENIE") { list[i].backgroundColor = "#9999e6"; }

                if (list[i].label == "HYPER") { list[i].backgroundColor = "#ccccff"; }


                //Fuelmate products
                if (list[i].label == "50000011") { list[i].backgroundColor = "#6D1704"; }

                if (list[i].label == "50000012") { list[i].backgroundColor = "#C06A57"; }

                if (list[i].label == "50000013") { list[i].backgroundColor = "#DCBDB6"; }

                if (list[i].label == "50000032") { list[i].backgroundColor = "#074B9D"; }

                if (list[i].label == "50000042") { list[i].backgroundColor = "#666D75"; }

                if (list[i].label == "50000171") { list[i].backgroundColor = "#0674F2"; }

                if (list[i].label == "50000211") { list[i].backgroundColor = "#0CC856"; }

                if (list[i].label == "50000231") { list[i].backgroundColor = "#12F7D7"; }

                if (list[i].label == "60000011") { list[i].backgroundColor = "#BB0B81"; }

                if (list[i].label == "60000021") { list[i].backgroundColor = "#E4F705"; }

                if (list[i].label == "60000022") { list[i].backgroundColor = "#A00692"; }
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }

    public class ds
    {
        public string label { get; set; }
        public List<long> data { get; set; }
        public string backgroundColor { get; set; }        
    }
}