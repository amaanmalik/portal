﻿using CorporatePortal.Api;
using CorporatePortal.Api.Requests;
using CorporatePortal.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    public class SpendProfileController : Controller
    {
        IApiService _apiService;

        public SpendProfileController(IApiService apiService)
        {
            _apiService = apiService;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }

        // GET: SpendProfile
        public ActionResult Index()
        {
            var response = _apiService.GetSpendProfileList();

            var model = new SpentProfileListPageModel
            {
                ProfileList = response.SpendProfileList
            };

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new SpendProfileModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(SpendProfileModel model)
        {
           var response =  _apiService.CreateSpendProfile(model);

            return RedirectToAction("Index");
        }

        public ActionResult Update(int spendProfileId)
        {
            var spendProfileResponse = _apiService.GetSpendProfileById(spendProfileId);

            var purchaseCategoriesResponse = _apiService.GetPurchaseCategoriesBySpendProfile(spendProfileId);

            var allPurchaseCategoriesResponse = _apiService.GetPurchaseCategories();

            var allCats = allPurchaseCategoriesResponse.Categories;
            var selectedCats = purchaseCategoriesResponse.Categories;

            var model = new UpdateSpendProfileViewModel();

            model.SpendProfile = spendProfileResponse.SpendProfile;

            var categories = new List<SelectListItem>();


            foreach (var allCategories in allCats.ToList())
            {
                foreach (var selectedCategories in selectedCats.ToList())
                {
                    if(allCategories.Id == selectedCategories.Id)
                    {
                        categories.Add(new SelectListItem
                        {
                            Text = allCategories.Name,
                            Value = allCategories.Id.ToString(),
                            Selected = true
                        });

                        allCats.Remove(allCategories);
                    }
                }
            }

            foreach (var cats in allCats)
            {
                categories.Add(new SelectListItem
                {
                    Text = cats.Name,
                    Value = cats.Id.ToString(),
                    Selected = false
                });
            }
         

            model.PurchaseCategories = categories;

            return View(model);
        }

        [HttpPost]
        public ActionResult Update(UpdateSpendProfileViewModel model)
        {
            var spendProfile = model.SpendProfile;

            var categoriesToUpdate = new List<PurchaseCategory>();

            foreach (var cat in model.PurchaseCategories.ToList())
            {
                if(cat.Selected == true)
                {
                    categoriesToUpdate.Add(new PurchaseCategory
                    {
                        Id = Convert.ToInt16(cat.Value),
                        Name = cat.Text
                    });
                }
            }

            var request = new UpdateSpendProfileRequest
            {
                SpendProfile = model.SpendProfile,
                PurchaseCategories = categoriesToUpdate
            };

            var response = _apiService.UpdateSpendProfile(request);

            return RedirectToAction("Index");
        }

        public ActionResult PurchaseCategories()
        {
            var response = _apiService.GetPurchaseCategories();
            var model = new PurchaseCategoryPageModel
            {
                purchaseCategories = response.Categories
            };

            return View(model);
        }

        public ActionResult CreatePurchaseCategory()
        {
            var model = new PurchaseCategory();

            return View(model);
        }

        [HttpPost]
        public ActionResult CreatePurchaseCategory(PurchaseCategory model)
        {
            var response = _apiService.CreatePurchaseCategory(model);

            return RedirectToAction("PurchaseCategories");
        }

        public ActionResult UpdatePurchaseCategory(int categoryId)
        {
           var purchaseCategoriesResponse = _apiService.GetPurchaseCategories().Categories.Where(p => p.Id == categoryId).FirstOrDefault();

            var purchaseCategoryMccResponse = _apiService.GetMerchantCodesByPurchaseCategory(categoryId);

            var allMccResponse = _apiService.GetMerchantCodes();


            var all = allMccResponse.MerchantCodes;
            var selected = purchaseCategoryMccResponse.MerchantCodes;

            var model = new UpdatePurchaseCategoryViewModel();

            model.PurchaseCategory = purchaseCategoriesResponse;

            var mccList = new List<SelectListItem>();

            foreach (var allCategories in all.ToList())
            {
                foreach (var selectedCategories in selected.ToList())
                {
                    if (allCategories.MerchantCode == selectedCategories.MerchantCode)
                    {
                        mccList.Add(new SelectListItem
                        {
                            Text = allCategories.MerchantCode + " || " + allCategories.Description,
                            Value = allCategories.MerchantCode.ToString(),
                            Selected = true
                        });

                        all.Remove(allCategories);
                    }
                }
            }

            foreach (var cats in all)
            {
                mccList.Add(new SelectListItem
                {
                    Text = cats.MerchantCode + " || " + cats.Description,
                    Value = cats.MerchantCode.ToString(),
                    Selected = false
                });
            }

            model.Mcc = mccList;

            return View(model);
        }

        [HttpPost]
        public ActionResult UpdatePurchaseCategory(UpdatePurchaseCategoryViewModel model)
        {
            var purchaseCategory = model.PurchaseCategory;

            var mccToUpdate = new List<MerchantCodeModel>();

            foreach (var cat in model.Mcc.ToList())
            {
                if (cat.Selected == true)
                {
                    mccToUpdate.Add(new MerchantCodeModel
                    {
                        MerchantCode = Convert.ToInt16(cat.Value),
                        Description = cat.Text
                    });
                }
            }

            var request = new UpdatePurchaseCategoryRequest
            {
                PurchaseCategory = model.PurchaseCategory,
                MerchantCodes = mccToUpdate
            };

            var response = _apiService.UpdatePurchaseCategory(request);

            return RedirectToAction("PurchaseCategories");
          
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager")]
        [HttpGet]
        public ActionResult DeleteSpendProfile(int spendProfileId)
        {
            var response = _apiService.DeleteSpendProfile(spendProfileId);

            return RedirectToAction("Index");
        }
    }
}