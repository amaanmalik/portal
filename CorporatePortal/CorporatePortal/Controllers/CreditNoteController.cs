﻿using CorporatePortal.Api;
using CorporatePortal.Api.Requests.CredNotes;
using CorporatePortal.Models.CreditNotes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    public class CreditNoteController : Controller
    {

        IApiService _apiService;

        public CreditNoteController(IApiService apiService)
        {
            _apiService = apiService;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }

        public ActionResult Index()
        {
            var response = _apiService.GetCreditNotes();

            var list = response.CreditNotes.OrderByDescending(c => c.CreatedDate).ToList();

            var model = new CreditNotesPageModel
            {
                CreditNotes = list
            };

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new AddCreditNotePageModel();

            model.CreditProfiles = GetCreditProfilesDdl();
            model.Types = GetTypeList();
            model.StatementType = "Credit";
            
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(AddCreditNotePageModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var credProf = _apiService.GetCreditProfileById(model.CreditProfileId.Value);

            var request = new AddCreditNoteRequestModel
            {
                CreditProfileId = model.CreditProfileId.Value,
                CreditProfileName = credProf.CreditProfile.ProfileName,
                Description = model.Description,
                StatementType = "Credit",
                TotalSpent = model.TotalSpent,
                Type = model.Type
            };

            var response = _apiService.AddCreditNote(request);

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager")]
        [HttpGet]
        public ActionResult DeleteCreditNote(int creditNoteId)
        {
            var response = _apiService.DeleteCreditNote(creditNoteId);

            return RedirectToAction("Index");
        }

        public ActionResult Update(int creditNoteId)
        {
            var response = _apiService.GetCreditNoteById(creditNoteId);

            var model = new UpdateCreditNotePageModel
            {
                CreditNoteId = response.CreditNote.CreditNoteId,
                Amount = response.CreditNote.TotalSpent,
                CreatedDate = response.CreditNote.CreatedDate,
                CreditProfileId = response.CreditNote.CreditProfileId,
                CreditProfileName = response.CreditNote.CreditProfileName,
                Description = response.CreditNote.Description,
                StatementType = response.CreditNote.StatementType
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Update(UpdateCreditNotePageModel model)
        {
            var request = new UpdateCreditNoteRequestModel
            {
               Description = model.Description,
               Amount = model.Amount,
               statementId = model.CreditNoteId
            };

            var response = _apiService.UpdateCreditNote(model.CreditNoteId, request);

            return RedirectToAction("Index");
        }

        private List<SelectListItem> GetCreditProfilesDdl()
        {
            var list = _apiService.GetCreditProfilesList();

            List<SelectListItem> creditProfilesList = (from p in list.CreditProfileList.AsEnumerable()
                                                       select new SelectListItem
                                                       {
                                                           Text = p.ProfileName,
                                                           Value = p.CreditProfileId.ToString()
                                                       }).ToList();



            creditProfilesList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            creditProfilesList.Insert(1, new SelectListItem { Text = "--None --", Value = "", Disabled = false, Selected = false });
            return creditProfilesList;
        }

        private static List<SelectListItem> GetTypeList()
        {
            List<SelectListItem> statusList = new List<SelectListItem>();
                        
            statusList.Add(new SelectListItem
            {
                Text = "Expensemate Card Spend",
                Value = "EXPENSEMATE 10"
            });

            statusList.Add(new SelectListItem
            {
                Text = "Expensemate Foreign Spend",
                Value = "EXPENSEMATE 10FX"
            });

            statusList.Add(new SelectListItem
            {
                Text = "Expensemate Online Spend",
                Value = "EXPENSEMATE 20"
            });

            statusList.Add(new SelectListItem
            {
                Text = "Expensemate OnLine Foreign Spend",
                Value = "EXPENSEMATE 20FX"
            });

            statusList.Add(new SelectListItem
            {
                Text = "Finance Fee",
                Value = "EXPENSEMATE ADM"
            });

            statusList.Add(new SelectListItem
            {
                Text = "Active Card Fee",
                Value = "EXPENSEMATE CCA"
            });

            statusList.Add(new SelectListItem
            {
                Text = "Issue Card Fee",
                Value = "EXPENSEMATE CCN"
            });

            statusList.Add(new SelectListItem
            {
                Text = "FX Fee Total",
                Value = "EXPENSEMATE FFX"
            });

            statusList.Add(new SelectListItem
            {
                Text = "Expensemate Refund",
                Value = "REFUND"
            });

            statusList.Add(new SelectListItem
            {
                Text = "Bespoke Projects ",
                Value = "EXPENSEMATE"
            });


            //Add Default Item at First Position.
            statusList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            return statusList;
        }
    }
}