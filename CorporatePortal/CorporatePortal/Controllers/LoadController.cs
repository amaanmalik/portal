﻿using CorporatePortal.Api;
using CorporatePortal.Api.Requests;
using CorporatePortal.Api.Requests.CardLoad;
using CorporatePortal.Models;
using CorporatePortal.Models.ActivateCard;
using CorporatePortal.Models.CardLoad;
using CorporatePortal.Models.CardOrders;
using CorporatePortal.Models.ChangeCardStatus;
using CorporatePortal.Models.User;
using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    [Authorize(Roles = "Admin,CorporateAdmin")]
    public class LoadController : Controller
    {
        IApiService _apiService;

        public LoadController(IApiService apiService)
        {
            _apiService = apiService;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }

        public ActionResult Home()
        {
            var products = _apiService.GetProducts();

            var model = new LoadPageModel
            {
                ProductsList = GetProductsDdl(products.ProductList),
                TypeList = GetTypesDdl()
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Home(LoadPageModel model)
        {
            var products = _apiService.GetProducts();

            model.ProductsList = GetProductsDdl(products.ProductList);
            model.TypeList = GetTypesDdl();
            var list = _apiService.GetManufacturerCardsByProduct(model.productCode, model.status);

            model.vcards = list.VirtualCards;
            model.activecards = list.ActiveCards;
            model.inactivecards = list.InactiveCards;
            return View(model);
        }

        public ActionResult CardDetails(string productCode, string status, string cardProxyId)
        {
            var model = new LoadPageModel();
            model.productCode = productCode;
            model.status = status;
           
            var products = _apiService.GetProducts();

            model.ProductsList = GetProductsDdl(products.ProductList,productCode);
            
            model.TypeList = GetTypesDdl(status);
            var list = _apiService.GetManufacturerCardsByProduct(productCode, status);

            if(list.VirtualCards != null)
            {
                var vcards = list.VirtualCards;
                var index = vcards.FindIndex(x => x.CardProxyId == cardProxyId);
                var item = vcards[index];
                vcards[index] = vcards[0];
                vcards[0] = item;

                model.vcards = vcards;
            }

            if (list.ActiveCards != null)
            {
                var activeCards = list.ActiveCards;
                var index = activeCards.FindIndex(x => x.CardProxyId == cardProxyId);
                var item = activeCards[index];
                activeCards[index] = activeCards[0];
                activeCards[0] = item;

                model.activecards = activeCards;
            }

            if (list.InactiveCards != null)
            {
                var inActiveCards = list.InactiveCards;
                var index = inActiveCards.FindIndex(x => x.CardProxyId == cardProxyId);
                var item = inActiveCards[index];
                inActiveCards[index] = inActiveCards[0];
                inActiveCards[0] = item;

                model.inactivecards = inActiveCards;
            }
            return View(model);
        }        

        private static List<SelectListItem> GetProductsDdl(List<Models.Product.QcsProduct> productList, string productCode = null)
        {
            List<SelectListItem> products = (from p in productList.AsEnumerable().Where(t=>t.FundingSource == Models.Product.FundingSource.Corporate_Load_Pot)
                                             select new SelectListItem
                                             {
                                                 Text = p.ProductCode,
                                                 Value = p.ProductCode
                                             }).ToList();


            if(productCode != null)
            {
                var selected = products.Where(x => x.Text == productCode).First();
                selected.Selected = true;
                products.Insert(0, new SelectListItem { Text = "--Select Product--", Value = "", Disabled = true, Selected = false });
                return products;
            }

            products.Insert(0, new SelectListItem { Text = "--Select Product--", Value = "", Disabled = true, Selected = true });

            return products;
        }

        private static List<SelectListItem> GetTypesDdl(string status = null)
        {

            List<SelectListItem> types = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "Active",
                                                 Value = "Active"
                                             },
                new SelectListItem
                                             {
                                                Text = "Inactive",
                                                 Value = "Inactive"
                                             }
            }.ToList();

            if (status != null)
            {
                var selected = types.Where(x => x.Text == status).First();
                selected.Selected = true;
                types.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = false });
                return types;
            }

            //Add Default Item at First Position.
            types.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            return types;
        }
                     

        public ActionResult LoadCard(string cardProxyId, string productCode, string status)
        {
            var accounts = _apiService.GetAccountsList();
            var LoadCardVM = new LoadCardVM
            {
                productCode = productCode,
                CardProxyId = cardProxyId,
                CorporateAccounts = accounts.AccountsList,
                status = status
            };

            return View(LoadCardVM);
        }

        [HttpPost]
        public ActionResult LoadCard(LoadCardVM model)
        {
            var request = new LoadCardRequest
            {
                CardProxyId = model.CardProxyId,
                Amount = model.Amount,
                CorporateAccountId = model.AccountId
            };

            var response = _apiService.LoadCardWithCorporateAccount(request);

            if (response.IsSuccess == true)
            {
                var cardDetails = _apiService.GetCardInformation(model.CardProxyId);               

                TempData["Success"] = String.Format("CardId : {0} : loaded successfully with amount of {1}{2}. New balance is {3}{4}", model.CardProxyId, cardDetails.Currency, model.Amount,cardDetails.Currency,cardDetails.Balance);
                return RedirectToAction("CardDetails", "Load", new
                {
                    productCode = model.productCode,
                    status = model.status,
                    cardproxyId = model.CardProxyId
                });
            }

            var accounts = _apiService.GetAccountsList();
            var LoadCardVM = new LoadCardVM
            {
                productCode = model.productCode,
                CardProxyId = model.CardProxyId,
                CorporateAccounts = accounts.AccountsList,
                status = model.status
            };

            if (response.ErrorCodes[0] == "10000000")
            {
                ModelState.AddModelError("", "Card not loaded. Error : No card found with id supplied.");
                TempData["FailureMessage"] = "Card not loaded. Error : No card found with id: supplied!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000000")
            {
                ModelState.AddModelError("", "Card not loaded. Error : No corporate account found with id supplied.");
                TempData["FailureMessage"] = "Card not loaded. Error : No corporate account found with id: supplied!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000003")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Insufficient funds in corporate account.");
                TempData["FailureMessage"] = "Card not loaded. Error : Insufficient funds in corporate account!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000004")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Account currency does not match card currency.");
                TempData["FailureMessage"] = "Card not loaded. Error : Account currency does not match card currency!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000005")
            {
                ModelState.AddModelError("", "Card not loaded. Error : No country card load information found for card.");
                TempData["FailureMessage"] = "Card not loaded. Error : No country card load information found for card!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000006")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Max load limit for non-kyc exceeded.");
                TempData["FailureMessage"] = "Card not loaded. Error : Max load limit for non-kyc exceeded!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000007")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Max corporate balance allowed exceeded.");
                TempData["FailureMessage"] = "Card not loaded. Error : Max corporate balance allowed exceeded!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000008")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Max daily balance allowed exceeded.");
                TempData["FailureMessage"] = "Card not loaded. Error : Max daily balance allowed exceeded!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000009")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Max daily load allowed exceeded.");
                TempData["FailureMessage"] = "Card not loaded. Error : Max daily load allowed exceeded!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000010")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Max weekly load allowed exceeded.");
                TempData["FailureMessage"] = "Card not loaded. Error : Max weekly load allowed exceeded!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000011")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Max monthly load allowed exceeded.");
                TempData["FailureMessage"] = "Card not loaded. Error : Max monthly load allowed exceeded!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000012")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Max yearly load allowed exceeded.");
                TempData["FailureMessage"] = "Card not loaded. Error : Max yearly load allowed exceeded!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000013")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Card not reloadable. Already been loaded");
                TempData["FailureMessage"] = "Card not loaded. Error : Card not reloadable. Already been loaded!";
                return View(LoadCardVM);
            }

            if (response.ErrorCodes[0] == "17000014")
            {
                ModelState.AddModelError("", "Card not loaded. Error : Product not setup to allow load without activation first.");
                TempData["FailureMessage"] = "Card not loaded. Error : Product not setup to allow load without activation first!";
                return View(LoadCardVM);
            }

            TempData["FailureMessage"] = String.Format("There was a problem loading the card! Please try again. {0}",response);
            return View(LoadCardVM);

        }

        public ActionResult BatchLoadCards()
        {
            var model = new BatchLoadCards();
            return View(model);
        }

        public ActionResult UnloadCard(string cardProxyId, string productCode)
        {
            var accounts = _apiService.GetAccountsList();
            var LoadCardVM = new LoadCardVM
            {
                productCode = productCode,
                CardProxyId = cardProxyId,
                CorporateAccounts = accounts.AccountsList
            };

            return View(LoadCardVM);
        }

        [HttpPost]
        public ActionResult UnloadCard(LoadCardVM model)
        {
            var request = new UnloadCardRequest
            {
                CardProxyId = model.CardProxyId,
                Amount = model.Amount,
                CorporateAccountId = model.AccountId
            };

            var response = _apiService.UnloadCardWithCorporateAccount(request);

            //if (model.productCode != null)
            //{
            //    var list = _apiService.GetManufacturerCardsByProduct(model.productCode);
            //    list.ProductCode = model.productCode;
            //    var accounts = _apiService.GetCorporateAccountsFundsSummary();
            //    list.AccountsList = accounts.AccountsList;
            //    return View("LoadedCards", list);
            //}

            return RedirectToAction("Index");

        }

        [HttpPost]
        public ActionResult BatchLoadCards(BatchLoadCards cardFile)
        {
            var checkExtension = Path.GetExtension(cardFile.postedFile.FileName).ToLower();

            if(checkExtension != "csv")
            {
                ModelState.AddModelError("","Invalid file format. Please upload a csv file!");
                TempData["FailureMessage"] = "Invalid file format. Please upload a csv file!";
                return View(cardFile);
            }
            //var usersList = new List<ApiUserDetails>();
            var invalidRecordsModel = new InvalidBatchOrderRecords { Records = new List<string>() };
            var cardOrders = new List<OrderCardRequest>();
            var loads = new List<LoadCardRequest>();
            StreamReader csvReader = new StreamReader(cardFile.postedFile.InputStream);
            {
                string inputLine = "";

                List<string> Lines = new List<string>();

                while ((inputLine = csvReader.ReadLine()) != null)
                {
                    Lines.Add(inputLine);

                    string[] values = inputLine.Split(new Char[] { ',' });
                    
                    if (values.Length < 4)
                    {
                        invalidRecordsModel.Records.Add(inputLine);
                        Lines.Remove(inputLine);
                        continue;
                    }

                    if (values.Length == 4)
                    {
                        if (string.IsNullOrEmpty(values[0]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }
                        if (string.IsNullOrEmpty(values[1]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }
                        if (string.IsNullOrEmpty(values[2]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }
                        if (string.IsNullOrEmpty(values[3]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        loads.Add(new LoadCardRequest
                        {
                            CorporateAccountId = Convert.ToInt16(values[1]),
                            CardProxyId = values[2],
                            Amount = Convert.ToDecimal(values[3])
                        });
                    }
                }
                csvReader.Close();
            }

            if (invalidRecordsModel.Records.Count > 0)
            {
                TempData["FailureMessage"] = "Invalid File";

                return View("InvalidBatchLoadFile", invalidRecordsModel);
            }

            var request = new BatchLoadCardsRequest
            {
                Loads = loads,
                CorporateAccountId = loads[0].CorporateAccountId,
                FileName = Path.GetFileNameWithoutExtension(cardFile.postedFile.FileName)
            };


            var response = _apiService.BatchLoadCards(request);

            if (response.IsSuccess)
            {
                return RedirectToAction("BatchLoadHistory");
            }

            return View("Error");

            //var list = _apiService.GetManufacturerCardsByProduct(request.productCode);
            //list.ProductCode = request.productCode;
            //var accounts = _apiService.GetCorporateAccountsFundsSummary();
            //list.AccountsList = accounts.AccountsList;
            //return View("LoadedCards", list);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BatchLoadCardsExcel(BatchLoadCards cardFile)
        {
            if (cardFile.postedFile == null || cardFile.postedFile.ContentLength < 0)
            {
                ModelState.AddModelError("", "Invalid file!");
                TempData["FailureMessage"] = "Invalid file!";
                return View("BatchLoadCards",cardFile);
            }

            var loads = new List<LoadCardRequest>();

            if (cardFile.postedFile != null && cardFile.postedFile.ContentLength > 0)
            {
                //ExcelDataReader works on binary excel file
                Stream stream = cardFile.postedFile.InputStream;
                //We need to written the Interface.
                IExcelDataReader reader = null;
                if (cardFile.postedFile.FileName.EndsWith(".xls"))
                {
                    //reads the excel file with .xls extension
                    reader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else if (cardFile.postedFile.FileName.EndsWith(".xlsx"))
                {
                    //reads excel file with .xlsx extension
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    //Shows error if uploaded file is not Excel file
                    ModelState.AddModelError("", "Invalid file format. Please upload an excel file!");
                    TempData["FailureMessage"] = "Invalid file format. Please upload an excel file!";
                    return View("BatchLoadCards",cardFile);
                }
                //treats the first row of excel file as Coluymn Names
                reader.IsFirstRowAsColumnNames = true;
                //Adding reader data to DataSet()
                DataSet result = reader.AsDataSet();
                reader.Close();

                var table = result.Tables[0];

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[i].ToString()))
                    {
                        table.Rows.RemoveAt(i);
                    }
                }

                var invalidRows = new List<DataRow>();

                var invalidRecords = table.Clone();

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    //row id
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[0].ToString()))
                    {
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    //productcode
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[1].ToString()))
                    {

                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    //first name
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[2].ToString()))
                    {

                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    //last name
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[3].ToString()))
                    {

                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    loads.Add(new LoadCardRequest
                    {
                        CorporateAccountId = Convert.ToInt16(table.Rows[i].ItemArray[1].ToString()),
                        CardProxyId = table.Rows[i].ItemArray[2].ToString(),
                        Amount = Convert.ToDecimal(table.Rows[i].ItemArray[3].ToString())
                    });
                }

                if (invalidRecords.Rows.Count > 0)
                {
                    //Shows error if uploaded file has invalid records
                    ModelState.AddModelError("", "Invalid records in file!");
                    TempData["FailureMessage"] = "Invalid records in file!";

                    return View("InvalidBatchLoadExcel", invalidRecords);
                }
            }

            var request = new BatchLoadCardsRequest
            {
                Loads = loads,
                CorporateAccountId = loads[0].CorporateAccountId,
                FileName = Path.GetFileNameWithoutExtension(cardFile.postedFile.FileName)
            };

            var response = _apiService.BatchLoadCards(request);

            if (response.IsSuccess)
            {
                return RedirectToAction("BatchLoadHistory");
            }

            return View("Error");
        }

        public ActionResult BatchLoadHistory()
        {
            var response = _apiService.GetLoadHistory();

            //var list = (from it in model.LoadList
            //            group it by new { it.BatchLoadFileName, it.CreatedDate }
            //into g
            //            select new BatchLoadHistoryPageModel { FileName = g.Key.BatchLoadFileName, CreatedDate=g.Key.CreatedDate,  TotalAmount = g.Sum(item => item.Amount), TotalCards = g.Count(), List = model.LoadList }).ToList();

            var model = new List<BatchLoadHistoryPageModel>();

            foreach (var file in response.LoadList)
            {
                model.Add(new BatchLoadHistoryPageModel
                {
                    Id=file.Id,
                    FileName = file.FileName,
                    FileStatus = file.FileStatus,
                    CreatedDate = file.CreatedDate,
                    UpdatedDate = file.UpdatedDate,
                    TotalCards = file.TotalCards,
                    TotalAmount = file.TotalAmount,
                    
                }
                );
            }

            return View(model);
        }

        public ActionResult BatchLoadDetails(int fileId, string fileName)
        {
            var response = _apiService.GetBatchlaodDetails(fileId, fileName);

            var model = response.LoadList;

            return View(model);
        }

        public ActionResult ProcessBatchLoad(int fileId, string fileName)
        {
            var request = new ProcessBatchLoadRequest
            {
                fileId = fileId
            };

            var response = _apiService.ProcessBatchLoad(request);

            return RedirectToAction("BatchLoadDetails", new { fileId = fileId, fileName = fileName });
        }      

        public ActionResult TransferFunds(string cardProxyId, string productCode)
        {
            var cardHolderId = _apiService.GetCardholderFromProxy(cardProxyId).Card.CardholderId;

            var cardFrom = _apiService.GetCardInformation(cardProxyId);

            var model = new TransferFundsPageModel();
            model.cardProxyIdFrom = cardProxyId;
            model.productCode = productCode;
            model.cardholderId = cardHolderId;
            model.cardProxyIdFromBalance = cardFrom.Balance;
            model.cardProxyIdFromStatus = cardFrom.Status;
           

            return View(model);
        }

        [HttpPost]
        public ActionResult TransferFunds(TransferFundsPageModel model)
        {
            var response = _apiService.GetCardProxyIdByPAN(model.PAN);

            if (response == null)
            {
                ModelState.AddModelError("", "CardProxyId not found for PAN!");
                TempData["FailureMessage"] = "CardProxyId not found for PAN!";

                return View();
            }

            if (!response.IsSuccess)
            {
                ModelState.AddModelError("", "CardProxyId not found!");
                TempData["FailureMessage"] = "CardProxyId not found!";

                return View();
            }



            return RedirectToAction("TransferFundsBetweenCards", new { cardproxyIdFrom = model.cardProxyIdFrom, cardProxyIdFromStatus = model.cardProxyIdFromStatus, cardProxyIdFromBalance = model.cardProxyIdFromBalance,productCode = model.productCode, cardholderId = model.cardholderId, cardProxyIdTo = response.CardProxyId,PAN = model.PAN});

        }

        public ActionResult TransferFundsBetweenCards(string cardProxyIdFrom, string cardProxyIdFromStatus, double cardProxyIdFromBalance, string productCode, string cardholderId, string cardProxyIdTo, string PAN)
        {
            var model = new TransferFundsPageModel
            {
                cardholderId = cardholderId,
                cardProxyIdFrom = cardProxyIdFrom,
                cardProxyIdFromStatus = cardProxyIdFromStatus,
                cardProxyIdFromBalance = cardProxyIdFromBalance,
                PAN = PAN,
                productCode = productCode,
                cardProxyIdTo = cardProxyIdTo
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult TransferFundsBetweenCards(TransferFundsPageModel model)
        {

            var request = new CardToCardTransferRequest
            {
                Amount = model.Amount,
                FromCardProxyId = model.cardProxyIdFrom,
                ToCardProxyId = model.cardProxyIdTo,
                CardHolderId = model.cardholderId
            };

            var response = _apiService.CardToCardTransfer(request);

            TempData["SuccessMessage"] = "Funds transferred successfully.!";
            TempData["Success"] = String.Format("Funds successfulyl transferred from : CardId :{0} ", model.cardProxyIdFrom );


            var cardTo = _apiService.GetCardInformation(model.cardProxyIdTo);

            model.cardProxyIdToBalance = cardTo.Balance;

            return View("TransferFundsSuccess", model);


            //return RedirectToAction("CardDetails", new { productCode = model.productCode, status = model.cardProxyIdFromStatus, cardProxyId = model.cardProxyIdFrom });
        }
        
        public ActionResult DownloadFile()
        {
            try
            {
                string fullPath = Path.Combine(Server.MapPath("/Files"), "BatchLoadTemplate.xlsx");
                return File(fullPath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "BatchLoadTemplate.xlsx");
            }
            catch (Exception ex)
            {
                TempData["FailureMessage"] = "Error in downloading template!";
                return View("BatchLoadCards");
            }
        }
    }
}