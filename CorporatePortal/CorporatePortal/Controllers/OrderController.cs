﻿using CorporatePortal.Api;
using CorporatePortal.Api.Requests;
using CorporatePortal.Api.Requests.CardOrder;
using CorporatePortal.Api.Responses;
using CorporatePortal.Models;
using CorporatePortal.Models.CardLoad;
using CorporatePortal.Models.CardOrders;
using CorporatePortal.Models.User;
using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager,CorporateEmployee")]
    public class OrderController : Controller
    {
        IApiService _apiService;

        public OrderController(IApiService apiService)
        {
          _apiService = apiService;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }

        public ActionResult Index()
        {
            var batchOrdersList = _apiService.GetOrderHistory().Orders.OrderByDescending(o => o.BatchOrderId).Reverse().ToList();
            var individualOrdersList = _apiService.GetIndividualOrderHistory().Orders;
            var virtualCards = _apiService.GetVirtualCards();
            var accounts = _apiService.GetCorporateAccountsFundsSummary();
            var model = new OrderHomePageModel
            {
                batchOrders = batchOrdersList,
                individualOrders = individualOrdersList,
                AccountsList = accounts.AccountsList,
                VirtualCards = virtualCards.VirtualCards
            };

            return View(model);
        }

        #region BatchOrder
        public ActionResult BatchOrderCards()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BatchOrderCardsExcel(BatchOrderCards cardFile)
        {
            if (cardFile.postedFile == null || cardFile.postedFile.ContentLength < 0)
            {
                ModelState.AddModelError("", "Invalid file!");
                TempData["FailureMessage"] = "Invalid file!";
                return View("BatchOrderCards", cardFile);
            }

            var cardOrders = new List<OrderIndividualCardRequest>();

            if (cardFile.postedFile != null && cardFile.postedFile.ContentLength > 0)
            {
                //ExcelDataReader works on binary excel file
                Stream stream = cardFile.postedFile.InputStream;
                //We need to written the Interface.
                IExcelDataReader reader = null;
                if (cardFile.postedFile.FileName.EndsWith(".xls"))
                {
                    //reads the excel file with .xls extension
                    reader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else if (cardFile.postedFile.FileName.EndsWith(".xlsx"))
                {
                    //reads excel file with .xlsx extension
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    //Shows error if uploaded file is not Excel file
                    ModelState.AddModelError("", "Invalid file format. Please upload an excel file!");
                    TempData["FailureMessage"] = "Invalid file format. Please upload an excel file!";
                    return View("BatchOrderCards",cardFile);
                }
                //treats the first row of excel file as Coluymn Names
                reader.IsFirstRowAsColumnNames = true;
                //Adding reader data to DataSet()
                DataSet result = reader.AsDataSet();
                reader.Close();

                var table = result.Tables[0];
                
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[i].ToString()))
                    {
                        table.Rows.RemoveAt(i);
                    }
                }

                var invalidRows = new List<DataRow>();

                var invalidRecords = table.Clone();

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    //row id
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[0].ToString()))
                    {                       
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    //productcode
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[1].ToString()))
                    {
                      
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }
                    
                    //card design
                    //if (string.IsNullOrEmpty(values[2]))
                    //{
                    //    invalidRecordsModel.Records.Add(inputLine);
                    //    Lines.Remove(inputLine);
                    //    continue;
                    //}


                    //first name
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[3].ToString()) || (table.Rows[i].ItemArray[3].ToString().Length > 20))
                    {
                       
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);

                        continue;
                    }

                    //last name
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[4].ToString()) || (table.Rows[i].ItemArray[4].ToString().Length > 20))
                    {
                        
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    //NameOnCard
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[5].ToString()) || (table.Rows[i].ItemArray[5].ToString().Length > 20))
                    {
                        
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    //// AddtionalEmbossCardData
                    //if (string.IsNullOrEmpty(table.Rows[i].ItemArray[6].ToString()))
                    //{

                    //    invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                    //    continue;
                    //}


                    //dob
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[7].ToString()))
                    {
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;                           
                    }
                    else
                    {
                        var dob = table.Rows[i].ItemArray[7].ToString();
                        DateTime dt;
                        if (DateTime.TryParseExact(dob, "dd/MM/yyyy", null, DateTimeStyles.None, out dt) != true)
                        {
                            invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                            continue;
                        }
                    }

                    //email
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[8].ToString()) || (table.Rows[i].ItemArray[8].ToString().Length > 64))
                    {
                        
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    //mobile
                    //if (string.IsNullOrEmpty(values[9]))
                    //{
                    //    invalidRecordsModel.Records.Add(inputLine);
                    //    Lines.Remove(inputLine);
                    //    continue;
                    //}

                    //address1
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[10].ToString()) || (table.Rows[i].ItemArray[10].ToString().Length > 35))
                    {
                   
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    ////address2
                    //if (string.IsNullOrEmpty(values[11]))
                    //{
                    //    invalidRecordsModel.Records.Add(inputLine);
                    //    Lines.Remove(inputLine);
                    //    continue;
                    //}

                    ////address3
                    //if (string.IsNullOrEmpty(values[12]))
                    //{
                    //    invalidRecordsModel.Records.Add(inputLine);
                    //    Lines.Remove(inputLine);
                    //    continue;
                    //}

                    //city
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[13].ToString()) || (table.Rows[i].ItemArray[13].ToString().Length > 20))
                    {
                        
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    ////state
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[14].ToString()) || (table.Rows[i].ItemArray[14].ToString().Length > 20))
                    {

                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    //country code
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[15].ToString()) || (table.Rows[i].ItemArray[15].ToString().Length != 3))
                    {
                        
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                    //postcode
                    if (string.IsNullOrEmpty(table.Rows[i].ItemArray[16].ToString()) || (table.Rows[i].ItemArray[16].ToString().Length > 10))
                    {
                        
                        invalidRecords.Rows.Add(table.Rows[i].ItemArray);
                        continue;
                    }

                   

                    cardOrders.Add(new OrderIndividualCardRequest
                    {
                        ProductCode = table.Rows[i].ItemArray[1].ToString(),
                        CardDesign = table.Rows[i].ItemArray[2].ToString(),
                        FirstName = table.Rows[i].ItemArray[3].ToString(),
                        LastName = table.Rows[i].ItemArray[4].ToString(),
                        NameOnCard = table.Rows[i].ItemArray[5].ToString(),
                        AdditionalCardEmbossData = table.Rows[i].ItemArray[6].ToString(),
                        Dob = table.Rows[i].ItemArray[7].ToString(),
                        RecipientEmail = table.Rows[i].ItemArray[8].ToString(),
                        Mobile = table.Rows[i].ItemArray[9].ToString(),
                        Address1 = table.Rows[i].ItemArray[10].ToString(),
                        Address2 = table.Rows[i].ItemArray[11].ToString(),
                        Address3 = table.Rows[i].ItemArray[12].ToString(),
                        City = table.Rows[i].ItemArray[13].ToString(),
                        State = table.Rows[i].ItemArray[14].ToString(),
                        ISOCountryCode = table.Rows[i].ItemArray[15].ToString(),
                        PostCode = table.Rows[i].ItemArray[16].ToString(),
                        cardFileName = cardFile.postedFile.FileName,
                        
                    });
                }

                if (invalidRecords.Rows.Count > 0)
                {
                    //Shows error if uploaded file has invalid records
                    ModelState.AddModelError("", "Invalid records in file!");
                    TempData["FailureMessage"] = "Invalid records in file!";

                    return View("InvalidBatchOrderExcel", invalidRecords);
                }
            }

            var model = new BatchOrder
            {
                CardOrders = cardOrders
            };

            return BatchOrderSummary(model);
        }

        [HttpPost]
        public ActionResult BatchOrderCards(BatchOrderCards cardFile)
        {
            var usersList = new List<CardHolderDetails>();
            var invalidRecordsModel = new InvalidBatchOrderRecords { Records = new List<string>() };
            var cardOrders = new List<OrderIndividualCardRequest>();

            StreamReader csvReader = new StreamReader(cardFile.postedFile.InputStream);
            {
                string inputLine = "";
                string productCode = "";
                List<string> Lines = new List<string>();

                while ((inputLine = csvReader.ReadLine()) != null)
                {
                    Lines.Add(inputLine);

                    string[] values = inputLine.Split(new Char[] { ',' });
                    if (values.Length < 14)
                    {
                        invalidRecordsModel.Records.Add(inputLine);
                        Lines.Remove(inputLine);
                        continue;
                    }

                    if (values.Length == 14)
                    {
                        //row id
                        if (string.IsNullOrEmpty(values[0]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        //productcode
                        if (string.IsNullOrEmpty(values[1]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        //first name
                        if (string.IsNullOrEmpty(values[2]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        //last name
                        if (string.IsNullOrEmpty(values[3]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        //NameOnCard
                        if (string.IsNullOrEmpty(values[4]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        //dob
                        if (string.IsNullOrEmpty(values[5]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        //email
                        if (string.IsNullOrEmpty(values[6]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        //mobile
                        //if (string.IsNullOrEmpty(values[7]))
                        //{
                        //    invalidRecordsModel.Records.Add(inputLine);
                        //    Lines.Remove(inputLine);
                        //    continue;
                        //}

                        //address1
                        if (string.IsNullOrEmpty(values[8]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        ////address2
                        //if (string.IsNullOrEmpty(values[9]))
                        //{
                        //    invalidRecordsModel.Records.Add(inputLine);
                        //    Lines.Remove(inputLine);
                        //    continue;
                        //}

                        //city
                        if (string.IsNullOrEmpty(values[10]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        ////state
                        //if (string.IsNullOrEmpty(values[11]))
                        //{
                        //    invalidRecordsModel.Records.Add(inputLine);
                        //    Lines.Remove(inputLine);
                        //    continue;
                        //}

                        //country code
                        if (string.IsNullOrEmpty(values[12]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        //postcode
                        if (string.IsNullOrEmpty(values[13]))
                        {
                            invalidRecordsModel.Records.Add(inputLine);
                            Lines.Remove(inputLine);
                            continue;
                        }

                        cardOrders.Add(new OrderIndividualCardRequest
                        {
                            ProductCode = values[1],
                            FirstName = values[2],
                            LastName = values[3],
                            NameOnCard = values[4],
                            Dob = values[5],
                            RecipientEmail= values[6],
                            Mobile = values[7],
                            Address1 = values[8],
                            Address2 = values[9],
                            City = values[10],
                            State = values[11],
                            ISOCountryCode =values[12],
                            PostCode = values[13],
                            cardFileName = cardFile.postedFile.FileName,
                            AdditionalCardEmbossData = values[14]
                        });
                    }
                }
                csvReader.Close();
            }

            if (invalidRecordsModel.Records.Count > 0)
            {
                TempData["FailureMessage"] = "Invalid File";

                return View("InvalidBatchOrderFile", invalidRecordsModel);
            }

            var model = new BatchOrder
            {
                CardOrders = cardOrders
            };

            return BatchOrderSummary(model);
        }

        public ActionResult BatchOrderSummary(BatchOrder model)
        {
            TempData["SuccessMessage"] = "Valid File. Submit to process!";
            return View("BatchOrderSummary", model);
        }

        [HttpPost]
        public ActionResult SubmitOrder(BatchOrder batchOrder)
        {
            var request = new BatchOrderCardRequest
            {
                Orders = batchOrder.CardOrders
            };

            var response = _apiService.BatchOrderCards(request);

            if (!response.IsSuccess)
            {

            }

            TempData["SuccessMessage"] = "Submitted Successfully";
            TempData["Success"] = String.Format("Batch order submitted successfully.  BatchOrderId : {0}",response.Id);
            return RedirectToAction("BatchOrderHistory");
        }

        public ActionResult BatchOrderHistory()
        {
            var response = _apiService.GetOrderHistory();

            if (!response.IsSuccess)
            {

            }

            var model = response.Orders.OrderByDescending(o => o.BatchOrderId).Reverse();
            
            return View(model);
        }

        public ActionResult GetBatchOrderDetails(int batchOrderId)
        {
            var response = _apiService.GetBatchOrderDetails(batchOrderId);

            if (!response.IsSuccess)
            {

            }

            return View(response.Orders);
        }

        public ActionResult CancelBatchOrder(int batchOrderId)
        {
            //var response = _apiService.GetBatchOrderDetails(batchOrderId);
            return RedirectToAction("BatchOrderHistory");
        }
        #endregion

        #region IndividualOrder

        public ActionResult IndividualCardOrder(string cardholderId)
        {
            var model = new OrderIndividualCardPageModel();

            var response = _apiService.GetProducts();

            var products = response.ProductList.Where(p => p.IsVirtual != true).ToList();

            model.Products = GetProductsDdl(products);

            model.Countries = GetCountrySelectListItems();

            if (cardholderId != null)
            {
                var getCardholderResponse = _apiService.GetCardHolderDetails(cardholderId);
                var cardHolder = getCardholderResponse.User;
                model.Address1 = cardHolder.Address1;
                model.Address2 = cardHolder.Address2;
                model.EmailAddress = cardHolder.EmailAddress;
                model.City = cardHolder.City;
                model.DateOfBirth = cardHolder.DateOfBirth;
                model.FirstName = cardHolder.FirstName;
                model.LastName = cardHolder.LastName;
                model.Mobile = cardHolder.Mobile;
                model.PostCode = cardHolder.PostCode;
                model.State = cardHolder.State;
                model.CardholderId = cardHolder.Id;
                
                var selectedCountry = model.Countries.Where(x => x.Value == cardHolder.ISOCountryCode).First();
                selectedCountry.Selected = true;
            }
           

            return View(model);
        }

        [HttpPost]
        public ActionResult IndividualCardOrder(OrderIndividualCardPageModel model)
        {
            if (!ModelState.IsValid)
            {
                var productList = _apiService.GetProducts();

                var products = productList.ProductList.Where(p => p.IsVirtual != true).ToList();

                model.Products = GetProductsDdl(products);

                model.Countries = GetCountrySelectListItems();

                return View(model);
            }

            var request = new OrderIndividualCardRequest
            {
                CardholderId = model.CardholderId,
                ProductCode = model.ProductCode,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address1 = model.Address1,
                Address2 = model.Address2,
                PostCode = model.PostCode,
                City = model.City,
                ISOCountryCode = model.ISOCountryCode,
                RecipientEmail = model.EmailAddress,
                Dob = model.DateOfBirth.ToShortDateString(),
                State = model.State,
                Mobile = model.Mobile,
                NameOnCard = model.NameOnCard,
                AdditionalCardEmbossData = model.AdditionalCardEmbossData,
                Address3 = model.Address3,
                CardDesign = model.CardDesign
            };

            var response = _apiService.OrderCard(request);
           
            if (!response.IsSuccess)
            {

            }

            TempData["Success"] = String.Format("Order submitted successfully. OrderId : {0}", response.Id);
            return RedirectToAction("InvidualCardOrderHistory");
        }

        public ActionResult IndividualCardOrderExistingCardholder(string cardholderId)
        {
            var model = new OrderIndividualCardPageModel();

            var response = _apiService.GetProducts();

            var products = response.ProductList.Where(p => p.IsVirtual != true).ToList();

            model.Products = GetProductsDdl(products);

            model.Countries = GetCountrySelectListItems();

            if (cardholderId != null)
            {
                var getCardholderResponse = _apiService.GetCardHolderDetails(cardholderId);
                var cardHolder = getCardholderResponse.User;
                model.Address1 = cardHolder.Address1;
                model.Address2 = cardHolder.Address2;
                model.EmailAddress = cardHolder.EmailAddress;
                model.City = cardHolder.City;
                model.DateOfBirth = cardHolder.DateOfBirth;
                //model.FirstName = cardHolder.FirstName;
                //model.LastName = cardHolder.LastName;
                model.Mobile = cardHolder.Mobile;
                model.PostCode = cardHolder.PostCode;
                model.State = cardHolder.State;
                model.CardholderId = cardHolder.Id;
                model.NameOnCard = cardHolder.FirstName + " " + cardHolder.LastName;

                var selectedCountry = model.Countries.Where(x => x.Value == cardHolder.ISOCountryCode).First();
                selectedCountry.Selected = true;
            }


            return View("IndividualCardOrderExistingCardholder", model);
        }

        [HttpPost]
        public ActionResult IndividualCardOrderExistingCardholder(OrderIndividualCardPageModel model)
        {
            if (!ModelState.IsValid)
            {
                var productList = _apiService.GetProducts();

                var products = productList.ProductList.Where(p => p.IsVirtual != true).ToList();

                model.Products = GetProductsDdl(products);

                model.Countries = GetCountrySelectListItems();

                return View("IndividualCardOrderExistingCardholder", model);
            }

            var request = new OrderIndividualCardRequest
            {
                CardholderId = model.CardholderId,
                ProductCode = model.ProductCode,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address1 = model.Address1,
                Address2 = model.Address2,
                PostCode = model.PostCode,
                City = model.City,
                ISOCountryCode = model.ISOCountryCode,
                RecipientEmail = model.EmailAddress,
                Dob = model.DateOfBirth.ToShortDateString(),
                State = model.State,
                Mobile = model.Mobile,
                NameOnCard = model.NameOnCard,
                AdditionalCardEmbossData = model.AdditionalCardEmbossData,
                Address3 = model.Address3,
                CardDesign = model.CardDesign
            };

            var response = _apiService.OrderCard(request);

            if (!response.IsSuccess)
            {

            }

            TempData["Success"] = String.Format("Order submitted successfully. OrderId : {0}", response.Id);
            return RedirectToAction("InvidualCardOrderHistory");
        }

        [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager,CorporateEmployee")]
        public ActionResult InvidualCardOrderHistory()
        {
            var response = _apiService.GetIndividualOrderHistory();

            if (!response.IsSuccess)
            {

            }

            var model = response.Orders;

            return View(model);
        }

        #endregion

        #region VirtualCardOrder

        public ActionResult OrderVirtualCard()
        {
            var model = new OrderVirtualCardPageModel();
            
            model.cardholderTypeList = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "New",
                                                 Value = "New"
                                             },
                new SelectListItem
                                             {
                                                Text = "Existing",
                                                 Value = "Existing"
                                             }
            }.ToList();

            model.cardholderTypeList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });

            return View(model);



            //var products = _apiService.GetProducts().ProductList.Where(p=>p.IsVirtual==true).ToList();
            
            //var model = new CreateCardHolderPageModel();
            //model.Countries = GetCountrySelectListItems();
            //model.Products = GetProductsDdl(products);
            //return View(model);
        }

        private static List<SelectListItem> GetProductsDdl(List<Models.Product.QcsProduct> productList)
        {
            List<SelectListItem> products = (from p in productList.AsEnumerable()
                                             select new SelectListItem
                                             {
                                                 Text = p.ProductCode,
                                                 Value = p.ProductCode
                                             }).ToList();



            products.Insert(0, new SelectListItem { Text = "--Select Product--", Value = "", Disabled = true, Selected = true });
            return products;
        }

        public List<SelectListItem> GetCountrySelectListItems()
        {
           var countries = GetCountries().Select(s => new SelectListItem
            {
                Value = s.Alpha3,
                Text = s.Name
            }).ToList();

            countries.Insert(0, new SelectListItem { Text = "--Select Country--", Value = "", Disabled = true, Selected = true });
            return countries;

        }

        private IEnumerable<Country> GetCountries()
        {
            return new List<Country>
            {
                new Country("United Kingdom of Great Britain", "GB", "GBR", 826),
                

                new Country("Afghanistan", "AF", "AFG", 4),
                new Country("Åland Islands", "AX", "ALA", 248),
                new Country("Albania", "AL", "ALB", 8),
                new Country("Algeria", "DZ", "DZA", 12),
                new Country("American Samoa", "AS", "ASM", 16),
                new Country("Andorra", "AD", "AND", 20),
                new Country("Angola", "AO", "AGO", 24),
                new Country("Anguilla", "AI", "AIA", 660),
                new Country("Antarctica", "AQ", "ATA", 10),
                new Country("Antigua and Barbuda", "AG", "ATG", 28),
                new Country("Argentina", "AR", "ARG", 32),
                new Country("Armenia", "AM", "ARM", 51),
                new Country("Aruba", "AW", "ABW", 533),
                new Country("Australia", "AU", "AUS", 36),
                new Country("Austria", "AT", "AUT", 40),
                new Country("Azerbaijan", "AZ", "AZE", 31),
                new Country("Bahamas", "BS", "BHS", 44),
                new Country("Bahrain", "BH", "BHR", 48),
                new Country("Bangladesh", "BD", "BGD", 50),
                new Country("Barbados", "BB", "BRB", 52),
                new Country("Belarus", "BY", "BLR", 112),
                new Country("Belgium", "BE", "BEL", 56),
                new Country("Belize", "BZ", "BLZ", 84),
                new Country("Benin", "BJ", "BEN", 204),
                new Country("Bermuda", "BM", "BMU", 60),
                new Country("Bhutan", "BT", "BTN", 64),
                new Country("Bolivia (Plurinational State of)", "BO", "BOL", 68),
                new Country("Bonaire, Sint Eustatius and Saba", "BQ", "BES", 535),
                new Country("Bosnia and Herzegovina", "BA", "BIH", 70),
                new Country("Botswana", "BW", "BWA", 72),
                new Country("Bouvet Island", "BV", "BVT", 74),
                new Country("Brazil", "BR", "BRA", 76),
                new Country("British Indian Ocean Territory", "IO", "IOT", 86),
                new Country("Brunei Darussalam", "BN", "BRN", 96),
                new Country("Bulgaria", "BG", "BGR", 100),
                new Country("Burkina Faso", "BF", "BFA", 854),
                new Country("Burundi", "BI", "BDI", 108),
                new Country("Cabo Verde", "CV", "CPV", 132),
                new Country("Cambodia", "KH", "KHM", 116),
                new Country("Cameroon", "CM", "CMR", 120),
                new Country("Canada", "CA", "CAN", 124),
                new Country("Cayman Islands", "KY", "CYM", 136),
                new Country("Central African Republic", "CF", "CAF", 140),
                new Country("Chad", "TD", "TCD", 148),
                new Country("Chile", "CL", "CHL", 152),
                new Country("China", "CN", "CHN", 156),
                new Country("Christmas Island", "CX", "CXR", 162),
                new Country("Cocos (Keeling) Islands", "CC", "CCK", 166),
                new Country("Colombia", "CO", "COL", 170),
                new Country("Comoros", "KM", "COM", 174),
                new Country("Congo", "CG", "COG", 178),
                new Country("Congo (Democratic Republic of the)", "CD", "COD", 180),
                new Country("Cook Islands", "CK", "COK", 184),
                new Country("Costa Rica", "CR", "CRI", 188),
                new Country("Côte d'Ivoire", "CI", "CIV", 384),
                new Country("Croatia", "HR", "HRV", 191),
                new Country("Cuba", "CU", "CUB", 192),
                new Country("Curaçao", "CW", "CUW", 531),
                new Country("Cyprus", "CY", "CYP", 196),
                new Country("Czech Republic", "CZ", "CZE", 203),
                new Country("Denmark", "DK", "DNK", 208),
                new Country("Djibouti", "DJ", "DJI", 262),
                new Country("Dominica", "DM", "DMA", 212),
                new Country("Dominican Republic", "DO", "DOM", 214),
                new Country("Ecuador", "EC", "ECU", 218),
                new Country("Egypt", "EG", "EGY", 818),
                new Country("El Salvador", "SV", "SLV", 222),
                new Country("Equatorial Guinea", "GQ", "GNQ", 226),
                new Country("Eritrea", "ER", "ERI", 232),
                new Country("Estonia", "EE", "EST", 233),
                new Country("Ethiopia", "ET", "ETH", 231),
                new Country("Falkland Islands (Malvinas)", "FK", "FLK", 238),
                new Country("Faroe Islands", "FO", "FRO", 234),
                new Country("Fiji", "FJ", "FJI", 242),
                new Country("Finland", "FI", "FIN", 246),
                new Country("France", "FR", "FRA", 250),
                new Country("French Guiana", "GF", "GUF", 254),
                new Country("French Polynesia", "PF", "PYF", 258),
                new Country("French Southern Territories", "TF", "ATF", 260),
                new Country("Gabon", "GA", "GAB", 266),
                new Country("Gambia", "GM", "GMB", 270),
                new Country("Georgia", "GE", "GEO", 268),
                new Country("Germany", "DE", "DEU", 276),
                new Country("Ghana", "GH", "GHA", 288),
                new Country("Gibraltar", "GI", "GIB", 292),
                new Country("Greece", "GR", "GRC", 300),
                new Country("Greenland", "GL", "GRL", 304),
                new Country("Grenada", "GD", "GRD", 308),
                new Country("Guadeloupe", "GP", "GLP", 312),
                new Country("Guam", "GU", "GUM", 316),
                new Country("Guatemala", "GT", "GTM", 320),
                new Country("Guernsey", "GG", "GGY", 831),
                new Country("Guinea", "GN", "GIN", 324),
                new Country("Guinea-Bissau", "GW", "GNB", 624),
                new Country("Guyana", "GY", "GUY", 328),
                new Country("Haiti", "HT", "HTI", 332),
                new Country("Heard Island and McDonald Islands", "HM", "HMD", 334),
                new Country("Holy See", "VA", "VAT", 336),
                new Country("Honduras", "HN", "HND", 340),
                new Country("Hong Kong", "HK", "HKG", 344),
                new Country("Hungary", "HU", "HUN", 348),
                new Country("Iceland", "IS", "ISL", 352),
                new Country("India", "IN", "IND", 356),
                new Country("Indonesia", "ID", "IDN", 360),
                new Country("Iran (Islamic Republic of)", "IR", "IRN", 364),
                new Country("Iraq", "IQ", "IRQ", 368),
                new Country("Ireland", "IE", "IRL", 372),
                new Country("Isle of Man", "IM", "IMN", 833),
                new Country("Israel", "IL", "ISR", 376),
                new Country("Italy", "IT", "ITA", 380),
                new Country("Jamaica", "JM", "JAM", 388),
                new Country("Japan", "JP", "JPN", 392),
                new Country("Jersey", "JE", "JEY", 832),
                new Country("Jordan", "JO", "JOR", 400),
                new Country("Kazakhstan", "KZ", "KAZ", 398),
                new Country("Kenya", "KE", "KEN", 404),
                new Country("Kiribati", "KI", "KIR", 296),
                new Country("Korea (Democratic People's Republic of)", "KP", "PRK", 408),
                new Country("Korea (Republic of)", "KR", "KOR", 410),
                new Country("Kuwait", "KW", "KWT", 414),
                new Country("Kyrgyzstan", "KG", "KGZ", 417),
                new Country("Lao People's Democratic Republic", "LA", "LAO", 418),
                new Country("Latvia", "LV", "LVA", 428),
                new Country("Lebanon", "LB", "LBN", 422),
                new Country("Lesotho", "LS", "LSO", 426),
                new Country("Liberia", "LR", "LBR", 430),
                new Country("Libya", "LY", "LBY", 434),
                new Country("Liechtenstein", "LI", "LIE", 438),
                new Country("Lithuania", "LT", "LTU", 440),
                new Country("Luxembourg", "LU", "LUX", 442),
                new Country("Macao", "MO", "MAC", 446),
                new Country("Macedonia (the former Yugoslav Republic of)", "MK", "MKD", 807),
                new Country("Madagascar", "MG", "MDG", 450),
                new Country("Malawi", "MW", "MWI", 454),
                new Country("Malaysia", "MY", "MYS", 458),
                new Country("Maldives", "MV", "MDV", 462),
                new Country("Mali", "ML", "MLI", 466),
                new Country("Malta", "MT", "MLT", 470),
                new Country("Marshall Islands", "MH", "MHL", 584),
                new Country("Martinique", "MQ", "MTQ", 474),
                new Country("Mauritania", "MR", "MRT", 478),
                new Country("Mauritius", "MU", "MUS", 480),
                new Country("Mayotte", "YT", "MYT", 175),
                new Country("Mexico", "MX", "MEX", 484),
                new Country("Micronesia (Federated States of)", "FM", "FSM", 583),
                new Country("Moldova (Republic of)", "MD", "MDA", 498),
                new Country("Monaco", "MC", "MCO", 492),
                new Country("Mongolia", "MN", "MNG", 496),
                new Country("Montenegro", "ME", "MNE", 499),
                new Country("Montserrat", "MS", "MSR", 500),
                new Country("Morocco", "MA", "MAR", 504),
                new Country("Mozambique", "MZ", "MOZ", 508),
                new Country("Myanmar", "MM", "MMR", 104),
                new Country("Namibia", "NA", "NAM", 516),
                new Country("Nauru", "NR", "NRU", 520),
                new Country("Nepal", "NP", "NPL", 524),
                new Country("Netherlands", "NL", "NLD", 528),
                new Country("New Caledonia", "NC", "NCL", 540),
                new Country("New Zealand", "NZ", "NZL", 554),
                new Country("Nicaragua", "NI", "NIC", 558),
                new Country("Niger", "NE", "NER", 562),
                new Country("Nigeria", "NG", "NGA", 566),
                new Country("Niue", "NU", "NIU", 570),
                new Country("Norfolk Island", "NF", "NFK", 574),
                new Country("Northern Mariana Islands", "MP", "MNP", 580),
                new Country("Norway", "NO", "NOR", 578),
                new Country("Oman", "OM", "OMN", 512),
                new Country("Pakistan", "PK", "PAK", 586),
                new Country("Palau", "PW", "PLW", 585),
                new Country("Palestine, State of", "PS", "PSE", 275),
                new Country("Panama", "PA", "PAN", 591),
                new Country("Papua New Guinea", "PG", "PNG", 598),
                new Country("Paraguay", "PY", "PRY", 600),
                new Country("Peru", "PE", "PER", 604),
                new Country("Philippines", "PH", "PHL", 608),
                new Country("Pitcairn", "PN", "PCN", 612),
                new Country("Poland", "PL", "POL", 616),
                new Country("Portugal", "PT", "PRT", 620),
                new Country("Puerto Rico", "PR", "PRI", 630),
                new Country("Qatar", "QA", "QAT", 634),
                new Country("Réunion", "RE", "REU", 638),
                new Country("Romania", "RO", "ROU", 642),
                new Country("Russian Federation", "RU", "RUS", 643),
                new Country("Rwanda", "RW", "RWA", 646),
                new Country("Saint Barthélemy", "BL", "BLM", 652),
                new Country("Saint Helena, Ascension and Tristan da Cunha", "SH", "SHN", 654),
                new Country("Saint Kitts and Nevis", "KN", "KNA", 659),
                new Country("Saint Lucia", "LC", "LCA", 662),
                new Country("Saint Martin (French part)", "MF", "MAF", 663),
                new Country("Saint Pierre and Miquelon", "PM", "SPM", 666),
                new Country("Saint Vincent and the Grenadines", "VC", "VCT", 670),
                new Country("Samoa", "WS", "WSM", 882),
                new Country("San Marino", "SM", "SMR", 674),
                new Country("Sao Tome and Principe", "ST", "STP", 678),
                new Country("Saudi Arabia", "SA", "SAU", 682),
                new Country("Senegal", "SN", "SEN", 686),
                new Country("Serbia", "RS", "SRB", 688),
                new Country("Seychelles", "SC", "SYC", 690),
                new Country("Sierra Leone", "SL", "SLE", 694),
                new Country("Singapore", "SG", "SGP", 702),
                new Country("Sint Maarten (Dutch part)", "SX", "SXM", 534),
                new Country("Slovakia", "SK", "SVK", 703),
                new Country("Slovenia", "SI", "SVN", 705),
                new Country("Solomon Islands", "SB", "SLB", 90),
                new Country("Somalia", "SO", "SOM", 706),
                new Country("South Africa", "ZA", "ZAF", 710),
                new Country("South Georgia and the South Sandwich Islands", "GS", "SGS", 239),
                new Country("South Sudan", "SS", "SSD", 728),
                new Country("Spain", "ES", "ESP", 724),
                new Country("Sri Lanka", "LK", "LKA", 144),
                new Country("Sudan", "SD", "SDN", 729),
                new Country("Suriname", "SR", "SUR", 740),
                new Country("Svalbard and Jan Mayen", "SJ", "SJM", 744),
                new Country("Swaziland", "SZ", "SWZ", 748),
                new Country("Sweden", "SE", "SWE", 752),
                new Country("Switzerland", "CH", "CHE", 756),
                new Country("Syrian Arab Republic", "SY", "SYR", 760),
                new Country("Taiwan, Province of China[a]", "TW", "TWN", 158),
                new Country("Tajikistan", "TJ", "TJK", 762),
                new Country("Tanzania, United Republic of", "TZ", "TZA", 834),
                new Country("Thailand", "TH", "THA", 764),
                new Country("Timor-Leste", "TL", "TLS", 626),
                new Country("Togo", "TG", "TGO", 768),
                new Country("Tokelau", "TK", "TKL", 772),
                new Country("Tonga", "TO", "TON", 776),
                new Country("Trinidad and Tobago", "TT", "TTO", 780),
                new Country("Tunisia", "TN", "TUN", 788),
                new Country("Turkey", "TR", "TUR", 792),
                new Country("Turkmenistan", "TM", "TKM", 795),
                new Country("Turks and Caicos Islands", "TC", "TCA", 796),
                new Country("Tuvalu", "TV", "TUV", 798),
                new Country("Uganda", "UG", "UGA", 800),
                new Country("Ukraine", "UA", "UKR", 804),
                new Country("United Arab Emirates", "AE", "ARE", 784),
                new Country("United States of America", "US", "USA", 840),
                new Country("United States Minor Outlying Islands", "UM", "UMI", 581),
                new Country("Uruguay", "UY", "URY", 858),
                new Country("Uzbekistan", "UZ", "UZB", 860),
                new Country("Vanuatu", "VU", "VUT", 548),
                new Country("Venezuela (Bolivarian Republic of)", "VE", "VEN", 862),
                new Country("Viet Nam", "VN", "VNM", 704),
                new Country("Virgin Islands (British)", "VG", "VGB", 92),
                new Country("Virgin Islands (U.S.)", "VI", "VIR", 850),
                new Country("Wallis and Futuna", "WF", "WLF", 876),
                new Country("Western Sahara", "EH", "ESH", 732),
                new Country("Yemen", "YE", "YEM", 887),
                new Country("Zambia", "ZM", "ZMB", 894),
                new Country("Zimbabwe", "ZW", "ZWE", 716)
            };
        }


        [HttpPost]
        public ActionResult OrderVirtualCard(OrderVirtualCardPageModel model)
        {
            if (model.cardholderType == "New")
            {
                return RedirectToAction("OrderVirtualCardNewCardholder");
            }

            if (model.cardholderType == "Existing")
            {
                return RedirectToAction("OrderVirtualCardGetExistingCardholder");
            }

            return null;
        }

        public ActionResult OrderVirtualCardNewCardholder()
        {
            var products = _apiService.GetProducts().ProductList.Where(p => p.IsVirtual == true).ToList();

            var model = new CreateCardHolderPageModel();
            model.Countries = GetCountrySelectListItems();
            model.Products = GetProductsDdl(products);
            return View(model);
        }

        [HttpPost]
        public ActionResult OrderVirtualCardNewCardholder(CreateCardHolderPageModel model)
        {
            var productsList = _apiService.GetProducts().ProductList.Where(p => p.IsVirtual == true).ToList();
            model.Countries = GetCountrySelectListItems();
            model.Products = GetProductsDdl(productsList);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (ModelState.IsValid)
            {
                var request = new CreateCardholderRequest();
                request.FirstName = model.FirstName;
                request.LastName = model.LastName;
                request.Mobile = model.Mobile;
                request.Email = model.EmailAddress;
                request.Address1 = model.Address1;
                request.Address2 = model.Address2;
                request.City = model.City;
                request.PostCode = model.PostCode;
                request.Country = model.Country;
                request.DateOfBirth = model.DateOfBirth;
                request.UserName = model.UserName;
                request.Password = model.Password;
                request.HouseNumberOrBuilding = model.HouseNumberOrBuilding;
                request.State = model.State;



                var prodCurrency = productsList.Where(p => p.ProductCode == model.ProductCode).FirstOrDefault().Currency;
                var country = _apiService.GetCountryInfo(model.Country);
                var allowed = country.CardLoadInfos.Any(c => c.Currency == prodCurrency);

                if (!allowed)
                {
                    ModelState.AddModelError("", "Product not available for cardholder country!");
                    TempData["FailureMessage"] = "Product not available for cardholder country!";
                    return View(model);
                }

                var response = _apiService.CreateCardholder(request);

                if (!response.IsSuccess)
                {
                    if (response.ErrorCodes[0] == "30000016")
                    {
                        ModelState.AddModelError("", "Username is already taken. Please try a different username");
                        TempData["FailureMessage"] = "Create cardholder failed.Username already in use!";
                    }
                    if (response.ErrorCodes[0] == "30000017")
                    {
                        ModelState.AddModelError("", "Email is already taken. Please try a different email");
                        TempData["FailureMessage"] = "Create cardholder failed.Email already in use!";
                    }
                    if (response.ErrorCodes[0] == "30000025")
                    {
                        ModelState.AddModelError("", "Create cardholder failed!");
                        TempData["FailureMessage"] = "Create cardholder failed!";
                    }

                    return View(model);
                }

                var orderResponse = _apiService.OrderVirtualCard(new OrderVirtualCardRequest
                {
                    CardHolderId = response.CardHolderId,
                    ProductCode = model.ProductCode
                });

                if (!orderResponse.IsSuccess)
                {
                    if (orderResponse.ErrorCodes[0] == "18000002")
                    {
                        ModelState.AddModelError("", "Cardholder created. Card could not be ordered.");
                        TempData["FailureMessage"] = "Cardholder created. Card could not be ordered!";
                    }

                    return View(model);
                }

                TempData["Success"] = String.Format("CardId : {0} : ordered successfully for {1} {2}", orderResponse.CardProxyId, request.FirstName, request.LastName);
                return RedirectToAction("VirtualCardOrderHistory");
            }

            return View(model);
        }


        public ActionResult OrderVirtualCardGetExistingCardholder()
        {
            var model = new GetExistingCardholderPageModel();
            model.searchTypes = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "Email",
                                                 Value = "Email"
                                             },
                new SelectListItem
                                             {
                                                Text = "Username",
                                                 Value = "Username"
                                             }
            }.ToList();

            model.searchTypes.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });

            return View(model);


            //var products = _apiService.GetProducts().ProductList.Where(p => p.IsVirtual == true).ToList();

            //var model = new CreateCardHolderPageModel();
            //model.Countries = GetCountrySelectListItems();
            //model.Products = GetProductsDdl(products);
            //return View(model);
        }

        [HttpPost]
        public ActionResult OrderVirtualCardGetExistingCardholder(GetExistingCardholderPageModel model)
        {
            var response = _apiService.GetExistingCardholders(new ExistingCardholderRequestModel { searchType = model.searchType, searchValue = model.searchValue });

            if (response == null)
            {
                model.searchTypes = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "Email",
                                                 Value = "Email"
                                             },
                new SelectListItem
                                             {
                                                Text = "Username",
                                                 Value = "Username"
                                             }
            }.ToList();

                model.searchTypes.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });

                TempData["FailureMessage"] = "Cardholder not found. Please use a valid username/email.!";
                return View(model);
            }

            var cardholder = response.Cardholder;
            TempData["existingCardholder"] = cardholder;
            return RedirectToAction("OrderVirtualCardForExistingCardholder", new { cardholderId = cardholder.Id });
            

            //var products = _apiService.GetProducts().ProductList.Where(p => p.IsVirtual == true).ToList();

            //var orderCardExistingPageModel = new CreateCardHolderPageModel();
            //orderCardExistingPageModel.FirstName = cardholder.FirstName;
            //orderCardExistingPageModel.LastName = cardholder.LastName;
            //orderCardExistingPageModel.DateOfBirth = cardholder.DateOfBirth;
            //orderCardExistingPageModel.EmailAddress = cardholder.EmailAddress;
            //orderCardExistingPageModel.Mobile = cardholder.Mobile;
            //orderCardExistingPageModel.HouseNumberOrBuilding = cardholder.HouseNumberOrBuilding;
            //orderCardExistingPageModel.Address1 = cardholder.Address1;
            //orderCardExistingPageModel.Address2 = cardholder.Address2;
            //orderCardExistingPageModel.City = cardholder.City;
            //orderCardExistingPageModel.State = cardholder.State;
            //orderCardExistingPageModel.Country = cardholder.ISOCountryCode;
            //orderCardExistingPageModel.PostCode = cardholder.PostCode;
            //orderCardExistingPageModel.UserName = cardholder.Username;   
            //orderCardExistingPageModel.Countries = GetCountrySelectListItems();
            //orderCardExistingPageModel.Products = GetProductsDdl(products);

            //return View("OrderVirtualCardForExistingCardholder", orderCardExistingPageModel);
        }

        public ActionResult OrderVirtualCardForExistingCardholder(string cardHolderId)
        {
            var cardholder = (Api.Responses.CardHolderDetails)TempData["existingCardholder"];
            var products = _apiService.GetProducts().ProductList.Where(p => p.IsVirtual == true).ToList();

            var orderCardExistingPageModel = new OrderVirtualCardForExistingCardholdersPageModel();

            orderCardExistingPageModel.CardHolderId = cardholder.Id;
            orderCardExistingPageModel.FirstName = cardholder.FirstName;
            orderCardExistingPageModel.LastName = cardholder.LastName;
            orderCardExistingPageModel.DateOfBirth = cardholder.DateOfBirth;
            orderCardExistingPageModel.EmailAddress = cardholder.EmailAddress;
            orderCardExistingPageModel.Mobile = cardholder.Mobile;
            orderCardExistingPageModel.HouseNumberOrBuilding = cardholder.HouseNumberOrBuilding;
            orderCardExistingPageModel.Address1 = cardholder.Address1;
            orderCardExistingPageModel.Address2 = cardholder.Address2;
            orderCardExistingPageModel.City = cardholder.City;
            orderCardExistingPageModel.State = cardholder.State;
            orderCardExistingPageModel.Country = cardholder.ISOCountryCode;
            orderCardExistingPageModel.PostCode = cardholder.PostCode;
            orderCardExistingPageModel.UserName = cardholder.Username;
      
            orderCardExistingPageModel.Products = GetProductsDdl(products);

            return View(orderCardExistingPageModel);
        }

        [HttpPost]
        public ActionResult OrderVirtualCardForExistingCardholder(OrderVirtualCardForExistingCardholdersPageModel model)
        {
            var products = _apiService.GetProducts().ProductList.Where(p => p.IsVirtual == true).ToList();
            //var cardholder = _apiService.GetCardHolderDetails(model.CardHolderId).User;
            //var cardholder = (Api.Responses.CardHolderDetails)TempData["existingCardholder"];


            //var orderCardExistingPageModel = new OrderVirtualCardForExistingCardholdersPageModel();
            //orderCardExistingPageModel.CardHolderId = cardholder.Id;
            //orderCardExistingPageModel.FirstName = cardholder.FirstName;
            //orderCardExistingPageModel.LastName = cardholder.LastName;
            //orderCardExistingPageModel.DateOfBirth = cardholder.DateOfBirth;
            //orderCardExistingPageModel.EmailAddress = cardholder.EmailAddress;
            //orderCardExistingPageModel.Mobile = cardholder.Mobile;
            //orderCardExistingPageModel.HouseNumberOrBuilding = cardholder.HouseNumberOrBuilding;
            //orderCardExistingPageModel.Address1 = cardholder.Address1;
            //orderCardExistingPageModel.Address2 = cardholder.Address2;
            //orderCardExistingPageModel.City = cardholder.City;
            //orderCardExistingPageModel.State = cardholder.State;
            //orderCardExistingPageModel.Country = cardholder.ISOCountryCode;
            //orderCardExistingPageModel.PostCode = cardholder.PostCode;
            //orderCardExistingPageModel.UserName = cardholder.Username;
            //orderCardExistingPageModel.Countries = GetCountrySelectListItems();
        

            if (!ModelState.IsValid)
            {
                model.Products = GetProductsDdl(products);
                return View(model);
            }


            var prodCurrency = products.Where(p => p.ProductCode == model.ProductCode).FirstOrDefault().Currency;
            var country = _apiService.GetCountryInfo(model.Country);
            var allowed = country.CardLoadInfos.Any(c => c.Currency == prodCurrency);

            if (!allowed)
            {
                ModelState.AddModelError("", "Product not available for cardholder country!");
                TempData["FailureMessage"] = "Product not available for cardholder country!";
                return View(model);
            }

            var orderResponse = _apiService.OrderVirtualCard(new OrderVirtualCardRequest
            {
                CardHolderId = model.CardHolderId,
                ProductCode = model.ProductCode
            });

            if (!orderResponse.IsSuccess)
            {
                model.Products = GetProductsDdl(products);

                if (orderResponse.ErrorCodes[0] == "18000002")
                {
                    ModelState.AddModelError("", "Card could not be ordered.");
                    TempData["FailureMessage"] = "Card could not be ordered!";
                }

                return View(model);
            }

            TempData["Success"] = String.Format("CardId : {0} : ordered successfully for {1} {2}", orderResponse.CardProxyId, model.FirstName, model.LastName);
            return RedirectToAction("VirtualCardOrderHistory");

            //var cardholder = (Api.Responses.CardHolderDetails)TempData["existingCardholder"];
            //var products = _apiService.GetProducts().ProductList.Where(p => p.IsVirtual == true).ToList();

            //var orderCardExistingPageModel = new OrderVirtualCardForExistingCardholdersPageModel();
            //orderCardExistingPageModel.FirstName = cardholder.FirstName;
            //orderCardExistingPageModel.LastName = cardholder.LastName;
            //orderCardExistingPageModel.DateOfBirth = cardholder.DateOfBirth;
            //orderCardExistingPageModel.EmailAddress = cardholder.EmailAddress;
            //orderCardExistingPageModel.Mobile = cardholder.Mobile;
            //orderCardExistingPageModel.HouseNumberOrBuilding = cardholder.HouseNumberOrBuilding;
            //orderCardExistingPageModel.Address1 = cardholder.Address1;
            //orderCardExistingPageModel.Address2 = cardholder.Address2;
            //orderCardExistingPageModel.City = cardholder.City;
            //orderCardExistingPageModel.State = cardholder.State;
            //orderCardExistingPageModel.Country = cardholder.ISOCountryCode;
            //orderCardExistingPageModel.PostCode = cardholder.PostCode;
            //orderCardExistingPageModel.UserName = cardholder.Username;
            //orderCardExistingPageModel.Countries = GetCountrySelectListItems();
            //orderCardExistingPageModel.Products = GetProductsDdl(products);

            //return View(orderCardExistingPageModel);
        }

        public ActionResult GetVirtualCardDetail(string cardProxyId)
        {
            var virtualCards = _apiService.GetVirtualCards();

            var card = virtualCards.VirtualCards.Where(v => v.CardProxyId == cardProxyId).FirstOrDefault();

            var cardsList = new List<CorporateCard>();
            cardsList.Add(card);
            ViewBag.CardProxyId = card.CardProxyId;
            return View(cardsList);
        }

        public ActionResult VirtualCardOrderHistory()
        {
            var response = _apiService.GetVirtualCards();

            if (!response.IsSuccess)
            {

            }

            var model = response.VirtualCards;
            return View(model);
        }

        #endregion

        public ActionResult OrderReplacementCard(string cardProxyId, string productCode)
        {
            var cardHolderId = _apiService.GetCardholderFromProxy(cardProxyId).Card.CardholderId;

            var response = _apiService.GetCardHolderDetails(cardHolderId);

            var cardholder = response.User;

            var model = new OrderReplacementCardPageModel();

            model.FirstName = cardholder.FirstName;
            model.LastName = cardholder.LastName;
            model.DateOfBirth = cardholder.DateOfBirth;
            model.EmailAddress = cardholder.EmailAddress;
            model.Mobile = cardholder.Mobile;
            model.Address1 = cardholder.Address1;
            model.Address2 = cardholder.Address2;
            model.City = cardholder.City;
            model.State = cardholder.State;
            model.ISOCountryCode = cardholder.ISOCountryCode;
            model.PostCode = cardholder.PostCode;
            model.ProductCode = productCode;
            model.cardholderId = cardholder.Id;
            model.ReplacedCardProxyId = cardProxyId;
            model.cardholderId = cardHolderId;

            var countries = GetCountrySelectListItems();

            var selected = countries.Where(x => x.Value == model.ISOCountryCode).First();
            selected.Selected = true;

            model.Countries = countries;

            return View(model);
        }

        [HttpPost]
        public ActionResult OrderReplacementCard(OrderReplacementCardPageModel model)
        {
            if (!ModelState.IsValid)
            {
                var countries = GetCountrySelectListItems();

                var selected = countries.Where(x => x.Value == model.ISOCountryCode).First();
                selected.Selected = true;

                model.Countries = countries;

                return View(model);
            }

            var request = new OrderIndividualCardRequest
            {
                ProductCode = model.ProductCode,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address1 = model.Address1,
                Address2 = model.Address2,
                PostCode = model.PostCode,
                City = model.City,
                ISOCountryCode = model.ISOCountryCode,
                RecipientEmail = model.EmailAddress,
                Dob = model.DateOfBirth.ToShortDateString(),
                State = model.State,
                Mobile = model.Mobile,
                ReplacedCardProxyId = model.ReplacedCardProxyId,
                CardholderId = model.cardholderId
            };

            var response = _apiService.OrderCard(request);

            if (!response.IsSuccess)
            {

            }

            TempData["Success"] = String.Format("Replacement card Order submitted successfully. OrderId : {0}", response.Id);

            return RedirectToAction("InvidualCardOrderHistory");            
        }

        public ActionResult DownloadFile()
        {            
            try
            {
                string fullPath = Path.Combine(Server.MapPath("/Files"), "BatchOrderTemplate.xlsx");
                return File(fullPath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "BatchOrderTemplate.xlsx");
            }
            catch(Exception ex)
            {
                TempData["FailureMessage"] = "Error download template!";
                return View("BatchOrderCards");
            }            
        }
    }
}