﻿using CorporatePortal.Api;
using CorporatePortal.Api.Requests;
using CorporatePortal.Models;
using CorporatePortal.Models.CardLoad;
using CorporatePortal.Models.ManageCards;
using CorporatePortal.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorporatePortal.Controllers
{
    [Authorize(Roles = "Admin,CorporateAdmin,CorporateManager,CorporateEmployee")]
    public class ManageController : Controller
    {
        IApiService _apiService;

        public ManageController(IApiService apiService)
        {
            _apiService = apiService;
            var model = _apiService.GetPortalSummary();
            ViewBag.TopTiles = model.Summary;
        }

        public ActionResult Home()
        {
            var products = _apiService.GetProducts();

            var model = new ManageHomePageModel
            {
                ProductsList = GetProductsDdl(products.ProductList),
                TypeList = GetTypesDdl()
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Home(ManageHomePageModel model)
        {
            var products = _apiService.GetProducts();

            model.ProductsList = GetProductsDdl(products.ProductList);
            model.TypeList = GetTypesDdl();
            var list = _apiService.GetManufacturerCardsByProduct(model.productCode, model.status);

            model.vcards = list.VirtualCards;
            model.activecards = list.ActiveCards;
            model.inactivecards = list.InactiveCards;
            return View(model);
        }

        public ActionResult CardDetails(string productCode, string status, string cardProxyId)
        {
            var model = new ManageHomePageModel();
            model.productCode = productCode;
            model.status = status;

            var products = _apiService.GetProducts();

            model.ProductsList = GetProductsDdl(products.ProductList, productCode);

            model.TypeList = GetTypesDdl(status);
            var list = _apiService.GetManufacturerCardsByProduct(productCode, status);

            if (list.VirtualCards != null)
            {
                var vcards = list.VirtualCards;
                var index = vcards.FindIndex(x => x.CardProxyId == cardProxyId);
                var item = vcards[index];
                vcards[index] = vcards[0];
                vcards[0] = item;

                model.vcards = vcards;
            }

            if (list.ActiveCards != null)
            {
                var activeCards = list.ActiveCards;
                var index = activeCards.FindIndex(x => x.CardProxyId == cardProxyId);
                var item = activeCards[index];
                activeCards[index] = activeCards[0];
                activeCards[0] = item;

                model.activecards = activeCards;
            }

            if (list.InactiveCards != null)
            {
                var inActiveCards = list.InactiveCards;
                var index = inActiveCards.FindIndex(x => x.CardProxyId == cardProxyId);
                var item = inActiveCards[index];
                inActiveCards[index] = inActiveCards[0];
                inActiveCards[0] = item;

                model.inactivecards = inActiveCards;
            }
            return View(model);
        }

        public ActionResult ChangeCardStatus(string cardProxyId, string userId)
        {
            var response = _apiService.GetCardInformation(cardProxyId);
            string currentStatus = response.Status;

            return RedirectToAction("ChangeCardStatus", "User", new { currentStatus = currentStatus, userId = userId, cardProxyId = cardProxyId });
        }

        public ActionResult Activate(string cardProxyId, string productCode)
        {
            var model = new CreateCardHolderPageModel();
            model.Countries = GetCountrySelectListItems();
            model.ProductCode = productCode;
            model.CardProxyId = cardProxyId;
            return View(model);
        }

        public ActionResult ActivateExistingCardholder(string cardProxyId, string productCode, string cardholderId)
        {
            var response = _apiService.GetCardHolderDetails(cardholderId);

            var cardholder = response.User;
            //var cardholder = (Api.Responses.CardHolderDetails)TempData["existingCardholder"];

            var model = new ActivateExistingCardholderModel();
            model.CardHolder = cardholder;
            model.cardProxyId = cardProxyId;
            model.productCode = productCode;

            return View(model);
        }

        [HttpPost]
        public ActionResult ActivateExistingCardholder(ActivateExistingCardholderModel model)
        {
            var cardProxyId = model.cardProxyId;
            var cardHolderId = model.CardHolder.Id;

            var activateResponse = _apiService.ActivateCard(cardProxyId, cardHolderId, "Card");

            if (!activateResponse.IsSuccess)
            {
                if (activateResponse.ErrorCodes[0] == "30000025")
                {
                    ModelState.AddModelError("", "Card not activated. Error : User's country does not support the prepaid card's currency.");
                    TempData["FailureMessage"] = "Card not activated. Error : User's country does not support the prepaid card's currency.!";
                }
                return View(model);
            }

            TempData["Success"] = String.Format("CardId: {0} : successfully activated and issued to {1} {2}", cardProxyId, model.CardHolder.FirstName, model.CardHolder.LastName);

            return RedirectToAction("CardDetails", new { productCode = model.productCode, status = "Active", cardProxyId = cardProxyId });
        }

        [HttpPost]
        public ActionResult Activate(CreateCardHolderPageModel model)
        {
            if (ModelState.IsValid)
            {
                var request = new CreateCardholderRequest();
                request.FirstName = model.FirstName;
                request.LastName = model.LastName;
                request.Mobile = model.Mobile;
                request.Email = model.EmailAddress;
                request.Address1 = model.Address1;
                request.Address2 = model.Address2;
                request.City = model.City;
                request.State = model.State;
                request.PostCode = model.PostCode;
                request.Country = model.Country;
                request.DateOfBirth = model.DateOfBirth;
                request.UserName = model.UserName;
                request.Password = model.Password;

                var productsList = _apiService.GetProducts().ProductList.ToList();
                model.Countries = GetCountrySelectListItems();
                model.Products = GetProductsDdl(productsList);

                var prodCurrency = productsList.Where(p => p.ProductCode == model.ProductCode).First().Currency;
                var country = _apiService.GetCountryInfo(model.Country);

                if (country == null)
                {
                    ModelState.AddModelError("", "Product not available for cardholder country!");
                    TempData["FailureMessage"] = "Product not available for cardholder country!";
                    return View(model);
                }

                var allowed = country.CardLoadInfos.Any(c => c.Currency == prodCurrency);

                if (!allowed)
                {
                    ModelState.AddModelError("", "Product not available for cardholder country!");
                    TempData["FailureMessage"] = "Product not available for cardholder country!";
                    return View(model);
                }

                var response = _apiService.CreateCardholder(request);

                if (!response.IsSuccess)
                {
                    if (response.ErrorCodes[0] == "30000016")
                    {
                        ModelState.AddModelError("", "Username is already taken. Please try a different username");
                        TempData["FailureMessage"] = "Create cardholder failed.Username already in use!";
                    }
                    if (response.ErrorCodes[0] == "30000017")
                    {
                        ModelState.AddModelError("", "Email is already taken. Please try a different email");
                        TempData["FailureMessage"] = "Create cardholder failed.Email already in use!";
                    }
                    if (response.ErrorCodes[0] == "30000025")
                    {
                        ModelState.AddModelError("", "Create cardholder failed!");
                        TempData["FailureMessage"] = "Create cardholder failed!";
                    }

                    return View(model);
                }

                var cardHolderId = response.CardHolderId;

                var activateResponse = _apiService.ActivateCard(model.CardProxyId, cardHolderId, model.cardDisplayName);

                if (!activateResponse.IsSuccess)
                {
                    if (activateResponse.ErrorCodes[0] == "30000025")
                    {
                        ModelState.AddModelError("", "Cardholder created. Card not activated. Error : User's country does not support the prepaid card's currency.");
                        TempData["FailureMessage"] = "Cardholder created. Card not activated. Error : User's country does not support the prepaid card's currency.!";
                    }
                    return View(model);
                }

                TempData["Success"] = String.Format("CardId : {0} : successfully activated and issued to {1} {2}", model.CardProxyId, model.FirstName, model.LastName);

                return RedirectToAction("CardDetails", new { productCode = model.ProductCode, status = "Active", cardProxyId = model.CardProxyId });
            }

            model.Countries = GetCountrySelectListItems();
            model.ProductCode = model.ProductCode;
            model.CardProxyId = model.CardProxyId;
            return View(model);
        }

        public ActionResult GetCardholder(string cardProxyId, string productCode)
        {
            var model = new ActivateGetCardholderTypePageModel();

            model.cardProxyId = cardProxyId;
            model.productCode = productCode;

            model.cardholderTypeList = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "New",
                                                 Value = "New"
                                             },
                new SelectListItem
                                             {
                                                Text = "Existing",
                                                 Value = "Existing"
                                             }
            }.ToList();

            model.cardholderTypeList.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            return View(model);
        }

        [HttpPost]
        public ActionResult GetCardholder(ActivateGetCardholderTypePageModel model)
        {
            if (model.cardholderType == "New")
            {
                return RedirectToAction("Activate", new { cardProxyId = model.cardProxyId, productCode = model.productCode });
            }

            if (model.cardholderType == "Existing")
            {
                return RedirectToAction("GetExistingCardholder", new { cardProxyId = model.cardProxyId, productCode = model.productCode });
            }

            return null;
        }

        public ActionResult GetExistingCardholder(string cardProxyId, string productCode)
        {
            var model = new GetExistingCardholderPageModel();
            model.cardProxyId = cardProxyId;
            model.productCode = productCode;
            model.searchTypes = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "Email",
                                                 Value = "Email"
                                             },
                new SelectListItem
                                             {
                                                Text = "Username",
                                                 Value = "Username"
                                             }
            }.ToList();

            model.searchTypes.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });

            return View(model);
        }

        [HttpPost]
        public ActionResult GetExistingCardholder(GetExistingCardholderPageModel model)
        {
            if (!ModelState.IsValid)
            {
                model.searchTypes = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "Email",
                                                 Value = "Email"
                                             },
                new SelectListItem
                                             {
                                                Text = "Username",
                                                 Value = "Username"
                                             }
            }.ToList();

                model.searchTypes.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
                return View(model);
            }

            var response = _apiService.GetExistingCardholders(new ExistingCardholderRequestModel { searchType = model.searchType, searchValue = model.searchValue });

            if (response == null)
            {
                model.cardProxyId = model.cardProxyId;
                model.productCode = model.productCode;
                model.searchTypes = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "Email",
                                                 Value = "Email"
                                             },
                new SelectListItem
                                             {
                                                Text = "Username",
                                                 Value = "Username"
                                             }
            }.ToList();

                model.searchTypes.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });

                TempData["FailureMessage"] = "Cardholder not found. Please use a valid username/email.!";
                return View(model);
            }

            var cardholder = response.Cardholder;

            TempData["existingCardholder"] = cardholder;

            return RedirectToAction("ActivateExistingCardholder", new { cardProxyId = model.cardProxyId, productCode = model.productCode, cardholderId = cardholder.Id });
        }

        public ActionResult InstantIssue()
        {
            return View();
        }

        [HttpPost]
        public ActionResult InstantIssue(InstantIssueModel model)
        {
            var response = _apiService.GetCardProxyIdByPAN(model.PAN);

            if (response == null)
            {
                ModelState.AddModelError("", "CardProxyId not found for PAN!");
                TempData["FailureMessage"] = "CardProxyId not found for PAN!";

                return View();
            }

            if (!response.IsSuccess)
            {
                ModelState.AddModelError("", "CardProxyId not found!");
                TempData["FailureMessage"] = "CardProxyId not found!";

                return View();
            }

            var card = _apiService.GetManufacturerCard(response.CardProxyId);

            var product = _apiService.GetProductDetails(card.ProductCode);

            if (product.Product.FundsOwnership.Value.ToString() == "CardHolder")
            {
                ModelState.AddModelError("", "Card not allowed to be activated!");
                TempData["FailureMessage"] = "Card not allowed to be activated!";

                return View();
            }

            var checkCardAlreadyAssigned = _apiService.CheckCardAlreadyAssgined(response.CardProxyId);

            if (checkCardAlreadyAssigned.AlreadyAssigned == true)
            {
                ModelState.AddModelError("", "Card already assigned!");
                TempData["FailureMessage"] = "Card already assigned!";

                return View();
            }

            return RedirectToAction("GetCardholder", new { cardProxyId = response.CardProxyId, productCode = card.ProductCode });

        }

        public List<SelectListItem> GetCountrySelectListItems()
        {
            var countries = GetCountries().Select(s => new SelectListItem
            {
                Value = s.Alpha3,
                Text = s.Name
            }).ToList();

            countries.Insert(0, new SelectListItem { Text = "--Select Country--", Value = "", Disabled = true, Selected = true });
            return countries;

        }

        private IEnumerable<Country> GetCountries()
        {
            return new List<Country>
            {
                new Country("United Kingdom of Great Britain", "GB", "GBR", 826),
                new Country("Afghanistan", "AF", "AFG", 4),
                new Country("Åland Islands", "AX", "ALA", 248),
                new Country("Albania", "AL", "ALB", 8),
                new Country("Algeria", "DZ", "DZA", 12),
                new Country("American Samoa", "AS", "ASM", 16),
                new Country("Andorra", "AD", "AND", 20),
                new Country("Angola", "AO", "AGO", 24),
                new Country("Anguilla", "AI", "AIA", 660),
                new Country("Antarctica", "AQ", "ATA", 10),
                new Country("Antigua and Barbuda", "AG", "ATG", 28),
                new Country("Argentina", "AR", "ARG", 32),
                new Country("Armenia", "AM", "ARM", 51),
                new Country("Aruba", "AW", "ABW", 533),
                new Country("Australia", "AU", "AUS", 36),
                new Country("Austria", "AT", "AUT", 40),
                new Country("Azerbaijan", "AZ", "AZE", 31),
                new Country("Bahamas", "BS", "BHS", 44),
                new Country("Bahrain", "BH", "BHR", 48),
                new Country("Bangladesh", "BD", "BGD", 50),
                new Country("Barbados", "BB", "BRB", 52),
                new Country("Belarus", "BY", "BLR", 112),
                new Country("Belgium", "BE", "BEL", 56),
                new Country("Belize", "BZ", "BLZ", 84),
                new Country("Benin", "BJ", "BEN", 204),
                new Country("Bermuda", "BM", "BMU", 60),
                new Country("Bhutan", "BT", "BTN", 64),
                new Country("Bolivia (Plurinational State of)", "BO", "BOL", 68),
                new Country("Bonaire, Sint Eustatius and Saba", "BQ", "BES", 535),
                new Country("Bosnia and Herzegovina", "BA", "BIH", 70),
                new Country("Botswana", "BW", "BWA", 72),
                new Country("Bouvet Island", "BV", "BVT", 74),
                new Country("Brazil", "BR", "BRA", 76),
                new Country("British Indian Ocean Territory", "IO", "IOT", 86),
                new Country("Brunei Darussalam", "BN", "BRN", 96),
                new Country("Bulgaria", "BG", "BGR", 100),
                new Country("Burkina Faso", "BF", "BFA", 854),
                new Country("Burundi", "BI", "BDI", 108),
                new Country("Cabo Verde", "CV", "CPV", 132),
                new Country("Cambodia", "KH", "KHM", 116),
                new Country("Cameroon", "CM", "CMR", 120),
                new Country("Canada", "CA", "CAN", 124),
                new Country("Cayman Islands", "KY", "CYM", 136),
                new Country("Central African Republic", "CF", "CAF", 140),
                new Country("Chad", "TD", "TCD", 148),
                new Country("Chile", "CL", "CHL", 152),
                new Country("China", "CN", "CHN", 156),
                new Country("Christmas Island", "CX", "CXR", 162),
                new Country("Cocos (Keeling) Islands", "CC", "CCK", 166),
                new Country("Colombia", "CO", "COL", 170),
                new Country("Comoros", "KM", "COM", 174),
                new Country("Congo", "CG", "COG", 178),
                new Country("Congo (Democratic Republic of the)", "CD", "COD", 180),
                new Country("Cook Islands", "CK", "COK", 184),
                new Country("Costa Rica", "CR", "CRI", 188),
                new Country("Côte d'Ivoire", "CI", "CIV", 384),
                new Country("Croatia", "HR", "HRV", 191),
                new Country("Cuba", "CU", "CUB", 192),
                new Country("Curaçao", "CW", "CUW", 531),
                new Country("Cyprus", "CY", "CYP", 196),
                new Country("Czech Republic", "CZ", "CZE", 203),
                new Country("Denmark", "DK", "DNK", 208),
                new Country("Djibouti", "DJ", "DJI", 262),
                new Country("Dominica", "DM", "DMA", 212),
                new Country("Dominican Republic", "DO", "DOM", 214),
                new Country("Ecuador", "EC", "ECU", 218),
                new Country("Egypt", "EG", "EGY", 818),
                new Country("El Salvador", "SV", "SLV", 222),
                new Country("Equatorial Guinea", "GQ", "GNQ", 226),
                new Country("Eritrea", "ER", "ERI", 232),
                new Country("Estonia", "EE", "EST", 233),
                new Country("Ethiopia", "ET", "ETH", 231),
                new Country("Falkland Islands (Malvinas)", "FK", "FLK", 238),
                new Country("Faroe Islands", "FO", "FRO", 234),
                new Country("Fiji", "FJ", "FJI", 242),
                new Country("Finland", "FI", "FIN", 246),
                new Country("France", "FR", "FRA", 250),
                new Country("French Guiana", "GF", "GUF", 254),
                new Country("French Polynesia", "PF", "PYF", 258),
                new Country("French Southern Territories", "TF", "ATF", 260),
                new Country("Gabon", "GA", "GAB", 266),
                new Country("Gambia", "GM", "GMB", 270),
                new Country("Georgia", "GE", "GEO", 268),
                new Country("Germany", "DE", "DEU", 276),
                new Country("Ghana", "GH", "GHA", 288),
                new Country("Gibraltar", "GI", "GIB", 292),
                new Country("Greece", "GR", "GRC", 300),
                new Country("Greenland", "GL", "GRL", 304),
                new Country("Grenada", "GD", "GRD", 308),
                new Country("Guadeloupe", "GP", "GLP", 312),
                new Country("Guam", "GU", "GUM", 316),
                new Country("Guatemala", "GT", "GTM", 320),
                new Country("Guernsey", "GG", "GGY", 831),
                new Country("Guinea", "GN", "GIN", 324),
                new Country("Guinea-Bissau", "GW", "GNB", 624),
                new Country("Guyana", "GY", "GUY", 328),
                new Country("Haiti", "HT", "HTI", 332),
                new Country("Heard Island and McDonald Islands", "HM", "HMD", 334),
                new Country("Holy See", "VA", "VAT", 336),
                new Country("Honduras", "HN", "HND", 340),
                new Country("Hong Kong", "HK", "HKG", 344),
                new Country("Hungary", "HU", "HUN", 348),
                new Country("Iceland", "IS", "ISL", 352),
                new Country("India", "IN", "IND", 356),
                new Country("Indonesia", "ID", "IDN", 360),
                new Country("Iran (Islamic Republic of)", "IR", "IRN", 364),
                new Country("Iraq", "IQ", "IRQ", 368),
                new Country("Ireland", "IE", "IRL", 372),
                new Country("Isle of Man", "IM", "IMN", 833),
                new Country("Israel", "IL", "ISR", 376),
                new Country("Italy", "IT", "ITA", 380),
                new Country("Jamaica", "JM", "JAM", 388),
                new Country("Japan", "JP", "JPN", 392),
                new Country("Jersey", "JE", "JEY", 832),
                new Country("Jordan", "JO", "JOR", 400),
                new Country("Kazakhstan", "KZ", "KAZ", 398),
                new Country("Kenya", "KE", "KEN", 404),
                new Country("Kiribati", "KI", "KIR", 296),
                new Country("Korea (Democratic People's Republic of)", "KP", "PRK", 408),
                new Country("Korea (Republic of)", "KR", "KOR", 410),
                new Country("Kuwait", "KW", "KWT", 414),
                new Country("Kyrgyzstan", "KG", "KGZ", 417),
                new Country("Lao People's Democratic Republic", "LA", "LAO", 418),
                new Country("Latvia", "LV", "LVA", 428),
                new Country("Lebanon", "LB", "LBN", 422),
                new Country("Lesotho", "LS", "LSO", 426),
                new Country("Liberia", "LR", "LBR", 430),
                new Country("Libya", "LY", "LBY", 434),
                new Country("Liechtenstein", "LI", "LIE", 438),
                new Country("Lithuania", "LT", "LTU", 440),
                new Country("Luxembourg", "LU", "LUX", 442),
                new Country("Macao", "MO", "MAC", 446),
                new Country("Macedonia (the former Yugoslav Republic of)", "MK", "MKD", 807),
                new Country("Madagascar", "MG", "MDG", 450),
                new Country("Malawi", "MW", "MWI", 454),
                new Country("Malaysia", "MY", "MYS", 458),
                new Country("Maldives", "MV", "MDV", 462),
                new Country("Mali", "ML", "MLI", 466),
                new Country("Malta", "MT", "MLT", 470),
                new Country("Marshall Islands", "MH", "MHL", 584),
                new Country("Martinique", "MQ", "MTQ", 474),
                new Country("Mauritania", "MR", "MRT", 478),
                new Country("Mauritius", "MU", "MUS", 480),
                new Country("Mayotte", "YT", "MYT", 175),
                new Country("Mexico", "MX", "MEX", 484),
                new Country("Micronesia (Federated States of)", "FM", "FSM", 583),
                new Country("Moldova (Republic of)", "MD", "MDA", 498),
                new Country("Monaco", "MC", "MCO", 492),
                new Country("Mongolia", "MN", "MNG", 496),
                new Country("Montenegro", "ME", "MNE", 499),
                new Country("Montserrat", "MS", "MSR", 500),
                new Country("Morocco", "MA", "MAR", 504),
                new Country("Mozambique", "MZ", "MOZ", 508),
                new Country("Myanmar", "MM", "MMR", 104),
                new Country("Namibia", "NA", "NAM", 516),
                new Country("Nauru", "NR", "NRU", 520),
                new Country("Nepal", "NP", "NPL", 524),
                new Country("Netherlands", "NL", "NLD", 528),
                new Country("New Caledonia", "NC", "NCL", 540),
                new Country("New Zealand", "NZ", "NZL", 554),
                new Country("Nicaragua", "NI", "NIC", 558),
                new Country("Niger", "NE", "NER", 562),
                new Country("Nigeria", "NG", "NGA", 566),
                new Country("Niue", "NU", "NIU", 570),
                new Country("Norfolk Island", "NF", "NFK", 574),
                new Country("Northern Mariana Islands", "MP", "MNP", 580),
                new Country("Norway", "NO", "NOR", 578),
                new Country("Oman", "OM", "OMN", 512),
                new Country("Pakistan", "PK", "PAK", 586),
                new Country("Palau", "PW", "PLW", 585),
                new Country("Palestine, State of", "PS", "PSE", 275),
                new Country("Panama", "PA", "PAN", 591),
                new Country("Papua New Guinea", "PG", "PNG", 598),
                new Country("Paraguay", "PY", "PRY", 600),
                new Country("Peru", "PE", "PER", 604),
                new Country("Philippines", "PH", "PHL", 608),
                new Country("Pitcairn", "PN", "PCN", 612),
                new Country("Poland", "PL", "POL", 616),
                new Country("Portugal", "PT", "PRT", 620),
                new Country("Puerto Rico", "PR", "PRI", 630),
                new Country("Qatar", "QA", "QAT", 634),
                new Country("Réunion", "RE", "REU", 638),
                new Country("Romania", "RO", "ROU", 642),
                new Country("Russian Federation", "RU", "RUS", 643),
                new Country("Rwanda", "RW", "RWA", 646),
                new Country("Saint Barthélemy", "BL", "BLM", 652),
                new Country("Saint Helena, Ascension and Tristan da Cunha", "SH", "SHN", 654),
                new Country("Saint Kitts and Nevis", "KN", "KNA", 659),
                new Country("Saint Lucia", "LC", "LCA", 662),
                new Country("Saint Martin (French part)", "MF", "MAF", 663),
                new Country("Saint Pierre and Miquelon", "PM", "SPM", 666),
                new Country("Saint Vincent and the Grenadines", "VC", "VCT", 670),
                new Country("Samoa", "WS", "WSM", 882),
                new Country("San Marino", "SM", "SMR", 674),
                new Country("Sao Tome and Principe", "ST", "STP", 678),
                new Country("Saudi Arabia", "SA", "SAU", 682),
                new Country("Senegal", "SN", "SEN", 686),
                new Country("Serbia", "RS", "SRB", 688),
                new Country("Seychelles", "SC", "SYC", 690),
                new Country("Sierra Leone", "SL", "SLE", 694),
                new Country("Singapore", "SG", "SGP", 702),
                new Country("Sint Maarten (Dutch part)", "SX", "SXM", 534),
                new Country("Slovakia", "SK", "SVK", 703),
                new Country("Slovenia", "SI", "SVN", 705),
                new Country("Solomon Islands", "SB", "SLB", 90),
                new Country("Somalia", "SO", "SOM", 706),
                new Country("South Africa", "ZA", "ZAF", 710),
                new Country("South Georgia and the South Sandwich Islands", "GS", "SGS", 239),
                new Country("South Sudan", "SS", "SSD", 728),
                new Country("Spain", "ES", "ESP", 724),
                new Country("Sri Lanka", "LK", "LKA", 144),
                new Country("Sudan", "SD", "SDN", 729),
                new Country("Suriname", "SR", "SUR", 740),
                new Country("Svalbard and Jan Mayen", "SJ", "SJM", 744),
                new Country("Swaziland", "SZ", "SWZ", 748),
                new Country("Sweden", "SE", "SWE", 752),
                new Country("Switzerland", "CH", "CHE", 756),
                new Country("Syrian Arab Republic", "SY", "SYR", 760),
                new Country("Taiwan, Province of China[a]", "TW", "TWN", 158),
                new Country("Tajikistan", "TJ", "TJK", 762),
                new Country("Tanzania, United Republic of", "TZ", "TZA", 834),
                new Country("Thailand", "TH", "THA", 764),
                new Country("Timor-Leste", "TL", "TLS", 626),
                new Country("Togo", "TG", "TGO", 768),
                new Country("Tokelau", "TK", "TKL", 772),
                new Country("Tonga", "TO", "TON", 776),
                new Country("Trinidad and Tobago", "TT", "TTO", 780),
                new Country("Tunisia", "TN", "TUN", 788),
                new Country("Turkey", "TR", "TUR", 792),
                new Country("Turkmenistan", "TM", "TKM", 795),
                new Country("Turks and Caicos Islands", "TC", "TCA", 796),
                new Country("Tuvalu", "TV", "TUV", 798),
                new Country("Uganda", "UG", "UGA", 800),
                new Country("Ukraine", "UA", "UKR", 804),
                new Country("United Arab Emirates", "AE", "ARE", 784),
                new Country("United States of America", "US", "USA", 840),
                new Country("United States Minor Outlying Islands", "UM", "UMI", 581),
                new Country("Uruguay", "UY", "URY", 858),
                new Country("Uzbekistan", "UZ", "UZB", 860),
                new Country("Vanuatu", "VU", "VUT", 548),
                new Country("Venezuela (Bolivarian Republic of)", "VE", "VEN", 862),
                new Country("Viet Nam", "VN", "VNM", 704),
                new Country("Virgin Islands (British)", "VG", "VGB", 92),
                new Country("Virgin Islands (U.S.)", "VI", "VIR", 850),
                new Country("Wallis and Futuna", "WF", "WLF", 876),
                new Country("Western Sahara", "EH", "ESH", 732),
                new Country("Yemen", "YE", "YEM", 887),
                new Country("Zambia", "ZM", "ZMB", 894),
                new Country("Zimbabwe", "ZW", "ZWE", 716)
            };
        }

        private static List<SelectListItem> GetProductsDdl(List<Models.Product.QcsProduct> productList, string productCode = null)
        {
            List<SelectListItem> products = (from p in productList.AsEnumerable()
                                             select new SelectListItem
                                             {
                                                 Text = p.ProductCode,
                                                 Value = p.ProductCode
                                             }).ToList();


            if (productCode != null)
            {
                var selected = products.Where(x => x.Text == productCode).First();
                selected.Selected = true;
                products.Insert(0, new SelectListItem { Text = "--Select Product--", Value = "", Disabled = true, Selected = false });
                return products;
            }

            products.Insert(0, new SelectListItem { Text = "--Select Product--", Value = "", Disabled = true, Selected = true });

            return products;
        }

        private static List<SelectListItem> GetTypesDdl(string status = null)
        {

            List<SelectListItem> types = new List<SelectListItem>
            {
                new SelectListItem
                                             {
                                                 Text = "Active",
                                                 Value = "Active"
                                             },
                new SelectListItem
                                             {
                                                Text = "Inactive",
                                                 Value = "Inactive"
                                             }
            }.ToList();

            if (status != null)
            {
                var selected = types.Where(x => x.Text == status).First();
                selected.Selected = true;
                types.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = false });
                return types;
            }

            //Add Default Item at First Position.
            types.Insert(0, new SelectListItem { Text = "--Select --", Value = "", Disabled = true, Selected = true });
            return types;
        }
    }
}