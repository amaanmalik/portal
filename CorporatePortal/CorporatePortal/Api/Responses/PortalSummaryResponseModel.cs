﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class PortalSummaryResponseModel : BaseApiResponse
    {
        public bool IsSuccess { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public PortalSummary Summary { get; set; }
    }

    public class AmountSpentByProductResponseModel : BaseApiResponse
    {
        public bool IsSuccess { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public List<TotalsByProduct> Summary { get; set; }
    }

    public class TransactionByProductSummaryResponseModel : BaseApiResponse
    {
        public bool IsSuccess { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public List<TotalsByProduct> Summary { get; set; }
    }
}