﻿using CorporatePortal.Models.Product;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class CreditProfileTransactionsResponseModel : BaseApiResponse
    {
        public List<CreditProfileTransactionModel> Transactions { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class CreditProfileTransactionsByIdResponseModel : BaseApiResponse
    {
        public List<CreditProfileTransactionsModel> Transactions { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
