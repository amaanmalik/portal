﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CorporatePortal.Api.Responses
{
    public class GetPaymentMethodsResponse : BaseApiResponse
    {
        public List<CardFundSource> Cards { get; set; }

        public List<ACHFundSource> ACHs { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}