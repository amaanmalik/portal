﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class UpdateCardOrderResponse : BaseApiResponse
    {
        public string Id { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}