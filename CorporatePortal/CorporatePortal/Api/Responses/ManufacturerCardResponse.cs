﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class ManufacturerCardResponse : BaseApiResponse
    {
        public string Currency { get; set; }

        public string CardProxyId { get; set; }

        public string ProductCode { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}