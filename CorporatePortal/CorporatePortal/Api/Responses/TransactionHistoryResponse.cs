﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class TransactionHistoryResponse: BaseApiResponse
    {
        public TransactionHistoryResponse()
        {
            Transactions = new List<ApiTransaction>();
        }

        public List<ApiTransaction> Transactions { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}