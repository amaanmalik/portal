﻿using Newtonsoft.Json;
using CorporatePortal.Models.CardOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CorporatePortal.Models.User;

namespace CorporatePortal.Api.Responses
{
    public class BatchOrderHistoryResponseModel : BaseApiResponse
    {
        public List<BatchOrderHistory> Orders { get; set; }

        public bool IsSuccess { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

   
}