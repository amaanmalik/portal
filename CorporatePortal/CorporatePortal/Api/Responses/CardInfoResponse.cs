﻿using System;
using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class CardInfoResponse: BaseApiResponse
    {
        public string CardProxyId { get; set; }

        public double Balance { get; set; }

        public string Status { get; set; }

        public DateTime Expiry { get; set; }

        public string DisplayName { get; set; }

        public string Currency { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}