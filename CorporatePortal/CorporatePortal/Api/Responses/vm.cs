﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class CorrporateCardholderVM
    {
        public CardHolderDetailsResponse user { get; set; }
        public CardholderCardsResponse cards { get; set; }
    }
}