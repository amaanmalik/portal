﻿using Newtonsoft.Json;
using CorporatePortal.Models.CardOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class BatchOrderDetailResponseModel : BaseApiResponse
    {
        public bool IsSuccess { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

      public List<PrepaidCardOrder> Orders { get; set; }
    }
}