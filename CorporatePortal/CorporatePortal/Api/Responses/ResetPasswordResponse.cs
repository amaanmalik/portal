﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class ResetPasswordResponse: BaseApiResponse
    {
        public ApiUser UserInformation { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}