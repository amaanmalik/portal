﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class CreateCardholderResponse: BaseApiResponse
    {
        public string CardHolderId { get; set; }

        public bool HasPassedPEP { get; set; }

        public bool HasPassedSanctions { get; set; }

        public bool? HasPassedKYC { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}