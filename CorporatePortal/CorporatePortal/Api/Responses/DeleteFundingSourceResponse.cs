﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class DeleteFundingSourceResponse: BaseApiResponse
    {
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}