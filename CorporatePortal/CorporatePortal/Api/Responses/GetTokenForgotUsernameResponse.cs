﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class GetTokenForgotUsernameResponse: BaseApiResponse
    {
        public string Token { get; set; }

        public string Username { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}