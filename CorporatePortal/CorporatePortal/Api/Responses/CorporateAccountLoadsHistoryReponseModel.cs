﻿using CorporatePortal.Models.CorporateAccount;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class CorporateAccountLoadsHistoryReponseModel : BaseApiResponse
    {
        public List<CorporateAccountLoad> AccountLoads { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}