﻿using Newtonsoft.Json;
using CorporatePortal.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class NewCorporateUserResponse : BaseApiResponse
    {
        public CorporateStaffModel User { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}