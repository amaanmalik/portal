﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class CardLoadInfo
    {
        public string Currency { get; set; }

        public decimal MaxNonKycCardLoadAmount { get; set; } = 0;

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}