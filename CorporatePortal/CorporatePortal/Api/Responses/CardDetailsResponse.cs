﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class CardECommerceResponse: BaseApiResponse
    {
        public string CardNumber { get; set; }

        public string CVV { get; set; }

        public string ExpiryDate { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}