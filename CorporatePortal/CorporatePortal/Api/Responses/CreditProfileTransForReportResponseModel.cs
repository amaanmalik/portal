﻿using CorporatePortal.Models.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class CreditProfileTransForReportResponseModel : BaseApiResponse
    {
        public IList<CreditProfileTransForReportModel> Transactions { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class StatementsForReportResponseModel : BaseApiResponse
    {
        public IList<StatementForReportModel> Statements { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

}
