﻿using CorporatePortal.Api.Requests.CredNotes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses.CredNotes
{
    public class CreditNotesListResponseModel : BaseApiResponse
    {
        public List<CreditNoteModel> CreditNotes { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
}