﻿using CorporatePortal.Api.Requests.CredNotes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses.CredNotes
{
    public class CreditNoteResponseModel : BaseApiResponse
    {
        public CreditNoteModel CreditNote { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}