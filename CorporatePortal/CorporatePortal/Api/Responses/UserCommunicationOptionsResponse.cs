﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class UserCommunicationOptionsResponse: BaseApiResponse
    {
        public ApiUserCommunicationOption ManualTopup { get; set; } = new ApiUserCommunicationOption();

        public ApiUserCommunicationOption AutoTopup { get; set; } = new ApiUserCommunicationOption();

        public ApiUserCommunicationOption CardToCardTransfer { get; set; } = new ApiUserCommunicationOption();

        public ApiUserCommunicationOption ScheduledTransfer { get; set; } = new ApiUserCommunicationOption();

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}