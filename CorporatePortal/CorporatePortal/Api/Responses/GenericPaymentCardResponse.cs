﻿
using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{ 
    public class GenericPaymentCardResponse : BaseApiResponse
    {
        public string PaymentReference { get; set; }

        public bool IsSuccess { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}