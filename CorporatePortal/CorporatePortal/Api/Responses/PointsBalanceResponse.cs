﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class PointsBalanceResponse: BaseApiResponse
    {
        public int RewardPoints { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}