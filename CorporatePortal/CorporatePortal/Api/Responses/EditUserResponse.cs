﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class EditUserResponse : BaseApiResponse
    {
       

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}