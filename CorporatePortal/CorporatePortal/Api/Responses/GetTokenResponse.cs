﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class GetTokenResponse: BaseApiResponse
    {
        public string Token { get; set; }

        public string CardholderId { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}