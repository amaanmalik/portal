﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class PrepaidCardModel
    {
        public string CardProxyId { get; set; }

        public double Balance { get; set; }

        public string Status { get; set; }

        public DateTime Expiry { get; set; }

        public string DisplayName { get; set; }

        private string DisplayCurrency { get; set; }
            

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}