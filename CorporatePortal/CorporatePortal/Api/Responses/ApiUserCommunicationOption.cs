﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class ApiUserCommunicationOption
    {
        public bool SMS { get; set; } = false;

        public bool Email { get; set; } = false;

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}