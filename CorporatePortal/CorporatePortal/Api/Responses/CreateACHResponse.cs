﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class CreateACHResponse : BaseApiResponse
    {
        public int? ACHId { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}