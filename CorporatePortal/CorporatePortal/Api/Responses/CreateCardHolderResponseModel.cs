﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class CreateCardHolderResponseModel : BaseApiResponse
    {
       
        public string CardHolderId { get; set; }

        public bool HasPassedPEP { get; set; }

        public bool HasPassedSanctions { get; set; }

        public bool? HasPassedKYC { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}