﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class CardFundSource
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Pan { get; set; }

        public DateTime ExpiryDate { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}