﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class ApiUser
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<string> Roles { get; set; }
        public bool IsActive { get; set; }
        public bool HasActiveCard { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool? PassedKyc { get; set; }
        public bool? PassedPeps { get; set; }
        public bool? PassedSanctions { get; set; }
        public DateTime CreatedDate { get; set; }
        public string SpendProfile { get; set; }
        public string CardProxyId { get; set; }
        public string CardStatus { get; set; }

        public string ProductCode { get; set; }

        public string CreditProfile { get; set; }

        public bool? AllowATM { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}