﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class AutoTopupSettingsResponse : BaseApiResponse
    {
        public decimal Amount { get; set; }

        public bool Enabled { get; set; }

        public FundingSourceModel FundSource { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}