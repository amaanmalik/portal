﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class SpendDetailsResponse : BaseApiResponse
    {

        public string CardProxyId { get; set; }

        public double DailySpend { get; set; }

        public double WeeklySpend { get; set; }

        public double MonthlySpend { get; set; }

        public string DailyLimit { get; set; }

        public string WeeklyLimit { get; set; }

        public string MonthlyLimit { get; set; }

        public string DailyLeftToSpend { get; set; }

        public string WeeklyLeftToSpend { get; set; }

        public string MonthlyLeftToSpend { get; set; }

        public double LeftToSpend { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }    
}