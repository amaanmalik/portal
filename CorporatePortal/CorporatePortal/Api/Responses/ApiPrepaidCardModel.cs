﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class ApiPrepaidCardModel
    {
        public string ProductCode { get; set; }

        public string CardProxyId { get; set; }

        public double Balance { get; set; }

        public string Status { get; set; }

        public string CardholderId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Expiry { get; set; }

        public string DisplayName { get; set; }

        private string DisplayCurrency { get; set; }

        public string Currency
        {
            get { return DisplayCurrency; }
            set
            {
                DisplayCurrency = CurrencyHelper.ConvertStringToSymbol(value);
            }
        }

        public virtual string FundsOwnership { get; set; }

        public PrepaidCardSpendDetails SpendDetail { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public static class CurrencyHelper
    {
        public static string ConvertStringToSymbol(string stringCurrency)
        {
            string convertedCurrency;
            switch (stringCurrency)
            {
                case "EUR":
                    convertedCurrency = "€";
                    break;
                case "GBP":
                    convertedCurrency = "£";
                    break;
                case "USD":
                    convertedCurrency = "$";
                    break;
                default:
                    convertedCurrency = stringCurrency;
                    break;
            }

            return convertedCurrency;
        }
    }
}