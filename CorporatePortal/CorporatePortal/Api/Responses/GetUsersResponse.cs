﻿using Newtonsoft.Json;
using CorporatePortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class GetUsersResponse : BaseApiResponse
    {
        public List<ApiUser> Users { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class GetUsersWithRolesResponse : BaseApiResponse
    {
        public List<ApiUser> Users { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}