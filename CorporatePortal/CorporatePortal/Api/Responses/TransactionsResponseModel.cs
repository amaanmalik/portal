﻿using CorporatePortal.Models.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class TransactionsResponseModel : BaseApiResponse
    {
        public IList<TransactionsReportModel> Transactions { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}