﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class GetFundingSourcesResponse: BaseApiResponse
    {
        public List<ApiFundingSourceModel> FundingSources { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}