﻿using CorporatePortal.Models.Product;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class PurchaseCategoryResponseModel : BaseApiResponse
    {
        public PurchaseCategory Category { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}