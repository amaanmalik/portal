﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{

    public class UserInformationResponse : BaseApiResponse
    {       

        public List<UserInformation> Users { get; set; } = new List<UserInformation>();

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class UserInformation
    {
        public UserInformation()
        {
        }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public List<string> Roles { get; set; }
    }
}