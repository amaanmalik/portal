﻿using CorporatePortal.Models.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class AccountTransactionHistoryResponseModel : BaseApiResponse
    {
        public IList<Transaction> Transactions { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}