﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class PortalSummary
    {
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal TotalPrepaidCards { get; set; }
        public decimal TotalInactiveCards { get; set; }
        public decimal TotalCorporateBalanceAvailable { get; set; }
        public decimal TotalFundsLoaded { get; set; }
        public decimal TotalCardLoads { get; set; }
        public decimal TotalCorporateCardLoads { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal TotalCardsOrdered { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public decimal TotalTransactions { get; set; }
        public int TotalCardHolders { get; set; }
    }

    public class TotalsByProduct
    {
        public string ProductCode { get; set; }
        public decimal Total { get; set; }
    }
}