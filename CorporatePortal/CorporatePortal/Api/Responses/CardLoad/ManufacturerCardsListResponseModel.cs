﻿using Newtonsoft.Json;
using CorporatePortal.Models.CardLoad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses.CardLoad
{
    public class ManufacturerCardsListResponseModel : BaseApiResponse
    {
        public List<CorporateCard> ActiveCards { get; set; }
        public List<CorporateCard> InactiveCards { get; set; }
        public List<CorporateCard> VirtualCards { get; set; }
        public string ProductCode { get; set; }
        public List<CorporateFundsSummary> AccountsList { get; set; }
        public bool IsSuccess { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
