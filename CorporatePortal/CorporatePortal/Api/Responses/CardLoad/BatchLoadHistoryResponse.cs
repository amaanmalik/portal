﻿using CorporatePortal.Models.CardLoad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses.CardLoad
{
    public class BatchLoadHistoryResponse : BaseApiResponse
    {      
        public List<BatchLoadHistory> LoadList { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }

    public class BatchLoadDetailsResponse : BaseApiResponse
    {
        public List<CorporateBatchLoadOrder> LoadList { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}