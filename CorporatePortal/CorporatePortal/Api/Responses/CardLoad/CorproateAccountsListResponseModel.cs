﻿using Newtonsoft.Json;
using CorporatePortal.Models.CardLoad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses.CardLoad
{
    public class CorporateAccountsListResponseModel : BaseApiResponse
    {
        public List<CorporateAccount> AccountsList { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}