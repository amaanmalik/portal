﻿using CorporatePortal.Models.Product;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class CreditProfileListResponseModel : BaseApiResponse
    {
        public List<CreditProfileModel> CreditProfileList { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}