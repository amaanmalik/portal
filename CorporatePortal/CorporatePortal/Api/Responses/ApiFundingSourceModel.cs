﻿using System;
using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class ApiFundingSourceModel
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the source.
        /// </summary>
        /// <value>
        /// The name of the source.
        /// </value>
        public string SourceName { get; set; }

        /// <summary>
        /// Gets or sets the obscured pan number (**** **** **** last4).
        /// </summary>
        /// <value>
        /// The pan.
        /// </value>
        public string Pan { get; set; }

        /// <summary>
        /// Gets or sets the expiry date.
        /// </summary>
        /// <value>
        /// The expiry date.
        /// </value>
        public DateTime ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is used for current autop top up.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is used for current autop top up; otherwise, <c>false</c>.
        /// </value>
        public bool IsUsedForCurrentAutopTopUp { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}