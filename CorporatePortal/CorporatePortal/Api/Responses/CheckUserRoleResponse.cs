﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class CheckUserRoleResponse : BaseApiResponse
    {
        public bool IsInRole { get; set; }
    }
}

