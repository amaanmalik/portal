﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public enum ApiErrorCode
    {
        //Generic codes 
        Generic_CardProxyIdNotFound = 10000000,
        Generic_UserNotLoggedIn = 10000001,
        Generic_CardAlreadyInUse = 10000002,
        Generic_CardIdValidation = 10000003,
        Generic_CardDisplayNameValidation = 10000004,
        Generic_CardDoesNotBelongToUser = 10000005,
        Generic_PaymentMethodDoesNotBelongToUser = 10000006,
        Generic_CardholderNotFound = 10000007,

        //Login
        Login_AuthTokenRetrievalError = 20000000,
        Login_UserInactive = 20000001,

        //CardInfo

        //Activate
        Activate_ActivateCardFailed = 30000000,
        Activate_TermsAgreedValidation = 30000001,
        Activate_DateOfBirthValidation = 30000002,
        Activate_CardIdValidation = 30000003,
        Activate_CardDisplayNameValidation = 30000004,
        Activate_FirstNameValidation = 30000005,
        Activate_LastNameValidation = 30000006,
        Activate_Address1Validation = 30000007,
        Activate_NationalityValidation = 30000008,
        Activate_SSNValidation = 30000009,
        Activate_StateValidation = 30000010,
        Activate_ZipCodeValidation = 30000011,
        Activate_CityValidation = 30000012,
        Activate_MobileValidation = 30000013,
        Activate_EmailAddressValidation = 30000014,
        Activate_EmailConfirmValidation = 30000015,
        Activate_UsernameAlreadyTaken = 30000016,
        Activate_EmailAlreadyTaken = 30000017,
        Activate_UserCreationError = 30000018,
        Activate_KycFailed = 30000019,
        Activate_PPAssignCardFailed = 30000021,
        Activate_CardHolderTermsAgreedValidation = 30000022,
        Activate_EDisclosureTermsAgreedValidation = 30000023,
        Activate_PrivacyPolicyTermsAgreedValidation = 30000024,
        CreateCardHolder_Failed = 30000025,

        Rings_UnableToRetrieveInfoFromPP = 40000000,

        ResetPin_InvalidPin = 50000000,
        ResetPin_UpdateFailed = 50000001,

        CardDetails_UnableToRetrieveDetails = 60000001,

        UpdateStatus_InvalidStatus = 70000000,
        UpdateStatus_ErrorUpdatingStatus = 70000001,

        PaymentMethods_UnableToRetrievePaymentMethodsFromPP = 80000000,

        CreateCardPaymentMethod_ErrorCreatingCardAtPP = 90000000,
        CreateCardPaymentMethod_ErrorRetrievingCardAtPP = 90000001,
        CreateCardPaymentMethod_InvalidCardNumber = 90000002,
        CreateCardPaymentMethod_PanValidation = 90000003,
        CreateCardPaymentMethod_CvvValidation = 90000004,
        CreateCardPaymentMethod_ExpiryDateValidation = 90000005,
        // CreateCardPaymentMethod_ExpiryYearValidation = 90000006,
        CreateCardPaymentMethod_NameOnCardValidation = 90000007,
        CreateCardPaymentMethod_BillingPostalCodeValidation = 90000008,
        CreateCardPaymentMethod_BillingAddressValidation = 90000009,
        CreateCardPaymentMethod_BillingTownCityValidation = 90000010,

        CreatePaymentMethod_TooManyPaymentMethods = 10000000,

        Topup_AuthSuccessLoadFailure = 11000000,
        Topup_AuthFailure = 11000001,
        Topup_BankLoadFailure = 11000002,
        Topup_LoadLimitExceeded = 11000003,
        Topup_PrepaidCardidValidation = 11000004,
        Topup_PaymentMethodIdValidation = 11000005,
        Topup_AmountValidation = 11000006,
        Topup_PrepaidCardInvalidTopupState = 11000007,
        Topup_LoadFailure = 11000008, //TopupResult.LoadFailure - money wasn't able to be taken
        Topup_RefundSuccess = 11000009, //TopupResult.RefundSuccess - the money was taken but not loaded so was refunded
        Topup_RefundFailure = 11000010, //TopupResult.RefundFailure - money taken, couldn't load and couldn't refund

        ValidateTrialAmounts_InvalidPaymentMethodId = 12000000,
        ValidateTrialAmounts_InvalidTrialAmounts = 12000001,
        ValidateTrialAmounts_ExceededTrialAmountValidationAttempts = 12000002,
        ValidateTrialAmounts_UnknownError = 12000003,

        CreateBankPaymentMethod_BankAccountNameValidation = 13000000,
        CreateBankPaymentMethod_BankRoutingNumberValidation = 13000001,
        CreateBankPaymentMethod_BankAccountNumberValidation = 13000002,

        UnloadCard_UnknownFailure = 14000000,

        Account_InvalidCredentials = 15000000,
        Account_ChangePasswordError = 15000001,
        Account_InvalidEmailAddress = 15000002,

        Contact_InvalidEmailAddress = 16000000,

        Corporate_Account_Not_Found = 17000000,
        Corporate_Account_PaymentCard_Not_Found = 17000002,
        Corporate_Account_CardLoad_Insufficient_Funds = 17000003,
        Corporate_Account_CardLoad_Invalid_Currency = 17000004,

        Invalid_ProductCode = 18000001,
        Order_Virtual_Card_Failed = 18000002,

        Unknown_Error = -99999999,
    }
}