﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class CountryResponse : BaseApiResponse
    {
        public string CountryCode { get; set; }

        public string LocalCurrency { get; set; }

        public bool IsKycAvailable { get; set; }

        public List<CardLoadInfo> CardLoadInfos { get; set; } = new List<CardLoadInfo>();

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
