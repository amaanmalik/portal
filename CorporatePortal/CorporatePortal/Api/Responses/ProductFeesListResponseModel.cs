﻿using CorporatePortal.Models.ProductFee;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class ProductFeesListResponseModel : BaseApiResponse
    {
        public List<ProductFeeModel> ProductFees{ get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}