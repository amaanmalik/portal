﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class CardholderCardsResponse: BaseApiResponse
    {
        public List<ApiPrepaidCardModel> Cards { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class GetCardholderFromProxyResponse : BaseApiResponse
    {
        public ApiPrepaidCardModel Card { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}