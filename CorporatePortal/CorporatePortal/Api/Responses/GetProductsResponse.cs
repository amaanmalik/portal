﻿using CorporatePortal.Models.Product;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{

    public class QcsProductListResponseModel : BaseApiResponse
    {
        public List<QcsProduct> ProductList { get; set; } = new List<QcsProduct>();

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class QcsProductResponseModel : BaseApiResponse
    {
        public QcsProductResponseModel() : base()
        {
        }

        public QcsProduct Product { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}