﻿using System;
using Newtonsoft.Json;

namespace CorporatePortal.Api.Responses
{
    public class ApiTransaction
    {
        public string Id { get; set; }

        public DateTime AuthDate { get; set; }

        public double Amount { get; set; }

        public string TransactionType { get; set; }

        public string Currency { get; set; }

        public string Description { get; set; }

        public string MerchantName { get; set; }

        public string Reference { get; set; }

        public string ItemType { get; set; }

        public double BillAmount { get; set; }

        public double Balance { get; set; }

        public string CardId { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}