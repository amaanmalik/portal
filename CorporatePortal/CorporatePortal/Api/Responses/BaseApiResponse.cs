﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Responses
{
    public class BaseApiResponse
    {
        public bool IsSuccess { get; set; }

        public List<string> ErrorCodes { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}