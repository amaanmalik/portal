﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api
{
    public enum CardStatus
    {
        Normal = 0,        
        Lost = 3,
        Stolen = 4,
        Suspend = 7,
        Block = 10
    }
}