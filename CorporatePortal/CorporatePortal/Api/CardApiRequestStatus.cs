﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api
{
    public enum CardApiRequestStatus
    {
        Normal,
        Suspend,
        Block,
        Stolen
    }
}