﻿
using Newtonsoft.Json;
using CorporatePortal.Api.Responses;

namespace UsaSoccer2018.Web.Api.Requests
{
    public class UpdateUserDetailsRequest
    {
        public CardHolderDetails UserToUpdate { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}