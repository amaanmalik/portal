﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests.CardOrder
{
    public class OrderIndividualCardRequest
    {
        [Required]
        public string ProductCode { get; set; }

        public string Title { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        [Display(Name = "County")]
        public string State { get; set; }

        [Required]
        public string PostCode { get; set; }

        public string ISOCountryCode { get; set; }

        public string CardholderId { get; set; }

        public string RecipientEmail { get; set; }

        public string Dob { get; set; }

        public string Language { get; set; }

        public string Mobile { get; set; }

        public string cardFileName { get; set; }
        /// <summary>
        /// Indicates that the request is for a replacement card.
        /// The card proxy id is the card to be replaced.
        /// </summary>
        public string ReplacedCardProxyId { get; set; }

        public string NameOnCard { get; set; }

        public string AdditionalCardEmbossData { get; set; }

        public string Address3 { get; set; }

        public string CardDesign { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}