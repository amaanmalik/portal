﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class CreateSpendProfileOverrideRequest
    {
        public double Amount { get; set; }

        public string CardId { get; set; }
    }
}