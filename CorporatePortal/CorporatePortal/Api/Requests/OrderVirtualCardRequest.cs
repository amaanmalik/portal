﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class OrderVirtualCardRequest
    {
        public string ProductCode { get; set; }

        public string CardHolderId { get; set; }
    }
}