﻿using CorporatePortal.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class UpdateSpendProfileRequest
    {
        public SpendProfileModel SpendProfile { get; set; }

        public List<PurchaseCategory> PurchaseCategories { get; set; }
    }
}


