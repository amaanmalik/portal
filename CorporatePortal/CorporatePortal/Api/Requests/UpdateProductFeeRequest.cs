﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class UpdateProductFeeRequest
    {
        public string Name { get; set; }

        public double FinanceFee { get; set; }

        public double CardFee { get; set; }

        public double CardIsueeFee { get; set; }

        public double FXFee { get; set; }
    }
}