﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests.CardLoad
{
    public class LoadCardRequest
    {
        public string CardProxyId { get; set; }

        public decimal Amount { get; set; }

        public int CorporateAccountId { get; set; }

        public string CurrencyCode { get; set; }

    }
}