﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests.CardLoad
{
    public class GetManufacturerCardsByProductRequest
    {
        public string ProductCode { get; set; }
    }
}
