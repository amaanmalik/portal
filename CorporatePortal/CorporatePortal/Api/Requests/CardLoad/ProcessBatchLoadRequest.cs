﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests.CardLoad
{
    public class ProcessBatchLoadRequest
    {
        public int fileId { get; set; }
    }
}