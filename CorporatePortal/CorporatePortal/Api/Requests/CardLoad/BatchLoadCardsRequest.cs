﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests.CardLoad
{
    public class BatchLoadCardsRequest
    {
        public List<LoadCardRequest> Loads { get; set; }
        
        public int CorporateAccountId { get; set; }

        public string FileName { get; set; }
    }
}