﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class ChangeCardStatusRequest
    {
        public string CardProxyId { get; set; }

        public string Status { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

   
}