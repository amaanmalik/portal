﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class AddSpendProfileOverrideRequestModel
    {
        public string CardId { get; set; }

        public double Amount { get; set; }

        public string UserId { get; set; }
    }
}