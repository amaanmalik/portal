﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class NewPaymentCard
    {
        public string CardName { get; set; }

        public string Pan { get; set; }

        public string Cvv { get; set; }

        private string _paddedExpiryMonth;
        public string ExpiryMonth
        {
            get { return _paddedExpiryMonth; }
            set
            {
                if (value.Length == 1)
                {
                    _paddedExpiryMonth = $"0{value}";
                }
                else
                {
                    _paddedExpiryMonth = $"{value}";
                }
            }
        }

        public string ExpiryYear { get; set; }

        public string NameOnCard { get; set; }

        public string BillingPostalCode { get; set; }

        public string BillingAddress { get; set; }

        public string BillingTownCity { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}