﻿using System;
using Newtonsoft.Json;
using CorporatePortal.Api.Responses;

namespace CorporatePortal.Api.Requests
{
    public class CreateCardholderRequest
    {
        public string Mobile { get; set; }

        public string LandlineTelephone { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Language { get; set; }

        public DateTime DateOfBirth { get; set; }

        public Gender? Gender { get; set; }

        public string SocialSecurityNumber { get; set; }

        public string IdCardNumber { get; set; }

        public string TaxIdCardNumber { get; set; }

        public string Title { get; set; }

        public string HouseNumberOrBuilding { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string SecondSurname { get; set; }

        public string Nationality { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        public string Country { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}