﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests 
{
    public class TopupWithExistingCardRequest
    {
        public string CardProxyId { get; set; }

        public string FundSourceId { get; set; }

        public decimal Amount { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}