﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class ActivateCardRequest
    {
        public string CardHolderId { get; set; }

        public string CardProxyId { get; set; }

        public string CardDisplayName { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}