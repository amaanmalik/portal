﻿namespace CorporatePortal.Api.Requests
{
    public class OrderCardRequest
    {
        public string ProductCode { get; set; }

        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        public string ISOCountryCode { get; set; }
        public string CardholderId { get; set; }
        public string RecipientEmail { get; set; }
        public string Dob { get; set; }
        public string Language { get; set; }
    }
}