﻿namespace CorporatePortal.Api.Requests
{
    public class DeleteACHRequest
    {
        public int ACHId { get; set; }
    }
}