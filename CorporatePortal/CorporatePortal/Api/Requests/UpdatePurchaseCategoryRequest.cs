﻿using CorporatePortal.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class UpdatePurchaseCategoryRequest
    {
        public PurchaseCategory PurchaseCategory { get; set; }

        public List<MerchantCodeModel> MerchantCodes { get; set; }
    }
}