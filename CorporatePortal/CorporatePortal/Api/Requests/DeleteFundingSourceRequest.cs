﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class DeleteFundingSourceRequest
    {
        public int FundSourceId { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}