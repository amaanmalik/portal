﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class ResetPasswordRequest
    {
        public string CardholderId { get; set; }

        public string Token { get; set; }

        public string NewPassword { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}