﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class TopupWithNewCardRequest
    {
        public string CardProxyId { get; set; }

        public NewPaymentCard CardFundSource { get; set; }

        public decimal Amount { get; set; }

        public bool SaveCard { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}