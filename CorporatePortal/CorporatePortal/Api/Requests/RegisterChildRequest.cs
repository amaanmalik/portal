﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class RegisterChildRequest
    {       
        public string ParentId { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }        

        public DateTime DateOfBirth { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }       

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}