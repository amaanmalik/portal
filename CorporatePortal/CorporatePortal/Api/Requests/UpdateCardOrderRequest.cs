﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class UpdateCardOrderRequest
    {
        public int OrderId { get; set; }

        public bool Paid { get; set; }

        public string PaymentRef { get; set; }

        public bool SendConfirmation { get; set; }

        public string BillingName { get; set; }

        public string BillingAddress1 { get; set; }

        public string BillingAddress2 { get; set; }

        public string BillingCountry { get; set; }

        public string BillingPostcode { get; set; }

        public decimal Amount { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}