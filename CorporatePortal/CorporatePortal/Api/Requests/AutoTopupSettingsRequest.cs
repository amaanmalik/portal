﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class AutoTopupSettingsRequest
    {
        public string CardProxyId { get; set; }

        public decimal Amount { get; set; }

        public bool Enabled { get; set; }

        public int FundSourceId { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}