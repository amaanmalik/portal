﻿namespace CorporatePortal.Api.Requests
{
    public class ValidateACHRequest
    {
        public int Amount1 { get; set; }

        public int Amount2 { get; set; }

        public int ACHId { get; set; }
    }
}