﻿using CorporatePortal.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class NewCorporateStaffRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public UserRole Role { get; set; }
    }
}