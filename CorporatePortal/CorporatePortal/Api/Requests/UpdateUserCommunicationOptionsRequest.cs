﻿
using Newtonsoft.Json;
using CorporatePortal.Api.Responses;

namespace CorporatePortal.Api.Requests
{
    public class UpdateUserCommunicationOptionsRequest
    {
        public UpdateUserCommunicationOptionsRequest()
        {
            ManualTopup = new ApiUserCommunicationOption();
            AutoTopup = new ApiUserCommunicationOption();
            CardToCardTransfer = new ApiUserCommunicationOption();
            ScheduledTransfer = new ApiUserCommunicationOption();
        }

        public string CardholderId { get; set; }

        public ApiUserCommunicationOption ManualTopup { get; set; }

        public ApiUserCommunicationOption AutoTopup { get; set; }

        public ApiUserCommunicationOption CardToCardTransfer { get; set; }

        public ApiUserCommunicationOption ScheduledTransfer { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}