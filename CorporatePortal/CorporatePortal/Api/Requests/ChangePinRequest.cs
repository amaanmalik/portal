﻿namespace CorporatePortal.Api.Requests
{
    public class ChangePinRequest
    {
        public string CardProxyId { get; set; }

        public string PIN { get; set; }
    }
}