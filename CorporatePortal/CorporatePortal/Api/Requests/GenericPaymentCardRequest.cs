﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class GenericPaymentCardRequest
    {
        public NewPaymentCard CardFundSource { get; set; }

        public decimal Amount { get; set; }

        public decimal CreditFeePercentage { get; set; }

        public decimal DebitFeePercentage { get; set; }

        public string Currency { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}