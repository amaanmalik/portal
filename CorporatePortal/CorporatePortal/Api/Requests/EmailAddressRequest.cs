﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class EmailAddressRequest
    {
        public string EmailAddress { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}