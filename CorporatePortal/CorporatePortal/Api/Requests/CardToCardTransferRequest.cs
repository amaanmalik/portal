﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class CardToCardTransferRequest
    {
        public string CardHolderId { get; set; }

        public string FromCardProxyId { get; set; }

        public string ToCardProxyId { get; set; }

        public double Amount { get; set; }
    }
}