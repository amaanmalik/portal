﻿using Newtonsoft.Json;

namespace CorporatePortal.Api.Requests
{
    public class ContactFormRequest
    {
        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}