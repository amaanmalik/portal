﻿using CorporatePortal.Api.Requests.CardOrder;
using CorporatePortal.Models.CardOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class BatchOrderCardRequest
    {
        public List<OrderIndividualCardRequest> Orders { get; set; }
    }   
}