﻿namespace CorporatePortal.Api.Requests
{
    public class CreateACHRequest
    {
        public string BankAccountNumber { get; set; }

        public string BankRoutingNumber { get; set; }

        public string CardProxyId { get; set; }

        public string Name { get; set; }
    }
}