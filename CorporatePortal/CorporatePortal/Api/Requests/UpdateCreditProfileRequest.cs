﻿using CorporatePortal.Models.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class UpdateCreditProfileRequest
    {
        public CreditProfileModel CreditProfile { get; set; }
    }
}