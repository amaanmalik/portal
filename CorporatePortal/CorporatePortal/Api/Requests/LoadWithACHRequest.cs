﻿namespace CorporatePortal.Api.Requests
{
    public class LoadWithACHRequest
    {
        public decimal Amount { get; set; }

        public string CardProxyId { get; set; }

        public int ACHId { get; set; }
    }
}