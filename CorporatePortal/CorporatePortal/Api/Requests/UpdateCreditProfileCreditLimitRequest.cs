﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class UpdateCreditProfileCreditLimitRequest
    {       
        public int? CreditProfileId { get; set; }

        public double? CreditLimit { get; set; }

        public string CreditProfileName { get; set; }
    }
}