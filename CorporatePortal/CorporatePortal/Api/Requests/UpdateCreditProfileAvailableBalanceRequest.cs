﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests
{
    public class UpdateCreditProfileAvailableBalanceRequest
    {
        public int? CreditProfileId { get; set; }

        public double? AvailableBalance { get; set; }

        public string CreditProfileName { get; set; }

        public string Comments { get; set; }
    }
}