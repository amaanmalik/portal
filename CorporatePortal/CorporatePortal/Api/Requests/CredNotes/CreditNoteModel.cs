﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests.CredNotes
{
    public class CreditNoteModel
    {
        [JsonProperty(PropertyName = "statementId")]
        public int CreditNoteId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        [JsonProperty(PropertyName = "updatedDate")]
        public DateTime UpdatedDate { get; set; }

        [JsonProperty(PropertyName = "statementType")]
        public string StatementType { get; set; }

        [JsonProperty(PropertyName = "creditprofileId")]
        public int CreditProfileId { get; set; }

        [JsonProperty(PropertyName = "creditProfileName")]
        public string CreditProfileName { get; set; }

        [JsonProperty(PropertyName = "statementYear")]
        public string StatementYear { get; set; }

        [JsonProperty(PropertyName = "statementMonth")]
        public string StatementMonth { get; set; }

        [JsonProperty(PropertyName = "timeActioned")]
        public string TimeActioned { get; set; }

        [JsonProperty(PropertyName = "totalSpent")]
        public double TotalSpent { get; set; }

        [JsonProperty(PropertyName = "financeFeeTotal")]
        public double FinanceFeeTotal { get; set; }

        [JsonProperty(PropertyName = "cardFeeGross")]
        public double CardFeeGross { get; set; }

        [JsonProperty(PropertyName = "cardFeeNet")]
        public double CardFeeNet { get; set; }

        [JsonProperty(PropertyName = "cardIssueFeeGross")]
        public double CardIssueFeeGross { get; set; }

        [JsonProperty(PropertyName = "cardIssueFeeNet")]
        public double CardIssueFeeNet { get; set; }

        [JsonProperty(PropertyName = "feeVatPercentage")]
        public double FeeVatPercentage { get; set; }

        [JsonProperty(PropertyName = "fxFeeTotal")]
        public double FXFeeTotal { get; set; }

        [JsonProperty(PropertyName = "totalGross")]
        public double TotalGross { get; set; }

        [JsonProperty(PropertyName = "totalNet")]
        public double TotalNet { get; set; }

        [JsonProperty(PropertyName = "paid")]
        public bool Paid { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        public string BusinessCentralId { get; set; }
    }
}