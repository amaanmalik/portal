﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests.CredNotes
{
    public class AddCreditNoteRequestModel
    {
        public virtual DateTime CreatedDate { get; set; }

        public virtual DateTime UpdatedDate { get; set; }

        public string StatementType { get; set; }

        public int CreditProfileId { get; set; }

        public string CreditProfileName { get; set; }

        public string StatementYear { get; set; }

        public string StatementMonth { get; set; }

        public string TimeActioned { get; set; }

        public double TotalSpent { get; set; }

        public bool Paid { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }
    }
}