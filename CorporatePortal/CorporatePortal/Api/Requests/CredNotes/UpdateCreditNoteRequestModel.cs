﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api.Requests.CredNotes
{
    public class UpdateCreditNoteRequestModel
    {
        public int statementId { get; set; }

        public string Description { get; set; }

        public double Amount { get; set; }
    }
}