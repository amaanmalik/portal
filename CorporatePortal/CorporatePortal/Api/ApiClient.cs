﻿using System;
using NLog;
using CorporatePortal.Api.Requests;
using CorporatePortal.Api.Requests.CardLoad;
using CorporatePortal.Api.Requests.CardOrder;
using CorporatePortal.Api.Responses;
using CorporatePortal.Api.Responses.CardLoad;
using CorporatePortal.Models.Product;
using CorporatePortal.Models.Report;
using CorporatePortal.Models.CardLoad;
using CorporatePortal.Models.ProductFee;
using CorporatePortal.Api.Responses.CredNotes;
using CorporatePortal.Api.Requests.CredNotes;

namespace CorporatePortal.Api
{
    public class ApiClient : BaseApiClient, IApiService
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public ApiClient(ApiConfig config) : base(config)
        {
        }

        public CreditProfileTransForReportResponseModel GetCreditProfileTransForReport()
        {
            var requestUrl = String.Format(ApiConfig.CreditProfileTransForReport);
            var response = MakeGetCall<CreditProfileTransForReportResponseModel>(requestUrl);

            Logger.Debug("Response = {0}", response);

            return response;
        }

        public StatementsForReportResponseModel GetStatementsForReport()
        {
            var requestUrl = String.Format(ApiConfig.StatementsForReport);
            var response = MakeGetCall<StatementsForReportResponseModel>(requestUrl);

            Logger.Debug("Response = {0}", response);

            return response;
        }

        public SpendOverrideListResponseModel GetSpendProfileOverridList()
        {
            var requestUrl = String.Format(ApiConfig.GetSpendProfileOverrideList);
            var response = MakeGetCall<SpendOverrideListResponseModel>(requestUrl);
            
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse CreateSpendProfileOverride(CreateSpendProfileOverrideRequest request)
        {
            Logger.Debug("Create CreditNote : request = {0} ", request);
            var response = MakePostCall<CreateSpendProfileOverrideRequest, BaseApiResponse>(ApiConfig.CreateSpendProfileOverride, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditNoteResponseModel AddCreditNote(AddCreditNoteRequestModel request)
        {
            Logger.Debug("Create CreditNote : request = {0} ", request);
            var response = MakePostCall<AddCreditNoteRequestModel, CreditNoteResponseModel>(ApiConfig.AddCreditNote, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditNoteResponseModel GetCreditNoteById(int creditNoteId)
        {
            Logger.Debug("Getting CreditNoteyId : startDate = {0} ", creditNoteId);
            var requestUrl = String.Format(ApiConfig.GetCreditNoteDetails, creditNoteId);
            var response = MakeGetCall<CreditNoteResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditNotesListResponseModel GetCreditNotes()
        {
            var requestUrl = String.Format(ApiConfig.GetCreditNotesList);
            var response = MakeGetCall<CreditNotesListResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse DeleteCreditNote(int creditNoteId)
        {
            var requestUrl = String.Format(ApiConfig.DeleteCreditNote, creditNoteId);
            var response = MakeGetCall<BaseApiResponse>(requestUrl);
           

            return response;
        }

        public CreditNoteResponseModel UpdateCreditNote(int creditNoteId, UpdateCreditNoteRequestModel request)
        {
            var requestUrl = String.Format(ApiConfig.EditCreditNote, creditNoteId);
            var response = MakePostCall<UpdateCreditNoteRequestModel, CreditNoteResponseModel>(requestUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public SpendDetailsResponse GetSpendDetails(string cardProxyId)
        {
            Logger.Debug("Getting CreditProfiles ");
            var requestUrl = String.Format(ApiConfig.GetSpendDetails,cardProxyId);
            var response = MakeGetCall<SpendDetailsResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public TransactionHistoryResponse GetCorporaeAccountCardTransactionHistory(string cardProxyId, DateTime startDate, int months)
        {
            Logger.Debug("GetTransactions (cardProxyId = {0}, startDate = {1}, months = {2}", cardProxyId, startDate, months);
            var requestUrl = String.Format(ApiConfig.CorporateAccountCardTransactionsUrl, cardProxyId, startDate.ToString("yyyy-MM-dd"), months);
            var response = MakeGetCall<TransactionHistoryResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CorporateAccountLoadsHistoryReponseModel GetCorporateAccountLoadHistory(int corporateAccountId)
        {
            Logger.Debug("Getting AccountLoadsHistory : startDate = {0} ", corporateAccountId);
            var requestUrl = String.Format(ApiConfig.CorporateAccountLoadsHistoryUrl, corporateAccountId);
            var response = MakeGetCall<CorporateAccountLoadsHistoryReponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse GetCorporaeAccountCardTransactionHistory(string cardProxyId)
        {
            return null;
        }

        #region CreditProfile

        public CreditProfileListResponseModel GetCreditProfilesList()
        {
            Logger.Debug("Getting CreditProfiles ");
            var requestUrl = String.Format(ApiConfig.GetCreditProfileList);
            var response = MakeGetCall<CreditProfileListResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditProfileResponseModel GetCreditProfileById(int creditProfileId)
        {
            Logger.Debug("Getting CreditProfileById : startDate = {0} ", creditProfileId);
            var requestUrl = String.Format(ApiConfig.GetCreditProfileById, creditProfileId);
            var response = MakeGetCall<CreditProfileResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditProfileResponseModel CreateCreditProfile(CreditProfileModel request)
        {
            Logger.Debug("Create CredutProfile : request = {0} ", request);
            var response = MakePostCall<CreditProfileModel, CreditProfileResponseModel>(ApiConfig.CreateCreditProfile, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        //public CreditProfileResponseModel UpdateCreditProfile(UpdateCreditProfileRequest request)
        //{
        //    Logger.Debug("Update CreditProfile : request = {0} ", request);
        //    var response = MakePostCall<UpdateCreditProfileRequest, CreditProfileResponseModel>(ApiConfig.UpdateCreditProfile, request);
        //    Logger.Debug("Response = {0}", response);

        //    return response;
        //}

        public CreditProfileResponseModel UpdateCreditProfileCreditLimit(UpdateCreditProfileCreditLimitRequest request)
        {
            Logger.Debug("Update CreditProfile CreditLimit : request = {0} ", request);
            var response = MakePostCall<UpdateCreditProfileCreditLimitRequest, CreditProfileResponseModel>(ApiConfig.UpdateCreditProfileCreditLimit, request);
            Logger.Debug("Response = {0}", response);

            return response;

        }

        public CreditProfileResponseModel UpdateCreditProfileAvailableBalance(UpdateCreditProfileAvailableBalanceRequest request)
        {
            Logger.Debug("Update CreditProfile AvailableBalance: request = {0} ", request);
            var response = MakePostCall<UpdateCreditProfileAvailableBalanceRequest, CreditProfileResponseModel>(ApiConfig.UpdateCreditProfileAvailableBalance, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditProfileTransactionsResponseModel GetCreditProfileTransactions(int creditProfileId)
        {
            Logger.Debug("Getting CreditProfile Transactions: CreditProfileId {0} ", creditProfileId);
            var requestUrl = String.Format(ApiConfig.GetCreditProfileTransactions, creditProfileId);
            var response = MakeGetCall<CreditProfileTransactionsResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditProfileTransactionsByIdResponseModel GetCreditProfileTransactionsById(int creditProfileId)
        {
            Logger.Debug("Getting CreditProfile Transactions: CreditProfileId {0} ", creditProfileId);
            var requestUrl = String.Format(ApiConfig.GetCreditProfileTransactionsById, creditProfileId);
            var response = MakeGetCall<CreditProfileTransactionsByIdResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditProfileTransactionsByIdResponseModel SuperReportTransactions(SuperReportRequestModel request)
        {
            Logger.Debug("Getting SuperReport Transactions: request {0} ", request);
            var response = MakePostCall<SuperReportRequestModel, CreditProfileTransactionsByIdResponseModel>(ApiConfig.SuperReportTransactions, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }


        public BaseApiResponse DeleteCreditProfile(int creditProfileId)
        {
            Logger.Debug("Delete CreditProfile : CreditProfileId {0} ", creditProfileId);
            var requestUrl = String.Format(ApiConfig.DeleteCreditProfile, creditProfileId);
            var response = MakeGetCall<BaseApiResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse DeleteSpendProfile(int spendProfileId)
        {
            Logger.Debug("Delete SpendProfile : SpendProfileId {0} ", spendProfileId);
            var requestUrl = String.Format(ApiConfig.DeleteSpendProfile, spendProfileId);
            var response = MakeGetCall<BaseApiResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditProfileResponseModel UpdateCreditProfileName(UpdateCreditProfileNameRequest request)
        {
            Logger.Debug("Update CreditProfile Name: request = {0} ", request);
            var response = MakePostCall<UpdateCreditProfileNameRequest, CreditProfileResponseModel>(ApiConfig.UpdateCreditProfileName, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CreditProfileResponseModel UpdateCreditProfileIsPrePaid(UpdateCreditProfileIsPrepaidRequest request)
        {
            Logger.Debug("Update CreditProfile IsPrepaid: request = {0} ", request);
            var response = MakePostCall<UpdateCreditProfileIsPrepaidRequest, CreditProfileResponseModel>(ApiConfig.UpdateCreditProfileIsPrePaid, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        #endregion

        #region SpendProfile

        public AddSApendProfileOverrideResponseModel  AddSpendProfile(AddSpendProfileOverrideRequestModel request)
        {
            Logger.Debug("Create SpendProfile : request = {0} ", request);
            var response = MakePostCall<AddSpendProfileOverrideRequestModel, AddSApendProfileOverrideResponseModel>(ApiConfig.CreateSpendProfileOverride, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public SpendProfileListResponseModel GetSpendProfileList()
        {
            Logger.Debug("Getting SpendProfiles ");
            var requestUrl = String.Format(ApiConfig.GetSpendProfileList);
            var response = MakeGetCall<SpendProfileListResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public SpendProfileResponseModel GetSpendProfileById(int spendProfileId)
        {
            Logger.Debug("Getting SpendProfileById : startDate = {0} ", spendProfileId);
            var requestUrl = String.Format(ApiConfig.GetSpendProfileById, spendProfileId);
            var response = MakeGetCall<SpendProfileResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public SpendProfileResponseModel GetSpendProfileByCardId(string cardId)
        {
            Logger.Debug("Getting SpendProfileByCardId : startDate = {0} ", cardId);
            var requestUrl = String.Format(ApiConfig.GetSpendProfileByCardId, cardId);
            var response = MakeGetCall<SpendProfileResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public SpendProfileResponseModel CreateSpendProfile(SpendProfileModel request)
        {
            Logger.Debug("Create SpendProfile : request = {0} ", request);
            var response = MakePostCall<SpendProfileModel, SpendProfileResponseModel>(ApiConfig.CreateSpendProfile, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public SpendProfileResponseModel UpdateSpendProfile(UpdateSpendProfileRequest request)
        {
            Logger.Debug("Update SpendProfile : request = {0} ", request);
            var response = MakePostCall<UpdateSpendProfileRequest, SpendProfileResponseModel>(ApiConfig.UpdateSpendProfile, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        //public BaseApiResponse DeleteSpendProfile(int spendProfileId)
        //{
        //    Logger.Debug("DeleteSpendProfile (spendProfileId = {0})", spendProfileId);
        //    var request = new DeleteSpendProfileRequest { SpendProfileId = spendProfileId };
        //    var response = MakeDeleteCall<DeleteSpendProfileRequest, BaseApiResponse>(ApiConfig.DeleteSpendProfile, request);
        //    Logger.Debug("Response = {0}", response);

        //    return response;
        //}

        public PurchaseCategoryResponseModel CreatePurchaseCategory(PurchaseCategory request)
        {
            Logger.Debug("Create PurchaseCategory : request = {0} ", request);
            var response = MakePostCall<PurchaseCategory, PurchaseCategoryResponseModel>(ApiConfig.CreatePurchaseCategory, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public PurchaseCategoryResponse GetPurchaseCategories()
        {
            Logger.Debug("Getting SpendProfiles ");
            var requestUrl = String.Format(ApiConfig.GetPurchaseCategories);
            var response = MakeGetCall<PurchaseCategoryResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public PurchaseCategoryResponse GetPurchaseCategoriesBySpendProfile(int spendProfileId)
        {
            Logger.Debug("Getting PurcahseCategoriesBySpendProfile : spendProfileId = {0} ", spendProfileId);
            var requestUrl = String.Format(ApiConfig.GetPurchaseCategoriesBySpendProfile, spendProfileId);
            var response = MakeGetCall<PurchaseCategoryResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public MerchantCodesResponse GetMerchantCodes()
        {
            Logger.Debug("Getting MerchantCodes ");
            var requestUrl = String.Format(ApiConfig.GetMerchantCodes);
            var response = MakeGetCall<MerchantCodesResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public MerchantCodesResponse GetMerchantCodesByPurchaseCategory(int purchaseCategoryId)
        {
            Logger.Debug("Getting MerchantCodesByCategory : purchaseCategoryId = {0} ", purchaseCategoryId);
            var requestUrl = String.Format(ApiConfig.GetMerchantCodesByPurchaseCategory, purchaseCategoryId);
            var response = MakeGetCall<MerchantCodesResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public MerchantCodeResponse UpdatePurchaseCategory(UpdatePurchaseCategoryRequest request)
        {
            Logger.Debug("Update PurchaseCategory : request = {0} ", request);
            var response = MakePostCall<UpdatePurchaseCategoryRequest, MerchantCodeResponse>(ApiConfig.UpdatePurchaseCategory, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        #endregion

       #region ProductFees

        public ProductFeesListResponseModel GetProductFeesList()
        {
            Logger.Debug("Getting ProductFees ");
            var requestUrl = String.Format(ApiConfig.GetProductFeesList);
            var response = MakeGetCall<ProductFeesListResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public ProductFeeResponseModel GetProductFeeById(int productFeeId)
        {
            Logger.Debug("Getting ProductFeeById : startDate = {0} ", productFeeId);
            var requestUrl = String.Format(ApiConfig.GetProductFeeById, productFeeId);
            var response = MakeGetCall<ProductFeeResponseModel>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public ProductFeeResponseModel CreateProductFee(ProductFeeModel request)
        {
            Logger.Debug("Create ProductFee : request = {0} ", request);
            var response = MakePostCall<ProductFeeModel, ProductFeeResponseModel>(ApiConfig.CreateProductFee, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public ProductFeeResponseModel UpdateProductFee(int productFeeId, UpdateProductFeeRequest request)
        {
            Logger.Debug("Update Product Fee : ProductFeeId {0} ", productFeeId);
            var requestUrl = String.Format(ApiConfig.UpdateProductFee, productFeeId);
            var response = MakePostCall<UpdateProductFeeRequest, ProductFeeResponseModel>(requestUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse DeleteProductFee(int productFeeId)
        {
            Logger.Debug("Delete Product Fee : ProductFeeId {0} ", productFeeId);
            var requestUrl = String.Format(ApiConfig.DeleteProductFee, productFeeId);
            var response = MakeGetCall<BaseApiResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        

        #endregion

        public CardholderDetailsFromPanResponse GetCardholderDetailsFromPan(string PAN)
        {
            Logger.Debug("Getting Cardholder details from PAN ");
            var response = MakeGetCall<CardholderDetailsFromPanResponse>($"{ApiConfig.GetCardholderFromPan}{PAN}");
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetTransactionsWithCardholder()
        {
            var requestUrl = String.Format(ApiConfig.TransactionsWithCardholderUrl);
            var response = MakeGetCall<AccountTransactionHistoryResponseModel>(requestUrl);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetTransactionsByDate(TransactionsByDateRequestModel request)
        {
            var response = MakePostCall<TransactionsByDateRequestModel, AccountTransactionHistoryResponseModel>(ApiConfig.TransactionsByDateUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetTransactionsByDateAndProductCode(TransactionsByDateRequestModel request)
        {
            var response = MakePostCall<TransactionsByDateRequestModel, AccountTransactionHistoryResponseModel>(ApiConfig.TransactionsByDateAndProductCodeUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetCardholdersBySpendProfileDate(CardholdersBySpendProfilesRequestModel request)
        {
            var response = MakePostCall<CardholdersBySpendProfilesRequestModel, AccountTransactionHistoryResponseModel>(ApiConfig.CardholderBySpendProfileByDateUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetCardholdersBySpendProfileDateAndProfile(CardholdersBySpendProfilesRequestModel request)
        {
            var response = MakePostCall<CardholdersBySpendProfilesRequestModel, AccountTransactionHistoryResponseModel>(ApiConfig.CardholderBySpendProfileByDateAndProfileUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetDeclinedTransactionsByDate(TransactionsByDateRequestModel request)
        {
            var response = MakePostCall<TransactionsByDateRequestModel, AccountTransactionHistoryResponseModel>(ApiConfig.DeclinedTransactionsByDateUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetDeclinedTransactionsByDateAndProductCode(TransactionsByDateRequestModel request)
        {
            var response = MakePostCall<TransactionsByDateRequestModel, AccountTransactionHistoryResponseModel>(ApiConfig.DeclinedTransactionsByDateAndProductCodeUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetLoadsByProductCode(string productCode)
        {
            var requestUrl = String.Format(ApiConfig.LoadsByProductCodeUrl, productCode);
            var response = MakeGetCall<AccountTransactionHistoryResponseModel>(requestUrl);

            return response;
        }

        public GetCardholderFromProxyResponse GetCardholderFromProxy(string cardProxyId)
        {
            var requestUrl = String.Format(ApiConfig.CardholderFromProxyId, cardProxyId);
            var response = MakeGetCall<GetCardholderFromProxyResponse>(requestUrl);

            return response;
        }

        public CheckCardAlreadyAssginedReponse CheckCardAlreadyAssgined(string cardProxyId)
        {
            var requestUrl = String.Format(ApiConfig.CheckCardAlreadyAssigned, cardProxyId);
            var response = MakeGetCall<CheckCardAlreadyAssginedReponse>(requestUrl);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetTransactionsByCardBalances()
        {
            var requestUrl = String.Format(ApiConfig.TransactionsByCardBalancesUrl);
            var response = MakeGetCall<AccountTransactionHistoryResponseModel>(requestUrl);

            return response;
        }

        public AccountTransactionHistoryResponseModel CardsActivationFirstTransaction()
        {
            var requestUrl = String.Format(ApiConfig.CardsActivationFirstTransaction);
            var response = MakeGetCall<AccountTransactionHistoryResponseModel>(requestUrl);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetGroupedTransactions(GetGroupedTransactionsRequestModel request)
        {
            var response = MakePostCall<GetGroupedTransactionsRequestModel, AccountTransactionHistoryResponseModel>(ApiConfig.GroupedTransactionsUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }


        public AccountTransactionHistoryResponseModel GetGroupedTransactionsByProduct(GetGroupedTransactionsRequestModel request)
        {
            var response = MakePostCall<GetGroupedTransactionsRequestModel, AccountTransactionHistoryResponseModel>(ApiConfig.GroupedTransactionsByProductUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetTransactionsByCardStatus(string cardStatus)
        {
            var requestUrl = String.Format(ApiConfig.TransactionsByCardStatusUrl, cardStatus);
            var response = MakeGetCall<AccountTransactionHistoryResponseModel>(requestUrl);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetAllTransactions()
        {
            var requestUrl = String.Format(ApiConfig.AllTransactionsUrl);
            var response = MakeGetCall<AccountTransactionHistoryResponseModel>(requestUrl);

            return response;
        }

        public AccountTransactionHistoryResponseModel GetTransactionByProductCode(string productCode)
        {
            var requestUrl = String.Format(ApiConfig.TransactionsByProductCodeUrl, productCode);
            var response = MakeGetCall<AccountTransactionHistoryResponseModel>(requestUrl);

            return response;
        }

        public ExistingCardholdersReponse GetExistingCardholders(ExistingCardholderRequestModel request)
        {
            Logger.Debug("GetExistingCardholder (request = {0})", request);
            var response = MakePostCall<ExistingCardholderRequestModel, ExistingCardholdersReponse>(ApiConfig.GetExistingCardHolders, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public OrdersResponse GetOrders()
        {
            var requestUrl = String.Format(ApiConfig.GetOrders);
            var response = MakeGetCall<OrdersResponse>(requestUrl);
            return response;
        }

        public TransactionHistoryResponse GetAccountTransactionHistory(string cardProxyId)
        {
            var requestUrl = String.Format(ApiConfig.AccountTransactionHistoryUrl, cardProxyId);
            var response = MakeGetCall<TransactionHistoryResponse>(requestUrl);
            return response;

        }

        #region CorporateStaff

        public GetUsersWithRolesResponse GetUsersWithRoles()
        {
            var response = MakeGetCall<GetUsersWithRolesResponse>(ApiConfig.GetUsersWithRoles);
            return response;
        }

        public NewCorporateUserResponse NewUser(NewCorporateStaffRequest request)
        {
            var response = MakePostCall<NewCorporateStaffRequest, NewCorporateUserResponse>(ApiConfig.NewUser, request);

            return response;

        }

        public BaseApiResponse UpdateStaffDetails(string cardHolderId, CardHolderDetails userDetails)
        {
            Logger.Debug("UpdateUserDetails (cardHolderId = {0}, userDetails = {1})", cardHolderId, userDetails);
            var requestUrl = String.Format(ApiConfig.UpdateUserDetailsUrl, cardHolderId);
            var response = MakePostCall<CardHolderDetails, BaseApiResponse>(requestUrl, userDetails);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        #endregion

        #region Product

        public QcsProductListResponseModel GetProducts()
        {
            var response = MakeGetCall<QcsProductListResponseModel>(ApiConfig.GetProducts);
            return response;
        }

        public QcsProductListResponseModel GetPortalProducts()
        {
            var response = MakeGetCall<QcsProductListResponseModel>(ApiConfig.GetPortalProducts);
            return response;
        }

        public QcsProductResponseModel AddNewProduct(NewProductRequest request)
        {
            var response = MakePostCall<NewProductRequest, QcsProductResponseModel>(ApiConfig.AddProduct, request);

            return response;
        }

        public QcsProductResponseModel GetProductDetails(string productCode)
        {
            var requestUrl = String.Format(ApiConfig.GetProductDetails, productCode);
            var response = MakeGetCall<QcsProductResponseModel>(requestUrl);

            return response;
        }


        public QcsProductResponseModel UpdateProduct(string productCode, QcsProduct request)
        {
            var requestUrl = String.Format(ApiConfig.UpdateProductDetails, productCode);

            var response = MakePostCall<QcsProduct, QcsProductResponseModel>(requestUrl, request);

            return response;
        }

        #endregion

        public BaseApiResponse ProcessBatchLoad(ProcessBatchLoadRequest request)
        {

            var response = MakePostCall<ProcessBatchLoadRequest, BaseApiResponse>(ApiConfig.ProcessBatchLoad, request);

            return response;
        }

        public BatchLoadHistoryResponse GetLoadHistory()
        {
            var response = MakeGetCall<BatchLoadHistoryResponse>(ApiConfig.GetBatchLoadHistory);

            return response;
        }

        public BatchLoadDetailsResponse GetBatchlaodDetails(int fileId, string fileName)
        {
            var requestUrl = String.Format(ApiConfig.GetBatchLoadDetails, fileId, fileName);
            var response = MakeGetCall<BatchLoadDetailsResponse>(requestUrl);

            return response;
        }

        public CorporateFundSummaryResponseModel GetCorporateAccountsFundsSummary()
        {
            var response = MakeGetCall<CorporateFundSummaryResponseModel>(ApiConfig.FundsSummaryUrl);

            return response;
        }

        public BaseApiResponse BatchLoadCards(BatchLoadCardsRequest request)
        {
            var response = MakePostCall<BatchLoadCardsRequest, BaseApiResponse>(ApiConfig.BatchLoadCardsUrl, request);

            return response;
        }

        public CorporateAccountsListResponseModel GetAccountsList()
        {
            var requestUrl = ApiConfig.GetCorproateAccountsListUrl;
            var response = MakeGetCall<CorporateAccountsListResponseModel>(requestUrl);

            return response;
        }

        public ManufacturerCardsListResponseModel GetManufacturerCardsByProduct(string productCode, string type)
        {
            var requestUrl = String.Format(ApiConfig.GetManufacturerCardsByProductUrl, productCode, type);
            var response = MakeGetCall<ManufacturerCardsListResponseModel>(requestUrl);

            return response;
        }


        public ManufacturerCardsListResponseModel GetVirtualCards()
        {
            var requestUrl = String.Format(ApiConfig.GetVirtualCardsUrl);
            var response = MakeGetCall<ManufacturerCardsListResponseModel>(requestUrl);

            return response;
        }

        public EditUserResponse EditUser(string userName)
        {
            var requestUrl = String.Format(ApiConfig.EditUser, userName);
            var response = MakeGetCall<EditUserResponse>(requestUrl);
            return response;
        }

        public GetUsersResponse GetAllCardHolders()
        {
            var response = MakeGetCall<GetUsersResponse>(ApiConfig.GetCardHolders);
            return response;
        }

        public GetUsersResponse GetAllCardHoldersWithSpendProfiles()
        {
            var response = MakeGetCall<GetUsersResponse>(ApiConfig.GetCardHoldersWithSpendProfiles);
            return response;
        }

        public CorporateAccountBalanceResponseModel GetCorporateAccountBalance(int corporateAccountId)
        {
            var requestUrl = String.Format(ApiConfig.CorporateAccountBalanace, corporateAccountId);
            var response = MakeGetCall<CorporateAccountBalanceResponseModel>(requestUrl);

            return response;
        }

        public BaseApiResponse LoadCardWithCorporateAccount(LoadCardRequest request)
        {
            var response = MakePostCall<LoadCardRequest, BaseApiResponse>(ApiConfig.LoadCardWithCorporateAccount, request);

            return response;
        }

        public BaseApiResponse UnloadCardWithCorporateAccount(UnloadCardRequest request)
        {
            var response = MakePostCall<UnloadCardRequest, BaseApiResponse>(ApiConfig.UnloadCardWithCorporateAccount, request);

            return response;
        }

        public bool IsParentUser(string cardHodlerId)
        {
            return true;
        }

        public CheckUserRoleResponse CheckUserRolw(string cardHolderId, string role)
        {
            //Logger.Debug("GetCardInformation (cardProxyId = {0})", cardProxyId);
            var requestUrl = String.Format(ApiConfig.CheckUserRoleUrl, cardHolderId, role);
            var response = MakeGetCall<CheckUserRoleResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        #region Card API
        public CardInfoResponse GetCardInformation(string cardProxyId)
        {
            Logger.Debug("GetCardInformation (cardProxyId = {0})", cardProxyId);
            var response = MakeGetCall<CardInfoResponse>($"{ApiConfig.CardInformationUrl}{cardProxyId}");
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public OrderCardResponse BatchOrderCards(BatchOrderCardRequest request)
        {
            Logger.Debug("OrderCard (request = {0})", request);
            var response = MakePostCall<BatchOrderCardRequest, OrderCardResponse>(ApiConfig.BatchOrderCardUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        public UpdateCardOrderResponse UpdateCardOrder(UpdateCardOrderRequest request)
        {
            Logger.Debug("UpdateCardOrder (request = {0})", request);
            var response = MakePostCall<UpdateCardOrderRequest, UpdateCardOrderResponse>(ApiConfig.UpdateCardOrderUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public GenericPaymentCardResponse MakeGenericPayment(GenericPaymentCardRequest request)
        {
            Logger.Debug("GenericPayment (request = {0})", request);
            var response = MakePostCall<GenericPaymentCardRequest, GenericPaymentCardResponse>(ApiConfig.MakeGenericPayment, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public bool CheckIsValidPrepaidCard(string cardProxyId)
        {
            Logger.Debug("CheckIsValidPrepaidCard (cardProxyId = {0})", cardProxyId);


            var clientResponse = MakeGetCall<BaseApiResponse>($"{ApiConfig.CheckCardProxyId}{cardProxyId}");
            bool response = false;
            if (clientResponse.IsSuccess)
            {
                response = true;
            }
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CardECommerceResponse GetECommerceDetails(string cardProxyId)
        {
            Logger.Debug("GetECommerceDetails (cardProxyId = {0})", cardProxyId);

            var requestUrl = String.Format(ApiConfig.CardEcommerceUrl, cardProxyId);
            var response = MakeGetCall<CardECommerceResponse>(requestUrl);
            if (response.IsSuccess)
            {

            }
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse ChangeCardStatus(string cardProxyId, string status)
        {
            Logger.Debug("ChangeCardStatus (cardProxyId = {0}, status = {1})", cardProxyId, status);
            var request = new ChangeCardStatusRequest { CardProxyId = cardProxyId, Status = status };
            var response = MakePostCall<ChangeCardStatusRequest, BaseApiResponse>(ApiConfig.ChangeCardStatusUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse ChangeInactiveCardStatus(string cardProxyId, string status)
        {
            Logger.Debug("ChangeCardStatus (cardProxyId = {0}, status = {1})", cardProxyId, status);
            var request = new ChangeCardStatusRequest { CardProxyId = cardProxyId, Status = status };
            var response = MakePostCall<ChangeCardStatusRequest, BaseApiResponse>(ApiConfig.ChangeInactiveCardStatusUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse ChangeProductCode(string cardProxyId, string productCode)
        {
            Logger.Debug("ChangeCardProduct (cardProxyId = {0}, product = {1})", cardProxyId, productCode);
            var request = new ChangeCardProductCodeRequest { CardId = cardProxyId, ProductCode = productCode };
            var response = MakePostCall<ChangeCardProductCodeRequest, BaseApiResponse>(ApiConfig.ChangeProductCodeUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        //public TransactionHistoryResponse GetTransactions(string cardProxyId, DateTime startDate, int months)
        //{
        //    Logger.Debug("GetTransactions (cardProxyId = {0}, startDate = {1}, months = {2}", cardProxyId, startDate, months);
        //    var requestUrl = String.Format(ApiConfig.TransactionHistoryUrl, cardProxyId, startDate.ToString("yyyy-MM-dd"), months);
        //    var response = MakeGetCall(requestUrl);
        //    Logger.Debug("Response = {0}", response);

        //    return response;
        //}

        public TransactionHistoryResponse GetTransactions(string cardProxyId, DateTime startDate, int months)
        {
            Logger.Debug("GetTransactions (cardProxyId = {0}, startDate = {1}, months = {2}", cardProxyId, startDate, months);
            var requestUrl = String.Format(ApiConfig.TransactionHistoryUrl, cardProxyId, startDate.ToString("yyyy-MM-dd"), months);
            var response = MakeGetCall<TransactionHistoryResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse ActivateCard(string cardProxyId, string cardHolderId, string displayName)
        {
            Logger.Debug("ActivateCard (cardProxyId = {0}, cardHolderId ={1})", cardProxyId, cardHolderId);
            var request = new ActivateCardRequest { CardHolderId = cardHolderId, CardProxyId = cardProxyId, CardDisplayName = displayName };
            var response = MakePostCall<ActivateCardRequest, BaseApiResponse>(ApiConfig.ActivateCardUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CardholderCardsResponse GetPrepaidCards(string cardHolderId)
        {
            Logger.Debug("GetPrepaidCards (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.CardholderCardsUrl, cardHolderId);
            var response = MakeGetCall<CardholderCardsResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CardholderCardsResponse GetChildrenPrepaidCards(string cardHolderId)
        {
            Logger.Debug("GetPrepaidCards (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.CardholderChildrenCardsUrl, cardHolderId);
            var response = MakeGetCall<CardholderCardsResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        #endregion

        #region Account API
        public CheckCredentialsResponse CheckUserCredentials(string username, string password)
        {
            Logger.Debug("CheckUserCredentails (username = {0}, password = xxx)", username);
            var request = new CheckCredentialsRequest { Username = username, Password = password };
            var response = MakePostCall<CheckCredentialsRequest, CheckCredentialsResponse>(ApiConfig.CheckCredentialsUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse ChangePassword(string cardholderId, string currentPassword, string newPassword)
        {
            Logger.Debug("ChangePassword (cardHolderId = {0}, currentPassword = xxx, newPassword = xxx)", cardholderId);
            var requestUrl = String.Format(ApiConfig.ChangePasswordUrl, cardholderId);
            var request = new ChangePasswordRequest { NewPassword = newPassword, CurrentPassword = currentPassword };
            var response = MakePostCall<ChangePasswordRequest, BaseApiResponse>(requestUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public GetTokenResponse GenerateForgottenPasswordToken(string emailAddress)
        {
            Logger.Debug("GenerateForgottenPasswordToken (emailAddress = {0})", emailAddress);
            var requestUrl = String.Format(ApiConfig.GeneratePasswordResetTokenUrl);
            var request = new EmailAddressRequest { EmailAddress = emailAddress };
            var response = MakePostCall<EmailAddressRequest, GetTokenResponse>(requestUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public GetTokenForgotUsernameResponse GenerateForgottenUsernameToken(string emailAddress)
        {
            Logger.Debug("GenerateForgottenUsernameToken (emailAddress = {0})", emailAddress);
            var requestUrl = String.Format(ApiConfig.GenerateForgottenUsernameTokenUrl);
            var request = new EmailAddressRequest { EmailAddress = emailAddress };
            var clientResponse = MakePostCall<EmailAddressRequest, GetTokenForgotUsernameResponse>(requestUrl, request);
            Logger.Debug("Response = {0}", clientResponse);

            return clientResponse;
        }

        public ResetPasswordResponse ResetPasswordWithToken(string cardholderId, string token, string newPassword)
        {
            Logger.Debug("ResetPasswordWithToken (cardHolderId = {0}, token = {1}, newPassword = xxx)", cardholderId, token, newPassword);
            var request = new ResetPasswordRequest
            {
                CardholderId = cardholderId,
                NewPassword = newPassword,
                Token = token
            };
            var response = MakePostCall<ResetPasswordRequest, ResetPasswordResponse>(ApiConfig.ResetPasswordUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        #endregion

        #region User API

        public RegisterChildResponse RegisterChild(RegisterChildRequest request)
        {
            Logger.Debug("CreateCardHolder (request = {0})", request);
            var response = MakePostCall<RegisterChildRequest, RegisterChildResponse>(ApiConfig.RegisterChildUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }


        public CreateCardholderResponse CreateCardholder(CreateCardholderRequest request)
        {
            Logger.Debug("CreateCardHolder (request = {0})", request);
            var response = MakePostCall<CreateCardholderRequest, CreateCardholderResponse>(ApiConfig.CreateCardholderCardsUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public CardHolderDetailsResponse GetCardHolderDetails(string cardHolderId)
        {
            Logger.Debug("GetUserDetails (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.UserDetailsUrl, cardHolderId);
            var response = MakeGetCall<CardHolderDetailsResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse ConfirmEmail(string cardholderId, CardHolderDetails userDetails)
        {
            Logger.Debug("UpdateUserDetails (cardHolderId = {0}, userDetails = {1})", cardholderId, userDetails);
            var requestUrl = String.Format(ApiConfig.ConfirmEmail, cardholderId);
            var response = MakePostCall<CardHolderDetails, BaseApiResponse>(requestUrl, userDetails);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse UpdateUserDetails(string cardHolderId, CardHolderDetails userDetails)
        {
            Logger.Debug("UpdateUserDetails (cardHolderId = {0}, userDetails = {1})", cardHolderId, userDetails);
            var requestUrl = String.Format(ApiConfig.UserDetailsUrl, cardHolderId);
            var response = MakePostCall<CardHolderDetails, BaseApiResponse>(requestUrl, userDetails);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public UserCommunicationOptionsResponse GetUserCommunicationOptions(string cardHolderId)
        {
            Logger.Debug("GetUserCommunicationOptions (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.GetUserCommunicationsUrl, cardHolderId);
            var response = MakeGetCall<UserCommunicationOptionsResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse UpdateUserCommunicationOptions(UpdateUserCommunicationOptionsRequest request)
        {
            Logger.Debug("UpdateUserCommunicationOptions (request = {0})", request);
            var response = MakePostCall<UpdateUserCommunicationOptionsRequest, BaseApiResponse>(ApiConfig.UpdateUserCommunicationsUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        #endregion

        #region Contact API
        public BaseApiResponse SubmitContactForm(string name, string emailAddress, string message)
        {
            Logger.Debug("SubmitContactForm (name = {0}, emailAddress = {1}, message = {2})", name, emailAddress, message);
            var request = new ContactFormRequest { Name = name, EmailAddress = emailAddress, Message = message };
            var response = MakePostCall<ContactFormRequest, BaseApiResponse>(ApiConfig.ContactFormUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        #endregion

        #region Payment

        public BaseApiResponse CardToCardTransfer(CardToCardTransferRequest request)
        {
            Logger.Debug("CardToCardTransfer (request = {0})", request);
            var response = MakePostCall<CardToCardTransferRequest, BaseApiResponse>(ApiConfig.CardToCardTransfer, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }


        public BaseApiResponse TopupWithNewCard(TopupWithNewCardRequest request)
        {
            Logger.Debug("TopupWithNewCard (request = {0})", request);
            var response = MakePostCall<TopupWithNewCardRequest, BaseApiResponse>(ApiConfig.TopupWithNewCardUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse TopupWithExistingCard(string cardProxyId, string fundingSourceId, decimal amount)
        {
            Logger.Debug("TopupWithExistingCard (cardProxyId = {0}, fundingSourceId = {1}, amount = {2})", cardProxyId, fundingSourceId, amount);
            var request = new TopupWithExistingCardRequest { CardProxyId = cardProxyId, FundSourceId = fundingSourceId, Amount = amount };
            var response = MakePostCall<TopupWithExistingCardRequest, BaseApiResponse>(ApiConfig.TopupWithExistingCardUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public GetFundingSourcesResponse GetFundingSources(string cardHolderId)
        {
            Logger.Debug("GetFundingSources (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.GetFundingSourcesUrl, cardHolderId);
            var response = MakeGetCall<GetFundingSourcesResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public DeleteFundingSourceResponse DeleteFundingSource(int fundingSourceId)
        {
            Logger.Debug("DeleteFundingSource (fundingSourceId = {0})", fundingSourceId);
            var request = new DeleteFundingSourceRequest { FundSourceId = fundingSourceId };
            var response = MakeDeleteCall<DeleteFundingSourceRequest, DeleteFundingSourceResponse>(ApiConfig.DeleteFundingSourcesUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public AutoTopupSettingsResponse GetAutoTopupSettings(string cardProxyId)
        {
            Logger.Debug("GetAutoTopupSettings (cardProxyId = {0})", cardProxyId);
            var requestUrl = String.Format(ApiConfig.GetAutoTopupSettingsUrl, cardProxyId);
            var response = MakeGetCall<AutoTopupSettingsResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse SetAutoTopupSettings(AutoTopupSettingsRequest request)
        {
            Logger.Debug("SetAutoTopupSettings (request = {0})", request);
            var response = MakePostCall<AutoTopupSettingsRequest, BaseApiResponse>(ApiConfig.SetAutoTopupSettingsUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        #endregion

        #region Country
        public CountryResponse GetCountryInfo(string countryCode)
        {
            Logger.Debug("GetCountryInfo (countryCode = {0})", countryCode);
            var requesturl = String.Format(ApiConfig.GetCountryInfoUrl, countryCode);
            var response = MakeGetCall<CountryResponse>(requesturl);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        #endregion

        #region Points 
        public PointsBalanceResponse GetPointsBalance(string cardHolderId)
        {
            Logger.Debug("GetPointsBalance (cardHolderId = {0})", cardHolderId);
            var requesturl = String.Format(ApiConfig.GetPointsBalanceUrl, cardHolderId);
            var response = MakeGetCall<PointsBalanceResponse>(requesturl);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        #endregion

        #region Order Card

        public BatchOrderDetailResponseModel GetBatchOrderDetails(int batchOrderId)
        {
            var requestUrl = String.Format(ApiConfig.BatchOrderDetailsUrl, batchOrderId);
            var response = MakeGetCall<BatchOrderDetailResponseModel>(requestUrl);
            return response;
        }

        public OrderCardResponse OrderCard(OrderIndividualCardRequest request)
        {
            Logger.Debug("OrderCard (request = {0})", request);
            var response = MakePostCall<OrderIndividualCardRequest, OrderCardResponse>(ApiConfig.OrderCardUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public OrderVirtualCardResponse OrderVirtualCard(OrderVirtualCardRequest request)
        {
            Logger.Debug("OrderVirtualCard (request = {0})", request);
            var response = MakePostCall<OrderVirtualCardRequest, OrderVirtualCardResponse>(ApiConfig.OrderVirtualCardUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BatchOrderHistoryResponseModel GetOrderHistory()
        {
            var response = MakeGetCall<BatchOrderHistoryResponseModel>(ApiConfig.BatchOrderHistoryUrl);
            return response;
        }

        public IndividualCardOrderHistoryResponseModel GetIndividualOrderHistory()
        {
            var response = MakeGetCall<IndividualCardOrderHistoryResponseModel>(ApiConfig.IndividualOrderHistoryUrl);
            return response;
        }
        #endregion

        #region Get card proxy id
        public CardProxyIdResponse GetCardProxyIdByPAN(string pan)
        {
            Logger.Debug("GetCardProxyIdByPAN (pan = {0})", pan);
            string requestUrl = ApiConfig.GetCardProxyIdUrl;
            CardProxyIdRequest request = new CardProxyIdRequest
            {
                PAN = pan
            };
            CardProxyIdResponse response = MakePostCall<CardProxyIdRequest, CardProxyIdResponse>(requestUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        #endregion

        #region Get manufacturer card
        public ManufacturerCardResponse GetManufacturerCard(string cardProxyId)
        {
            Logger.Debug("GetManufacturerCard (cardProxyId = {0})", cardProxyId);
            string requestUrl = string.Format(ApiConfig.GetManufacturerCardUrl, cardProxyId);
            var response = MakeGetCall<ManufacturerCardResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }
        #endregion

        #region US ACH
        public CreateACHResponse CreateACH(CreateACHRequest request)
        {
            Logger.Debug("CreateACH (request = {0})", request);
            var response = MakePostCall<CreateACHRequest, CreateACHResponse>(ApiConfig.CreateACHUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse ValidateACH(ValidateACHRequest request)
        {
            Logger.Debug("ValidateACH (request = {0})", request);
            var response = MakePostCall<ValidateACHRequest, BaseApiResponse>(ApiConfig.ValidateACHUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse LoadWithACH(LoadWithACHRequest request)
        {
            Logger.Debug("LoadWithACH (request = {0})", request);
            var response = MakePostCall<LoadWithACHRequest, BaseApiResponse>(ApiConfig.LoadWithACHUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public GetPaymentMethodsResponse GetUSFundingSources(string cardHolderId)
        {
            Logger.Debug("GetUSFundingSources (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.GetUSFundingSourceUrl, cardHolderId);
            var response = MakeGetCall<GetPaymentMethodsResponse>(requestUrl);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse DeleteACH(int achId)
        {
            Logger.Debug("DeleteACH (achId = {0})", achId);
            var request = new DeleteACHRequest { ACHId = achId };
            var response = MakeDeleteCall<DeleteACHRequest, BaseApiResponse>(ApiConfig.DeleteACHUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public BaseApiResponse ChangePin(ChangePinRequest request)
        {
            Logger.Debug("ChangePin (request = {0})", request);
            var response = MakePostCall<ChangePinRequest, BaseApiResponse>(ApiConfig.ChangePinUrl, request);
            Logger.Debug("Response = {0}", response);

            return response;
        }

        public PortalSummaryResponseModel GetPortalSummary()
        {
            //Logger.Debug("GetUSFundingSources (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.GetPortalSummary);
            var response = MakeGetCall<PortalSummaryResponseModel>(requestUrl);
            //Logger.Debug("Response = {0}", response);

            return response;
        }

        public AmountSpentByProductResponseModel GetAmountByProductSummary()
        {
            //Logger.Debug("GetUSFundingSources (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.GetAmountByProductSummary);
            var response = MakeGetCall<AmountSpentByProductResponseModel>(requestUrl);
            //Logger.Debug("Response = {0}", response);

            return response;
        }

        public TransactionByProductSummaryResponseModel GetTransactionByProductSummary()
        {
            //Logger.Debug("GetUSFundingSources (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.GetTransactionByProduct);
            var response = MakeGetCall<TransactionByProductSummaryResponseModel>(requestUrl);
            //Logger.Debug("Response = {0}", response);

            return response;

        }

        public AmountSpentByProductResponseModel GetAmountSpentByProductSummary()
        {
            //Logger.Debug("GetUSFundingSources (cardHolderId = {0})", cardHolderId);
            var requestUrl = String.Format(ApiConfig.GetAmountByProductSummary);
            var response = MakeGetCall<AmountSpentByProductResponseModel>(requestUrl);
            //Logger.Debug("Response = {0}", response);

            return response;

        }

        #endregion
    }
}