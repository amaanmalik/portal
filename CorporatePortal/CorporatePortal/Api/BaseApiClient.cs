﻿using CorporatePortal.Api.Responses;
using Newtonsoft.Json;
using NLog;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace CorporatePortal.Api
{
    public abstract class BaseApiClient : HttpClient
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly ApiConfig _config;

        public BaseApiClient(ApiConfig config)
        {
            _config = config;
        }

        protected T MakeGetCall<T>(string url) where T : BaseApiResponse, new()
        {
            Logger.Debug("MakeGetCall (url = {0})", url);
            try
            {
                var client = new RestClient();
                client.BaseUrl = _config.BaseUrl;
                client.Authenticator = new HttpBasicAuthenticator(_config.WebApiUsername, _config.WebApiPassword);
                client.AddHandler("application/json", NewtonsoftJsonSerializer.Default);
                client.AddHandler("text/json", NewtonsoftJsonSerializer.Default);
                client.AddHandler("text/x-json", NewtonsoftJsonSerializer.Default);
                client.AddHandler("text/javascript", NewtonsoftJsonSerializer.Default);
                client.AddHandler("*+json", NewtonsoftJsonSerializer.Default);

                var request = new RestRequest();
                request.Resource = url;
                request.JsonSerializer = NewtonsoftJsonSerializer.Default;
                var response = client.Execute<T>(request);
                if (response.ErrorException != null)
                {
                    throw new Exception("Error making REST ApiCall", response.ErrorException);
                }

                return response.Data;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Unknown error making GET call of API, url = {0}", url);
                throw ex;
            }
        }

        //protected TransactionHistoryResponse MakeGetCall(string url)
        //{
        //    Logger.Debug("MakeGetCall (url = {0})", url);
        //    try
        //    {
        //        var client = new RestClient();
        //        client.BaseUrl = _config.BaseUrl;
        //        client.Authenticator = new HttpBasicAuthenticator(_config.WebApiUsername, _config.WebApiPassword);

        //        var request = new RestRequest();
        //        request.Resource = url;

        //        var response = client.Execute(request);
        //       var res = JsonConvert.DeserializeObject<TransactionHistoryResponse>(response.Content);
        //        if (response.ErrorException != null)
        //        {
        //            throw new Exception("Error making REST ApiCall", response.ErrorException);
        //        }

        //        return res;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, "Unknown error making GET call of API, url = {0}", url);
        //        throw ex;
        //    }
        //}

        protected CardholderDetailsFromPanResponse MakeGetCall(string url)
        {
            Logger.Debug("MakeGetCall (url = {0})", url);
            try
            {
                var client = new RestClient();
                client.BaseUrl = _config.BaseUrl;
                client.Authenticator = new HttpBasicAuthenticator(_config.WebApiUsername, _config.WebApiPassword);

                var request = new RestRequest();
                request.Resource = url;

                var response = client.Execute(request);
                var res = JsonConvert.DeserializeObject<CardholderDetailsFromPanResponse>(response.Content);
                if (response.ErrorException != null)
                {
                    throw new Exception("Error making REST ApiCall", response.ErrorException);
                }

                return res;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Unknown error making GET call of API, url = {0}", url);
                throw ex;
            }
        }

        //protected GetUsersWithRolesResponse MakeGetCallCorporateStaff(string url)
        //{
        //    Logger.Debug("MakeGetCall (url = {0})", url);
        //    try
        //    {
        //        var client = new RestClient();
        //        client.BaseUrl = _config.BaseUrl;
        //        client.Authenticator = new HttpBasicAuthenticator(_config.WebApiUsername, _config.WebApiPassword);

        //        var request = new RestRequest();
        //        request.Resource = url;

        //        var response = client.Execute(request);
        //        var res = JsonConvert.DeserializeObject<GetUsersWithRolesResponse>(response.Content);
        //        if (response.ErrorException != null)
        //        {
        //            throw new Exception("Error making REST ApiCall", response.ErrorException);
        //        }

        //        return res;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex, "Unknown error making GET call of API, url = {0}", url);
        //        throw ex;
        //    }
        //}

        protected TResponse MakePostCall<TRequest, TResponse>(string url, TRequest request) where TResponse : BaseApiResponse, new()
        {
            return MakeCallWithBody<TRequest, TResponse>(url, request, Method.POST);
        }

        protected TResponse MakeDeleteCall<TRequest, TResponse>(string url, TRequest request) where TResponse : BaseApiResponse, new()
        {
            return MakeCallWithBody<TRequest, TResponse>(url, request, Method.DELETE);
        }

        private TResponse MakeCallWithBody<TRequest, TResponse>(string url, TRequest request, Method method) where TResponse : BaseApiResponse, new()
        {
            Logger.Debug("MakeCallWithBody (url = {0}, request = {1}, method = {2})", url, request, method);
            try
            {
                var client = new RestClient();
                client.BaseUrl = _config.BaseUrl;
                client.Authenticator = new HttpBasicAuthenticator(_config.WebApiUsername, _config.WebApiPassword);
                client.AddHandler("application/json", NewtonsoftJsonSerializer.Default);
                client.AddHandler("text/json", NewtonsoftJsonSerializer.Default);
                client.AddHandler("text/x-json", NewtonsoftJsonSerializer.Default);
                client.AddHandler("text/javascript", NewtonsoftJsonSerializer.Default);
                client.AddHandler("*+json", NewtonsoftJsonSerializer.Default);

                var restRequest = new RestRequest(method);
                restRequest.Resource = url;
                restRequest.JsonSerializer = NewtonsoftJsonSerializer.Default;
                restRequest.AddJsonBody(request);
                var response = client.Execute<TResponse>(restRequest);
                                

                if (response.ErrorException != null)
                {
                    throw new Exception("Error making REST ApiCall", response.ErrorException);
                }

                return response.Data;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error making {0} call of API, url = {1}, request = {2}", method, url, request);
                throw ex;
            }
        }

        protected string ConvertStatus(CardStatus status)
        {
            switch (status)
            {
                case CardStatus.Normal:
                    return CardApiRequestStatus.Normal.ToString();
                case CardStatus.Suspend:
                case CardStatus.Lost:
                    return CardApiRequestStatus.Block.ToString();
                case CardStatus.Stolen:
                    return CardApiRequestStatus.Stolen.ToString();
                default:
                    throw new ArgumentException($"Invalid Status. Status {status.ToString()} is not supported", "status");
            }
        }
    }
}