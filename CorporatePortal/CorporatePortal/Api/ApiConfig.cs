﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorporatePortal.Api
{
    public class ApiConfig
    {
        public static readonly string StatementsForReport = "cardservice/v1/statements/statementsForReport";

        public static readonly string CreditProfileTransForReport = "cardservice/v1/statements/creditprofiletransactionsForReport";

        #region CreditNotes

        public static readonly string AddCreditNote = "cardService/v1/statements/AddCreditNote";

        public static readonly string GetCreditNotesList = "cardService/v1/statements/CreditNotes";

        public static readonly string GetCreditNoteDetails = "cardService/v1/statements/CreditNote/CreditNoteId/{0}";

        public static readonly string EditCreditNote = "cardService/v1/statements/EditCreditNote";

        public static readonly string DeleteCreditNote = "cardService/v1/statements/DeleteCreditNote/{0}";

        #endregion
        #region CreditProfile

        public static readonly string GetSpendDetails = "cardService/v1/product/spendProfile/spendDetails/portal/{0}";

        public static readonly string DeleteSpendProfile = "cardService/v1/product/spendProfile/deleteSpendProfile/{0}";

        public static readonly string DeleteCreditProfile = "cardService/v1/product/creditProfile/deleteCreditProfile/{0}";

        public static readonly string UpdateCreditProfileName = "cardService/v1/product/creditProfile/updateCreditProfileName";

        public static readonly string UpdateCreditProfileIsPrePaid = "cardService/v1/product/creditProfile/updateCreditProfileIsPrepaid";

        public static readonly string GetCreditProfileList = "cardService/v1/product/creditProfiles";

        public static readonly string GetCreditProfileById = "cardService/v1/product/creditProfile/{0}";

        public static readonly string CreateCreditProfile = "cardService/v1/product/creditProfile";

        public static readonly string UpdateCreditProfileCreditLimit = "cardService/v1/product/updateCreditProfile/creditLimit";

        public static readonly string UpdateCreditProfileAvailableBalance = "cardService/v1/product/updateCreditProfile/availableBalance";

        public static readonly string GetCreditProfileTransactions = "cardService/v1/product/creditProfile/transactions/{0}";

        public static readonly string GetCreditProfileTransactionsById = "cardService/v1/product/creditProfile/creditProfiletransactions/{0}";

        public static readonly string SuperReportTransactions = "cardService/v1/product/creditProfile/allcreditProfileTransactions";

        #endregion

        #region ProductFees

        public static readonly string GetProductFeesList = "cardservice/v1/product/productfees";

        public static readonly string CreateProductFee = "cardservice/v1/product/productfee";

        public static readonly string DeleteProductFee = "cardservice/v1/product/deleteproductfee/{0}";

        public static readonly string GetProductFeeById = "cardservice/v1/product/productfee/{0}";

        public static readonly string UpdateProductFee = "cardservice/v1/product/updateproductfee/{0}";


        #endregion

        #region SpendProfile

        public static readonly string GetSpendProfileOverrideList = "cardservice/v1/product/spendProfileOverride";

        public static readonly string CreateSpendProfileOverride = "cardservice/v1/product/spendProfileOverride";

        public static readonly string GetSpendProfileList = "cardService/v1/product/spendProfiles";

        public static readonly string GetSpendProfileById = "cardService/v1/product/spendProfile/{0}";

        public static readonly string GetSpendProfileByCardId = "cardService/v1/product/spendProfile/cardId/{0}";

        public static readonly string CreateSpendProfile = "cardService/v1/product/spendProfile";

        public static readonly string UpdateSpendProfile = "cardService/v1/product/updateSpendProfile";

        //public static readonly string DeleteSpendProfile = "cardService/v1/product/DeleteSpendProfile";

        public static readonly string CreatePurchaseCategory = "cardService/v1/product/purchaseCategory";

        public static readonly string GetPurchaseCategories = "cardService/v1/product/purchaseCategory";

        public static readonly string GetPurchaseCategoriesBySpendProfile = "cardService/v1/product/purchaseCategoriesBySpendProfile/{0}";

        public static readonly string GetMerchantCodes = "cardService/v1/product/merchantCodes";

        public static readonly string GetMerchantCodesByPurchaseCategory = "cardService/v1/product/merchantCodeByPurchaseCategory/{0}";

        public static readonly string UpdatePurchaseCategory = "cardService/v1/product/updatePurchaseCategory";


        #endregion

        public static readonly string GetCardholderFromPan = "cardService/v1/card/cardholder/";

        public static readonly string TransactionsWithCardholderUrl = "report/transactions/TransactionsWithCardholders";

        public static readonly string TransactionsByDateUrl = "report/transactions/TransactionsByDate";

        public static readonly string TransactionsByDateAndProductCodeUrl = "report/transactions/TransactionsByDateAndProductCode";

        public static readonly string CardholderBySpendProfileByDateUrl = "report/transactions/cardholdersBySpendProfile";

        public static readonly string CardholderBySpendProfileByDateAndProfileUrl = "report/transactions/cardholdersBySpendProfileAndDate";

        public static readonly string DeclinedTransactionsByDateUrl = "report/transactions/DeclinedTransactionsByDate";

        public static readonly string DeclinedTransactionsByDateAndProductCodeUrl = "report/transactions/DeclinedTransactionsByDateAndProductCode";

        public static readonly string CardholderFromProxyId = "cardService/v1/getCardholder/cardProxyId/{0}";

        public static readonly string CheckCardAlreadyAssigned = "cardService/v1/checkCardAlreadyAssigned/{0}";

        public static readonly string TransactionHistoryUrl = "cardService/v1/transaction/cardProxyId/{0}/startDate/{1}/months/{2}";
        public static readonly string AccountTransactionHistoryUrl = "cardService/v1/transaction/cardProxyId/{0}";
        public static readonly string TransactionsByProductCodeUrl = "report/transactions/productCode/{0}";
        public static readonly string TransactionsByCardStatusUrl = "report/transactions/cardStatus/{0}";
        public static readonly string TransactionsByCardStatusButtlesUrl = "report/transactions/cardStatus/buttles/{0}";
        public static readonly string TransactionsByCardBalancesUrl = "report/transactions/cardBalanceList";
        public static readonly string GroupedTransactionsUrl = "report/transactions/GroupedTransactions";
        public static readonly string GroupedTransactionsByProductUrl = "report/transactions/GroupedTransactionsByProduct";
        public static readonly string CardsActivationFirstTransaction = "report/transactions/ActivationTransaction";
        public static readonly string LoadsByProductCodeUrl = "report/loads/productCode/{0}";
        public static readonly string AllTransactionsUrl = "report/transactions";



        public static readonly string GetPortalSummary = "report/summary";
        public static readonly string GetAmountByProductSummary = "report/amountspentsummary";
        public static readonly string GetTransactionByProduct = "report/productTransactionsummary";
        public static readonly string GetOrders = "report/orders";

        public static readonly string CorporateAccountLoadsHistoryUrl = "cardService/v1/card/corporateAccount/AccountLoads/{0}";
        public static readonly string CorporateAccountCardTransactionsUrl = " cardservice/v1/transaction/corporate/cardProxyId/{0}/startDate/{1}/months/{2}";




        #region CorporateStaff
        public static readonly string ConfirmEmail = "accountService/v1/confirm/email/{0}";
        public static readonly string GetUsersWithRoles = "cardService/v1/corporate/staff";
        public static readonly string NewUser = "cardService/v1/corporate/staff/newUser";
        public static readonly string UpdateUserDetailsUrl = "cardService/v1/corporate/staff/{0}/";

        #endregion

        #region CorporateCardholders

        public static readonly string GetCardHolders = "cardService/v1/corporate/cardholders";
        public static readonly string GetExistingCardHolders = "cardService/v1/corporate/carddolders/";
        public static readonly string GetCardHoldersWithSpendProfiles = "report/transactions/corporate/cardholders";


        #endregion

        #region CorporateLoad
        public static readonly string GetManufacturerCardsByProductUrl = "cardService/v1/card/corporate/{0}/type/{1}";

        public static readonly string GetVirtualCardsUrl = "cardService/v1/card/corporate/virtual";

        public static readonly string GetCorproateAccountsListUrl = "cardService/v1/card/corporateAccounts";

        public static readonly string BatchLoadCardsUrl = "cardService/v1/card/load/corporate/batchLoad";

        public static readonly string FundsSummaryUrl = "cardservice/v1/card/corporateAccounts/fundsSummary";

        public static readonly string CorporateAccountBalanace = "cardService/v1/card/load/corporateAccountBalance/{0}";

        public static readonly string LoadCardWithCorporateAccount = "cardService/v1/card/load/corporateCard/corporateAccount";

        public static readonly string UnloadCardWithCorporateAccount = "cardService/v1/card/unload/corporateAccount";

        public static readonly string GetBatchLoadHistory = "cardService/v1/card/load/corporate/batchLoadHistory";

        public static readonly string GetBatchLoadDetails = "cardService/v1/card/load/corporate/batchLoadDetails/{0}/{1}";

        public static readonly string ProcessBatchLoad = "cardservice/v1/card/load/corporate/ProcessBatchLoad";

        #endregion

        #region Products

        public static readonly string AddProduct = "cardService/v1/product";
        public static readonly string GetProducts = "cardService/v1/product";
        public static readonly string GetPortalProducts = "cardService/v1/product/portalproducts";
        public static readonly string GetProductDetails = "cardService/v1/product/productCode/{0}";
        public static readonly string UpdateProductDetails = "cardService/v1/product/productCode/{0}";
        public static readonly string ChangeProductCodeUrl = "cardService/v1/product/card/updateCardProductCode";

        #endregion

        #region Orders
        public static readonly string OrderCardUrl = "cardService/v1/order/";
        public static readonly string BatchOrderCardUrl = "cardService/v1/batchorder/";
        public static readonly string BatchOrderHistoryUrl = "cardService/v1/batchorderhistory/";
        public static readonly string IndividualOrderHistoryUrl = "cardService/v1/invidualorderhistory";
        public static readonly string BatchOrderDetailsUrl = "cardService/v1/batchorderdetails/{0}";
        #endregion

        public static readonly string EditUser = "cardservice/v1/user/edituser/{0}";



        public static readonly string CardToCardTransfer = "cardService/v1/card/load/corporate/cardToCard";
        public static readonly string OrderVirtualCardUrl = "cardService/v1/orderVirtualCard";
        public static readonly string RegisterChildUrl = "cardService/v1/childCardHolder/";
        public static readonly string CardholderChildrenCardsUrl = "cardService/v1/cards/cardholderId/children/{0}/";
        public static readonly string CheckUserRoleUrl = "accountService/v1/credentials/role/cardHolderId/{0}/role/{1}";

        public static readonly string CreateCardholderCardsUrl = "cardService/v1/cardholder/";
        public static readonly string CardholderCardsUrl = "cardService/v1/corporate/cards/cardholderId/{0}/";
        public static readonly string CardInformationUrl = "cardService/v1/corporate/card/cardProxyId/";
        public static readonly string GetUserCommunicationsUrl = "cardService/v1/user/communicationSettings/cardHolderId/{0}";
        public static readonly string UpdateUserCommunicationsUrl = "cardService/v1/user/communicationSettings/";
        public static readonly string ChangeCardStatusUrl = "cardService/v1/card/status";
        public static readonly string ChangeInactiveCardStatusUrl = "cardService/v1/card/inactive/status";
        public static readonly string CheckCardProxyId = "cardService/v1/checkCardProxyId/";

        public static readonly string UpdateCardOrderUrl = "cardService/v1/update-order/";

        public static readonly string ActivateCardUrl = "cardService/v1/card/activate";
        public static readonly string CardEcommerceUrl = "cardService/v1/card/ecommerce/cardProxyId/{0}";

        public static readonly string CheckCredentialsUrl = "accountService/v1/user/credentials";
        public static readonly string ChangePasswordUrl = "accountService/v1/changePassword/cardHolderId/{0}/";
        public static readonly string GenerateForgottenUsernameTokenUrl = "accountService/v1/token/forgottenusername/";
        public static readonly string GeneratePasswordResetTokenUrl = "accountService/v1/token/passwordreset/";
        public static readonly string ResetPasswordUrl = "accountService/v1/token/passwordreset/reset";
        public static readonly string UserDetailsUrl = "cardService/v1/user/cardholderId/{0}/";

        public static readonly string ContactFormUrl = "contactService/v1/contactform/";

        public static readonly string TopupWithNewCardUrl = "cardService/v1/card/load/newFundSource";
        public static readonly string TopupWithExistingCardUrl = "cardService/v1/card/load/existingFundSource";
        public static readonly string GetFundingSourcesUrl = "cardService/v1/fundSource/cardholderId/{0}";
        public static readonly string DeleteFundingSourcesUrl = "cardService/v1/fundSource/";
        public static readonly string GetAutoTopupSettingsUrl = "cardService/v1/autoTopup/cardProxyId/{0}";
        public static readonly string SetAutoTopupSettingsUrl = "cardService/v1/autoTopup";
        public static readonly string GetPointsBalanceUrl = "cardService/v1/user/rewardPoints/cardHolderId/{0}";

        public static readonly string MakeGenericPayment = "cardService/v1/payments/takeGenericPayment/";

        public static readonly string GetCountryInfoUrl = "cardService/v1/country/countryCode/{0}";

        public static readonly string GetCardProxyIdUrl = "cardService/v1/cardProxyId";

        public static readonly string GetManufacturerCardUrl = "cardService/v1/manufacturerCard/cardProxyId/{0}";

        public static readonly string CreateACHUrl = "cardService/v1/fundSource/ACH/";
        public static readonly string ValidateACHUrl = "cardService/v1/fundSource/ACH/validate";
        public static readonly string LoadWithACHUrl = "cardService/v1/card/load/ACH";
        public static readonly string GetUSFundingSourceUrl = "cardService/v1/US/fundSource/cardholderId/{0}";
        public static readonly string DeleteACHUrl = "cardService/v1/fundSource/ACH/";
        public static readonly string ChangePinUrl = "cardService/v1/card/pin";

        public ApiConfig(string baseUrl, string webApiUsername, string webApiPassword)
        {
            _baseUrl = new Uri(baseUrl);
            WebApiUsername = webApiUsername;
            WebApiPassword = webApiPassword;

        }

        private readonly Uri _baseUrl;
        public Uri BaseUrl
        {
            get
            {
                return _baseUrl;
            }
        }

        public string WebApiUsername { get; set; }

        public string WebApiPassword { get; set; }

    }
}