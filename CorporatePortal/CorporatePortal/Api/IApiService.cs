﻿using CorporatePortal.Api.Requests;
using CorporatePortal.Api.Requests.CardLoad;
using CorporatePortal.Api.Requests.CardOrder;
using CorporatePortal.Api.Requests.CredNotes;
using CorporatePortal.Api.Responses;
using CorporatePortal.Api.Responses.CardLoad;
using CorporatePortal.Api.Responses.CredNotes;
using CorporatePortal.Models.CardLoad;
using CorporatePortal.Models.Product;
using CorporatePortal.Models.ProductFee;
using CorporatePortal.Models.Report;
using System;

namespace CorporatePortal.Api
{
    public interface IApiService
    {
        StatementsForReportResponseModel GetStatementsForReport();
        CreditProfileTransForReportResponseModel GetCreditProfileTransForReport();
        BaseApiResponse ChangeProductCode(string cardProxyId, string productCode);

        #region CreditNotes

        CreditNoteResponseModel AddCreditNote(AddCreditNoteRequestModel request);

        CreditNoteResponseModel GetCreditNoteById(int creditNoteId);

        CreditNotesListResponseModel GetCreditNotes();

        BaseApiResponse DeleteCreditNote(int creditNoteId);

        CreditNoteResponseModel UpdateCreditNote(int creditNoteId, UpdateCreditNoteRequestModel request);

        #endregion

        #region CreditProfile

        CreditProfileListResponseModel GetCreditProfilesList();

        CreditProfileResponseModel GetCreditProfileById(int creditProfileId);

        CreditProfileResponseModel CreateCreditProfile(CreditProfileModel request);

        //CreditProfileResponseModel UpdateCreditProfile(UpdateCreditProfileRequest request);

        CreditProfileResponseModel UpdateCreditProfileCreditLimit(UpdateCreditProfileCreditLimitRequest request);

        CreditProfileResponseModel UpdateCreditProfileAvailableBalance(UpdateCreditProfileAvailableBalanceRequest request);

        CreditProfileTransactionsResponseModel GetCreditProfileTransactions(int creditProfileId);

        CreditProfileResponseModel UpdateCreditProfileIsPrePaid(UpdateCreditProfileIsPrepaidRequest request);

        #endregion

        #region SpendProfile

        SpendProfileResponseModel GetSpendProfileByCardId(string cardId);
        AddSApendProfileOverrideResponseModel AddSpendProfile(AddSpendProfileOverrideRequestModel request);

        SpendOverrideListResponseModel GetSpendProfileOverridList();

        BaseApiResponse CreateSpendProfileOverride(CreateSpendProfileOverrideRequest request);
        SpendProfileListResponseModel GetSpendProfileList();

        SpendProfileResponseModel GetSpendProfileById(int spendProfileId);

        SpendProfileResponseModel CreateSpendProfile(SpendProfileModel request);

        SpendProfileResponseModel UpdateSpendProfile(UpdateSpendProfileRequest request);

        BaseApiResponse DeleteSpendProfile(int spendProfileId);

        PurchaseCategoryResponseModel CreatePurchaseCategory(PurchaseCategory request);

        PurchaseCategoryResponse GetPurchaseCategories();

        PurchaseCategoryResponse GetPurchaseCategoriesBySpendProfile(int spendProfileId);

        MerchantCodesResponse GetMerchantCodes();

        MerchantCodesResponse GetMerchantCodesByPurchaseCategory(int purchaseCategoryId);

        MerchantCodeResponse UpdatePurchaseCategory(UpdatePurchaseCategoryRequest request);

        #endregion

        #region ProductFees

        ProductFeesListResponseModel GetProductFeesList();

        ProductFeeResponseModel CreateProductFee(ProductFeeModel request);

        BaseApiResponse DeleteProductFee(int productFeeId);

        ProductFeeResponseModel GetProductFeeById(int productFeeId);

        ProductFeeResponseModel UpdateProductFee(int productFeeId, UpdateProductFeeRequest request);

        #endregion

        /// <summary>
        /// Gets cardholder information from the PAN
        /// </summary>
        /// <param name="cardProxyId">The card proxy identifier.</param>
        /// <returns></returns>
        CardholderDetailsFromPanResponse GetCardholderDetailsFromPan(string PAN);

        AccountTransactionHistoryResponseModel GetTransactionsWithCardholder();

        AccountTransactionHistoryResponseModel GetTransactionsByDate(TransactionsByDateRequestModel request);

        AccountTransactionHistoryResponseModel GetTransactionsByDateAndProductCode(TransactionsByDateRequestModel request);

        AccountTransactionHistoryResponseModel GetDeclinedTransactionsByDate(TransactionsByDateRequestModel request);

        AccountTransactionHistoryResponseModel GetDeclinedTransactionsByDateAndProductCode(TransactionsByDateRequestModel request);

        GetCardholderFromProxyResponse GetCardholderFromProxy(string cardProxyId);

        CheckCardAlreadyAssginedReponse CheckCardAlreadyAssgined(string cardProxyId);

        ExistingCardholdersReponse GetExistingCardholders(ExistingCardholderRequestModel request);

        TransactionHistoryResponse GetAccountTransactionHistory(string cardProxyId);

        AccountTransactionHistoryResponseModel GetAllTransactions();

        AccountTransactionHistoryResponseModel GetTransactionByProductCode(string cardProxyId);

        AccountTransactionHistoryResponseModel GetTransactionsByCardStatus(string cardStatus);

        AccountTransactionHistoryResponseModel GetTransactionsByCardBalances();

        AccountTransactionHistoryResponseModel GetGroupedTransactions(GetGroupedTransactionsRequestModel request);

        AccountTransactionHistoryResponseModel GetGroupedTransactionsByProduct(GetGroupedTransactionsRequestModel request);

        AccountTransactionHistoryResponseModel CardsActivationFirstTransaction();

        AccountTransactionHistoryResponseModel GetLoadsByProductCode(string productCode);

        CreditProfileTransactionsByIdResponseModel SuperReportTransactions(SuperReportRequestModel request);

        SpendDetailsResponse GetSpendDetails(string cardProxyId);

        CreditProfileResponseModel UpdateCreditProfileName(UpdateCreditProfileNameRequest request);

        BaseApiResponse DeleteCreditProfile(int creditProfileId);

        CreditProfileTransactionsByIdResponseModel GetCreditProfileTransactionsById(int creditProfileId);

        AccountTransactionHistoryResponseModel GetCardholdersBySpendProfileDate(CardholdersBySpendProfilesRequestModel request);

        AccountTransactionHistoryResponseModel GetCardholdersBySpendProfileDateAndProfile(CardholdersBySpendProfilesRequestModel request);


        CorporateAccountLoadsHistoryReponseModel GetCorporateAccountLoadHistory(int corporateAccountId);

        TransactionHistoryResponse GetCorporaeAccountCardTransactionHistory(string cardProxyId, DateTime startDate, int months);

        #region Reports

        PortalSummaryResponseModel GetPortalSummary();

        AmountSpentByProductResponseModel GetAmountByProductSummary();

        TransactionByProductSummaryResponseModel GetTransactionByProductSummary();

        OrdersResponse GetOrders();

        #endregion


        #region CorporateStaff

        GetUsersWithRolesResponse GetUsersWithRoles();
        NewCorporateUserResponse NewUser(NewCorporateStaffRequest request);
        BaseApiResponse UpdateStaffDetails(string cardholderId, CardHolderDetails userDetails);
        BaseApiResponse ConfirmEmail(string cardholderId, CardHolderDetails userDetails);

        #endregion

        #region Product

        QcsProductListResponseModel GetProducts();

        QcsProductListResponseModel GetPortalProducts();

        QcsProductResponseModel AddNewProduct(NewProductRequest request);

        QcsProductResponseModel GetProductDetails(string productCode);

        QcsProductResponseModel UpdateProduct(string productCode, QcsProduct request);

        #endregion

        #region Load

        BaseApiResponse UnloadCardWithCorporateAccount(UnloadCardRequest request);

        #endregion
        BaseApiResponse ProcessBatchLoad(ProcessBatchLoadRequest request);

        BatchLoadHistoryResponse GetLoadHistory();

        BatchLoadDetailsResponse GetBatchlaodDetails(int fileId, string fileName);

        CorporateFundSummaryResponseModel GetCorporateAccountsFundsSummary();

        BaseApiResponse BatchLoadCards(BatchLoadCardsRequest request);

        CorporateAccountsListResponseModel GetAccountsList();

        ManufacturerCardsListResponseModel GetManufacturerCardsByProduct(string productCode, string type);

        ManufacturerCardsListResponseModel GetVirtualCards();

        BatchOrderHistoryResponseModel GetOrderHistory();

        IndividualCardOrderHistoryResponseModel GetIndividualOrderHistory();

        BatchOrderDetailResponseModel GetBatchOrderDetails(int batchOrderId);

        GetUsersResponse GetAllCardHolders();

        GetUsersResponse GetAllCardHoldersWithSpendProfiles();

        CorporateAccountBalanceResponseModel GetCorporateAccountBalance(int corporateAccountId);

        BaseApiResponse LoadCardWithCorporateAccount(LoadCardRequest request);

        EditUserResponse EditUser(string userName);

        /// <summary>
        /// Checkf if the user is in Parent role.
        /// </summary>
        /// <param name="cardHolderId">The card holder ID.</param>
        /// <returns></returns>
        bool IsParentUser(string cardHolderId);

        /// <summary>
        /// Gets the all the prepaid cards for user's children.
        /// </summary>
        /// <param name="cardHolderId">The card holder identifier.</param>
        /// <returns></returns>
        CardholderCardsResponse GetChildrenPrepaidCards(string cardHolderId);

        /// <summary>
        /// Transfers funds between users accounts.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseApiResponse CardToCardTransfer(CardToCardTransferRequest request);

        /// <summary>
        /// Registers a child user.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        RegisterChildResponse RegisterChild(RegisterChildRequest request);

        /// <summary>
        /// Orders a virtual card for a user.
        /// </summary>
        /// <param name="request">The card proxy identifier.</param>
        /// <returns></returns>
        OrderVirtualCardResponse OrderVirtualCard(OrderVirtualCardRequest request);


        CheckUserRoleResponse CheckUserRolw(string cardHolderId, string role);


        /// <summary>
        /// Gets the card information.
        /// </summary>
        /// <param name="cardProxyId">The card proxy identifier.</param>
        /// <returns></returns>
        CardInfoResponse GetCardInformation(string cardProxyId);

        /// <summary>
        /// Gets the prepaid cards.
        /// </summary>
        /// <param name="cardHolderId">The card holder identifier.</param>
        /// <returns></returns>
        CardholderCardsResponse GetPrepaidCards(string cardHolderId);

        /// <summary>
        /// Gets the points balance.
        /// </summary>
        /// <param name="cardHolderId">The card holder identifier.</param>
        /// <returns></returns>
        PointsBalanceResponse GetPointsBalance(string cardHolderId);

        /// <summary>
        /// Gets the transactions.
        /// </summary>
        /// <param name="cardProxyId">The card proxy identifier.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="months">The months.</param>
        /// <returns></returns>
        TransactionHistoryResponse GetTransactions(string cardProxyId, DateTime startDate, int months);

        /// <summary>
        /// Checks the user credentials.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        CheckCredentialsResponse CheckUserCredentials(string username, string password);

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="cardholderId">The cardholder identifier.</param>
        /// <param name="currentPassword">The current password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        BaseApiResponse ChangePassword(string cardholderId, string currentPassword, string newPassword);

        /// <summary>
        /// Generates the forgotten password token.
        /// </summary>
        /// <param name="cardholderId">The cardholder identifier.</param>
        /// <returns></returns>
        GetTokenResponse GenerateForgottenPasswordToken(string emailAddress);

        ResetPasswordResponse ResetPasswordWithToken(string cardholderId, string token, string newPassword);

        /// <summary>
        /// Generates the forgotten username token.
        /// </summary>
        /// <param name="email">The email identifier.</param>
        /// <returns></returns>
        GetTokenForgotUsernameResponse GenerateForgottenUsernameToken(string emailAddress);

        /// <summary>
        /// Submits the contact form.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        BaseApiResponse SubmitContactForm(string name, string emailAddress, string message);

        /// <summary>
        /// Changes the card status.
        /// </summary>
        /// <param name="cardProxyId">The card proxy identifier.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        BaseApiResponse ChangeCardStatus(string cardProxyId, string status);

        BaseApiResponse ChangeInactiveCardStatus(string cardProxyId, string status);

        /// <summary>
        /// Orders the card.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>

        OrderCardResponse BatchOrderCards(BatchOrderCardRequest request);
        /// <summary>
        /// Updates card order after successfull payment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        UpdateCardOrderResponse UpdateCardOrder(UpdateCardOrderRequest request);

        /// <summary>
        /// Makes a greneric card payment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        GenericPaymentCardResponse MakeGenericPayment(GenericPaymentCardRequest request);

        /// <summary>
        /// Checks the is valid prepaid card.
        /// </summary>
        /// <param name="cardProxyId">The card proxy identifier.</param>
        /// <returns></returns>
        bool CheckIsValidPrepaidCard(string cardProxyId);

        /// <summary>
        /// Creates the cardholder.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        CreateCardholderResponse CreateCardholder(CreateCardholderRequest request);

        /// <summary>
        /// Activates the card.
        /// </summary>
        /// <param name="cardProxyId">The card proxy identifier.</param>
        /// <param name="cardDisplayName">Display name of the card.</param>
        /// <param name="cardHolderId">The card holder identifier.</param>
        /// <returns></returns>
        BaseApiResponse ActivateCard(string cardProxyId, string cardHolderId, string DisplayName);

        /// <summary>
        /// Gets the user details.
        /// </summary>
        /// <param name="cardholderId">The cardholder identifier.</param>
        /// <returns></returns>
        CardHolderDetailsResponse GetCardHolderDetails(string cardholderId);

        /// <summary>
        /// Updates the user details.
        /// </summary>
        /// <param name="cardholderId">The cardholder identifier.</param>
        /// <param name="userDetails">The user details.</param>
        /// <returns></returns>
        BaseApiResponse UpdateUserDetails(string cardholderId, CardHolderDetails userDetails);

        /// <summary>
        /// Topups the with new card.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseApiResponse TopupWithNewCard(TopupWithNewCardRequest request);

        /// <summary>
        /// Topups the with existing card.
        /// </summary>
        /// <param name="cardProxyId">The card proxy identifier.</param>
        /// <param name="fundingSourceId">The funding source identifier.</param>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        BaseApiResponse TopupWithExistingCard(string cardProxyId, string fundingSourceId, decimal amount);

        /// <summary>
        /// Gets the funding sources.
        /// </summary>
        /// <param name="cardholderId">The cardholder identifier.</param>
        /// <returns></returns>
        GetFundingSourcesResponse GetFundingSources(string cardholderId);

        DeleteFundingSourceResponse DeleteFundingSource(int sourceId);

        AutoTopupSettingsResponse GetAutoTopupSettings(string cardProxyId);

        BaseApiResponse SetAutoTopupSettings(AutoTopupSettingsRequest request);

        /// <summary>
        /// Gets the user communication options.
        /// </summary>
        /// <param name="cardholderId">The cardholder identifier.</param>
        /// <returns></returns>
        UserCommunicationOptionsResponse GetUserCommunicationOptions(string cardholderId);

        /// <summary>
        /// Updates the user communication options.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseApiResponse UpdateUserCommunicationOptions(UpdateUserCommunicationOptionsRequest request);

        /// <summary>
        /// Get KYC information for a country.
        /// </summary>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        CountryResponse GetCountryInfo(string countryCode);

        /// <summary>
        /// Order a card.
        /// </summary>
        /// <param name="cardHolderId"></param>
        /// <param name="productCode"></param>
        /// <returns></returns>
        OrderCardResponse OrderCard(OrderIndividualCardRequest request);

        /// <summary>
        /// Gets the e commerce details.
        /// </summary>
        /// <param name="cardProxyId">The card proxy identifier.</param>
        /// <returns></returns>
        CardECommerceResponse GetECommerceDetails(string cardProxyId);

        /// <summary>
        /// Lookup card proxy id by PAN.
        /// </summary>
        /// <param name="pan"></param>
        /// <returns></returns>
        CardProxyIdResponse GetCardProxyIdByPAN(string pan);

        /// <summary>
        /// Lookup manufacturer card information by card proxy id.
        /// This is used to get the currency of the card to check if the user
        /// can load the card before activating it.
        /// </summary>
        /// <param name="cardProxyId"></param>
        /// <returns></returns>
        ManufacturerCardResponse GetManufacturerCard(string cardProxyId);

        /// <summary>
        /// Creates ACH.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        CreateACHResponse CreateACH(CreateACHRequest request);

        /// <summary>
        /// Validates ACH.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseApiResponse ValidateACH(ValidateACHRequest request);

        /// <summary>
        /// Loads ACH.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseApiResponse LoadWithACH(LoadWithACHRequest request);

        /// <summary>
        /// Gets the US funding sources.
        /// </summary>
        /// <param name="cardholderId">The cardholder identifier.</param>
        /// <returns></returns>
        GetPaymentMethodsResponse GetUSFundingSources(string cardHolderId);

        BaseApiResponse DeleteACH(int achId);

        /// <summary>
        /// Changes Pin.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        BaseApiResponse ChangePin(ChangePinRequest request);
    }
}