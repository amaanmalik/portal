﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CorporatePortal.Api.Requests;
using CorporatePortal.Api.Requests.CardLoad;
using CorporatePortal.Api.Requests.CardOrder;
using CorporatePortal.Api.Responses;
using CorporatePortal.Api.Responses.CardLoad;
using CorporatePortal.Controllers;
using CorporatePortal.Models.Product;

namespace CorporatePortal.Api
{
    public class MockApiService : IApiService
    {
        #region CorporateStaff
        public GetUsersWithRolesResponse GetUsersWithRoles()
        {
            var response = new GetUsersWithRolesResponse
            {
                IsSuccess = true,
                Users = new List<ApiUser>
                {
                    new ApiUser
                    {
                        FirstName="amana",
                        LastName ="malik",
                        Email="adas@asdas.com",
                        UserId="sdfd",
                        Roles = new List<string>
                        {
                            "Admin"
                        }
                    },
                    new ApiUser
                    {
                        FirstName="amana",
                        LastName ="malik",
                        Email="adas@asdas.com",
                        UserId="sdfd",
                        Roles = new List<string>
                        {
                            "Admin"
                        }
                    },
                    new ApiUser
                    {
                        FirstName="amana",
                        LastName ="malik",
                        Email="adas@asdas.com",
                        UserId="sdfd",
                        Roles = new List<string>
                        {
                            "Admin"
                        }
                    },
                    new ApiUser
                    {
                        FirstName="amana",
                        LastName ="malik",
                        Email="adas@asdas.com",
                        UserId="sdfd",
                        Roles = new List<string>
                        {
                            "Manager"
                        }
                    },
                    new ApiUser
                    {
                        FirstName="amana",
                        LastName ="malik",
                        Email="adas@asdas.com",
                        UserId="sdfd",
                        Roles = new List<string>
                        {
                            "Manager"
                        }
                    },
                    new ApiUser
                    {
                        FirstName="amana",
                        LastName ="malik",
                        Email="adas@asdas.com",
                        UserId="sdfd",
                        Roles = new List<string>
                        {
                            "Manager"
                        }
                    },
                    new ApiUser
                    {
                        FirstName="amana",
                        LastName ="malik",
                        Email="adas@asdas.com",
                        UserId="sdfd",
                        Roles = new List<string>
                        {
                            "Manager"
                        }
                    },
                    new ApiUser
                    {
                        FirstName="amana",
                        LastName ="malik",
                        Email="adas@asdas.com",
                        UserId="sdfd",
                        Roles = new List<string>
                        {
                            "Manager"
                        }
                    },
                    new ApiUser
                    {
                        FirstName="amana",
                        LastName ="malik",
                        Email="adas@asdas.com",
                        UserId="sdfd",
                        Roles = new List<string>
                        {
                            "Manager"
                        }
                    }
                }
            };

            return response;
        }

        public NewCorporateUserResponse NewUser(NewCorporateStaffRequest request)
        {
            throw new NotImplementedException();
        }
        public BaseApiResponse ConfirmEmail(string cardholderId, ApiUserDetails userDetails)
        {
            throw new NotImplementedException();
        }

        public BaseApiResponse UpdateStaffDetails(string cardholderId, ApiUserDetails userDetails)
        {
            throw new NotImplementedException();
        }

        #endregion

        public RegisterChildResponse RegisterChild(RegisterChildRequest request)
        {
            var response = new RegisterChildResponse { CardHolderId = "999" };

            return response;
        }

        public OrderVirtualCardResponse OrderVirtualCard(OrderVirtualCardRequest request)
        {

            var response = new OrderVirtualCardResponse
            {
                CardProxyId = "123",
                IsSuccess = true
            };

            return response;
        }

        public BaseApiResponse ActivateCard(string cardProxyId, string cardDisplayName, string cardHolderId)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse ActivateCard(string cardProxyId, string cardHolderId)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse ChangeCardStatus(string cardProxyId, CardStatus status)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse ChangePassword(string cardholderId, string currentPassword, string newPassword)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public bool CheckIsValidPrepaidCard(string cardProxyId)
        {
            return true;
        }

        public CheckCredentialsResponse CheckUserCredentials(string username, string password)
        {
            var result = new CheckCredentialsResponse
            {
                IsSuccess = false,
                ErrorCodes = new List<string>
                {
                    "ErrorCode"
                }
            };

            if (username == "parent" && password == "parent")
            {
                result.IsSuccess = true;
                result.ErrorCodes = null;
                result.UserInformation = new ApiUser
                {
                    UserId = "123",
                    Email = "test@test.com",
                    FirstName = "parent",
                    LastName = "Smith"
                };
            }

            if (username == "child" && password == "child")
            {
                result.IsSuccess = true;
                result.ErrorCodes = null;
                result.UserInformation = new ApiUser
                {
                    UserId = "999",
                    Email = "test@test.com",
                    FirstName = "child",
                    LastName = "Smith"
                };
            }

            return result;
        }

        public CreateCardholderResponse CreateCardholder(CreateCardholderRequest request)
        {
            var result = new CreateCardholderResponse
            {
                CardHolderId = "123",
                IsSuccess = true,
                HasPassedKYC = true,
                HasPassedPEP = true,
                HasPassedSanctions = true
            };
            return result;
        }

        public DeleteFundingSourceResponse DeleteFundingSource(int sourceId)
        {
            var result = new DeleteFundingSourceResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public GetTokenResponse GenerateForgottenPasswordToken(string emailAddress)
        {
            var result = new GetTokenResponse
            {
                IsSuccess = true,
                CardholderId = "1234123412341234",
                Token = "token"
            };
            return result;
        }

        public GetTokenForgotUsernameResponse GenerateForgottenUsernameToken(string emailAddress)
        {
            var result = new GetTokenForgotUsernameResponse
            {
                IsSuccess = true,
                Username = "username",
                Token = "token"
            };
            return result;
        }

        public CardInfoResponse GetCardInformation(string cardProxyId)
        {
            var result = new CardInfoResponse
            {
                IsSuccess = true,
                Balance = 12.34,
                CardProxyId = cardProxyId,
                Currency = "GBR",
                DisplayName = "Mock Card",
                Expiry = DateTime.Now.AddYears(2),
                Status = CardStatus.Normal.ToString()
            };
            return result;
        }

        public CardProxyIdResponse GetCardProxyIdByPAN(string pan)
        {
            var result = new CardProxyIdResponse
            {
                CardProxyId = "1234567812345678",
                IsSuccess = true
            };
            return result;
        }

        public CountryResponse GetCountryInfo(string countryCode)
        {
            var result = new CountryResponse
            {
                IsSuccess = true,
                CountryCode = countryCode,
                IsKycAvailable = true,
                LocalCurrency = "EUR",
                CardLoadInfos = new List<CardLoadInfo>
                {
                    new CardLoadInfo
                    {
                        Currency = "EUR",
                        MaxNonKycCardLoadAmount = 250
                    }
                }
            };
            return result;
        }

        public CardECommerceResponse GetECommerceDetails(string cardProxyId)
        {
            var result = new CardECommerceResponse
            {
                IsSuccess = true,
                CardNumber = "1234123412341234",
                CVV = "123",
                ExpiryDate = DateTime.Now.AddYears(2).ToString("MM/yyyy")
            };
            return result;
        }

        public GetFundingSourcesResponse GetFundingSources(string cardholderId)
        {
            if (cardholderId == "123")
            {
                var result = new GetFundingSourcesResponse
                {
                    IsSuccess = true,
                    FundingSources = new List<ApiFundingSourceModel>
                {
                    new ApiFundingSourceModel
                    {
                        Id = 1,
                        ExpiryDate = DateTime.Now.AddYears(2),
                        IsUsedForCurrentAutopTopUp = true,
                        Pan = "**** **** **** 1234",
                        SourceName = "Test 1"
                    },
                    new ApiFundingSourceModel
                    {
                        Id = 2,
                        ExpiryDate = DateTime.Now.AddYears(3),
                        IsUsedForCurrentAutopTopUp = false,
                        Pan = "**** **** **** 4444",
                        SourceName = "Test 2"
                    }
                }
                };
                return result;
            }

            return new GetFundingSourcesResponse
            {
                IsSuccess = true
            };

        }

        public AutoTopupSettingsResponse GetAutoTopupSettings(string cardProxyId)
        {
            var result = new AutoTopupSettingsResponse
            {
                Amount = 45,
                Enabled = true,
                IsSuccess = true,
                FundSource = new FundingSourceModel
                {
                    ExpiryDate = "042008",
                    Id = 1,
                    IsUsedForCurrentAutopTopUp = true,
                    Pan = "**** **** **** 1234",
                    SourceName = "Test 1"
                }
            };
            return result;
        }

        public BaseApiResponse SetAutoTopupSettings(AutoTopupSettingsRequest request)
        {
            return new BaseApiResponse
            {
                IsSuccess = true
            };
        }

        public PointsBalanceResponse GetPointsBalance(string cardHolderId)
        {
            var result = new PointsBalanceResponse
            {
                IsSuccess = true,
                RewardPoints = 525
            };
            return result;
        }

        public CardholderCardsResponse GetPrepaidCards(string cardHolderId)
        {
            var parentCard = new CardholderCardsResponse
            {
                IsSuccess = true,
                Cards = new List<ApiPrepaidCardModel>
                {
                    new ApiPrepaidCardModel
                    {
                        ProductCode = "TestProd1",
                        Balance = 1240.55,
                        CardProxyId = "13423",
                        Currency = "USD",
                        DisplayName = "Parent Test Card 1",
                        Expiry = DateTime.Now.AddYears(12),
                        Status = CardStatus.Normal.ToString()
                    },
                     new ApiPrepaidCardModel
                    {
                        ProductCode = "TestProd1",
                        Balance = 120.55,
                        CardProxyId = "12323",
                        Currency = "USD",
                        DisplayName = " Card 1",
                        Expiry = DateTime.Now.AddYears(21),
                        Status = CardStatus.Normal.ToString()
                    },
                      new ApiPrepaidCardModel
                    {
                          ProductCode = "TestProd2",
                        Balance = 1230.55,
                        CardProxyId = "13223",
                        Currency = "USD",
                        DisplayName = "Parent Test Card 1",
                        Expiry = DateTime.Now.AddYears(2),
                        Status = CardStatus.Normal.ToString()
                    },
                       new ApiPrepaidCardModel
                    {
                           ProductCode = "TestProd2",
                        Balance = 12055,
                        CardProxyId = "132323",
                        Currency = "USD",
                        DisplayName = "Parent Test Card 1",
                        Expiry = DateTime.Now.AddYears(2),
                        Status = CardStatus.Normal.ToString()
                    },
                        new ApiPrepaidCardModel
                    {
                            ProductCode = "TestProd1",
                        Balance = 55,
                        CardProxyId = "126767653",
                        Currency = "USD",
                        DisplayName = "Parent Test Card 1",
                        Expiry = DateTime.Now.AddYears(32),
                        Status = CardStatus.Normal.ToString()
                    }
                }
            };            

            return parentCard;
        }

        public TransactionHistoryResponse GetTransactions(string cardProxyId, DateTime startDate, int months)
        {
            var result = new TransactionHistoryResponse
            {
                IsSuccess = true,
                Transactions = new List<ApiTransaction>
                {
                    new ApiTransaction
                    {
                        Amount = 12.34,
                        AuthDate = DateTime.Now.AddDays(-25),
                        Currency = "EUR",
                        Description = "Description 1",
                        Id = "ID1",
                        MerchantName = "Merchant name 1",
                        Reference = "reference 1",
                        TransactionType = "Debit"
                    },
                    new ApiTransaction
                    {
                        Amount = 23.45,
                        AuthDate = DateTime.Now.AddDays(-20),
                        Currency = "EUR",
                        Description = "Description 2",
                        Id = "ID1",
                        MerchantName = "Merchant name 2",
                        Reference = "reference 2",
                        TransactionType = "Credit"
                    }
                }
            };
            return result;
        }

        public UserCommunicationOptionsResponse GetUserCommunicationOptions(string cardholderId)
        {
            var result = new UserCommunicationOptionsResponse
            {
                IsSuccess = true,
                AutoTopup = new ApiUserCommunicationOption
                {
                    Email = false,
                    SMS = true
                },
                ManualTopup = new ApiUserCommunicationOption
                {
                    Email = true,
                    SMS = false
                }
            };
            return result;
        }

        public UserDetailsResponse GetUserDetails(string cardholderId)
        {
            var result = new UserDetailsResponse
            {
                IsSuccess = true,
                User = new ApiUserDetails
                {
                    AcceptedTAndCs = true,
                    Address1 = "Address 1",
                    Address2 = "Address 2",
                    City = "City",
                    CreatedDate = DateTime.Now,
                    DateOfBirth = DateTime.Now.AddYears(-25),
                    EmailAddress = "test@test.com",
                    EmailConfirmed = true,
                    FirstName = "Firstname",
                    Gender = Gender.Male,
                    Id = "ID",
                    IdCardNumber = "IdCardNumber",
                    IsActive = true,
                    ISOCountryCode = "GBR",
                    LastName = "Lastname",
                    Mobile = "00000 123456",
                    Nationality = "GBR",
                    PassedKyc = true,
                    PassedPeps = true,
                    PassedSanctions = true,
                    PaymentGatewayId = "PAYMENTGATEWAYID",
                    PostCode = "AA11 1AA",
                    SecondSurname = "SecondSurname",
                    ShownAutoCommsOptIn = true,
                    SocialSecurityNumber = "SocialSecurityNumber",
                    State = "State",
                    TaxIdCardNumber = "TaxIdCardNumber",
                    Title = "Mr",
                    Language = "en-GB"
                }
            };
            return result;
        }

        public OrderCardResponse OrderCard(OrderCardRequest request)
        {
            var result = new OrderCardResponse
            {
                IsSuccess = true,
                Id = "123"
            };
            return result;
        }

        public UpdateCardOrderResponse UpdateCardOrder(UpdateCardOrderRequest request)
        {
            var result = new UpdateCardOrderResponse
            {
                IsSuccess = true,
                Id = "OrderId"
            };
            return result;
        }

        public GenericPaymentCardResponse MakeGenericPayment(GenericPaymentCardRequest request)
        {
            var result = new GenericPaymentCardResponse
            {
                PaymentReference = "abc123",
                IsSuccess = true,
                ErrorCodes = null
            };
            return result;
        }

        public BaseApiResponse OrderCard(string cardHolderId, string productCode)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public ResetPasswordResponse ResetPasswordWithToken(string cardholderId, string token, string newPassword)
        {
            var result = new ResetPasswordResponse
            {
                IsSuccess = true,
                UserInformation = new ApiUser
                {
                    UserId = "1234123412341234",
                    Email = "test@test.com",
                    FirstName = "John",
                    LastName = "Smith"
                }
            };
            return result;
        }

        public BaseApiResponse SubmitContactForm(string name, string emailAddress, string message)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse TopupWithExistingCard(string cardProxyId, string fundingSourceId, decimal amount)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse TopupWithNewCard(TopupWithNewCardRequest request)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse UpdateUserCommunicationOptions(UpdateUserCommunicationOptionsRequest request)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse UpdateUserDetails(string cardholderId, ApiUserDetails userDetails)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public ManufacturerCardResponse GetManufacturerCard(string cardProxyId)
        {
            if (cardProxyId == "123")
            {
                return new ManufacturerCardResponse
                {
                    IsSuccess = true,
                    CardProxyId = cardProxyId,
                    Currency = "USD",
                    ProductCode = "USVPROD"
                };
            }

            return new ManufacturerCardResponse
            {
                IsSuccess = true,
                CardProxyId = cardProxyId,
                Currency = "USD",
                ProductCode = "USBASKETBALL"
            };

        }

        public CreateACHResponse CreateACH(CreateACHRequest request)
        {
            return new CreateACHResponse
            {
                ACHId = 12345,
                IsSuccess = true
            };
        }

        public BaseApiResponse ValidateACH(ValidateACHRequest request)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse LoadWithACH(LoadWithACHRequest request)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public GetPaymentMethodsResponse GetUSFundingSources(string cardHolderId)
        {
            var result = new GetPaymentMethodsResponse
            {
                Cards = new List<CardFundSource>()
                {
                    new CardFundSource
                    {
                        Id = 1,
                        ExpiryDate = DateTime.Now,
                        Name = "test card",
                        Pan = "123"
                    },
                    new CardFundSource
                    {
                        Id = 1,
                        ExpiryDate = DateTime.Now,
                        Name = "test card",
                        Pan = "123"
                    },
                    new CardFundSource
                    {
                        Id = 1,
                        ExpiryDate = DateTime.Now,
                        Name = "test card",
                        Pan = "123"
                    }
                },
                ACHs = new List<ACHFundSource>()
                {
                    new ACHFundSource
                    {
                        Id =1,
                        Name="Bank1",
                        Verified=true
                    }
                },
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse DeleteACH(int achId)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse ChangePin(ChangePinRequest request)
        {
            var result = new BaseApiResponse
            {
                IsSuccess = true
            };
            return result;
        }

        public BaseApiResponse CardToCardTransfer(CardToCardTransferRequest request)
        {
            return new BaseApiResponse
            {
                IsSuccess = true
            };
        }

        public bool IsParentUser(string cardHodlerId)
        {
            return true;
        }

        public CardholderCardsResponse GetChildrenPrepaidCards(string cardHolderId)
        {
            var childCard = new CardholderCardsResponse
            {
                IsSuccess = true,
                Cards = new List<ApiPrepaidCardModel>
                {
                    new ApiPrepaidCardModel
                    {
                        Balance = 120.55,
                        CardProxyId = "999",
                        Currency = "EUR",
                        DisplayName = "Child Test Card 1",
                        Expiry = DateTime.Now.AddYears(2),
                        Status = CardStatus.Normal.ToString()
                    }
                }
            };

            return childCard;
        }

        public CheckUserRoleResponse CheckUserRolw(string cardHolderId, string role)
        {
            if (cardHolderId == "123")
                return new CheckUserRoleResponse { IsSuccess = true, IsInRole = true };

            if (cardHolderId == "999")
                return new CheckUserRoleResponse { IsSuccess = true, IsInRole = false };

            return null;
        }

        public GetUsersResponse GetAllCardHolders()
        {
            var response = new GetUsersResponse
            {
                //IsSuccess = true,
                //Users = new List<ApiUserDetails>
                //{
                //    new ApiUserDetails
                //    {
                //    AcceptedTAndCs = true,
                //    Address1 = "Address 1",
                //    Address2 = "Address 2",
                //    City = "City",
                //    CreatedDate = DateTime.Now,
                //    DateOfBirth = DateTime.Now.AddYears(-25),
                //    EmailAddress = "test@test.com",
                //    EmailConfirmed = true,
                //    FirstName = "Firstname",
                //    Gender = Gender.Male,
                //    Id = "ID",
                //    IdCardNumber = "IdCardNumber",
                //    IsActive = true,
                //    ISOCountryCode = "GBR",
                //    LastName = "Lastname",
                //    Mobile = "00000 123456",
                //    Nationality = "GBR",
                //    PassedKyc = true,
                //    PassedPeps = true,
                //    PassedSanctions = true,
                //    PaymentGatewayId = "PAYMENTGATEWAYID",
                //    PostCode = "AA11 1AA",
                //    SecondSurname = "SecondSurname",
                //    ShownAutoCommsOptIn = true,
                //    SocialSecurityNumber = "SocialSecurityNumber",
                //    State = "State",
                //    TaxIdCardNumber = "TaxIdCardNumber",
                //    Title = "Mr",
                //    Language = "en-GB"
                //},
                //    new ApiUserDetails
                //    {
                //    AcceptedTAndCs = true,
                //    Address1 = "Address 1",
                //    Address2 = "Address 2",
                //    City = "City",
                //    CreatedDate = DateTime.Now,
                //    DateOfBirth = DateTime.Now.AddYears(-25),
                //    EmailAddress = "test@test.com",
                //    EmailConfirmed = true,
                //    FirstName = "sdfdsfsd",
                //    Gender = Gender.Male,
                //    Id = "ID",
                //    IdCardNumber = "IdCardNumber",
                //    IsActive = true,
                //    ISOCountryCode = "GBR",
                //    LastName = "Lastname",
                //    Mobile = "00000 123456",
                //    Nationality = "GBR",
                //    PassedKyc = true,
                //    PassedPeps = true,
                //    PassedSanctions = true,
                //    PaymentGatewayId = "PAYMENTGATEWAYID",
                //    PostCode = "AA11 1AA",
                //    SecondSurname = "SecondSurname",
                //    ShownAutoCommsOptIn = true,
                //    SocialSecurityNumber = "SocialSecurityNumber",
                //    State = "State",
                //    TaxIdCardNumber = "TaxIdCardNumber",
                //    Title = "Mr",
                //    Language = "en-GB"
                //},
                //    new ApiUserDetails
                //    {
                //    AcceptedTAndCs = true,
                //    Address1 = "Address 1",
                //    Address2 = "Address 2",
                //    City = "City",
                //    CreatedDate = DateTime.Now,
                //    DateOfBirth = DateTime.Now.AddYears(-25),
                //    EmailAddress = "test@test.com",
                //    EmailConfirmed = true,
                //    FirstName = "sdfsdfdsf",
                //    Gender = Gender.Male,
                //    Id = "ID",
                //    IdCardNumber = "IdCardNumber",
                //    IsActive = true,
                //    ISOCountryCode = "GBR",
                //    LastName = "Lastname",
                //    Mobile = "00000 123456",
                //    Nationality = "GBR",
                //    PassedKyc = true,
                //    PassedPeps = true,
                //    PassedSanctions = true,
                //    PaymentGatewayId = "PAYMENTGATEWAYID",
                //    PostCode = "AA11 1AA",
                //    SecondSurname = "SecondSurname",
                //    ShownAutoCommsOptIn = true,
                //    SocialSecurityNumber = "SocialSecurityNumber",
                //    State = "State",
                //    TaxIdCardNumber = "TaxIdCardNumber",
                //    Title = "Mr",
                //    Language = "en-GB"
                //},
                //    new ApiUserDetails
                //    {
                //    AcceptedTAndCs = true,
                //    Address1 = "Address 1",
                //    Address2 = "Address 2",
                //    City = "City",
                //    CreatedDate = DateTime.Now,
                //    DateOfBirth = DateTime.Now.AddYears(-25),
                //    EmailAddress = "test@test.com",
                //    EmailConfirmed = true,
                //    FirstName = "sdfdsfds",
                //    Gender = Gender.Male,
                //    Id = "ID",
                //    IdCardNumber = "IdCardNumber",
                //    IsActive = true,
                //    ISOCountryCode = "GBR",
                //    LastName = "Lastname",
                //    Mobile = "00000 123456",
                //    Nationality = "GBR",
                //    PassedKyc = true,
                //    PassedPeps = true,
                //    PassedSanctions = true,
                //    PaymentGatewayId = "PAYMENTGATEWAYID",
                //    PostCode = "AA11 1AA",
                //    SecondSurname = "SecondSurname",
                //    ShownAutoCommsOptIn = true,
                //    SocialSecurityNumber = "SocialSecurityNumber",
                //    State = "State",
                //    TaxIdCardNumber = "TaxIdCardNumber",
                //    Title = "Mr",
                //    Language = "en-GB"
                //},
                //    new ApiUserDetails
                //    {
                //    AcceptedTAndCs = true,
                //    Address1 = "Address 1",
                //    Address2 = "Address 2",
                //    City = "City",
                //    CreatedDate = DateTime.Now,
                //    DateOfBirth = DateTime.Now.AddYears(-25),
                //    EmailAddress = "test@test.com",
                //    EmailConfirmed = true,
                //    FirstName = "sdfdsfdsfsdgfg",
                //    Gender = Gender.Male,
                //    Id = "ID",
                //    IdCardNumber = "IdCardNumber",
                //    IsActive = true,
                //    ISOCountryCode = "GBR",
                //    LastName = "Lastname",
                //    Mobile = "00000 123456",
                //    Nationality = "GBR",
                //    PassedKyc = true,
                //    PassedPeps = true,
                //    PassedSanctions = true,
                //    PaymentGatewayId = "PAYMENTGATEWAYID",
                //    PostCode = "AA11 1AA",
                //    SecondSurname = "SecondSurname",
                //    ShownAutoCommsOptIn = true,
                //    SocialSecurityNumber = "SocialSecurityNumber",
                //    State = "State",
                //    TaxIdCardNumber = "TaxIdCardNumber",
                //    Title = "Mr",
                //    Language = "en-GB"
                //}
                //}
            };

            return response;
        }

        

        

        public QcsProductListResponseModel GetProducts()
        {
            var response = new QcsProductListResponseModel
            {
                IsSuccess = true,
                ProductList = new List<QcsProduct>
                {
                    new QcsProduct
                    {
                        ProductCode = "TestProd1",
                        Currency ="GBP"
                    },
                     new QcsProduct
                    {
                        ProductCode = "TestProd2",
                        Currency ="GBP"
                    },
                      new QcsProduct
                    {
                        ProductCode = "TestProd3",
                        Currency ="GBP"
                    },
                       new QcsProduct
                    {
                        ProductCode = "TestProd4",
                        Currency ="GBP"
                    },
                        new QcsProduct
                    {
                        ProductCode = "TestProd5",
                        Currency ="GBP"
                    },
                         new QcsProduct
                    {
                        ProductCode = "TestProd6",
                        Currency ="GBP"
                    },
                          new QcsProduct
                    {
                        ProductCode = "TestProd7",
                        Currency ="GBP"
                    }
                }
            };

            return response;
        }

        public QcsProductResponseModel AddNewProduct(NewProductRequest request)
        {
            throw new NotImplementedException();
        }

        public QcsProductResponseModel GetProductDetails(string productCode)
        {
            QcsProductResponseModel response = new QcsProductResponseModel
            {
                IsSuccess = true,
                Product = new QcsProduct
                {
                    ProductCode = productCode,
                    Currency = "GBP"
                }
            };

            return response;
        }

        public QcsProductResponseModel UpdateProduct(string productCode, QcsProduct request)
        {
            throw new NotImplementedException();
        }

        public CorporateAccountBalanceResponseModel GetCorporateAccountBalance(int corporateAccountId)
        {
            throw new NotImplementedException();
        }

        public BaseApiResponse LoadCardWithCorporateAccount(LoadCardRequest request)
        {
            throw new NotImplementedException();
        }

        public EditUserResponse EditUser(string userName)
        {
            throw new NotImplementedException();
        }

        public OrderCardResponse BatchOrderCards(BatchOrderCardRequest request)
        {
            throw new NotImplementedException();
        }

        public BatchOrderHistoryResponseModel GetOrderHistory()
        {
            throw new NotImplementedException();
        }

        public IndividualCardOrderHistoryResponseModel GetIndividualOrderHistory()
        {
            throw new NotImplementedException();
        }

        public OrderCardResponse OrderCard(OrderIndividualCardRequest request)
        {
            throw new NotImplementedException();
        }

        public BatchOrderDetailResponseModel GetBatchOrderDetails(int batchOrderId)
        {
            throw new NotImplementedException();
        }

        public ManufacturerCardsListResponseModel GetManufacturerCardsByProduct(string productCode)
        {
            throw new NotImplementedException();
        }

        public CorporateAccountsListResponseModel GetAccountsList()
        {
            throw new NotImplementedException();
        }

        public BaseApiResponse BatchLoadCards(BatchLoadCardsRequest request)
        {
            throw new NotImplementedException();
        }

        public CorporateFundSummaryResponseModel GetCorporateAccountsFundsSummary()
        {
            throw new NotImplementedException();
        }

        public BatchLoadHistoryResponse GetLoadHistory()
        {
            throw new NotImplementedException();
        }

        public BaseApiResponse ProcessBatchLoad()
        {
            throw new NotImplementedException();
        }

       
    }
}